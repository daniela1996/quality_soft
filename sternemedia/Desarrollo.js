$(document).ready(function(){
	var type = urljs+"index.php/Desarrollo/";
	var typeNum = 0;
	var nombTab = '';
	var diasEstimados = 0;
	var diasReales = 0;
	var diasRetrasados = 0;
	var tablaForm = $("#tablaForm");
	var matenimientoForm = $("#matenimientoForm");
	$(".isSelect").select2();
	$(".selectpicker").selectpicker({title: '-- Seleccionar --'});
	cargarProyectos();
	cargarEmpresa();
	cargarEstados();
	cargarPrioridad();
	cargarUsuarios();
	cargarUsuariosPorModulo();
	$("#tblModulos").dataTable();

	jQuery('.withoutTwoOrMoreSpaces').keyup(function () {
		this.value = this.value.replace(/([ ]{2,})|[[]\/]/, '');
	});

	$(function(){
		$('.fechas').datepicker({
			autoclose: true,
			language: 'es',
			startDate: new Date(),
			today: 'today',
			date: 'fullDate',
			format: 'dd/mm/yyyy'
		});
	});

	tablaForm.validate({
		rules:{
			descripcion: {
				required: true
				// SinDosEspaciosNumeroTexto: true
			},
			nombreProyecto: {
				required: true,
				Letrascontildes: true
				// SinDosEspaciosSoloTexto: true
			},
			estadoProyecto: {
				required: true
			},
			empresa: {
				required: true
			}
		},
		messages: {
			descripcion: {
				required: "Por favor, ingrese un valor."
			},
			nombreProyecto: {
				required: "Por favor, ingrese un valor."
			},
			estadoProyecto: {
				required: "Por favor, seleccione un valor."
			},
			empresa: {
				required: "Por favor, seleccione un valor."
			}
		},
		errorPlacement: function(error, element) {
			if ($(element).is("select")) {
				$(error).appendTo($(element).parent().parent());
				$(element).parent().css("border", "1px solid #B60E16");
			}
			else {
				$(error).insertAfter(element);
			}
		}
	});

	matenimientoForm.validate({
		rules: {
			idproject: {
				required: true
			},
			modulo: {
				required: true,
				Letrascontildes: true
				// SinDosEspaciosSoloTexto: true
			},
			valor: {
				required: true
			},
			descripcion_mod: {
				required: true
				// SinDosEspaciosNumeroTexto: true
			},
			prioridad: {
				required: true
			},
			initialdate: {
				required: true
			},
			finaldate: {
				required: true
			}

		},
		messages: {
			idproject: {
				required: "Por favor, seleccione un valor."
			},
			modulo: {
				required: "Por favor, ingrese un valor."
			},
			valor: {
				required: "Por favor, ingrese un valor."
			},
			descripcion_mod: {
				required: "Por favor, ingrese un valor."
			},
			prioridad: {
				required: "Por favor, seleccione un valor."
			},
			initialdate: {
				required: "Por favor, ingrese un valor."
			},
			finaldate: {
				required: "Por favor, ingrese un valor."
			}
		},
		errorPlacement: function(error, element) {
			if ($(element).is("select")) {
				$(error).appendTo($(element).parent().parent());
				$(element).parent().css("border", "1px solid #B60E16");
			}
			else {
				$(error).insertAfter(element);
			}
		}
	});			

	$("#descripcion").blur(function(){
		var descripcion = $.trim($("#descripcion").val());
		$("#descripcion").val(descripcion);
	});

	$("#nombreProyecto").blur(function(){
		var nombreProyecto = $.trim($("#nombreProyecto").val());
		$("#nombreProyecto").val(nombreProyecto);
	});

	// $("#estadoProyecto").blur(function(){
	// 	var estadoProyecto = $.trim($("#estadoProyecto").val());
	// 	$("#estadoProyecto").val(estadoProyecto);
	// });

	$("#modulo").blur(function(){
		var modulo = $.trim($("#modulo").val());
		$("#modulo").val(modulo);
	});

	$("#valor").blur(function(){
		var valor = $.trim($("#valor").val());
		$("#valor").val(valor);
	});

	$("#descripcion_mod").blur(function(){
		var descripcion_mod = $.trim($("#descripcion_mod").val());
		$("#descripcion_mod").val(descripcion_mod);
	});

	$('#idproject').change(function(){
		$('#div_idtabla').css('border', '');
		$('#idproject-error').remove();
	});

	$('#estadoProyecto').change(function(){
		$('#div_estadoProyecto').css('border', '');
		$('#estadoProyecto-error').remove();
	});

	$('#empresa').change(function(){
		$('#div_empresa').css('border', '');
		$('#empresa-error').remove();
	});

	$('#prioridad').change(function(){
		$('#div_prioridad').css('border', '');
		$('#prioridad-error').remove();
	});

	$("#initialdate").on("change",function(){
        $('#initialdate').removeClass('error');
		$('#initialdate-error').remove();
		$('div_initialdate').css('border', '');
    });

    $("#finaldate").on("change",function(){
        $('#finaldate').removeClass('error');
		$('#finaldate-error').remove();
		$('div_finaldate').css('border', '');
    });

	$("#newMantenimiento").click(function() {
		typeNum = 1;
		nombTab = 'infoTabla';
		$("#theHeader").html('Nuevo Proyecto');
		displayOrHide(false);
		// cargarUsuarios();
		$("#container").show();
		$("#idproject").select2("val","");
		LimpiarTabla("#tblModulos");
		// $('#enviarmant').addClass('btn-primary').removeClass('btn-success').html("Guardar");
		$('#enviarmant').addClass('btn btn-primary').removeClass('btn-success').html("<i class='fa fa-plus'></i>&nbsp; Agregar");
		$("#modalMantenimiento").modal();
	});

	$('#modalMantenimiento').on('hidden.bs.modal', function(e) {
		$('.nav-tabs a[href="#infotabla"]').tab('show');
		$(".nav li#infoMantenimiento").removeClass('active');
		$(".nav li#infoTabla").addClass('active');
		$("#infoMantenimiento").show();
		$('#infoTabla').show();
		// $("#idproject").select2("val","");
		// $("#prioridad").select2("val","");
		// inputs = ['#modulo', '#descripcion_mod', '#initialdate', '#finaldate'];
		// LimpiarInput(inputs, ['']);
		$("#estadoProyecto").select2("val","");
		$("#empresa").select2("val","");
		$(".mentenimientosinputs").val("");
		$('#encargado').selectpicker('val', '');
		$("#prioridad").select2("val","");
		$('#encargadoModulo').selectpicker('val', '');
		$('.mentenimientosinputs').removeClass('error');
		$('.error').remove();
		$('div').css('border', '');

		//$("#codmantenimiento").prop('disabled', false);
		//$("#nombreProyecto").prop('disabled', false);
	});

	$('#anclaTabla').on('click', '.cargarInfo', function(e){
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var idProyecto = row.eq(0).text();
		cargarModulos(idProyecto);
	});

	$('#displayTables').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		}
		else {
			$('#tablaTablasMant').dataTable().$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		nombTab = e.target.id;
		console.log(nombTab);
	});

	$('#displayTables').on('click', '.editTablas', function (e) {
		typeNum = 2;
		nombTab = 'infoTabla';
		$("#theHeader").html('Editar Proyecto');
		$('.nav-tabs a[href="#infotabla"]').tab('show');
		$('#infoTabla').show();
		$('#infoMantenimiento').hide();
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#idProyecto").val(row.eq(0).text());
		$("#nombreProyecto").val(row.eq(1).text());
		//$("#nombreProyecto").prop('disabled', true);
		$("#descripcion").val(row.eq(2).text());
		$("#estadoProyecto").select2('val', row.eq(3).text());

		$("#empresa").select2('val', row.eq(4).text());

		// console.log($("#idProyecto").val());
		cargarArregloUsuarios($("#idProyecto").val());

		displayOrHide(false);
		// $('#enviarmant').addClass('btn-success').removeClass('btn-danger').html("Editar");
		$("#enviarmant").show();
		$("#cancelar").show();
		$('#enviarmant').addClass('btn btn-success').html("<i class='fa fa-floppy-o'></i>&nbsp; Guardar");	
		$("#modalMantenimiento").modal();
	});

	$('#displayTable').on('click', '.editMantenimiento', function (e) {
		typeNum = 2;
		nombTab = 'infoMantenimiento';
		$("#container").hide();
		$('.nav-tabs a[href="#infomantenimientos"]').tab('show');
		$("#theHeader").html('Editar Módulos');
		$('.nav-tabs a[href="#infomantenimientos"]').tab('show');
		$('#infoMantenimiento').show();
		$('#infoTabla').hide();
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#idmodulo").val(row.eq(0).text());
		$("#idproject").select2('val', row.eq(1).text());
		$("#idproject").prop("disabled", true);
		// $("#codmantenimiento").val(row.eq(2).text());
		// $("#codmantenimiento").prop('disabled', true);
		$("#modulo").val(row.eq(2).text());
		// $("#valor").val(row.eq(4).text());
		$("#descripcion_mod").val(row.eq(3).text());
		$("#prioridad").select2('val', row.eq(4).text());
		$("#initialdate").val(row.eq(5).text());
		$("#finaldate").val(row.eq(6).text());
		$("#numero_modulo").val(row.eq(7).text());

		cargarArregloUsuariosPorModulo($("#idmodulo").val());

		displayOrHide(false);
		// $('#enviarmant').addClass('btn-success').removeClass('btn-danger').html("Editar");
		$("#enviarmant").show();
		$("#cancelar").show();
		$('#enviarmant').addClass('btn btn-success').html("<i class='fa fa-floppy-o'></i>&nbsp; Guardar");	
		$("#modalMantenimiento").modal();
	});

	// $("#enviarmant").click(function() {
	// 	if (nombTab == 'infoTabla') {
	// 		if (tablaForm.valid())
	// 		tablaForm.submit();
	// 	} else {
	// 		if (matenimientoForm.valid())
	// 		 matenimientoForm.submit();
	// 	}
		
	// });

	// tablaForm.submit(function(event){
	// 	event.preventDefault();
	// 	var actividadDir = "";
	// 	if (typeNum == 1) {
	// 		actividadDir = "tablasInsert";
	// 	} else if (typeNum == 2) {
	// 		actividadDir = "tablasUpdate";
	// 	}
	// 	var params = {};
	// 	params.ID_TABLA = $("#idProyecto").val();
	// 	params.CODIGO_TABLA = $("#nombreProyecto").val();
	// 	params.NOMBRE = $("#descripcion").val();
	// 	params.ESTADO = $("#estadoProyecto").val();
	// 	$.post(type+actividadDir, params, function(data) {
	// 		var json = jQuery.parseJSON(data);
	// 		if (json.estado == 1){
	// 		GenerarAlerta(json.mensaje, 'Mantenimiento', 1);
	// 		} else{
	// 		GenerarAlerta(json.mensaje, 'Alerta', 2);
	// 		setTimeout(function(){$("#cerrarMant").click();});
	// 		cargarProyectos();
	// 		}		
	// 	});
	// });

	$("#enviarmant").click(function() {
		if (nombTab == 'infoTabla') {
			if (tablaForm.valid())
			tablaForm.submit();
		}else {
			if (matenimientoForm.valid())
			matenimientoForm.submit();
		}	
	});

	tablaForm.submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "projectsInsert";
		} else if (typeNum == 2) {
			actividadDir = "projectsUpdate";
		}
		var params = {};
		params.IDPROYECTO = $("#idProyecto").val();
		params.NOMBRE = $("#nombreProyecto").val();
		params.DESCRIPCION = $("#descripcion").val();

		var valueStatus = $("#estadoProyecto").val();
		console.log(valueStatus);
		var textStatus = $("#estadoProyecto option:selected").text();
		console.log(textStatus);
		params.ESTADO = valueStatus;

		params.EMPRESA = $("#empresa").val();

		// SELECTPICKER

		var listaUsuarios = [];
		var i = 0;
		$.each($('#encargado option:selected'), function(){
			listaUsuarios.push($(this).val());
			console.log($(this).val());
			i++;
		});

		params.usuarios = listaUsuarios;
		// var valueSelectPicker = $("#encargado").val();
		// var textSelectPicker = $("#encargado option:selected").text();
		// console.log(valueSelectPicker);
		// console.log(textSelectPicker);

		//FUNCIONANDO SIN SELECTPCIKER
		// $.post(type+actividadDir, params, function(data) {
		// 	console.log(data);
		// 	var json = jQuery.parseJSON(data);
		// 	console.log(json);
		// 	if (json.estado == 1){
		// 	GenerarAlerta(json.mensaje, 'Mantenimiento', 1);
		// 	} else{
		// 	GenerarAlerta(json.mensaje, 'Alerta', 2);
		// 	setTimeout(function(){$("#cerrarMant").click();});
		// 	cargarProyectos();
		// 	cargarEmpresa();
		// 	cargarEstados();
		// 	}		
		// });

		var x = JSON.stringify(params);
		$.post(type+actividadDir, {data : x}, function(data) {
			var json = jQuery.parseJSON(data);
			if (json.estado == 1){
			GenerarAlerta(json.mensaje, 'Mantenimiento', 1);
			console.log(json.mensaje);
			} else{
			GenerarAlerta(json.mensaje, 'Alerta', 2);
			setTimeout(function(){$("#cerrarMant").click();});
			cargarProyectos();
			cargarEmpresa();
			cargarEstados();
			}
		});
	});

	// matenimientoForm.submit(function(event){
	// 	event.preventDefault();
	// 	var actividadDir = "";
	// 	if (typeNum == 1) {
	// 		actividadDir = "mantenimientosInsert";
	// 	} else if (typeNum == 2) {
	// 		actividadDir = "mantenimientosUpdate";
	// 	}
	// 	var params = {};
	// 	params.ID_MANTENIMIENTO = $("#idmantenimiento").val();
	// 	params.COD_MANTENIMIENTO = $("#codmantenimiento").val();
	// 	params.ID_TABLA = $("#idtbl").val();
	// 	params.NOMBRE = $("#mantenimiento").val();
	// 	// params.VALOR = $("#valor").val();
	// 	params.DESCRIPCION = $("#descripcion_mant").val();
	// 	$.post(type+actividadDir, params, function(data) {
	// 		var json = jQuery.parseJSON(data);
	// 		if (json.estado == 1){
	// 		GenerarAlerta(json.mensaje, 'Solicitud', 1);
	// 		} else{
	// 		GenerarAlerta(json.mensaje, 'Alerta', 2);
	// 		cargarModulos($("#idtbl").val());
	// 		setTimeout(function(){$("#cerrarMant").click();});
	// 		}		
	// 	});
	// });

	$("#infoTabla").click(function() {
		$("#enviarmant").show();
		$("#cancelar").show();
	});

	$("#infoMantenimiento").click(function() {
		$("#enviarmant").hide();
		$("#cancelar").hide();
	});

	$("#agregarMod").click(function() {
		if (matenimientoForm.valid())
			matenimientoForm.submit();

		/*inputs = ['#modulo', '#descripcion_mod', '#initialdate', '#finaldate'];
		LimpiarInput(inputs, ['']);*/

		// var params = {};
		// params.ID = $("#idproject").val();
		// params.PROJECTNAME = $("#idproject option:selected").text();
		// params.NOMBREMOD = $("#modulo").val();
		// params.DESCRIPCION = $("#descripcion_mod").val();
		// params.PRIORIDAD = $("#prioridad").val();
		// params.INICIAL = $("#initialdate").val();
		// params.FINAL = $("#finaldate").val();

		// diasEstimados = 0;
		// diasEstimados = calcularDias(params.FINAL, params.INICIAL);
		// console.log(diasEstimados);

		// addRow(params, '#tblModulos');
		// console.log(params);
	});

	matenimientoForm.submit(function(event){
		event.preventDefault();

		// var fechaini = $("#initialdate").val();
		// var fechafin = $("#finaldate").val();
		// if (fechaini != "" || fechafin != "") {
		// 	var fechasvalidas = validaCamposFecha(fechaini, fechafin);
		// 	if (fechasvalidas == false) {
		// 		GenerarAlerta('La fecha de inicio debe ser menor que la fecha de finalización.','Mantenimiento', 1);
		// 		return false;
		// 	}
		// }

		$("#initialdate").datepicker({format: 'dd/mm/yyyy', autoclose: true, startDate: new Date()}).on('changeDate', function (selected) {//ok
			$('#finaldate').datepicker('setStartDate', new Date(selected.date.valueOf()));
		});
		
		$("#finaldate").datepicker({format: 'dd/mm/yyyy', autoclose: true, startDate: new Date()}).on('changeDate', function (selected) {//ok
			$('#initialdate').datepicker('setEndDate', new Date(selected.date.valueOf()));
		});

		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "modulosInsert";
		} else if (typeNum == 2) {
			actividadDir = "modulosUpdate";
		}
		var params = {};
		params.IDMODULO = $("#idmodulo").val();
		console.log(params.IDMODULO);
		params.IDPROYECTO = $("#idproject").val();
		params.NOMBRE = $("#modulo").val();
		params.DESCRIPCION = $("#descripcion_mod").val();
		params.PRIORIDAD = $("#prioridad").val();
		params.INICIAL = $("#initialdate").val();
		params.FINAL = $("#finaldate").val();

		diasEstimados = 0;
		diasEstimados = calcularDias(params.FINAL, params.INICIAL);
		console.log(diasEstimados);

		params.ESTIMADOS = diasEstimados;

		// SELECTPICKER

		var listaUsuariosModulos = [];
		var i = 0;
		$.each($('#encargadoModulo option:selected'), function(){
			listaUsuariosModulos.push($(this).val());
			console.log($(this).val());
			i++;
		});

		params.usuarios = listaUsuariosModulos;

		params.NUMEROMODULO = $("#numero_modulo").val();

		console.log(params);

		var x = JSON.stringify(params);
		$.post(type+actividadDir, {data : x}, function(data) {
			var json = jQuery.parseJSON(data);

			if (json.id != ""){
				cargarModuloProyecto(json.id);
			}

			if (json.estado == 1)
			{
				GenerarAlerta(json.mensaje, 'Mantenimiento', 1);
				console.log(json.mensaje);
			}

			if(typeNum != 1)
			{
				GenerarAlerta(json.mensaje, 'Alerta', 2);
				setTimeout(function(){$("#cerrarMant").click();});
				console.log(params.IDPROYECTO);
				cargarModulos(params.IDPROYECTO);
			}
		});

		//SIN SELECTPICKER
		// $.post(type+actividadDir, params, function(data) {
		// 	console.log(data);
		// 	var json = jQuery.parseJSON(data);
		// 	console.log(json);
			
		// 	if (json.id != ""){
		// 		cargarModuloProyecto(json.id);
		// 	}

		// 	if(json.estado == 1)
		// 	{
		// 		GenerarAlerta(json.mensaje, 'Mantenimiento', 1);
		// 	}

		// 	if(typeNum != 1)
		// 	{
		// 		GenerarAlerta(json.mensaje, 'Alerta', 2);
		// 		setTimeout(function(){$("#cerrarMant").click();});
		// 		console.log(params.IDPROYECTO);
		// 		cargarModulos(params.IDPROYECTO);
		// 	}
		// });
	});

	function displayOrHide(isDisplaying) {
    	if(isDisplaying) {
     	 	$("#sure").show();
     	 	$(".greatInputContainer").hide();
    	} else {
      		$(".greatInputContainer").show();
      		$("#sure").hide();
    	}
  	}

	// function cargarTablas(){
	// 	var path = type + "tablasGet";
	// 	var posting = $.post(path, {param1: 'value1'});
	// 	posting.done(function(data, status, xhr){
	// 		LimpiarTablaSimple("#tablaTablasMant");
	// 		// LimpiarSelect($("#idtbl"));
	// 		if (data != false) {
	// 			for (var i = data.length - 1; i >= 0; i--) {
	// 				var nodo = data[i]
	// 				$("#idtbl").append('<option value="' + nodo.ID_MAINTENANCE_TABLE + '">' + nodo.TABLE_NAME + '</option>');
	// 			}
	// 			var columns = [{"mDataProp":"ID_MAINTENANCE_TABLE", "className":"hidden"},
	// 						{"mDataProp":"TABLE_CODE", "className":"text-left cargarInfo"},
	// 						{"mDataProp":"TABLE_NAME", "className":"text-left cargarInfo"},
	// 						{"mDataProp":"DESCRIPTION", "className":"text-left cargarInfo"},
	// 						{
	//                          "targets": -1,
	//                           "data": [{"mDataProp":""}],
	//                           "defaultContent": "<span class='editTablas btn btn-success botonVED'><i class='icon-edit icon-white fa fa-pencil'></i></span>", "className": "text-center"
	//                   		}];
	// 			LoadDataTableSimple(data, '#tablaTablasMant', columns);
	// 		}
	// 	});
	// }

	function cargarProyectos(){
		var path = type + "proyectosGet";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			LimpiarTablaSimple("#tablaTablasMant");
			// LimpiarSelect($("#idtbl"));
			$("#idproject").select2("val","");
			
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var nodo = data[i]
					$("#idproject").append('<option value="' + nodo.ID_PROJECTS + '">' + nodo.PROJECT_NAME + '</option>');
				}
				var columns = [{"mDataProp":"ID_PROJECTS", "className":"hidden"},
							{"mDataProp":"PROJECT_NAME", "className":"text-left cargarInfo"},
							{"mDataProp":"DESCRIPTION", "className":"text-left cargarInfo"},
							{"mDataProp":"STATUS_PROJECT", "className":"hidden"},
							{"mDataProp":"ID_BUSINESS_PROJECTS", "className":"hidden"},
							{
	                         "targets": -1,
	                          "data": [{"mDataProp":""}],
	                          "defaultContent": "<span class='editTablas btn btn-success botonVED'><i class='icon-edit icon-white fa fa-pencil'></i></span>", "className": "text-center"
	                  		}];
				LoadDataTableSimple(data, '#tablaTablasMant', columns);
			}
		});
	}

	function cargarModulos(idProyecto){
		var path = type + "modulosGet";
		var objeto = {};
		objeto.idProyecto = idProyecto;
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){
			LimpiarTablaSimple("#tablaMantenimientos");			
			if (data != false) {
				var columns = [{"mDataProp":"ID_PROJECT_MODULES", "className":"hidden"},
								{"mDataProp":"ID_PROJECTS", "className":"hidden"},
								{"mDataProp":"PROJECT_MODULE_NAME", "className":"text-left cargarInfo"},
								{"mDataProp":"DESCRIPTION", "className":"text-left cargarInfo"},
								{"mDataProp":"PRIORITY", "className":"hidden"},
								{"mDataProp":"INITIAL_ESTIMATED_DATE", "className":"hidden"},
								{"mDataProp":"FINAL_ESTIMATED_DATE", "className":"hidden"},
								{"mDataProp":"MODULES_NUMBER", "className":"hidden"},
								{
	                          	 "targets": -1,
	                          	 "data": [{"mDataProp":""}],
	                         	 "defaultContent": "<span class='viewModulo btn btn-primary botonVED'><i class='fa fa-eye'></i></span><span class='editMantenimiento btn btn-success botonVED'><i class='icon-edit icon-white fa fa-pencil'></i></span><span class='deleteModuloIndex btn btn-danger botonVED'><i class='icon-edit icon-white fa fa-trash-o'></i></span>", "className": "text-center"
	                 			 }];
				LoadDataTableSimple(data, '#tablaMantenimientos', columns);
			}
		});
	}

	function cargarModuloProyecto(id){
		var path = type + "ModuloProyectoGet";
		var objeto = {};
		objeto.id = id;
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){
			$("#prioridad").select2("val","");
			$('#encargadoModulo').selectpicker('val', '');
			inputs = ['#modulo', '#descripcion_mod', '#initialdate', '#finaldate'];
			LimpiarInput(inputs, ['']);

			if (data != false) {
				// console.log(data);
				 addRow(data, "#tblModulos");
			}
		});
	}

	function cargarEmpresa() {
		var metodo = type + 'empresasGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			$("#empresa").select2("val","");
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var empresas = data[i]
					$("#empresa").append('<option value="' + empresas.ID_BUSINESS_PROJECTS + '">' + empresas.BUSINESS_NAME + '</option>');
				}
			}
		});
	}

	function cargarEstados() {
		var metodo = type + 'estadosGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			$("#estadoProyecto").select2("val","");
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var estados = data[i]
					$("#estadoProyecto").append('<option value="' + estados.ID_MAINTENANCE + '">' + estados.MAINTENANCE_NAME + '</option>');
				}
			}
		});
	}

	function cargarPrioridad() {
		var metodo = type + 'prioridadGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			$("#prioridad").select2("val","");
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var estados = data[i]
					$("#prioridad").append('<option value="' + estados.ID_MAINTENANCE + '">' + estados.MAINTENANCE_NAME + '</option>');
				}
			}
		});
	}

	function cargarUsuarios() {
		var opciones = [];
		var metodo = type + 'encargadosGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			LimpiarSelectPicker($("#encargado"));
			// $('.selectpicker').selectpicker('val', '');
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var encargado = data[i]
					var opcion = '<option value="' + encargado.U_ID + '">' + encargado.NAME + '</option>';
					opciones.push(opcion);
				}
				$("#encargado").append(opciones);
				$("#encargado").selectpicker('refresh');
			}
		});
	}

	function cargarUsuariosPorModulo() {
		var opciones = [];
		var metodo = type + 'encargadosGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			LimpiarSelectPicker($("#encargadoModulo"));
			// $('.selectpicker').selectpicker('val', '');
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var encargado = data[i]
					var opcion = '<option value="' + encargado.U_ID + '">' + encargado.NAME + '</option>';
					opciones.push(opcion);
				}
				$("#encargadoModulo").append(opciones);
				$("#encargadoModulo").selectpicker('refresh');
			}
		});
	}

	function addRow(ArrayData, TableName){
                    var newRow = $(TableName).dataTable().fnAddData([
                               ArrayData['ID_PROJECT_MODULES'],
                               ArrayData['PROJECT_NAME'],
                               ArrayData['PROJECT_MODULE_NAME'],
                               ArrayData['DESCRIPTION'],
                               ArrayData['MAINTENANCE_NAME'],
                               ArrayData['ESTIMADA_INICIO'],
                               ArrayData['ESTIMADA_FINAL'],
                               "<span class='deleteModulo btn btn-danger botonVED btn-sm'><i class='icon-edit icon-white fa fa-trash-o'></i></span>"
                               ]);
                               var theNode = $(TableName).dataTable().fnSettings().aoData[newRow[0]].nTr;
                               theNode.setAttribute('id', ArrayData['ID']);
                               $('td', theNode)[0].setAttribute( 'class', 'hidden id' );   
    }

    $('#tbodyModulos').on('click', '.deleteModulo', function (e) {
    	lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var idModulo = row.eq(0).text();
		
        var path = type + "moduleDelete";
		var objeto = {};
		objeto.idModulo = idModulo;
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){			
			if (data != false) {
				var tblModulos = $('#tblModulos').DataTable();
				tblModulos.row(row).remove().draw();
			}
		});
        
        //$('#enviarTipoConstancia').addClass('btn-danger').removeClass('btn-primary').html("Eliminar");
	});

	$('#displayTable').on('click', '.deleteModuloIndex', function (e) {
		// typeNum = 3;
		$("#modalDelete").modal();
	    $("#theHeader3").html('Eliminar Módulo');
	    displayOrHide(true);

    	lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var idModulo = row.eq(0).text();
		var idProyecto = row.eq(1).text();
		$("#nombre_span").html(row.eq(2).text());

		$("#eliminarModulo").click(function(){
    	var path = type + "moduleDelete";
		var objeto = {};
		objeto.idModulo = idModulo;
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){			
			if (data.estado == 1){
			GenerarAlerta(data.mensaje, 'Mantenimiento', 1);
			} else{
			GenerarAlerta(data.mensaje, 'Alerta', 2);
			setTimeout(function(){$("#cerrarSure").click();});
			cargarModulos(idProyecto);
			}
		});
	});
	});

	$('#displayTable').on('click', '.viewModulo', function (e) {
		$("#modalView").modal();
		$("#theHeader2").html('Detalle del Módulo');
		var id = $(this).parent().parent().find("td").eq(0).text();

		var path = type + "ModuloProyectoGet";
		var objeto = {};
		objeto.id = id;
		
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){			
			if (data != false) {
				console.log(data);
				diasReales = 0;
				diasReales = calcularDias(data.REAL_FINAL, data.REAL_INICIAL);
				console.log(diasReales);

				diasEstimados = 0;
				diasEstimados = calcularDias(data.ESTIMADA_FINAL, data.ESTIMADA_INICIO);
				console.log(diasEstimados);

				diasRetrasados = 0;
				diasRetrasados = diasReales - diasEstimados;
				console.log(diasRetrasados);

				$("#VER_Proyecto").html(data.PROJECT_NAME);
				$("#VER_Modulo").html(data.PROJECT_MODULE_NAME);
				$("#VER_Numero_Modulos").html(data.MODULES_NUMBER);
				$("#VER_Prioridad").html(data.MAINTENANCE_NAME);
				$("#VER_Porcentaje_Avance").html(data.ADVANCE_PERCENTAGE);
				$("#VER_Tiempo_Estimado").html(data.ESTIMATED_TIME);
				$("#VER_Tiempo_Real").html(diasReales);
				$("#VER_Tiempo_Retraso").html(diasRetrasados);
				$("#VER_Fecha_Inicio").html(data.ESTIMADA_INICIO);
				$("#VER_Fecha_Final").html(data.ESTIMADA_FINAL);
				$("#VER_Fecha_Inicio_Real").html(data.REAL_INICIAL);
				$("#VER_Fecha_Final_Real").html(data.REAL_FINAL);
				$("#VER_Ciclo").html(data.DEVELOPMENT_CYCLE);
			}
		});
	});


	function cargarArregloUsuarios(idProyecto)
	{
		var path = type + "arregloUsuariosGet";
		var objeto = {};
		objeto.idProyecto = idProyecto;
		var posting = $.post(path, objeto);
		var selectActual = [];
		
		posting.done(function(data, status, xhr){
				// console.log(data);
				// console.log(data[0].U_ID);
			for (i in data) {
				selectActual.push(data[i].U_ID);
				$('#encargado').selectpicker('val', selectActual);
			// 	// $('#chk'+data[i].U_ID).attr('checked', 'true');
			}
		});
	}

	function cargarArregloUsuariosPorModulo(idModulo)
	{
		var path = type + "arregloUsuariosGetByModule";
		var objeto = {};
		objeto.idModulo = idModulo;
		var posting = $.post(path, objeto);
		var selectActual = [];
		
		posting.done(function(data, status, xhr){
				// console.log(data);
				// console.log(data[0].U_ID);
			for (i in data) {
				selectActual.push(data[i].U_ID);
				$('#encargadoModulo').selectpicker('val', selectActual);
			// 	// $('#chk'+data[i].U_ID).attr('checked', 'true');
			}
		});
	}

	// function cmbCliente()
	// {
	//   var infojson = jQuery.parseJSON( '{"input": "#cliente", ' +
	//                                   '"url": "index.php/clientes/getClientes", ' +
	//                                   '"default": "", ' +
	//                                   '"val": "ID_CLIENTE", ' +
	//                                   '"type": "POST", ' +
	//                                   '"data": "NULL", ' +
	//                                   '"text": "NOMBRE_CLIENTE"}');
	//   LoadComboBoxForm(infojson);
	// }
});
