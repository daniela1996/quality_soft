$(document).ready( function(){
	var type = urljs+"index.php/Gestiongps/";
	var urlregiones = urljs+"index.php/regiones/";
	var typeNum = 0;
	var lastRow = null;
	var GPSForm = $('#GPSForm');
	$('#celular').inputmask("+50499999999");
	$('#pais').select2();
	$('#depto').select2();
	CargarGPS();


	GPSForm.validate({
		rules:{
			pais: {
				required: true
			},
			depto: {
				required: true
			},
			municipio: {
				required: true,
				letterswithbasicpunc: true,
				maxlength: 25 
			},
			cod_vehiculo:{
				required:true,
				maxlength:15
			},
			celular: {
				required: true,
				minlength:10
			},
			cuenta: {
				required: true,
				maxlength:25
			},
			galones:{
				number:true
			},
			voltaje:{
				number: true
			}, 
			voltajemin:{
				number: true
			},
			descripcion:{
				maxlength: 200
			}

		},
		messages:{
			municipio: {
				letterswithbasicpunc: "Por favor, ingrese solo letras"
			}
		},
		errorPlacement: function(error, element) {
			//si el elemento es un select
			if ($(element).is("select")) {
				// colocar el error al final del contenedor
				$(error).appendTo($(element).parent().parent());
				// resaltar el borde del contenedor del select
				$(element).parent().css("border", "1px solid #B60E16");
			}
			else {
				// colocar el error después del elemento
				$(error).insertAfter(element);
			}
		}
	});

	$("#vehiculosForm").validate({
		rules: {
			compania: {
				required: true,
				maxlength: 50
			},
			marca: {
				required: true,
				maxlength: 30
			},
			modelo: {
				required: true,
				maxlength: 30
			},
			chasis:{
				required: true,
				minlength: 17,
				maxlength: 17,
				alphanumeric: true
			},
			year: {
				required: true,
				number:true,
				maxlength:5
			},
			color:{
				required: true,
				maxlength:20
			},
			motor:{
				required: true,
				maxlength: 40,
				minlength:10,
				alphanumeric: true
			},
			plate: {
				required: true,
				maxlength:10
			},
			imei: {
				required: false,
				maxlength: 10
			}

		},
		messages: {
			compania: {
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			marca: {
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			modelo: {
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			chasis: {
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres.",
				alphanumeric: "Por favor ingrese solo numeros y letras."
			},
			year: {
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			color: {
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			motor: {
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres.",
				alphanumeric: "Por favor ingrese solo numeros y letras."

			},
			plate:{
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			imei: {
				required: "Por favor, ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."

			}
		}	
	});

	$('#pais').change(function(){
		$('#div_pais').css('border', '');
		$('#pais-error').remove();
	});

	$('#depto').change(function(){
		$('#div_depto').css('border', '');
		$('#depto-error').remove();
	});

	$('#cod_vehiculo').blur(function(){
		var cod_vehiculo = $.trim($('#cod_vehiculo').val());
		$('#cod_vehiculo').val(cod_vehiculo);
	});

	$('#cuenta').blur(function(){
		var cuenta = $.trim($('#cuenta').val());
		$('#cuenta').val(cuenta);
	});

	$('#municipio').blur(function(){
		var municipio = $.trim($('#municipio').val());
		$('#municipio').val(municipio);
	});

	$('#compania').blur(function(){
		var compania = $.trim($('#compania').val());
		$('#compania').val(compania);
	});

	$('#marca').blur(function(){
		var marca = $.trim($('#marca').val());
		$('#marca').val(marca);
	});

	$('#modelo').blur(function(){
		var modelo = $.trim($('#modelo').val());
		$('#modelo').val(modelo);
	});

	$('#color').blur(function(){
		var color = $.trim($('#color').val());
		$('#color').val(color);
	});

	$('#plate').blur(function(){
		var plate = $.trim($('#plate').val());
		$('#plate').val(plate);
	});


	function displayOrHide(isDisplaying) {
    	if(isDisplaying) {
      	$("#InformacionCompleta").show();
      	$(".greatInputContainer").hide();
      	$("#enviargestion").hide();
    	} else {
      	$(".greatInputContainer").show();
      	$("#enviargestion").show();
      	$("#InformacionCompleta").hide();
    	}
  	}
  	$('#modalgestiongps').on('hidden.bs.modal', function(e) {
		$("#depto").select2("val","");
		$("#pais").select2("val","");
		$(".gestiongpsinputs").val("");
		$(".gestiongpsinputs").removeClass('error');
		$(".error").remove();
		$('div').css('border', '');
	});

	$("#newGestiongps").click(function() {
	typeNum = 2;
	$("#theHeader2").html('Nueva asignación de GPS');
	displayOrHide(false);
	$('#enviargestion').addClass('btn-primary').removeClass('btn-danger').html("Guardar");
	$("#modalgestiongps").modal();
	});

	$("#enviargestion").click(function() {
		if (GPSForm.valid())
			GPSForm.submit();
	});

	$('#GPSForm').submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "gestionUpdate";
		} else if (typeNum == 2) {
			actividadDir = "gestionInsert";
		}
		var params = {};
		params.AUTOID = $("#autoid").val();
		params.COD_PAIS = $("#pais").val();
		params.COD_DEPTO = $("#depto").val();
		params.MUNICIPIO = $("#municipio").val();
		params.CUENTA = $("#cuenta").val();
		params.COD_VEHICULO = $("#cod_vehiculo").val();
		params.CELULAR = $("#celular").val();
		params.VOLTAJE = $("#voltaje").val();
		params.VOLTAJEMIN = $("#voltajemin").val();
		params.GALONES = $("#galones").val();
		params.DESCRIPCION = $("#descripcion").val();

		var gestionName = JSON.stringify(params);
		$.post(type+actividadDir, params, function(data) {
			var json = jQuery.parseJSON(data);
			if (json.estado == 1){
			GenerarAlerta(json.mensaje, 'Gestion', 1);
			} else{	
			GenerarAlerta(json.mensaje, 'Alerta', 2);
			CargarGPS();
			setTimeout(function(){$("#cerrarModalGestion").click();});
			}
		});
	});
	$('#displayTable').on('click', '.editGestiones', function (e) {
		typeNum = 1;
		$("#theHeader2").html('Editar asignación de GPS');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#autoid").val(row.eq(0).text());
		$("#modalgestiongps").attr("data-id-depto", row.eq(13).text());
		$("#pais").select2("val", row.eq(12).text());
		$("#municipio").val(row.eq(3).text());
		$("#cod_vehiculo").val(row.eq(5).text());
		$("#celular").val(row.eq(6).text());
		$("#cuenta").val(row.eq(7).text());
		$("#galones").val(row.eq(9).text());
		$("#voltajemin").val(row.eq(11).text());
		$("#voltaje").val(row.eq(10).text());
		$("#descripcion").val(row.eq(8).text());
		displayOrHide(false);
		$('#enviargestion').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		$("#modalgestiongps").modal();
	});

	$('#displayTable').on('click', '.cargarInfo', function(e){
		displayOrHide(true);
		$("#theHeader2").html('Información del GPS');
		$("#InformacionCompleta").empty();
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#InformacionCompleta").prepend("<h2 align='center'>"+ row.eq(4).text() +"</h2>")
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>País: </b>"+row.eq(1).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Departamento: </b>"+row.eq(2).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Municipio: </b>"+row.eq(3).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Cod. Vehículo: </b>"+row.eq(5).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Celular: </b>"+row.eq(7).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Cuenta: </b>"+row.eq(8).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Galones: </b>"+row.eq(10).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Voltaje Min: </b>"+row.eq(12).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Voltaje: </b>"+row.eq(11).text()+ " </p> </div>");
		$("#InformacionCompleta").append("<div class='form-group col-md-6'> <p><b>Descripción: </b>"+row.eq(9).text()+ " </p> </div>");
		//$("#InformacionCompleta").append($('<div class="clear"></div>'));
		$('#modalgestiongps').modal();
	});

	function limpiarCmbDeptos(){
		$('#depto')
		.find('option')
		.remove()
		.end()
		.append('<option value="">Seleccionar...</option>')
		.select2("val", "");
		}

	$('#pais').change(function(){
		limpiarCmbDeptos();
		var idPais = $(this).val();

		if(idPais != "")
		cargarDeptos(idPais);
	});

	function cargarDeptos(id){
		var metodo = urlregiones + 'getDeptosPais/' + id;
		$.get(metodo, null, function(data) {
			var deptos = $.parseJSON(data);
			for (var i = 0; i < deptos.length; i++) {

				var depto = deptos[i];
				$("#depto").append('<option value="' + depto.COD_DEPTO + '">' + depto.NOMBRE + '</option>');
			}
			var idDepto = $("#modalgestiongps").attr("data-id-depto");

			if (idDepto != "") {
				$("#depto").select2("val", idDepto);
				$("#modalgestiongps").attr("data-id-depto", "");
			}
		});

	}

	function CargarGPS() 
	{
		// /animacion de loading/
		// LoadAnimation("body");
		var path = type + "getGPS";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function (data, status, xhr) {
			LimpiarTabla('#TblGestionGps');
			if (data != false) {
				var columns = [{ "mDataProp": "AUTO_ID", "className": "hidden id"},
                      {"mDataProp":"PAIS", "className":"hidden id"},
                      {"mDataProp":"NOMBRE", "className":"hidden id"},
                      {"mDataProp":"CIUDAD", "className":"hidden id" },
                      {"mDataProp":"IMEI", "className":"text-center" },
                      {"mDataProp":"COD_ANT", "className":"text-center" },
                      {"mDataProp":"CELULAR", "className":"text-center" },
                      {"mDataProp":"CUENTA", "className":"text-center" },
                      {"mDataProp":"DESCRIPCION", "className":"text-center" },
                      {"mDataProp":"GALONS", "className":"hidden id" },
                      {"mDataProp":"VOLTAJE", "className":"hidden id" },
                      {"mDataProp":"VOLTAJEMIN", "className":"hidden id" },
                      {"mDataProp":"COD_PAIS", "className":"hidden id" },
                      {"mDataProp":"COD_DEPTO", "className":"hidden id" },
                      {
	                          "targets": -1,
	                          "data": [{"mDataProp":""}],
	                          "defaultContent": "<span class='editGestiones btn btn-success botonVED' data-toggle='tooltip' data-placement='right' title='Editar GPS'><i class='icon-edit icon-white fa fa-pencil'></i></span>", "className": "text-center"
	                  }];

			LoadDataTable(data, '#TblGestionGps', columns);
			}
		});
		posting.fail(function (data, status, xhr) {
			console.log(data);
			// sendError(data);
			//GenerarErrorAlerta(xhr, "error");
			// goAlert();
		});
		posting.always(function (data, status, xhr) {
			// RemoveAnimation("body");
			// console.log(data);
		})
	}	
});