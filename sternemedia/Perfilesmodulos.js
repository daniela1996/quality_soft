$(document).ready(function() {
	var type = urljs+"index.php/perfilesmodulos/";
	var isSelected = Array();
	var orientation = false;
	var perfilOModulo = 0;
	var lastRow = null;
	var seleccionados = false;
	$("#perfil_permod").change(function() {
		perfilOModulo = 1;
		if((!isSelected[$(this).val()])&&($(this).val() != 0)) {
			isSelected[$(this).val()] = true;
			var eachProfile = '<div class="box with-border box-header boxProfile"><input type="hidden" class="losDatos" value="'+$(this).val()+'"><h3 class="box-title">'+$("#perfil_permod option[value='"+$(this).val()+"']").text()+'</h3><div class="box-tools pull-right"><button class="btn btn-box-tool thisButton"><i class="fa fa-remove"></i></button></div></div>';
			if(orientation) {
				$("#addRight").append(eachProfile);
				orientation = false;
			} else {
				$("#addLeft").append(eachProfile);
				orientation = true;
			}
		}
	});
	$("#modulo_permod").change(function() {
		perfilOModulo = 2;
		if((!isSelected[$(this).val()])&&($(this).val() != 0)) {
			isSelected[$(this).val()] = true;
			var eachModule = '<div class="box with-border box-header boxProfile"><input type="hidden" class="losDatos" value="'+$(this).val()+'"><h3 class="box-title">'+$("#modulo_permod option[value='"+$(this).val()+"']").text()+'</h3><div class="box-tools pull-right"><button class="btn btn-box-tool thisButton"><i class="fa fa-remove"></i></button></div></div>';
			if(orientation) {
				$("#addRight").append(eachModule);
				orientation = false;
			} else {
				$("#addLeft").append(eachModule);
				orientation = true;
			}
		}
	});
	$('#modalPerfilesModulos').on('click', '.thisButton', function() {
		$(this).closest('.boxProfile').remove();
		isSelected[$(this).closest('.boxProfile').find(".losDatos").val()] = false;
		orientation = !orientation;
	});
	$('#anclaTabla').on('click', '.asociacion', function() {
		lastRow = $(this).closest('tr');
		AproveDenied($(this).parent().parent().find("td").eq(0).text(), "A");
	});
	$('#anclaTabla').on('click', '.desligar', function() {
		lastRow = $(this).closest('tr');
		AproveDenied($(this).parent().parent().find("td").eq(0).text(), "D");
	});
	function AproveDenied(id, status) {
		var params = {};
				params.PROF_MODULEID = id;
				params.ESTADO = status;
		var perfilmoduloName = JSON.stringify(params);
		$.post(type+"perfilmoduloAcceptDecline", {perfilmoduloName :perfilmoduloName}, function(data) {
			GenerarAlerta(data, 'Alerta', 2)
			var statusColumn = lastRow.find("td").eq(5);
			statusColumn.empty();
			if (status === "A") {
				statusColumn.html("<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>");
			} else {
				statusColumn.html("<span class='asociacion btn btn-success botonVED'>Habilitar</span>");
			}
		});
	}
	$("#asociarPerfilModulo").click(function() {
		limpiarContenedorPefilesModulos();
		$("#modalPerfilesModulos").modal();

	});
	$("#enviarPermod").click(function() {
		hayseleccionados
		console.log(seleccionados);
		if (isSelected.length > 0) {
			
		var i = 0;
		var parametros = {};
				parametros.dataArray=[];
				parametros.PROCEDENCIA = perfilOModulo;
				parametros.LLAVE = $("#profileOrModule").html();
		$(".losDatos").each(function(indice) {
			parametros.dataArray[i] = {};
			parametros.dataArray[i].PROFILES_IDPROFILE = (perfilOModulo == 1) ? this.value: $("#profileOrModule").html();
			parametros.dataArray[i].MODULES_IDMODULES = (perfilOModulo == 1) ? $("#profileOrModule").html(): this.value;
			i++;
			console.log(parametros);
		});
		if(parametros.dataArray.length != 0) {
			var perfilmoduloName = JSON.stringify(parametros);
			$.post(type+"perfilmoduloInsert", {perfilmoduloName :perfilmoduloName}, function(data) {
				GenerarAlerta(data[0], 'Alerta', 2);
				setTimeout(function() { $("#cerrarPermod").click();});
				var rowsAgregados = data[1];
				var formaArreglo = [];
				for (var i = 0; i < rowsAgregados.length; i++) {
					if (perfilOModulo == 1) {
						formaArreglo = [rowsAgregados[i]['PROFMODULEID'], rowsAgregados[i]['MODULEID'], rowsAgregados[i]['NAMEMODULE'], rowsAgregados[i]['PROFILEID'], rowsAgregados[i]['NAMEPROFILE'], "<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>"];
						var valor = parseInt(rowsAgregados[i]['PROFILEID']);
						$("#perfil_permod option[value=" +valor+ "]" ).prop('disabled', true);
					
					} else {
						formaArreglo = [rowsAgregados[i]['PROFMODULEID'], rowsAgregados[i]['PROFILEID'], rowsAgregados[i]['NAMEPROFILE'], rowsAgregados[i]['MODULEID'], rowsAgregados[i]['NAMEMODULE'], "<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>"];
						var valor = parseInt(rowsAgregados[i]['MODULEID']);
						$("#modulo_permod option[value=" +valor+ "]" ).prop('disabled', true);
					}
					addOnTable(formaArreglo, [0,1,2,3]);
				}
				if (perfilOModulo == 1) {
					var perfilOptions = $("#perfil_permod").find('option');
					$("#perfil_permod").select2("destroy");
					$("#perfil_permod").empty();
					$("#perfil_permod").append(perfilOptions);
					$("#perfil_permod").select2();
				} else {
					var perfilOptions = $("#modulo_permod").find('option');
					$("#modulo_permod").select2("destroy");
					$("#modulo_permod").empty();
					$("#modulo_permod").append(perfilOptions);
					$("#modulo_permod").select2();
				}
			});
		}
		} else {
			GenerarAlerta('Por favor, seleccione una opción.','PerfilesModulos', 1 )
		}
	});
	function limpiarContenedorPefilesModulos(){
		$('#addLeft').empty();
		$('#addRight').empty();
		if ($('#perfil_permod').css('display')) {
			$('#perfil_permod').select2('val','0');
		} else {
			$('#modulo_permod').select2('val','0');
		}
		for (var i = 0; i < isSelected.length; i++) {
			isSelected[i] = false;
		}
	}

	function hayseleccionados(){
		for (var i = 0; i < isSelected.length; i++) {
			if (isSelected[i] = true) {
				seleccionados = true;
			}
		}
	}
});
