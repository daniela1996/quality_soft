$(document).ready(function () {
	var type = urljs+"index.php/perfiles/";
	var typeNum = 0;
	var lastRow = null;
	var perfilesForm = $("#perfilesForm");

	perfilesForm.validate({
		rules:{
			codigo_perfil:{
				required: true,
				maxlength: 10,
				nowhitespace: true,
				lettersonly:true,
			},
			nombreperfil:{
				required: true,
				maxlength: 30,
			},
			descripcion_perfil:{
				required: true,
				maxlength: 60,
				letterswithbasicpunc: true
			}
		},
		messages:{
			codigo_perfil:{
				required: "Por favor, ingrese un valor.",
				nowhitespace: "Por favor, no escriba espacios.",
				lettersonly: "Por favor, ingrese solo letras."
			},
			nombreperfil:{
				required: "Por favor, ingrese un valor."
			},
			descripcion_perfil:{
				required: "Por favor, ingrese un valor.",
				letterswithbasicpunc: "Por favor, ingrese solo letras y signos de puntuación."
			}
		}
	});


	function displayOrHide(isDisplaying) {
    	if(isDisplaying) {
      		$("#sure").show();
      		$(".greatInputContainer").hide();
    	} else {
      		$(".greatInputContainer").show();
      		$("#sure").hide();
    	}
  	}
  	$("#nombreperfil").blur(function(){
  		var nombreperfil = $.trim($("#nombreperfil").val());
  		$("#nombreperfil").val(nombreperfil);
  	});
  	$("#descripcion_perfil").blur(function(){
  		var descripcion_perfil = $.trim($("#descripcion_perfil").val());
  		$("#descripcion_perfil").val(descripcion_perfil);
  	});
	$('#modalPerfiles').on('hidden.bs.modal', function(e) {
		$(".profileinputs").val("");
		$(".profileinputs").removeClass("error");
		$(".error").remove();

	});
	$('#displayTable').on('click', '.editPerfil', function (e) {
		typeNum = 1;
		$("#theHeader").html('Editar Perfil');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#id_perfil").val(row.eq(0).text());
		$("#codigo_perfil").val(row.eq(1).text());
		$("#nombreperfil").val(row.eq(2).text());
		$("#descripcion_perfil").val(row.eq(3).text());
		displayOrHide(false);
		$('#enviarPerfil').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		$("#modalPerfiles").modal();
	});
	$("#newPerfil").click(function() {
		typeNum = 2;
		$("#theHeader").html('Nuevo Perfil');
		displayOrHide(false);
		$('#enviarPerfil').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$("#modalPerfiles").modal();
	});
	$('#displayTable').on('click', '.deletePerfil', function (e) {
		typeNum = 3;
		$("#theHeader").html('Eliminar Perfil');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#id_perfil").val(row.eq(0).text());
		$("#nombre_span").html(row.eq(2).text());
		displayOrHide(true);
		$('#enviarPerfil').addClass('btn-danger').removeClass('btn-primary').html("Eliminar");
		$("#modalPerfiles").modal();
	});
	$("#enviarPerfil").click(function() {
		if(perfilesForm.valid())
			perfilesForm.submit();
	});

	perfilesForm.submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "perfilUpdate";
		} else if (typeNum == 2) {
			actividadDir = "perfilInsert";
		} else {
			actividadDir = "perfilDelete";
		}
		var params = {};
				params.PROFILEID = $("#id_perfil").val();
				params.CODEPROFILE = $("#codigo_perfil").val();
				params.NAMEPROFILE = $("#nombreperfil").val();
				params.DESCPROFILE = $("#descripcion_perfil").val();
		var perfilName = JSON.stringify(params);
		$.post(type+actividadDir, {perfilName :perfilName}, function(data) {
			switch (data[2]) {
				case 1: { ArmarNuevo(data[1]); } break;
				case 2: {
					var allColumns = lastRow.find("td");
					allColumns.eq(0).html(data[1]['PROFILEID']);
					allColumns.eq(1).html(data[1]['CODEPROFILE']);
					allColumns.eq(2).html(data[1]['NAMEPROFILE']);
					allColumns.eq(3).html(data[1]['DESCPROFILE']);
					allColumns.eq(4).html(data[1]['DATES']);
				} break;
				case 3: { removeOnTable(lastRow); } break;
				default: { }
			}
			if (data[0].estado == 1){
			GenerarAlerta(data[0].mensaje, 'Perfiles', 1);
			} else{
			GenerarAlerta(data[0].mensaje, 'Alerta', 2);
			setTimeout(function(){$("#cerrarPerfil").click();});
			}
		});
	});


	function ArmarNuevo(rowsAgregados) {
		var formaArreglo = [];
		var perfilid = 0;
		var acciones = "";
		for (var i = 0; i < rowsAgregados.length; i++) {
			perfilid = rowsAgregados[i]['PROFILEID'];
			acciones = `<a href="perfilesusuarios?pu=1&vl=${perfilid}">
				<span class="btn purple botonVED" data-toggle="tooltip" data-placement="top" title="Crear Perfiles Por Usuarios">
					<i class="icon-edit icon-white fa fa-user"></i>
				</span>
			</a>
			<a href="perfilesmodulos?pm=1&vl=${perfilid}">
				<span class="btn btn-warning botonVED" data-toggle="tooltip" data-placement="top" title="Verificar Orden de Perfiles">
					<i class="icon-edit icon-white fa fa-list"></i>
				</span>
			</a>
			<span class="editPerfil btn btn-success botonVED">
				<i class="icon-edit icon-white fa fa-pencil"></i>
			</span>
			<span class="deletePerfil btn btn-danger botonVED">
				<i class="icon-edit icon-white fa fa-trash-o"></i>
			</span>`;
			formaArreglo = [perfilid, rowsAgregados[i]['CODEPROFILE'], rowsAgregados[i]['NAMEPROFILE'], rowsAgregados[i]['DESCPROFILE'], rowsAgregados[i]['DATES'], acciones];
			addOnTable(formaArreglo, [0]);
		}
	}
});
