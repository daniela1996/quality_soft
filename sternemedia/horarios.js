$(document).ready(function ()
{

	/*$('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY h:mm:ss'});*/
	/*$('.timepicker').timepicker();*/
	$('#txtInicio').timepicker({ 
		'scrollDefault': 'now', 
		'timeFormat': 'h:i A'
	});

	$('#txtFin').timepicker({ 
		'scrollDefault': 'now', 
		'timeFormat': 'h:i A'
	});

	loadHorarios();

	$('#btnAddHorarios').click(function(e)
	{
		/*Limpiamos los inputs*/
		LimpiarInput(inputsArray, ['']);
		/*Asignamos el valor al input del id*/
		$('#id_horario').val('-1');
		$('#theHeader').text('Nuevo Horario');
		$('#btnEnviar').text('Guardar');
	});

	$('#btnEnviar').on('click', function(e)
	{
		var inputs = ['#txtDesc', '#txtInicio', '#txtFin'];
		var mjs = ['Debe ingresar una descripción', 'Debe ingresar la hora de comienzo', 'Debe ingresar la hora de finalización'];
		var go = Validar(inputs, mjs);

		if (go) 
		{
			LoadAnimation('body');

			var id = $('#id_horario').val();
			var desc = $('#txtDesc').val();
			var inicio = $('#txtInicio').val();
			var fin = $('#txtFin').val();

			/*Hacemos el ajax con la peticion*/
			var params = {};
			params.dataArray = [];
			params.dataArray[0] = {};
			params.dataArray[0].ID = id;
			params.dataArray[0].DESCRIPCION = desc;
			params.dataArray[0].INICIA = inicio;
			params.dataArray[0].FIN = fin;

			var json = JSON.stringify(params);			
			var path = urljs + 'index.php/horarios/save/';
			var posting = $.post(path, {info: json});

			posting.done(function (data) 
			{				
				if (data.Estado) 
				{
					loadHorarios();
					$('#modalHorario').modal('hide');
					GenerarSuccessAlerta(data.Mensaje, "success");
					goAlert();
				}
				else 
				{
					$('#modalHorario').modal('hide');
					GenerarErrorAlerta(data.Mensaje, "error");
					goAlert();
				}
			});

			posting.fail(function (data, status, xhr)
			{
				$('#modalHorario').modal('hide');
				GenerarErrorAlerta(status, "error");
				goAlert();
			});

			posting.always(function() 
			{		    	
		    	RemoveAnimation("body");
		    });
		}
		else
		{
			GenerarErrorAlerta('Debe ingresar la información solicitada para continuar!', 'errorModal');
		}
	});

	$('#btnDeleteHorario').on('click', function(e){
		var id = $('#id_horario').val();

		if (id != "") 
		{
			LoadAnimation('body');			
			var path = urljs + 'index.php/horarios/delete';
			var posting = $.post(path, {info: id});

			posting.done(function (data) 
			{				
				if (data.Estado) 
				{
					loadHorarios();
					$('#modalHorariosDelete').modal('hide');
					GenerarSuccessAlerta(data.Mensaje, "success");
					goAlert();
				}
				else 
				{
					$('#modalHorariosDelete').modal('hide');
					GenerarErrorAlerta(data.Mensaje, "error");
					goAlert();
				}
			});

			posting.fail(function (data, status, xhr)
			{
				$('#modalHorariosDelete').modal('hide');
				GenerarErrorAlerta(status, "error");
				goAlert();
			});

			posting.always(function() 
			{
		    	RemoveAnimation("body");
		    });
		}
	});
});

var inputsArray = ['#id_horario', '#txtDesc'];
var inputsTimer = ['#txtInicio', '#txtFin'];

function loadHorarios() 
{
	try
	{
		/*animacion de loading*/
		LoadAnimation("body");

		var path = urljs + "index.php/horarios/getHorarios";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function (data, status, xhr) {
			if (data.Estado) 
			{
				LimpiarTabla("#example2");
				for (var i = data.Resultado.length - 1; i >= 0; i--) 
				{				
					addRow(data.Resultado[i], "#example2");
				}
			}
		});
		posting.fail(function (data, status, xhr) {
			GenerarErrorAlerta(xhr, "error");
			goAlert();
		});
		posting.always(function (data, status, xhr) {
			RemoveAnimation("body");
		});
	}
	catch(e)
	{
		RemoveAnimation("body");
		console.log(e);
	}
}

function addRow(ArrayData, TableName)
{
	var newRow = $(TableName).dataTable().fnAddData([
		ArrayData['HORARIO_ID'],
		ArrayData['DESCRIPCION'],
		ArrayData['INICIA'],
		ArrayData['FINAL'],
		"<button data-id='"+ArrayData['HORARIO_ID']+"' data-name='"+ArrayData['DESCRIPCION']+"' data-inicio='"+ArrayData['INICIA']+"' data-fin='"+ArrayData['FINAL']+"' title='Editar Horario' data-toggle='tooltip' onClick='EditarHorario(event)' id='btnEditar"+ArrayData['HORARIO_ID']+"' class='btn btn-primary text-center btn-sm'><i class='fa fa-pencil-square-o'></i></button>" +		
 		"<button data-id='"+ArrayData['HORARIO_ID']+"' data-name='"+ArrayData['DESCRIPCION']+"' data-inicio='"+ArrayData['INICIA']+"' data-fin='"+ArrayData['FINAL']+"' title='Eliminar Horario' data-toggle='tooltip' onClick='EliminarHorario(event)' id='btnEliminar"+ArrayData['HORARIO_ID']+"' class='btn btn-danger botonVED text-center btn-sm'><i class='fa fa-trash-o'></i></button>"
 		]);

	var theNode = $(TableName).dataTable().fnSettings().aoData[newRow[0]].nTr;
	theNode.setAttribute('id', ArrayData['HORARIO_ID']);
	$('td', theNode)[0].setAttribute( 'class', 'hidden id' );	
}

function EditarHorario(e)
{
	e.stopPropagation();
	//console.log('Cancel ticket');
	var id = $(e.currentTarget).attr('data-id');
	var name = $(e.currentTarget).attr('data-name');
	var inicio = $(e.currentTarget).attr('data-inicio');
	var fin = $(e.currentTarget).attr('data-fin');
	Limpiar();
	$('#id_horario').val(id);
	$('#txtDesc').val(name);
	$('#txtInicio').val(inicio);
	$('#txtFin').val(fin);
	$('#theHeader').html('Editar Horario - '+ name);
	$('#btnEnviar').text('Actualizar');
	$('#modalHorario').modal('show');	
}

function EliminarHorario(e)
{
	e.stopPropagation();
	//console.log('Cancel ticket');
	var id = $(e.currentTarget).attr('data-id');
	var name = $(e.currentTarget).attr('data-name');
	var inicio = $(e.currentTarget).attr('data-inicio');
	var fin = $(e.currentTarget).attr('data-fin');
	Limpiar();
	$('#id_horario').val(id);
	$('#lblhorario').html(name);
	$('#modaltitleDelete').html('Eliminar horario - '+ name + ' | ' + inicio + ' - ' + fin);	
	$('#modalHorariosDelete').modal('show');
}

function Limpiar()
{
	inputs = ['#id_horario', '#txtDesc', '#txtInicio', '#txtFin'];
	LimpiarInput(inputs, ['']);
}