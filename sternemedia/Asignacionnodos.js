$(document).ready(function(){
var type = urljs + "index.php/Asignacionnodos/";
var typeNum = 0;
var isNodoToUser = false;
var isEditNodo = false;
var selected = [];
var arraySelected = [];
var IdNodo;
var IdUser;
$('#guardarAsignaciones').prop('disabled', true);
var asignacionesForm = $('#asignacionesForm');
CargarNodos();

var validator = asignacionesForm.validate({
	rules:{
		FechaInicial:{
			required: true,
		},
		FechaFinal:{
			required: true,
		},
		descripcion:{
			required: false,
		},
	},
	messages:{

	}
});

	$("#temporal").on("click",function(){
		if ($("#temporal").prop("checked")) {
			$("#fechas").removeClass('oculto');
		}else{
			$("#fechas").addClass('oculto');
		}
		
	});

	$(function(){
		$('#FechaInicial').datepicker({
			autoclose: true,
			language: 'es',
			startDate: new Date(),
			today:'today'
		});

		$('#FechaFinal').datepicker({
			autoclose: true,
			language: 'es',
			startDate: new Date(),
			today:'today'
		});
	});

	$('#FechaInicial').change(function(){
		$('#FechaInicial').removeClass('error');
		$('#FechaInicial-error').remove();
	});

	$('#FechaFinal').change(function(){
		$('#FechaFinal').removeClass('error');
		$('#FechaFinal-error').remove();
	});

	$('#descripcion').blur(function(){
		var descripcion = $.trim($('#descripcion').val());
		$('#descripcion').val(descripcion);
	});

  	$('#modalAsignacion').on('hidden.bs.modal', function(e) {
  		LimpiarModal();
		LimpiarTablaSimple('#TblNodoId');
		LimpiarTablaSimple('#TblUserId');
	});

	function displayOrHide(isDisplaying) {
    	if(isDisplaying) {
      		$("#sure").show();
      		$(".greatInputContainer").hide();
    	} else {
      		$(".greatInputContainer").show();
      		$("#sure").hide();
    	}
  	}

	$('#newAsignacion').click(function(){
		typeNum = 1;
		isNodoToUser = true;
		$("#tab1").prependTo("#tabs");
		table = $("#TblNodoId").dataTable();
		table2 = $("#TblUserId").dataTable();
		displayOrHide(false);
    	LimpiarTablaSimple(table2);
		$('#theHeader').html('Nueva asignación.');
		$("#nodo").prop('disabled', false);
		$("#user").prop('disabled', false);
		table.$('tr.selected').removeClass('selected');
		cargarNodosId();
		$('#guardarAsig').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$('#modalAsignacion').modal();
	});

	$('#newAsignacionUser').click(function(){
		typeNum = 1;
		isNodoToUser = false;
		$("#tab2").prependTo("#tabs");
		table = $("#TblNodoId").dataTable();
		table2 = $("#TblUserId").dataTable();
		displayOrHide(false);
    	LimpiarTablaSimple(table);
		$('#theHeader').html('Nueva asignación.');
		table2.$('tr.selected').removeClass('selected');
		cargarUsuarios();
		$('#guardarAsig').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$('#modalAsignacion').modal();
	});

	asignacionesForm.submit(function(event){
		event.preventDefault();
		var fechaini = $("#FechaInicial").val();
		var fechafin = $("#FechaFinal").val();
		if (fechaini != "" || fechafin != "") {
			var fechasvalidas = validaCamposFecha(fechaini, fechafin);
			if (fechasvalidas == false) {
				GenerarAlerta('La fecha de inicio debe ser menor que la fecha de finalización.','Asignacion', 1);
				return false;
			}
		}	
			var actividad = "";
			if (typeNum == 1) {
				actividad = "AsignacionNodoInsert";
			} else if(typeNum == 2){
				actividad = "AsignacionNodoUpdate";
			} else{
				asignacionDelete();
				return false;
			}

			if (isNodoToUser == true) {
				DataTableSelected("#TblUserId");
				if (arraySelected.length != 0) {
				var fila;
				var arreglo = [];
				for (var i = arraySelected.length - 1; i >= 0; i--) {
				 	fila = arraySelected[i];
				 	var parametros = {};
				 	parametros.usuario = fila.U_ID;
				 	parametros.nodo = IdNodo;
				 	parametros.FechaInicial = $('#FechaInicial').val();
				 	parametros.FechaFinal = $('#FechaFinal').val();
				 	parametros.descripcion = $('#descripcion').val();
				 	arreglo.push(parametros);
				}

				var path = type + actividad;
				var posting = $.post(path, {"arreglo": arreglo});
				posting.done(function (data, status, xhr) {
					var json = jQuery.parseJSON(data);
					if (json.estado == 1){
						GenerarAlerta(json.mensaje, 'Asignacion', 1);
					} else{
						if (data != false) {
							CargarNodos();
							setTimeout(function(){$("#cerrarModalAsignacion").click();});
						}
					GenerarAlerta(json.mensaje, 'Alerta', 2);
					}	
				});
				posting.fail(function (data, status, xhr) {
					console.log(data);
				// sendError(data);
				// GenerarErrorAlerta(xhr, "error");
				// goAlert();
				});
			
				posting.always(function (data, status, xhr) {
				// RemoveAnimation("body");
				//console.log(data);
				});
			// }
			} else {
			GenerarAlerta('Por favor, seleccione un elemento.', 'Asignacion', 1);
			}
		} else {
			DataTableSelected("#TblNodoId");
			if (arraySelected.length != 0) {
			var fila;
			var arreglo = [];
			for (var i = arraySelected.length - 1; i >= 0; i--) {
				 fila = arraySelected[i];
				 var parametros = {};
				 parametros.usuario = IdUser;
				 parametros.nodo = fila.IMEI;
				 parametros.FechaInicial = $('#FechaInicial').val();
				 parametros.FechaFinal = $('#FechaFinal').val();
				 parametros.descripcion = $('#descripcion').val();
				 arreglo.push(parametros);
			}

			var path = type + actividad;
			// var dataArray = JSON.stringify(arreglo);
			var posting = $.post(path, {"arreglo": arreglo});
			posting.done(function (data, status, xhr) {
				var json = jQuery.parseJSON(data);
				if (json.estado == 1){
					GenerarAlerta(json.mensaje, 'Asignacion', 1);
				} else{
					if (data != false) {
						CargarNodos();
						setTimeout(function(){$("#cerrarModalAsignacion").click();}, 1000);
					}
				GenerarAlerta(json.mensaje, 'Alerta', 2);
				}	
			});
				posting.fail(function (data, status, xhr) {
					console.log(data);
				// sendError(data);
				// GenerarErrorAlerta(xhr, "error");
				// goAlert();
				});
			
				posting.always(function (data, status, xhr) {
				// RemoveAnimation("body");
				//console.log(data);
				});
		} else {
			GenerarAlerta('Por favor, seleccione un elemento.', 'Asignacion', 1);
		}
	}			
	});

	$('#guardarAsig').click(function(){
		if (asignacionesForm.valid())
		asignacionesForm.submit();
	});

	$('#displayTables').on('click', '.cargarInfo', function(e){
		if (isNodoToUser == true) {
			lastRow = $(this).closest('tr');
			var row = lastRow.find("td");
			var imei = row.eq(0).text();
			IdNodo = imei;
			cargarUsuariosId(imei);
		}
	});

	$('#displayTableUser').on('click', '.cargarInfo', function(e){
		if (isNodoToUser == false) {
			lastRow = $(this).closest('tr');
			var row = lastRow.find("td");
			var user = row.eq(0).text();
			IdUser = user;
			cargarNodosUser(user);
		}
	});

	$('#displayTables').on( 'click', 'tr', function () {
		if (isNodoToUser == true) {
			if ( $(this).hasClass('selected') ) {
            	$(this).removeClass('selected');
        	}
        	else {
            	$('#TblNodoId').dataTable().$('tr.selected').removeClass('selected');
            	$(this).addClass('selected');
        	}
		} else {
			var id = this.id
			var index = $.inArray(id, selected);

        	if (index === -1) {
            	selected.push(id);
        	} else {
           		selected.splice(index, 1);
        	}

       		$(this).toggleClass('selected');
		}	
    });

    $("#TblUserId").dataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "autoWidth": true,
      "info": false,
      "language": languageConf,
      "dom": '<"pull-left"f><"pull-right"l>tip',
      // "dom":' <"search"f><"top"l>rt<"bottom"ip><"clear">',
      "rowCallBack": function(row, data){
    		if ($.inArray(data.DT_RowID, selected) !== -1) {
    			$(row).addClass('selected');
    		}
    	}

    });

    $("#TblNodoId").dataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "autoWidth": true,
      "info": false,
      "language": languageConf,
      "dom": '<"pull-left"f><"pull-right"l>tip',
      // "dom":' <"search"f><"top"l>rt<"bottom"ip><"clear">',
      "rowCallBack": function(row, data){
    		if ($.inArray(data.DT_RowID, selected) !== -1) {
    			$(row).addClass('selected');
    		}
    	}

    });

	$('#displayTableUser').on( 'click', 'tr', function () {
		if (isNodoToUser == false) {
			if ( $(this).hasClass('selected') ) {
            	$(this).removeClass('selected');
        	}
       	 	else {
            	$('#TblUserId').dataTable().$('tr.selected').removeClass('selected');
            	$(this).addClass('selected');
        	}
		} else {
			var id = this.id
			var index = $.inArray(id, selected);

        	if (index === -1) {
            	selected.push(id);
        	} else {
           		selected.splice(index, 1);
        	}

       		$(this).toggleClass('selected');
   		}
		
    });
 
	$('#displayTable2').on('click', '.editAsignacion', function(e){
		typeNum = 2;
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$('div').animate({
      		scrollTop: $("#modalAsignacionNodos").offset().top
  		});
		var imei = row.eq(1).text();
		$('#auto_id').val(row.eq(0).text());
		$('#nodo').select2("val", imei);
		$('#user').select2("val", row.eq(2).text());
		$('#FechaInicial').val(row.eq(4).text());
		$('#FechaFinal').val(row.eq(5).text());
		$('#descripcion').val(row.eq(6).text());
	});

	$('#displayTable').on('click', '.deleteAsignacion', function(e){
		typeNum = 3;
		displayOrHide(true);
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$('#theHeader').html('Eliminar');
		$("#pregunta").html('¿Está seguro que desea eliminar este elemento?');
		$('#guardarAsig').addClass('btn-danger').removeClass('btn-primary').html("Eliminar");
		$('#auto_id').val(row.eq(0).text());
		$('#imei').val(row.eq(1).text());
		$('#cod_us').val(row.eq(2).text());
		$("#modalAsignacion").modal();
	});

	function asignacionDelete(){
		var imei = ($("#imei").val()).split(' ')[0];
		var parametros = {};
		parametros.auto_id = $("#auto_id").val();
		parametros.imei = imei;
		parametros.cod_us = $("#cod_us").val();

		var path = type + 'AsignacionNodoDelete';
		var posting = $.post(path, parametros);
		posting.done(function (data, status, xhr) {
			var json = jQuery.parseJSON(data);
			if (data != false) {
				CargarNodos();
			}
			setTimeout(function(){$("#cerrarModalAsignacion").click();}, 0);
			GenerarAlerta(json.mensaje, 'Alerta', 2);
		});
		posting.fail(function (data, status, xhr) {
			console.log(data);
		});
		posting.always(function (data, status, xhr) {
			console.log(data);
		});
	};

	function CargarNodos() 
	{
		// /animacion de loading/
		// LoadAnimation("body");
		var path = type + "obtenerNodos";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function (data, status, xhr) {
			LimpiarTabla('#TblNodos');
			if (data != false) {
				var columns = [{ "mDataProp": "AUTO_ID", "className": "hidden id"},
                      {"mDataProp":"IMEI", "className":"text-center"},
                      {"mDataProp":"COD_US", "className":"hidden id"},
                      {"mDataProp":"USUARIO", "className":"text-center" },
                      {"mDataProp":"FECHA_INICIAL", "className":"text-center" },
                      {"mDataProp":"FECHA_FINAL", "className":"text-center" },
                      {"mDataProp":"DESCRIPCION", "className":"hidden id" },
                      {"mDataProp":"TIPO_ASIG", "className":"text-center" },
                      {
	                          //Agregamos un button que servira para editar los registros.
	                          //<span class='editUserperNod btn btn-success botonVED' data-toggle='tooltip' data-placement='left' title='Ver usuarios por Nodos'><i class='icon-edit icon-white fa fa-tags'></i></span>
	                          //<span class='editAsignaciones btn btn-primary botonVED' data-toggle='tooltip' data-placement='right' title='Ver nodos por usuarios'><i class='icon-edit icon-white fa fa-users'></i></span> 
	                          "targets": -1,
	                          "data": [{"mDataProp":""}],
	                          "defaultContent": "<span class='deleteAsignacion btn btn-danger botonVED' data-toggle='tooltip' data-placement='right' title='Eliminar'><i class='icon-edit icon-white fa fa-trash'></i></span>", "className": "text-center"
	                  }];

			LoadDataTable(data, '#TblNodos', columns);
			}
		});
		posting.fail(function (data, status, xhr) {
			console.log(data);
			// sendError(data);
			//GenerarErrorAlerta(xhr, "error");
			// goAlert();
		});
		posting.always(function (data, status, xhr) {
			// RemoveAnimation("body");
			// console.log(data);
		})
	}

	function cargarNodosporUsuario(imei){
		var direccion = type + "obtenerNodosporUsuarios";
		obj = {}
		obj.IMEI = imei;
		var posting = $.post(direccion, obj);
		posting.done(function(data,status,xhr){
			LimpiarTablaSimple('#TblNodoId');
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var columns = [{"mDataProp":"IMEI" , "className":"text-center"}];
					LoadDataTableSimple(data, '#TblNodoId', columns);
				}
			}
		});
	}

	function cargarNodosId(){
		var path = type + "obtenerNodosId";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function(data, status,xhr){
			if (data != false){
					LimpiarTablaSimple('#TblNodoId')
					var columns = [{"mDataProp":"IMEI", "className":"hidden"},
									{"mDataProp":"DESCRIPCION", "className":"text-left cargarInfo",
									"mRender": function(data, type, full){
										return full.IMEI +' - '+ full.DESCRIPCION;
									}
								}
					];
					LoadDataTableSimple(data, '#TblNodoId', columns);
			}
		});
	}

	function cargarNodosUser(user){
		var path = type + "obtenerNodosUser";
		obj = {}
		obj.user = user;
		var posting = $.post(path, obj);
		posting.done(function(data, status, xhr){
			if (data != false){
					LimpiarTablaSimple('#TblNodoId')
					var columns = [{"mDataProp":"IMEI", "className":"hidden"},
									{"mDataProp":"DESCRIPCION", "className":"text-left cargarInfo",
									"mRender": function(data, type, full){
										return full.IMEI +' - '+ full.DESCRIPCION;
									}
								}
					];
					LoadDataTableSimple(data, '#TblNodoId', columns);
			}
		});
	}

	function cargarUsuariosId(nodo){
		var path = type + "obtenerUsuariosId";
		obj = {}
		obj.nodo = nodo;
		var posting = $.post(path, obj);
		posting.done(function(data, status, xhr){
			LimpiarTablaSimple("#TblUserId");	
			if (data != false) {
				var columns = [{"mDataProp":"U_ID", "className":"hidden"},
							{"mDataProp":"NAME", "className":"text-left cargarInfo"}];
				LoadDataTableSimple(data, '#TblUserId', columns);
			}
		});
	}

	function cargarUsuarios(){
		var path = type + "obtenerUsuarios";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			LimpiarTablaSimple("#TblUserId");			
			if (data != false) {
				var columns = [{"mDataProp":"U_ID", "className":"hidden"},
							{"mDataProp":"NAME", "className":"text-left cargarInfo"}];
				LoadDataTableSimple(data, '#TblUserId', columns);
			}
		});
	}

	function LimpiarModal(){
		$('#FechaInicial').datepicker('clearDates');
		$('#FechaFinal').datepicker('clearDates');
		$('.asignacionesinput').val('');
	}

	function DataTableSelected(tableSelected){
		var table = $(tableSelected).DataTable();
		var rows = $(tableSelected).dataTable().fnSettings().fnRecordsTotal();
    	var total = table.rows('.selected').data().length;
    	arraySelected = [];
    	if (rows > 0){
    		if (total > 0){
    			table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
    				var mydata = this.data();
          			var str = this.node().className;
          			var res = str.split(" ");
          			for (var ii = res.length - 1; ii >= 0; ii--){
          				if(res[ii] == "selected"){
          					arraySelected.push(mydata);
          				}
          			}
    			})
    		}
    	}
    }

});