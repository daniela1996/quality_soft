$(document).ready(function (){
	
	var filter = 0;
	
	$('#calendar').fullCalendar({
	header: {left: 'prev, next, today', center: 'title', right: 'month,agendaWeek,agendaDay'},
	buttonText: {today: 'today',month: 'month',week: 'week',day: 'day'},
	// events: {url: urljs + 'index.php/home/getData',type: 'POST'},
	
	eventRender: function (event, element) {
		element.attr('href', 'javascript:void(0);');
		element.click(function() {
		$("#modalDepartamento").modal();
		if(event.isPo == "yes"){
			currentPO(event.start._i, event.estado);
		}
		else{
			currentSolicitud(event.start._i, event.estado);
		}
			
		});
		
		switch(filter) 
		{
    		case 1:{
				return ((event.estado == 1)&&(event.isPo == "yes"));
			}break;
    		case 2:{
				return ((event.estado == 2)&&(event.isPo == "yes"));
			}break;
			case 3:{
				return ((event.estado == 0)&&(event.isPo == "yes"));
			}break;
			case 4:{
				return ((event.estado == 1)&&(event.isPo == "no"));
			}break;
			case 5:{
				return ((event.estado == 2)&&(event.isPo == "no"));
			}break;
			case 6:{
				return ((event.estado == 0)&&(event.isPo == "no"));
			}break;
    		default:{
    			return event.estado >= 0;
    		}	
		}
	}, editable: false, droppable: false});
	
	// function currentPO(date, status)
	// {
		
	// 	var params = {};
	//     params.dataArray=[];
	//     params.dataArray[0] = {};
	//     params.dataArray[0].STATUS_PO = status;
	//     params.dataArray[0].FECHA_CREACION_PO = date;
	// 	var ordenesComprasName = JSON.stringify(params);
	// 	$.post(urljs + "index.php/ordenescompras/ordenPorFecha", {ordenesComprasName :ordenesComprasName}, 
	// 	function(data) {
			
	// 		if(status == 0)
	// 		{
	// 			$("#theHeader").html('Ordenes rechazadas');
	// 		}
			
	// 		if(status == 1)
	// 		{
	// 			$("#theHeader").html('Ordenes pendientes');
	// 		}
			
	// 		if(status == 2)
	// 		{
	// 			$("#theHeader").html('Ordenes emitidas');
	// 		}
			
	// 		$("#informacion").empty();

	// 		var displayPo = "";
			
	// 		for(i = 0; i < data.length; i++)
	// 		{
				
	// 			displayPo += '<p id="poCalendar"><b>Orden: &nbsp;&nbsp;&nbsp;</b><span id="po_span">'+data[i]["PO"]+'&nbsp;&nbsp;&nbsp;'+data[i]["FECHA_CREACION_PO"]+'</span><a target="_blank" href="ordenespendientes?po='+data[i]["PO"]+'"><span id="VerProveedor" class="btn btn-success botonVED"><i class="icon-edit icon-white fa fa-eye"></i></span></a><a target="_blank" href="reportes/po/'+data[i]["PO"]+'"><span id="displayPDF" class="btn gray botonVED"><i class="icon-edit icon-white fa fa-file-pdf-o"></i></span></a></p>';
				
	// 		}
			
	// 		$("#informacion").append(displayPo);
			
	// 	});
						    
	// }

	// function currentSolicitud(date, status)
	// {
		
	// 	var params = {};
	//     params.dataArray=[];
	//     params.dataArray[0] = {};
	//     params.dataArray[0].STATUS = status;
	//     params.dataArray[0].FECHA_SOLICITUD = date;
	// 	var solicitudName = JSON.stringify(params);
	// 	$.post(urljs + "index.php/solicitudes/ordenPorFecha", {solicitudName :solicitudName}, 
	// 	function(data) {
			
	// 		if(status == 0)
	// 		{
	// 			$("#theHeader").html('Ordenes rechazadas');
	// 		}
			
	// 		if(status == 1)
	// 		{
	// 			$("#theHeader").html('Ordenes pendientes');
	// 		}
			
	// 		if(status == 2)
	// 		{
	// 			$("#theHeader").html('Ordenes emitidas');
	// 		}
			
	// 		$("#informacion").empty();

	// 		var displayPo = "";
			
	// 		for(i = 0; i < data.length; i++)
	// 		{
				
	// 			displayPo += '<p id="poCalendar"><b>Solicitado por: &nbsp;&nbsp;&nbsp;</b><span id="po_span">'+data[i]["SOLICITA"]+'&nbsp;&nbsp;&nbsp;'+data[i]["FECHA_SOLICITUD"]+'</span><a target="_blank" href="solicitudes?s='+data[i]["ID_SOLICITUD"]+'"><span id="VerProveedor" class="btn btn-success botonVED"><i class="icon-edit icon-white fa fa-eye"></i></span></a></p>';
				
	// 		}
			
	// 		$("#informacion").append(displayPo);
			
	// 	});
						    
	// }

	// $("#pendientePO").click(function(){
	// 	filter = 1;
	//     $('#calendar').fullCalendar('rerenderEvents');
	// });
	// $("#aceptadaPO").click(function(){
	// 	filter = 2;
	//     $('#calendar').fullCalendar('rerenderEvents');
	// });
	// $("#rechazadaPO").click(function(){
	// 	filter = 3;
	//     $('#calendar').fullCalendar('rerenderEvents');
	// });
	// $("#pendienteS").click(function(){
	// 	filter = 4;
	//     $('#calendar').fullCalendar('rerenderEvents');
	// });
	// $("#aceptadaS").click(function(){
	// 	filter = 5;
	//     $('#calendar').fullCalendar('rerenderEvents');
	// });
	// $("#rechazadaS").click(function(){
	// 	filter = 6;
	//     $('#calendar').fullCalendar('rerenderEvents');
	// });
	// $("#default").click(function(){
	// 	filter = 0;
	//     $('#calendar').fullCalendar('rerenderEvents');
	// });
	    
});