$(document).ready(function() {
	var type = urljs+"index.php/perfilesusuarios/";
	var isSelected = Array();
	var orientation = false;
	var perfilOUsuario = 0;
	var lastRow = null;
	// cargarPerfiles();

	$("#perfil_perus").change(function() {
		perfilOUsuario = 1;
		if((!isSelected[$(this).val()])&&($(this).val() != 0)) {
			isSelected[$(this).val()] = true;
			var eachProfile = '<div class="box with-border box-header boxProfile"><input type="hidden" class="losDatos" value="'+$(this).val()+'"><h3 class="box-title">'+$("#perfil_perus option[value='"+$(this).val()+"']").text()+'</h3><div class="box-tools pull-right"><button class="btn btn-box-tool thisButton"><i class="fa fa-remove"></i></button></div></div>';
			if(orientation) {
				$("#addRight").append(eachProfile);
				orientation = false;
			} else {
				$("#addLeft").append(eachProfile);
				orientation = true;
			}
		}
	});
	$("#user_perus").change(function() {
		perfilOUsuario = 2;
		if((!isSelected[$(this).val()])&&($(this).val() != 0)) {
			isSelected[$(this).val()] = true;
			var eachModule = '<div class="box with-border box-header boxProfile"><input type="hidden" class="losDatos" value="'+$(this).val()+'"><h3 class="box-title">'+$("#user_perus option[value='"+$(this).val()+"']").text()+'</h3><div class="box-tools pull-right"><button class="btn btn-box-tool thisButton"><i class="fa fa-remove"></i></button></div></div>';
			if(orientation) {
				$("#addRight").append(eachModule);
				orientation = false;
			} else {
				$("#addLeft").append(eachModule);
				orientation = true;
			}
		}
	});
	$('#modalPerfilesUsuarios').on('click', '.thisButton', function() {
		$(this).closest('.boxProfile').remove();
		isSelected[$(this).closest('.boxProfile').find(".losDatos").val()] = false;
		orientation = !orientation;
	});
	$('#anclaTabla').on('click', '.asociacion', function() {
		lastRow = $(this).closest('tr');
		AproveDenied($(this).parent().parent().find("td").eq(0).text(), "A");
	});
	$('#anclaTabla').on('click', '.desligar', function() {
		lastRow = $(this).closest('tr');
		AproveDenied($(this).parent().parent().find("td").eq(0).text(), "D");
	});
	function AproveDenied(id, status) {
		var params = {};
				params.PROF_MODULEID = id;
				params.ESTADO = status;
		var perfilusuarioName = JSON.stringify(params);
		$.post(type+"perfilusuarioAcceptDecline", {perfilusuarioName :perfilusuarioName}, function(data) {
			GenerarAlerta(data, 'Alerta', 2);
			var statusColumn = lastRow.find("td").eq(5);
			statusColumn.empty();
			if (status === "A") {
				statusColumn.html("<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>");
			} else {
				statusColumn.html("<span class='asociacion btn btn-success botonVED'>Habilitar</span>");
			}
		});
	}
	$("#asociarPerfilUsuario").click(function() {
		limpiarContenedorPefilesUsuarios();
		$("#modalPerfilesUsuarios").modal();
	});
	$("#enviarPerus").click(function() {
		if(isSelected.length){
		var i = 0;
		var parametros = {};
				parametros.dataArray = [];
				parametros.PROCEDENCIA = perfilOUsuario;
				parametros.LLAVE = $("#profileOrUser").html();
		$(".losDatos").each(function(indice) {
			parametros.dataArray[i] = {};
			parametros.dataArray[i].PROFILES_IDPROFILE = (perfilOUsuario == 1) ? this.value: $("#profileOrUser").html();
			parametros.dataArray[i].USERS_IDUSERS = (perfilOUsuario == 1) ? $("#profileOrUser").html(): this.value;
			i++;
		});
		if(parametros.dataArray.length != 0) {
			var perfilusuarioName = JSON.stringify(parametros);
			$.post(type+"perfilusuarioInsert", {perfilusuarioName :perfilusuarioName}, function(data) {
				GenerarAlerta(data[0], 'Alerta', 2);
				setTimeout(function(){$("#cerrarPerus").click();});
				var rowsAgregados = data[1];
				var formaArreglo = [];
				for (var i = 0; i < rowsAgregados.length; i++) {
					if (perfilOUsuario == 1) {
						formaArreglo = [rowsAgregados[i]['USERPROFILEID'], rowsAgregados[i]['USERID'], rowsAgregados[i]['USERNAME'], rowsAgregados[i]['PROFILEID'], rowsAgregados[i]['NAMEPROFILE'], "<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>"];
						var valor = parseInt(rowsAgregados[i]['PROFILEID']);
						$("#perfil_perus option[value=" +valor+ "]" ).prop('disabled', true);
					} else {
						formaArreglo = [rowsAgregados[i]['USERPROFILEID'], rowsAgregados[i]['PROFILEID'], rowsAgregados[i]['NAMEPROFILE'], rowsAgregados[i]['USERID'], rowsAgregados[i]['USERNAME'], "<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>"];
						var valor = parseInt(rowsAgregados[i]['USERID']);
						$("#user_perus option[value=" +valor+ "]" ).prop('disabled', true);
					}
					addOnTable(formaArreglo, [0,1,2,3]);
				}
				if (perfilOUsuario == 1) {
					var perfilOptions = $("#perfil_perus").find('option');
					$("#perfil_perus").select2("destroy");
					$("#perfil_perus").empty();
					$("#perfil_perus").append(perfilOptions);
					$("#perfil_perus").select2();
				} else {
					var perfilOptions = $("#user_perus").find('option');
					$("#user_perus").select2("destroy");
					$("#user_perus").empty();
					$("#user_perus").append(perfilOptions);
					$("#user_perus").select2();
				}
			});
		}
		} else{
			GenerarAlerta('Por favor, seleccione un elemento.', 'PerfilesUsuarios', 1);	
		}
	});

	function limpiarContenedorPefilesUsuarios(){
		$('#addLeft').empty();
		$('#addRight').empty();
		if ($('#usuario_container').css('display')) {
			$('#user_perus').select2('val','0');
		} else {
			$('#perfil_perus').select2('val','0');
		}
		for (var i = 0; i < isSelected.length; i++) {
			isSelected[i] = false;
		}
	}
});
