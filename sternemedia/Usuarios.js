$(document).ready(function (){
  var type = urljs+"index.php/usuarios/";
  var typeNum = 0;
  var imagecode64 = "";
  var hiddenUploadImage = null;
  var isSelected = Array();
	var orientation = false;
  var cropper = null;
  var lastRow = null;
  var usuariosForm = $("#usuariosForm");
  $('#celular_usuario').inputmask("+50499999999");

  usuariosForm.validate({
    rules:{
      nombre_usuario:{
        required: true,
        Letrascontildes: true
      },
      celular_usuario:{
        required: true
      },
      email_usuario:{
        required: true
      },
      nick_usuario:{
        required: true,
        maxlength:20,
        codigos: true
      },
      clave_usuario:{
        required: true,
        minlength: 8,
        password: true
      }
    },
    messages:{
      nombre_usuario:{
        letterswithbasicpunc: "Por favor, ingrese solo letras."
      },
      nick_usuario:{
        required: "Por favor, ingrese un valor.",
        nowhitespace: "Por favor, no escriba espacios.",
        lettersonly: "Por favor, ingrese solo letras."

      },
      clave_usuario:{
        password:"La contraseña debe tener al menos una letra mayúscula y un número."
      }
    }
  });

  $("#nombre_usuario").blur(function(){
    var nombre_usuario = $.trim($("#nombre_usuario").val());
    $("#nombre_usuario").val(nombre_usuario);
  });

  function setTheCropper() {
    var laImagen = document.getElementById('imagenSelecta');
    var options = {
      aspectRatio: 1 / 1,
      autoCropArea: 1,
      ready: function (e) {
        console.log(e.type);
      },
      cropstart: function (e) {
        console.log(e.type, e.detail.action);
      },
      cropmove: function (e) {
        console.log(e.type, e.detail.action);
      },
      cropend: function (e) {
        console.log(e.type, e.detail.action);
      },
      crop: function (e) {
        console.log(e.type);
      },
      zoom: function (e) {
        console.log(e.type, e.detail.ratio);
      }
    };
    cropper = new Cropper(laImagen, options);
    var laImagen = document.getElementById('imagenSelecta');
  }
  function displayOrHide(isDisplaying) {
    if(isDisplaying) {
      $(".displayBasicInfo").show();
      $(".greatInputContainer").hide();
      $("#enviarUsuario").hide();
      $("#imagendelusuario").hide();
    } else {
      $(".greatInputContainer").show();
      $(".displayBasicInfo").hide();
      $("#enviarUsuario").show();
      $("#imagendelusuario").show();
    }
  }
  $('#modalUsuarios').on('hidden.bs.modal', function(e) {
		$(".usersinputs").val("");
    $('.usersinputs').removeClass('error');
    $('.error').remove();    
    $("#addLeft").empty();
    $("#addRight").empty();
    $("#imagenSelecta").next().remove();
    isSelected = Array();
    orientation = false;
    if (cropper !== null) {
      cropper.destroy();
    }
	});
  $("#imagendelusuario").on('click', function() {
		var unaVez = false;
		if (hiddenUploadImage == null) {
			var container = document.createElement('input');
			container.setAttribute("type", "file");
			container.setAttribute("name", "viewerimageuploader");
			container.setAttribute("id", "viewerimageuploader");
			container.setAttribute("accept", "image/*");
			document.body.appendChild(container);
			hiddenUploadImage = container;
			unaVez = true;
		}
		var subirImagenes = function(evt) {
			var files = evt.target.files[0];
      var tamaño = evt.target.files[0].size;
      console.log(tamaño);
			var reader = new FileReader();
			reader.onload = (function(theFile) {
				return function(e) {
          var laImagen = document.getElementById('imagenSelecta');
              laImagen.nextSibling.remove();
              laImagen.setAttribute("src", e.target.result);
          if (cropper !== null) {
            cropper.destroy();
          }
          setTheCropper();
				};
			})(files);
			reader.readAsDataURL(files);
		};
		if (unaVez) {
			document.getElementById('viewerimageuploader').addEventListener('change', subirImagenes, false);
		}
		$('#viewerimageuploader').click();
	});
  $('#displayTable').on('click', '.editUsuario', function (e) {
    $("#theHeader").html('Editar Usuario');
      displayOrHide(false);
    typeNum = 1;
    lastRow = $(this).closest('tr');
    var params = {};
    params.IDUSUARIO = lastRow.find("td").eq(0).text();
      
    var usuarioName = JSON.stringify(params);
    $.post(type+"userParticularGet", {usuarioName :usuarioName}, function(data) {
      $("#id_usuario").val(data['U_ID']);
      $("#nombre_usuario").val(data['NAME']);
      $("#celular_usuario").val(data['PHONE']);
      $("#email_usuario").val(data['EMAIL']);
      $("#nick_usuario").val(data['NICKNAME']);
      if (data['PHOTO']) {
        $("#imagenSelecta").next().remove();
        $("#imagenSelecta").attr("src", "data:image/jpeg;base64,"+data["PHOTO"]);
        setTimeout(function(){ setTheCropper(); }, 2000);
      } else {
        $("#imagenSelecta").attr("src", "https://dummyimage.com/1000x1000/a1acb2/000000.jpg&text=Nueva+imagen+de+perfil");
      }
    });
    $('.nav-tabs a[href="#infoprincipalMenu"]').tab('show');
    $('#enviarUsuario').html("Editar");
    $("#modalUsuarios").modal();
  });
  $('#displayTable').on('click', '.cargarInfo', function (e) {
    $("#theHeader").html('Información Usuario');
      displayOrHide(true);
    lastRow = $(this).closest('tr');
    var params = {};
        params.IDUSUARIO = lastRow.find("td").eq(0).text();
    var usuarioName = JSON.stringify(params);
    $.post(type+"userParticularGet", {usuarioName :usuarioName}, function(data) {
      $("#usuarioInfo").empty();
      $("#imagenSelecta").next().remove();
      $("#usuarioInfo").prepend($("<h2>"+data['NAME']+"</h2>"));
      $("#usuarioInfo").append($("<p><b>Nombre:</b><span>"+data['NAME']+"</span></p>"));
      $("#usuarioInfo").append($("<p><b>Correo:</b><span>"+data['EMAIL']+"</span></p>"));
      $("#usuarioInfo").append($("<p><b>Celular:</b><span>"+data['PHONE']+"</span></p>"));
      $("#usuarioInfo").append($("<p><b>Estado:</b><span>"+((parseInt(data['ESTADO']) == 1) ? "Activo": "Inactivo")+"</span></p>"));
      $("#usuarioInfo").append($("<p><b>Fecha Ingreso:</b><span>"+data['CREATED_AT']+"</span></p>"));
      $("#usuarioInfo").append($("<p><b>Fecha Actualización:</b><span>"+data['UPDATED_AT']+"</span></p>"));
      $("#usuarioInfo").append($('<div class="clear"></div>'));
      if (data['PHOTO']) {
        $("#imagenSelecta").next().remove();
        $("#imagenSelecta").attr("src", "data:image/jpeg;base64,"+data["PHOTO"]);
      } else {
        $("#imagenSelecta").attr("src", "https://dummyimage.com/600x400/bab6ba/000000&text=No+hay+imagen.");
      }
      $("#usuariocuadInfo").empty();
      
    });
    $('.nav-tabs a[href="#infoprincipalMenu"]').tab('show');
    $("#modalUsuarios").modal();
  });
  $("#newUsuario").click(function() {
    typeNum = 2;
    $("#theHeader").html("Nuevo Usuario");
    displayOrHide(false);
    $("#imagenSelecta").attr("src", "https://dummyimage.com/1000x1000/a1acb2/000000.jpg&text=Nueva+imagen+de+perfil");
    $('#enviarUsuario').html("Agregar");
    $("#modalUsuarios").modal();
  });
	$('#modalUsuarios').on('click', '.thisButton', function() {
		$(this).closest('.boxProfile').remove();
		isSelected[$(this).closest('.boxProfile').find(".losDatos").val()] = false;
		orientation = !orientation;
	});
  $("#enviarUsuario").click(function() {
    if (usuariosForm.valid())
      (usuariosForm.submit());
  });

  usuariosForm.submit(function(event){
    event.preventDefault();
    var params = {};
    params.ID_USUARIO = $("#id_usuario").val();
    params.NOMBRE_USUARIO = $("#nombre_usuario").val();
    params.CELULAR_USUARIO = $("#celular_usuario").val();
    params.EMAIL_USUARIO = $("#email_usuario").val();
    params.NICKNAME_USUARIO = $("#nick_usuario").val();
    params.CLAVE_USUARIO = $("#clave_usuario").val();
    var elCropper = (cropper == null) ? false : cropper["getCroppedCanvas"]().toDataURL();
        elCropper = (cropper) ? elCropper.replace("data:image/jpeg;base64,", "").replace("data:image/jpg;base64,", "").replace("data:image/png;base64,", ""): false;
    params.FOTO_USUARIO = ($("#imagenSelecta").attr('alterada') === "false") ? false : elCropper;
    var usuarioName = JSON.stringify(params);
    actividadDir = (typeNum == 1) ? "usuariosUpdate" : "usuariosInsert";
    $.post(type+actividadDir, {usuarioName :usuarioName}, function(data) {
      if (data[2] == 1) {
        ArmarNuevo(data[1]);
      } else {
        var allColumns = lastRow.find("td");
        allColumns.eq(0).html(data[1]['U_ID']);
        allColumns.eq(1).html(data[1]['NAME']);
        allColumns.eq(2).html(data[1]['PHONE']);
        allColumns.eq(3).html(data[1]['EMAIL']);
      }
      if (data[0].estado == 1){
      GenerarAlerta(data[0].mensaje, 'Usuarios', 1);
      } else{
      GenerarAlerta(data[0].mensaje, 'Alerta', 2);
      setTimeout(function(){$("#cerrarUsuario").click();});
      }
      // alertify.set('notifier','position', 'top-right');
      // alertify.notify(data[0], 'alertaPositiva', 3);
      // setTimeout(function(){$("#cerrarUsuario").click();}, 1000);
    });
  });

  function ArmarNuevo(rowsAgregados) {
    var formaArreglo = [];
    var usuarioid = 0;
    var acciones = "";
    for (var i = 0; i < rowsAgregados.length; i++) {
      usuarioid = rowsAgregados[i]['U_ID'];
      acciones = `<a href="rolesuser?ru=2&vl=${usuarioid}">
        <span id='linkRoles' class='btn bg-orange botonVED'>
          <i class='icon-edit icon-white fa fa-arrow-circle-o-right'></i>
        </span>
      </a>
      <a href="perfilesusuarios?pu=2&vl=${usuarioid}">
        <span id='linkPerfil' class='btn purple botonVED'>
          <i class='icon-edit icon-white fa fa-user'></i>
        </span>
      </a>
      <span class=' btn btn-primary botonVED editUsuario'>
        <i class='icon-edit icon-white fa fa-pencil'></i>
      </span>`;
      formaArreglo = [usuarioid, rowsAgregados[i]['NAME'], rowsAgregados[i]['PHONE'], rowsAgregados[i]['EMAIL'], acciones];
      addOnTable(formaArreglo, [0]);
    }
  }
});
