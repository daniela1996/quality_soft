$(document).ready(function (){
	var type = urljs+"index.php/rolesuser/";
	var isSelected = Array();
	var orientation = false;
	var rolOUsuario = 0;
	var lastRow = null;
	$("#rol_rolus").change(function() {
		rolOUsuario = 1;
		if((!isSelected[$(this).val()])&&($(this).val() != 0)) {
			isSelected[$(this).val()] = true;
			var eachRol = '<div class="box with-border box-header boxProfile"><input type="hidden" class="losDatos" value="'+$(this).val()+'"><h3 class="box-title">'+$("#rol_rolus option[value='"+$(this).val()+"']").text()+'</h3><div class="box-tools pull-right"><button class="btn btn-box-tool thisButton"><i class="fa fa-remove"></i></button></div></div>';
			if(orientation) {
				$("#addRight").append(eachRol);
				orientation = false;
			} else {
				$("#addLeft").append(eachRol);
				orientation = true;
			}
		}
	});
	$("#usuario_rolus").change(function() {
		rolOUsuario = 2;
		if((!isSelected[$(this).val()])&&($(this).val() != 0)) {
			isSelected[$(this).val()] = true;
			var eachUser = '<div class="box with-border box-header boxProfile"><input type="hidden" class="losDatos" value="'+$(this).val()+'"><h3 class="box-title">'+$("#usuario_rolus option[value='"+$(this).val()+"']").text()+'</h3><div class="box-tools pull-right"><button class="btn btn-box-tool thisButton"><i class="fa fa-remove"></i></button></div></div>';
			if(orientation) {
				$("#addRight").append(eachUser);
				orientation = false;
			} else {
				$("#addLeft").append(eachUser);
				orientation = true;
			}
		}
	});
	$('#modalRolesUsusarios').on('click', '.thisButton', function() {
		$(this).closest('.boxProfile').remove();
		isSelected[$(this).closest('.boxProfile').find(".losDatos").val()] = false;
		orientation = !orientation;
	});
	$('#anclaTabla').on('click', '.asociacion', function() {
		lastRow = $(this).closest('tr');
		AproveDenied($(this).parent().parent().find("td").eq(0).text(), "A");
	});
	$('#anclaTabla').on('click', '.desligar', function() {
		lastRow = $(this).closest('tr');
		AproveDenied($(this).parent().parent().find("td").eq(0).text(), "D");
	});
	function AproveDenied(id, status) {
		var params = {};
				params.ROLE_USERID = id;
				params.ESTADO = status;
		var rolesusuariosName = JSON.stringify(params);
		$.post(type+"roleusuarioAcceptDecline", {rolesusuariosName :rolesusuariosName}, function(data) {
			GenerarAlerta(data, 'Alerta', 2);
			var statusColumn = lastRow.find("td").eq(5);
			statusColumn.empty();
			if (status === "A") {
				statusColumn.html("<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>");
			} else {
				statusColumn.html("<span class='asociacion btn btn-success botonVED'>Habilitar</span>");
			}
		});
	}
	$("#asociarRoleUser").click(function() {
		$("#theHeader").html('Asociar roles a usuarios')
		LimpiarRolesPorUsuario();
		$("#modalRolesUsusarios").modal();
	});
	$("#enviarRolus").click(function() {
		if (isSelected.length) {
		var i = 0;
		var parametros = {};
				parametros.dataArray = [];
				parametros.PROCEDENCIA = rolOUsuario;
				parametros.LLAVE = $("#roleOrUser").html();
		$(".losDatos").each(function(indice) {
			parametros.dataArray[i] = {};
			parametros.dataArray[i].ROLES_IDROLES = (rolOUsuario == 1) ? this.value: $("#roleOrUser").html();
			parametros.dataArray[i].USERS_IDUSERS = (rolOUsuario == 1) ? $("#roleOrUser").html(): this.value;
			i++;
		});
		if(parametros.dataArray.length != 0) {
			var rolesusuariosName = JSON.stringify(parametros);
			$.post(type+"roleusuarioInsert", {rolesusuariosName :rolesusuariosName}, function(data) {
			GenerarAlerta(data[0], 'Alerta', 2);
			setTimeout(function() { $("#cerrarRolus").click(); });
				var rowsAgregados = data[1];
				var formaArreglo = [];
				for (var i = 0; i < rowsAgregados.length; i++) {
					if (rolOUsuario == 1) {
						formaArreglo = [rowsAgregados[i]['ROLEUSERID'], rowsAgregados[i]['USERID'], rowsAgregados[i]['NAME'], rowsAgregados[i]['ROLEID'], rowsAgregados[i]['NAMEROLE'], "<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>"];
						var valor = parseInt(rowsAgregados[i]['ROLEID']);
						$("#rol_rolus option[value=" +valor+ "]" ).prop('disabled', true);
					} else {
						formaArreglo = [rowsAgregados[i]['ROLEUSERID'], rowsAgregados[i]['ROLEID'], rowsAgregados[i]['NAMEROLE'], rowsAgregados[i]['USERID'], rowsAgregados[i]['NAME'], "<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>"];
						var valor = parseInt(rowsAgregados[i]['USERID']);
						$("#usuario_rolus option[value=" +valor+ "]" ).prop('disabled', true);
					}
					addOnTable(formaArreglo, [0,1,2,3]);
				}

				if (rolOUsuario == 1) {
					var perfilOptions = $("#rol_rolus").find('option');
					$("#rol_rolus").select2("destroy");
					$("#rol_rolus").empty();
					$("#rol_rolus").append(perfilOptions);
					$("#rol_rolus").select2();
				} else {
					var perfilOptions = $("#usuario_rolus").find('option');
					$("#usuario_rolus").select2("destroy");
					$("#usuario_rolus").empty();
					$("#usuario_rolus").append(perfilOptions);
					$("#usuario_rolus").select2();
				}
			});
		} 
		
		}else{
			GenerarAlerta('Por favor, seleccione un elemento.', 'RolesUsuarios', 1);
		}
	});

	function LimpiarRolesPorUsuario(){
		$('#addLeft').empty();
		$('#addRight').empty();
		if ($('#usuario_rolus').css('display')) {
			$('#usuario_rolus').select2('val','0');
		} else {
			$('#rol_rolus').select2('val','0');
		}
		for (var i = 0; i < isSelected.length; i++) {
			isSelected[i] = false;
		}
	}
});
