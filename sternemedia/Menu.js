$(document).ready(function (){
	var type = urljs+"index.php/menus/";
	var typeNum = 0;
	var lastRow = null;
	var showmenu = 0;
	$('#padre_menu').select2();
	$('#modulo_menu').select2();
	var formMenu = $("#menuForm");


	formMenu.validate({
		rules:{
			titulo_menu: {
				required: true,
				maxlength: 100,
				Letrascontildes: true
			},
			url_menu: {
				required:true,
				maxlength: 25,
                url: true
			},
			padre_menu: {
				required: false
			},
			modulo_menu: {
				required:true
			},
			icono_menu: {
				required: false
			}
		},
		messages:{
			titulo_menu: {
				required:"Por favor, ingrese un valor."
			},
			url_menu:{
				required:"Por favor, ingrese un valor.",
			},
			padre_menu:{
			},
			modulo_menu:{
				required: "Por favor, seleccione una opción."
			},
			icono_menu:{
			}

		},
		errorPlacement: function(error, element) {
			//si el elemento es un select
			if ($(element).is("select")) {
				// colocar el error al final del contenedor
				$(error).appendTo($(element).parent().parent());
				// resaltar el borde del contenedor del select
				$(element).parent().css("border", "1px solid #B60E16");
			}
			else {
				// colocar el error después del elemento
				$(error).insertAfter(element);
			}
		}
	});

	$("#titulo_menu").blur(function(){
		var titulo_menu = $.trim($("#titulo_menu").val());
		$("#titulo_menu").val(titulo_menu);
	});

	$("#modulo_menu").change(function(){
		$('#div_modulo_menu').css('border', '');
		$('#modulo_menu-error').remove();
	});


	function displayOrHide(isDisplaying) {
    	if(isDisplaying) {
      		$("#sure").show();
      		$(".greatInputContainer").hide();
    	} else {
      		$(".greatInputContainer").show();
      		$("#sure").hide();
    	}
  	}
	$('#modalMenu').on('hidden.bs.modal', function(e) {
		$('.menuinputs').removeClass('error');
		$('.error').remove();
      	$('#div_modulo_menu').css('border', '');
      	$("#padre_menu").select2("val","0");
  		$("#modulo_menu").select2("val","");
		$(".menuinputs").val("");
		$('div').css('border', '');		
	});
	$('#displayTable').on('click', '.editMenu', function (e) {
		typeNum = 1;
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
    	$("#theHeader").html('Editar Menú Dinámico');
    	displayOrHide(false);
      $("#id_menu").val(row.eq(0).text());
      $("#titulo_menu").val(row.eq(1).text());
      $("#url_menu").val(row.eq(3).text());
      $("#padre_menu").select2('val',row.eq(5).text());
      $("#modulo_menu").select2('val',row.eq(6).text());
      $("#icono_menu").val(row.eq(7).text());
    $('#enviarMenu').addClass('btn-primary').removeClass('btn-danger').html("Editar");
    $("#modalMenu").modal();
	});
	$("#newMenu").click(function() {
		typeNum = 2;
		$("#theHeader").html('Nuevo Menú Dinámico');
		displayOrHide(false);
		$('#enviarMenu').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$("#modalMenu").modal();
	});
	$('#displayTable').on('click', '.deleteMenu', function (e) {
		typeNum = 3;
		showmenu = 0;
		$("#theHeader").html('Desactivar Menú Dinámico');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#pregunta").html('¿Está seguro que desea desactivar el menú: '+(row.eq(1).text())+'?');
		$("#id_menu").val(row.eq(0).text());
		displayOrHide(true);
		$('#enviarMenu').addClass('btn-danger').removeClass('btn-primary').html("Desactivar");
		$("#modalMenu").modal();
	});
		$('#displayTable').on('click', '.activarMenu', function (e) {
		typeNum = 3;
		showmenu = 1;
		$("#theHeader").html('Activar menú dinámico');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#pregunta").html('¿Esta seguro que desea activar el Menu: '+(row.eq(1).text())+'?');
		$("#id_menu").val(row.eq(0).text());
		displayOrHide(true);
		$('#enviarMenu').addClass('btn-primary').removeClass('btn-danger').html("Activar");
		$("#modalMenu").modal();
	});

	$("#enviarMenu").click(function() {
		if(formMenu.valid())
			formMenu.submit();
	});

	formMenu.submit(function(event){
		event.preventDefault();

		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "menusUpdate";
		} else if (typeNum == 2) {
			actividadDir = "menusInsert";
		} else {
			actividadDir = "menusDelete";
		}
		var params = {};
				params.ID = $("#id_menu").val();
				params.TITLE = $("#titulo_menu").val();
				params.URL = $("#url_menu").val();
				params.PARENT_ID = $("#padre_menu").val();
				params.MODULEID = $("#modulo_menu").val();
				params.ICON = $("#icono_menu").val();
				params.SHOW_MENU = showmenu;
		var menuName = JSON.stringify(params);
		$.post(type+actividadDir, {menuName :menuName}, function(data) {
			switch (data[2]) {
				case 1: { ArmarNuevo(data[1]); } break;
				case 2: {
					var allColumns = lastRow.find("td");
					allColumns.eq(0).html(data[1]['ID']);
					allColumns.eq(1).html(data[1]['TITLE']);
					allColumns.eq(2).html(data[1]['NAMEMODULE']);
					allColumns.eq(3).html(data[1]['URL']);
					allColumns.eq(5).html(data[1]['PARENT_ID']);
					allColumns.eq(6).html(data[1]['MODULEID']);
					allColumns.eq(7).html(data[1]['ICON']);
				} break;
				case 3: { 
					var allColumns = lastRow.find("td");
					if (data[1].SHOW_MENU == 0) {
						allColumns.eq(8).html("<span class='editMenu btn btn-success botonVED' data-toggle='tooltip' data-placement='right' title='Editar menú'><i class='icon-edit icon-white fa fa-pencil'></i></span><span class='activarMenu btn btn-info botonVED' data-toggle='tooltip' data-placement='top' title='Activar'><i class='icon-edit icon-white fa fa-check-square-o'></i></span>");
					} else {
						allColumns.eq(8).html("<span class='editMenu btn btn-success botonVED' data-toggle='tooltip' data-placement='right' title='Editar menú'><i class='icon-edit icon-white fa fa-pencil'></i></span><span class='deleteMenu btn btn-danger botonVED' data-toggle='tooltip' data-placement='top' title='Desactivar'><i class='icon-edit icon-white fa fa-minus-square-o'></i></span>");
					}
				 } break;
				default: { }
			}
			if (data[0].estado == 1){
				GenerarAlerta(data[0].mensaje, 'Menu', 1);
			} else if(data[0].estado == 2){
				GenerarAlerta(data[0].mensaje, 'Alerta', 2);
				setTimeout(function(){$("#cerrarMenu").click();});
			}else{
				GenerarAlerta(data[0].mensaje, 'Alerta', 2);
				setTimeout(function(){$("#cerrarMenu").click();});
			}
		});

	});

	function ArmarNuevo(rowsAgregados) {
		var formaArreglo = [];
		var acciones = "";
		for (var i = 0; i < rowsAgregados.length; i++) {
			acciones = `<span class='editMenu btn btn-success botonVED'>
				<i class='icon-edit icon-white fa fa-pencil'></i>
			</span>
			<span class='deleteMenu btn btn-danger botonVED'>
				<i class='icon-edit icon-white fa fa-minus-square-o'></i>
			</span>`;
			formaArreglo = [rowsAgregados[i]['ID'], rowsAgregados[i]['TITLE'], rowsAgregados[i]['NAMEMODULE'], rowsAgregados[i]['URL'], rowsAgregados[i]['DYN_GROUP_ID'], rowsAgregados[i]['PARENT_ID'],rowsAgregados[i]['MODULEID'],rowsAgregados[i]['ICON'], acciones];
			addOnTable(formaArreglo, [0,4,5,6,7]);
		}
	}
});
