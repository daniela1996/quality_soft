$(document).ready( function(){
	var type = urljs+"index.php/Vehiculos/";
	var typeNum = 0;
	var lastRow = null;
	CargarVehiculos();

	$("#vehiculosForm").validate({
		rules: {
			compania: {
				required: true,
				maxlength: 50
			},
			marca: {
				required: true,
				maxlength: 30
			},
			modelo: {
				required: true,
				maxlength: 30
			},
			chasis:{
				required: true,
				minlength: 17,
				maxlength: 17,
				alphanumeric: true
			},
			year: {
				required: true,
				number:true,
				maxlength:5
			},
			color:{
				required: true,
				maxlength:20
			},
			motor:{
				required: true,
				maxlength: 40,
				minlength:10,
				alphanumeric: true
			},
			plate: {
				required: true,
				maxlength:10
			},
			imei: {
				required: false,
				maxlength: 10
			}

		},
		messages: {
			compania: {
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			marca: {
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			modelo: {
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			chasis: {
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres.",
				alphanumeric: "Por favor ingrese solo numeros y letras."
			},
			year: {
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			color: {
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			motor: {
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres.",
				alphanumeric: "Por favor ingrese solo numeros y letras."

			},
			plate:{
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."
			},
			imei: {
				required: "Por favor ingrese un valor.",
				maxlength: "Se ha excedido el límite de caracteres."

			}
		}	
	});

	$('#marca').blur(function(){
		var marca = $.trim($('#marca').val());
		$('#marca').val(marca);
	});

	$('#modelo').blur(function(){
		var modelo = $.trim($('#modelo').val());
		$('#modelo').val(modelo);
	});

	$('#color').blur(function(){
		var color = $.trim($('#color').val());
		$('#color').val(color);
	});

	$('#plate').blur(function(){
		var plate = $.trim($('#plate').val());
		$('#plate').val(plate);
	});

	$('#compania').blur(function(){
		var compania = $.trim($('#compania').val());
		$('#compania').val(compania);
	});

	function displayOrHide(isDisplaying) {
    	if(isDisplaying) {
      	$("#InformacionVehiculo").show();
      	$(".greatInputContainer").hide();
    	} else {
      	$(".greatInputContainer").show();
      	$("#InformacionVehiculo").hide();
    	}
  	}

  	$('#modalvehiculos').on('hidden.bs.modal', function(e) {
		$(".vehiculosinputs").val("");
		$(".vehiculosinputs").removeClass('error');
		$(".error").remove();
		$("#imei").prop('disabled', true);
	});

	$("#newVehiculo").click(function() {
		typeNum = 2;
		$("#theHeader").html('Nuevo Vehículo');
		displayOrHide(false);
		$("#imei").prop('disabled', true);
		$('#enviarVehiculo').addClass('btn-primary').removeClass('btn-danger').html("Guardar");
		$("#modalvehiculos").modal();
	});

	$("#enviarVehiculo").click(function() {
		var vehiculosForm = $("#vehiculosForm");

		if (vehiculosForm.valid())
			vehiculosForm.submit();
	});

	$("#vehiculosForm").submit(function(event) {
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "vehiculosUpdate";
		} else if (typeNum == 2) {
			actividadDir = "vehiculosInsert";
		}
		var params = {};
		params.ID = $("#id").val();
		params.CHASIS = $("#chasis").val();
		params.COLOR = $("#color").val();
		params.COMPANIA = $("#compania").val();
		params.MARCA = $("#marca").val();
		params.MODELO = $("#modelo").val();
		params.MOTOR = $("#motor").val();
		params.PLATE = $("#plate").val();
		params.YEAR = $("#year").val();

		$.post(type+actividadDir, params, function(data) {
			var json = jQuery.parseJSON(data);
			if (json.estado == 1){
			GenerarAlerta(json.mensaje, 'Vehiculos', 1);
			} else{	
			CargarVehiculos();
			GenerarAlerta(json.mensaje, 'Alerta', 2);
			setTimeout(function(){$("#cerrarModalVehiculos").click();});
			}
		});
	});

	$('#displayTable').on('click', '.editVehiculo', function (e) {
		typeNum = 1;
		$("#theHeader").html('Editar Vehículo');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#id").val(row.eq(0).text());
		$("#imei").val(row.eq(1).text());
		$("#marca").val(row.eq(2).text());
		$("#modelo").val(row.eq(3).text());
		$("#color").val(row.eq(4).text());
		$("#year").val(row.eq(5).text());
		$("#plate").val(row.eq(6).text());
		$("#motor").val(row.eq(7).text());
		$("#chasis").val(row.eq(8).text());
		$("#compania").val(row.eq(9).text());
		displayOrHide(false);
		$("#imei").prop('disabled', true);
		$('#enviarVehiculo').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		$("#modalvehiculos").modal();
	});

		$('#displayTable').on('click', '.asignarVehiculo', function (e) {
		typeNum = 1;
		$("#theHeader3").html('Asignar Vehículo');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$('#enviarVehiculo').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		$("#modalasignacionvehiculos").modal();
	});

	$('#displayTable').on('click', '.cargarInfo', function(e) {
		displayOrHide(true);
		$("#InformacionVehiculo").empty();
		$("#theHeader").html('Informacion del Vehículo');
		lastRow = $(this).closest('tr');
		var row = lastRow.find('td');
		$("#InformacionVehiculo").prepend("<h2 align='center'>"+ row.eq(1).text() +"</h2>");
		$("#InformacionVehiculo").append("<div class='form-group col-md-6'> <p><b>Marca: </b>"+row.eq(2).text()+ " </p> </div>");
		$("#InformacionVehiculo").append("<div class='form-group col-md-6'> <p><b>Modelo: </b>"+row.eq(3).text()+ " </p> </div>");
		$("#InformacionVehiculo").append("<div class='form-group col-md-6'> <p><b>Color: </b>"+row.eq(4).text()+ " </p> </div>");
		$("#InformacionVehiculo").append("<div class='form-group col-md-6'> <p><b>Año: </b>"+row.eq(5).text()+ " </p> </div>");
		$("#InformacionVehiculo").append("<div class='form-group col-md-6'> <p><b>Placa: </b>"+row.eq(6).text()+ " </p> </div>");
		$("#InformacionVehiculo").append("<div class='form-group col-md-6'> <p><b>Motor: </b>"+row.eq(7).text()+ " </p> </div>");
		$("#InformacionVehiculo").append("<div class='form-group col-md-6'> <p><b>Chasis: </b>"+row.eq(8).text()+ " </p> </div>");
		$("#InformacionVehiculo").append("<div class='form-group col-md-6'> <p><b>Compañía: </b>"+row.eq(9).text()+ " </p> </div>");
		$('#modalvehiculos').modal();

	});

	function CargarVehiculos() {
		// /animacion de loading/
		// LoadAnimation("body");
		var path = type + "vehiculosGet";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function (data, status, xhr) {
			LimpiarTabla('#TblVehiculos');
			if (data != false) {
				var columns = [{ "mDataProp": "ID", "className": "hidden id"},
                      {"mDataProp":"IMEI", "className":"text-center" },
                      {"mDataProp":"MAKE", "className":"text-center" },
                      {"mDataProp":"MODEL", "className":"text-center" },
                      {"mDataProp":"COLOR", "className":"hidden id"},
                      {"mDataProp":"YEAR", "className":"text-center" },
                      {"mDataProp":"PLATE", "className":"text-center" },
                      {"mDataProp":"MOTOR", "className":"hidden id" },
                      {"mDataProp":"CHASSIS", "className":"hidden id"},
                      {"mDataProp":"COMPANY", "className":"hidden id" },
                      {
	                          "targets": -1,
	                          "data": [{"mDataProp":""}],
	                          "defaultContent": "<span class='editVehiculo btn btn-success botonVED' data-toggle='tooltip' data-placement='left' title='Editar vehículo'><i class='icon-edit icon-white fa fa-pencil'></i></span>", "className": "text-center"
	                  }];

			LoadDataTable(data, '#TblVehiculos', columns);
			}
		});
		posting.fail(function (data, status, xhr) {
			console.log(data);
			// sendError(data);
			//GenerarErrorAlerta(xhr, "error");
			// goAlert();
		});
		posting.always(function (data, status, xhr) {
			// RemoveAnimation("body");
			// console.log(data);
		})
	}


});