$(document).ready(function (){
	var type = urljs+"index.php/modulos/";
	var typeNum = 0;
	var lastRow = null;
	var modulosForm = $("#modulosForm");

	modulosForm.validate({
		rules:{
			codigo_modulo:{
				required: true,
				maxlength: 10,
				codigos: true
			},
			nombre_modulo:{
				required: true,
				Letrascontildes: true,
				maxlength: 50
			},
			descripcion_modulo:{
				required: true,
				maxlength:50
			}
		},
		messages:{
			codigo_modulo:{
				required: "Por favor, ingrese un valor."
			},
			nombre_modulo:{
				required: "Por favor, ingrese un valor."
			},
			descripcion_modulo:{
				required: "Por favor, ingrese un valor."
			}
		}
	});

	$("#nombre_modulo").blur(function(){
		var nombre_modulo = $.trim($("#nombre_modulo").val());
		$("#nombre_modulo").val(nombre_modulo);
	});

	$("#descripcion_modulo").blur(function(){
		var descripcion_modulo = $.trim($("#descripcion_modulo").val());
		$("#descripcion_modulo").val(descripcion_modulo);
	});

	function displayOrHide(isDisplaying) {
    	if(isDisplaying) {
    	  $("#sure").show();
    	  $(".greatInputContainer").hide();
    	} else {
    	  $(".greatInputContainer").show();
    	  $("#sure").hide();
    	}
  	}
	$('#modalModulos').on('hidden.bs.modal', function(e) {
		$(".moduleinputs").val("");
		$(".moduleinputs").removeClass('error');
		$(".error").remove();
	});
	$('#displayTable').on('click', '.editModulo', function (e) {
		typeNum = 1;
		$("#theHeader").html('Editar Módulo');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#id_modulo").val(row.eq(0).text());
		$("#codigo_modulo").val(row.eq(1).text());
		$("#nombre_modulo").val(row.eq(2).text());
		$("#descripcion_modulo").val(row.eq(3).text());
		displayOrHide(false);
		$('#enviarModulo').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		$("#modalModulos").modal();
	});
	$("#newModulo").click(function() {
		typeNum = 2;
		$("#theHeader").html('Nuevo Módulo');
		displayOrHide(false);
		$('#enviarModulo').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$("#modalModulos").modal();
	});
	$('#displayTable').on('click', '.deleteModulo', function (e) {
		typeNum = 3;
		$("#theHeader").html('Eliminar Módulo');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#id_modulo").val(row.eq(0).text());
		$("#nombre_span").html(row.eq(2).text());
		displayOrHide(true);
		$('#enviarModulo').addClass('btn-danger').removeClass('btn-primary').html("Eliminar");
		$("#modalModulos").modal();
	});
	$("#enviarModulo").click(function() {
		if(modulosForm.valid())
			(modulosForm.submit());
	});

	modulosForm.submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "moduloUpdate";
		} else if (typeNum == 2) {
			actividadDir = "modulosInsert";
		} else {
			actividadDir = "moduloDelete";
		}
		var params = {};
				params.MODULEID = $("#id_modulo").val();
				params.CODEMODULE = $("#codigo_modulo").val();
				params.NAMEMODULE = $("#nombre_modulo").val();
				params.DESCMODULE = $("#descripcion_modulo").val();
		var moduloName = JSON.stringify(params);
		$.post(type+actividadDir, {moduloName :moduloName}, function(data) {
			switch (data[2]) {
				case 1: { ArmarNuevo(data[1]); } break;
				case 2: {
					var allColumns = lastRow.find("td");
					allColumns.eq(0).html(data[1]['MODULEID']);
					allColumns.eq(1).html(data[1]['CODEMODULE']);
					allColumns.eq(2).html(data[1]['NAMEMODULE']);
					allColumns.eq(3).html(data[1]['DESCMODULE']);
					allColumns.eq(4).html(data[1]['DATES']);
				} break;
				case 3: { removeOnTable(lastRow); } break;
				default: { }
			}
			if (data[0].estado == 1){
			GenerarAlerta(data[0].mensaje, 'Modulos', 1);
			} else{
			GenerarAlerta(data[0].mensaje, 'Alerta', 2);
			setTimeout(function(){$("#cerrarModulo").click();});
			}
		});
	});

	function ArmarNuevo(rowsAgregados) {
		var formaArreglo = [];
		var moduloid = 0;
		var acciones = "";
		for (var i = 0; i < rowsAgregados.length; i++) {
			moduloid = rowsAgregados[i]['MODULEID'];
			acciones = `<a href="perfilesmodulos?pm=2&vl=${moduloid}">
				<span class='linkModulo btn btn-warning botonVED'>
				<i class='icon-edit icon-white fa fa-list'></i></span>
			</a>
			<span class='editModulo btn btn-success botonVED'>
				<i class='icon-edit icon-white fa fa-pencil'></i>
			</span>
			<span class='deleteModulo btn btn-danger botonVED'>
				<i class='icon-edit icon-white fa fa-trash-o'></i>
			</span>`;
			formaArreglo = [moduloid, rowsAgregados[i]['CODEMODULE'], rowsAgregados[i]['NAMEMODULE'], rowsAgregados[i]['DESCMODULE'], rowsAgregados[i]['DATES'], acciones];
			addOnTable(formaArreglo, [0]);
		}
	}
});
