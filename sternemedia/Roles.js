$(document).ready(function (){
  var type = urljs+"index.php/roles/";
  var typeNum = 0;
  var lastRow = null;
  $('#modulo_rol').select2();
  var rolesForm = $('#rolesForm');

  rolesForm.validate({
    rules:{
      codigo_rol:{
        required: true,
        maxlength: 10,
        codigos: true
      },
      nombre_rol:{
        required: true,
        Letrascontildes: true,
        maxlength: 20
      },
      modulo_rol:{
        required: true
      },
      descripcion_rol:{
        required: true
      }
    },
    messages:{
      codigo_rol:{
        required: "Por favor ingrese un valor."
      },
      nombre_rol:{
        required: "Por favor ingrese un valor."
      },
      modulo_rol:{
        required: "Por favor ingrese un valor."
      },
      descripcion_rol:{
        required: "Por favor ingrese un valor."
      }

    }
  });

  $("#nombre_rol").blur(function(){
    var nombre_rol = $.trim($("#nombre_rol").val());
    $("#nombre_rol").val(nombre_rol);
  });
  $("#descripcion_rol").blur(function(){
    var descripcion_rol = $.trim($("#descripcion_rol").val());
    $("#descripcion_rol").val(descripcion_rol);
  });

  function displayOrHide(isDisplaying) {
    if(isDisplaying) {
      $("#sure").show();
      $(".greatInputContainer").hide();
    } else {
      $(".greatInputContainer").show();
      $("#sure").hide();
    }
  }
  $('#modalRol').on('hidden.bs.modal', function(e) {
		$(".rolsinputs").val("");
    $("#modulo_rol").select2("val", "")
    $('.rolsinputs').removeClass('error');
    $('.error').remove();
	});
  $('#displayTable').on('click', '.editRol', function (e) {
    typeNum = 1;
		$("#theHeader").html('Editar Rol');
    displayOrHide(false);
    lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#id_rol").val(row.eq(0).text());
		$("#codigo_rol").val(row.eq(1).text());
		$("#nombre_rol").val(row.eq(2).text());
    $("#descripcion_rol").val(row.eq(3).text());
		$("#modulo_rol").select2("val", row.eq(4).text());
		$('#enviarRol').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		$("#modalRol").modal();
  });
  $("#newRol").click(function() {
    typeNum = 2;
    $("#theHeader").html("Nuevo Rol");
    displayOrHide(false);
    $('#enviarRol').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
    $("#modalRol").modal();
  });
  $('#displayTable').on('click', '.deleteRol', function (e) {
    typeNum = 3;
    $("#theHeader").html('Eliminar Rol');
    displayOrHide(true);
    lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
    $("#id_rol").val(row.eq(0).text());
    $("#nombre_span").html(row.eq(2).text());
    $('#enviarRol').addClass('btn-danger').removeClass('btn-primary').html("Eliminar");
    $("#modalRol").modal();
  });
  $("#enviarRol").click(function() {
    if (rolesForm.valid())
      (rolesForm.submit());
  });

  rolesForm.submit(function(event){
    event.preventDefault();
    var actividadDir = "";
    if (typeNum == 1) {
      actividadDir = "rolUpdate";
    } else if (typeNum == 2) {
      actividadDir = "rolInsert";
    } else {
      actividadDir = "rolDelete";
    }
    var params = {};
        params.ROLID = $("#id_rol").val();
        params.CODIGOROL = $("#codigo_rol").val();
        params.NOMBREROL = $("#nombre_rol").val();
        params.MODULOIDROL = $("#modulo_rol").val();
        params.DESCROL = $("#descripcion_rol").val();
    var rolesName = JSON.stringify(params);
    $.post(type+actividadDir, {rolesName :rolesName}, function(data) {
      switch (data[2]) {
        case 1: { ArmarNuevo(data[1]); } break;
        case 2: {
          var allColumns = lastRow.find("td");
          allColumns.eq(0).html(data[1]['ROLEID']);
          allColumns.eq(1).html(data[1]['CODEROLE']);
          allColumns.eq(2).html(data[1]['NAMEROLE']);
          allColumns.eq(3).html(data[1]['DESCROLE']);
          allColumns.eq(4).html(data[1]['IDMODULES']);
          allColumns.eq(5).html(data[1]['NAMEMODULE']);
        } break;
        case 3: { removeOnTable(lastRow); } break;
        default: { }
      }
      if (data[0].estado == 1){
        GenerarAlerta(data[0].mensaje, 'Roles', 1);
      } else if(data[0].estado == 2){
        GenerarAlerta(data[0].mensaje, 'Alerta', 2);
        setTimeout(function(){$("#cerrarRol").click();});
      }else{
        GenerarAlerta(data[0], 'Alerta', 2);
      setTimeout(function(){$("#cerrarRol").click();});
      }
    });
  });


  function ArmarNuevo(rowsAgregados) {
		var formaArreglo = [];
		var rolid = 0;
		var acciones = "";
		for (var i = 0; i < rowsAgregados.length; i++) {
			rolid = rowsAgregados[i]['ROLEID'];
			acciones = `<a href="rolesuser?ru=1&vl=${rolid}">
        <span class='btn bg-orange botonVED'>
          <i class='icon-edit icon-white fa fa-arrow-circle-o-right'></i>
        </span>
      </a>
      <span class='editRol btn btn-success botonVED'>
        <i class='icon-edit icon-white fa fa-pencil'></i>
      </span>
      <span class='deleteRol btn btn-danger botonVED'>
        <i class='icon-edit icon-white fa fa-trash-o'></i>
      </span>`;
			formaArreglo = [rolid, rowsAgregados[i]['CODEROLE'], rowsAgregados[i]['NAMEROLE'], rowsAgregados[i]['DESCROLE'], rowsAgregados[i]['IDMODULES'], rowsAgregados[i]['NAMEMODULE'], acciones];
			addOnTable(formaArreglo, [0,4]);
		}
	}
});
