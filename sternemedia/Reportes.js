
$(document).ready(function(){
	$("select").select2();
	LimpiarSelect($("#proyecto"));
	cargarProyectos();

	LimpiarSelect($("#proyecto1"));
	cargarProyectos1();

	LimpiarSelect($("#proyecto2"));
	cargarProyectos2();

	LimpiarSelect($("#proyecto3"));
	cargarProyectos3();

	$('#reporte_modulos').addClass('btn btn-success').html("Reporte Módulos");
	$('#reporte_incidencias').addClass('btn btn-success').html("Reporte Incidencias");
	$('#reportería_diaria').addClass('btn btn-success').html("Reportería Diaria");
	$('#matriz').addClass('btn btn-success').html("Matriz de Casos");

	var id;

	$('#proyecto').change(function()
	{
		id = $(this).val();
		console.log(id);

		if(id != "")
		{
			$('#reporte_modulos').prop('disabled', false);
			$('#enviarModulo').prop('disabled', false);
		}
		else
		{
			$('#reporte_modulos').prop('disabled', true);
			$('#enviarModulo').prop('disabled', true);
		}
	});

	$("#reporte_modulos").click(function() {
		var path = urljs + 'index.php/ReportePrueba/detalleModulos/' + id;
		window.open(path, '_blank');
	});

	/*$("#enviarModulo").click(function(){
		var path = urljs + 'index.php/ReportePrueba/setemail_Modulo/' + id;
		GenerarSuccessAlerta('Se ha enviado un archivo adjunto al/los correo(s)!', 'success');
		window.open(path, '_blank');
	});*/

	$("#enviarModulo").click(function(){
		var path = urljs + 'index.php/ReportePrueba/setemail_Modulo/' + id;
		$.get(path, null, function(data){
			GenerarSuccessAlerta('Se ha enviado un archivo adjunto al/los correo(s)!', 'success');
		});
	});

	var id1;

	$('#proyecto1').change(function()
	{
		id1 = $(this).val();
		console.log(id1);

		if(id1 != "")
		{
			$('#reporte_incidencias').prop('disabled', false);
			$('#enviarIncidencias').prop('disabled', false);
		}
		else
		{
			$('#reporte_incidencias').prop('disabled', true);
			$('#enviarIncidencias').prop('disabled', true);
		}
	});

	$("#reporte_incidencias").click(function() {
		var path = urljs + 'index.php/ReportePrueba/incidencias/' + id1;
		window.open(path, '_blank');
	});

	/*$("#enviarIncidencias").click(function(){
		var path = urljs + 'index.php/ReportePrueba/setemail_Incidencias/' + id1;
		GenerarSuccessAlerta('Se ha enviado un archivo adjunto al/los correo(s)!', 'success');
		window.open(path, '_blank');
	});*/

	$("#enviarIncidencias").click(function(){
		var path = urljs + 'index.php/ReportePrueba/setemail_Incidencias/' + id1;
		$.get(path, null, function(data){
			GenerarSuccessAlerta('Se ha enviado un archivo adjunto al/los correo(s)!', 'success');
		});
	});

	var id2;

	$('#proyecto2').change(function()
	{
		id2 = $(this).val();
		console.log(id2);

		if(id2 != "")
		{
			$('#reportería_diaria').prop('disabled', false);
			$('#enviarDiaria').prop('disabled', false);
		}
		else
		{
			$('#reportería_diaria').prop('disabled', true);
			$('#enviarDiaria').prop('disabled', true);
		}
	});

	$("#reportería_diaria").click(function() {
		var path = urljs + 'index.php/ReportePrueba/reporteriaDiaria/' + id2;
		window.open(path, '_blank');
	});

	/*$("#enviarDiaria").click(function(){
		var path = urljs + 'index.php/ReportePrueba/setemail_Diaria/' + id2;
		GenerarSuccessAlerta('Se ha enviado un archivo adjunto al/los correo(s)!', 'success');
		window.open(path, '_blank');
	});*/

	$("#enviarDiaria").click(function(){
		var path = urljs + 'index.php/ReportePrueba/setemail_Diaria/' + id2;
		$.get(path, null, function(data){
			GenerarSuccessAlerta('Se ha enviado un archivo adjunto al/los correo(s)!', 'success');
		});
	});

	var id3;

	$('#proyecto3').change(function()
	{
		id3 = $(this).val();
		console.log(id3);

		if(id3 != "")
		{
			$('#matriz').prop('disabled', false);
			$('#enviarMatriz').prop('disabled', false);
		}
		else
		{
			$('#matriz').prop('disabled', true);
			$('#enviarMatriz').prop('disabled', true);
		}
	});

	$("#matriz").click(function() {
		var path = urljs + 'index.php/ReportePrueba/matrizCompleta/' + id3;
		window.open(path, '_blank');
	});

	/*$("#enviarMatriz").click(function(){
		var path = urljs + 'index.php/ReportePrueba/setemail_Matriz/' + id3;
		GenerarSuccessAlerta('Se ha enviado un archivo adjunto al/los correo(s)!', 'success');
		window.open(path, '_blank');
	});*/

	$("#enviarMatriz").click(function(){
		var path = urljs + 'index.php/ReportePrueba/setemail_Matriz/' + id3;
		$.get(path, null, function(data){
			GenerarSuccessAlerta('Se ha enviado un archivo adjunto al/los correo(s)!', 'success');
		});
	});

	function cargarProyectos() {
		var metodo = urljs + 'index.php/Pruebas/' + 'proyectosGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			$("#proyecto").select2("val","");
			// LimpiarSelect($("#proyecto"));
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var proyectos = data[i]
					$("#proyecto").append('<option value="' + proyectos.ID_PROJECTS + '">' + proyectos.PROJECT_NAME + '</option>');
				}
			}
		});
	}

	function cargarProyectos1() {
		var metodo = urljs + 'index.php/Pruebas/' + 'proyectosGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			$("#proyecto1").select2("val","");
			// LimpiarSelect($("#proyecto"));
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var proyectos = data[i]
					$("#proyecto1").append('<option value="' + proyectos.ID_PROJECTS + '">' + proyectos.PROJECT_NAME + '</option>');
				}
			}
		});
	}

	function cargarProyectos2() {
		var metodo = urljs + 'index.php/Pruebas/' + 'proyectosGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			$("#proyecto2").select2("val","");
			// LimpiarSelect($("#proyecto"));
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var proyectos = data[i]
					$("#proyecto2").append('<option value="' + proyectos.ID_PROJECTS + '">' + proyectos.PROJECT_NAME + '</option>');
				}
			}
		});
	}

	function cargarProyectos3() {
		var metodo = urljs + 'index.php/Pruebas/' + 'proyectosGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			$("#proyecto3").select2("val","");
			// LimpiarSelect($("#proyecto"));
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var proyectos = data[i]
					$("#proyecto3").append('<option value="' + proyectos.ID_PROJECTS + '">' + proyectos.PROJECT_NAME + '</option>');
				}
			}
		});
	}
});