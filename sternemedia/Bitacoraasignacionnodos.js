$(document).ready(function(){
	var type = urljs + "index.php/Bitacoraasignacionnodos/"; 
	
	$(function(){
		$('#FechaInicial').datepicker({
			autoclose: true,
			language: 'es',
			today:'today'
		});

		$('#FechaFinal').datepicker({
			autoclose: true,
			language: 'es',
			today:'today'
		});
	});

	$("#searchBitacora").click(function(){
		var FechaInicial = $('#FechaInicial').val();
		var FechaFinal = $('#FechaFinal').val();
		var fechasvalidas = validaCamposFecha(FechaInicial, FechaFinal);
			if (fechasvalidas == false) {
				GenerarAlerta('La fecha de inicio debe ser menor que la fecha de finalización o verifique que ambas fechas esten seleccionadas.','Alerta',1);
				return false;
			} else{
				CargarBitacora(FechaInicial, FechaFinal);
			}
	});

	function CargarBitacora(FechaInicial, FechaFinal) 
	{
		var parametros = {}
		parametros.FechaInicial = FechaInicial;
		parametros.FechaFinal = FechaFinal;
		var path = type + "obtenerBitacora";
		var posting = $.post(path, parametros);
		posting.done(function (data, status, xhr) {
			LimpiarTabla('#TblBitacora');
			if (data != false) {
				var columns = [{"mDataProp":"USUARIO", "className":"text-center"},
					  { "mDataProp": "IMEI", "className": "text-center"},
					  { "mDataProp": "USUARIO_ASIG", "className": "text-center"},
                      {"mDataProp":"FECHA_VW", "className":"text-center"},
                      {"mDataProp":"ACCION", "className":"text-center"}];

			LoadDataTable(data, '#TblBitacora', columns);
			}
		});
		posting.fail(function (data, status, xhr) {
			console.log(data);
			// sendError(data);
			//GenerarErrorAlerta(xhr, "error");
			// goAlert();
		});
		posting.always(function (data, status, xhr) {
			// RemoveAnimation("body");
			// console.log(data);
		})
	}
});