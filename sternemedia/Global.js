var urljs = "http://localhost/qs_produccion/";
//var urljs = "http://qs.sitemtto.com/";

var languageConf = {"lengthMenu": "Ver _MENU_ por página",
                    "zeroRecords": "No se encontraron resultados",
                    "info": "Página _PAGE_ de _PAGES_ | Registros: _MAX_",
                    "infoEmpty": "Ningún registro disponible",
                    "infoFiltered": "(filtrado de un total de _MAX_ total registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {"sFirst": "Primero",
                                  "sLast": "Último",
                                  "sNext": "Siguiente",
                                  "sPrevious": "Anterior"},
                    "oAria": {"sSortAscending": "Activar para ordenar la columna de manera ascendente",
                              "sSortDescending": "Activar para ordenar la columna de manera descendente"}
                };

var principalTable = $('.datatable').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "language": languageConf,
                        "responsive": true ,
                        "scrollX": true,
                        "scrollY": "50vh",
                        "fixedHeader": true});
oSettings = principalTable.settings();

// Generar una alerta dentro del modal
function GenerarAlerta(mensaje, iden, tipoerror) {
  var prioridad = (tipoerror === 1) ? 'error' : ((tipoerror == 2) ? 'success': 'warning');
  $(document).trigger("set-alert-id-" + prioridad + "" +iden,  [{'message' : mensaje, 'priority': prioridad}]);
  goAlert();
}
//Genera una alerta de error al estilo bootstrap
function GenerarErrorAlerta(mensaje, IdAlert) {
  IdAlert = "set-alert-id-" + IdAlert; //Concatenamos el Id con la cadena
  $(document).trigger(IdAlert, [{'message': mensaje, 'priority': 'danger'}]);
}
//Genera una alerta de aprobacion al estilo bootstrap
function GenerarSuccessAlerta(mensaje, IdAlert) {
  IdAlert = "set-alert-id-" + IdAlert; //Concatenamos el Id con la cadena
  $(document).trigger(IdAlert, {message: mensaje, priority: "success"});
}
function run(field) {
  setTimeout(function() {
    var regex = /\d*\.?\d?/g;
    field.value = regex.exec(field.value);
  }, 0);
}
function isValidDate(day,month,year) {
  var dteDate;
  month=month-1;
  dteDate=new Date(year,month,day);
  return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
}
function sendError(data) {
  var params = {};
  params.dataArray=[];
  params.dataArray[0] = {};
  params.dataArray[0].readyState = data.readyState;
  params.dataArray[0].responseText = data.responseText;
  params.dataArray[0].status = data.status;
  params.dataArray[0].statusText = data.statusText;
  var error = JSON.stringify(params);
  var path = urljs + "index.php/inventario/registrarError";
  var posting = $.post(path, {info: error});
  posting.done(function (data, status, xhr) { console.log(data); });
  posting.fail(function (data, status, xhr) { console.log(data); });
  posting.always(function (data, status, xhr) { RemoveAnimation("body"); });
}
function validate_fecha(fecha) {
  var patron=new RegExp("^([0-9]{1,2})([/])([0-9]{1,2})([/])(19|20)+([0-9]{2})$");
  if(fecha.search(patron)==0) {
    var values=fecha.split("/");
    if(isValidDate(values[0],values[1],values[2])) {
      return true;
    }
  }
  return false;
}
function Validar(input, mensaje) {
  var go = true;
  /*Evalua si el parametro input es array*/
  if (Array.isArray(input)) {
    for (var i = input.length - 1; i >= 0; i--) {
      var obj = $.trim($(input[i]).val());
      errorContainer = $(input[i]).siblings();
      if(obj == '-1' || obj === -1 || obj == "") {
        $(errorContainer).find('p').text(mensaje[i]).addClass('label label-danger');
        go = false;
      } else {
        $(errorContainer).find('p').text("").removeClass('label label-danger');
      }
    }
  } else {
    var obj = Number($(input).val());
    errorContainer = $(input).siblings();
    if(obj == '-1' || obj === -1 || obj == "") {
      $(errorContainer).find('p').text(mensaje).addClass('label label-danger');
      go = false;
    } else {
      $(errorContainer).find('p').text("").removeClass('label label-danger');
    }
  }
  return go;
}
function calcularDias(fechaActual, fechaOrden) {
  var fechaInicial=fechaOrden
  var fechaFinal=fechaActual
  var resultado="";
  if(validate_fecha(fechaInicial) && validate_fecha(fechaFinal)) {
    inicial=fechaInicial.split("/");
    final=fechaFinal.split("/");
    // obtenemos las fechas en milisegundos
    var dateStart=new Date(inicial[2],(inicial[1]-1),inicial[0]);
    var dateEnd=new Date(final[2],(final[1]-1),final[0]);
    if(dateStart <= dateEnd) {
      //La diferencia entre las dos fechas, la dividimos entre 86400 segundos que tiene un dia, y posteriormente entre 1000 ya que estamos trabajando con milisegundos.
      resultado=(((dateEnd-dateStart)/86400)/1000);
    } else {
      resultado=-1;
    }
  } else {
    if(!validate_fecha(fechaInicial)) { resultado=-1; }
    if(!validate_fecha(fechaFinal)) { resultado=-1; }
  }
  return resultado;
}
function getDiasTranscurridos(fechaActual, fechaOrden) {
  var validaFecha = true;
  var dias = 0;
  if (fechaActual == '' || fechaOrden == '') {
    validaFecha = false;
  } else {
    var fechaIniVal = fechaActual;
    var fechaFinVal = fechaOrden;
    var inicio = fechaIniVal.split("/");
    var fin = fechaFinVal.split("/");
    //Se evalua el año
    if (inicio[2] == fin[2]) {
      //Se evalua el mes
      if (inicio[1] == fin[1]) {
        dias = inicio[0] - fin[0];
      } else {
        validaFecha = false;
      }
    } else {
      validaFecha = false;
    }
  }
  if (!validaFecha) { dias = -1; }
  return dias;
}
//Funcion que valida si la fechas inicial es mayor que la fecha final
function validaCamposFecha(fechaini, fechafin) {
  var validaFecha = false;
  var fechaIniVal = fechaini;
  var fechaFinVal = fechafin;
  if (fechaini == '' || fechafin == '') {
    validaFecha = false;
  } else {
    var inicio = fechaIniVal.split("/");
    var fin = fechaFinVal.split("/");
    //Validamos el año
    if (fin[2] >= inicio[2]) {
      //Validamos cuando el año es igual
      if (fin[2] == inicio[2]) {
        //Validamos el mes
        if (fin[1] >= inicio[1]) {
          //Validamos cuando el mes es igual
          if (fin[1] == inicio[1]) {
            //Validamos el dia
            if (fin[0] < inicio[0]) {
              validaFecha = false;
            } else {
              validaFecha = true;
            }
          } else {
            //Cuando el mes final es mayor
            validaFecha = true;
          }
        } else {
          validaFecha = false;
        }
      } else {
        //Cuando el año final es mayor.
        validaFecha = true;
      }
    } else {
      validaFecha = false;
    }
  }
  return validaFecha;
}
//Funcion que limpia inputs.
function LimpiarInput(arrayInput, arrayCombo) {
  if (arrayInput.length > 0) {
    //Recorremos el arreglo con los ID
    for (var i = 0; i < arrayInput.length; i++) {
      //Limpia el input
      $(arrayInput[i]).val("");
    }
  }
  if (arrayCombo.length > 0) {
    for (var i = arrayCombo.length - 1; i >= 0; i--) {
      $(arrayCombo[i]).val("-1").trigger('change');
    }
  }
}

function goAlert() {
  //Nos coloca en el mensaje de error
  $('html,body').animate({
      scrollTop: $("#MyAlerts").offset().top
  });
}
function goAlertByObj(pObj) {
  //Nos coloca en el mensaje de error
  $(pObj).animate({
      scrollTop: $(pObj).offset().top
  });
}
function ajaxindicatorstart(text) {
  if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading') {
    jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="'+urljs+'dist/img/facebook.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
  }
  jQuery('#resultLoading').css({
    'width':'100%',
    'height':'100%',
    'position':'fixed',
    'z-index':'10000000',
    'top':'0',
    'left':'0',
    'right':'0',
    'bottom':'0',
    'margin':'auto'
  });
  jQuery('#resultLoading .bg').css({
    'background':'#000000',
    'opacity':'0.7',
    'width':'100%',
    'height':'100%',
    'position':'absolute',
    'top':'0'
  });
  jQuery('#resultLoading>div:first').css({
    'width': '250px',
    'height':'75px',
    'text-align': 'center',
    'position': 'fixed',
    'top':'0',
    'left':'0',
    'right':'0',
    'bottom':'0',
    'margin':'auto',
    'font-size':'16px',
    'z-index':'10',
    'color':'#ffffff'
  });
  jQuery('#resultLoading .bg').height('100%');
  jQuery('#resultLoading').fadeIn(300);
  jQuery('body').css('cursor', 'wait');
}
function ajaxindicatorstop() {
  jQuery('#resultLoading .bg').height('100%');
  jQuery('#resultLoading').fadeOut(300);
  jQuery('body').css('cursor', 'default');
}
function EventAjax() {
  $( document ).ajaxStart(function() {
    console.log('Cargando.....');
    ajaxindicatorstart('Espere un momento...');
  });
  $( document ).ajaxError(function(e) {
    console.log('Se genero un error!');
    console.log(e);
    ajaxindicatorstop();
    //sendError(e);
  });
  $(document).ajaxStop(function() {
    console.log('Proceso Terminado');
    ajaxindicatorstop();
  });
}
function LoadComboBox(infojson) {
    /*animacion de loading*/
    //LoadAnimation("body");
    /*Limpiamos el ComboBox para no mostrar data repetida.*/
    $(infojson.input).html("");
    /*Agregamos 1era opcion que se mostrara en el ComboBox.*/
    $(infojson.input).append('<option value="-1">Ninguno...</option>');
    //console.log(infojson);
    switch (infojson.type) {
        case "POST":
            /* Send the data using post */
            var posting = $.post( urljs + infojson.url, { data: infojson.data } );
            /* Put the results in a div */
            posting.done(function( data ) {
                if (data.Estado) {
                    /*Recorremos el objeto objdata y agregamos los datos al ComboBox.*/
                    //for (var i = data.Resultado.length - 1; i >= 0; i--)
                    for (var i = 0;  i < data.Resultado.length; i++) {
                        var option = $(document.createElement('option'));
                        option.text(data.Resultado[i][infojson.text]);
                        option.val(data.Resultado[i][infojson.val]);
                        $(infojson.input).append(option);
                    }
                } else {
                    if (infojson.data != '-1') {
                        GenerarErrorAlerta(data.Mensaje, "error");
                        goAlert();
                    }
                }
                $(infojson.input).select2().trigger('change');
            });
            posting.fail(function(data, status, xhr) {
                sendError(data);
                $(infojson.input).html("");
                $(infojson.input).append('<option value="-1">Ninguno...</option>');
                GenerarErrorAlerta("Error cargando los datos - , " + xhr, "error");
                goAlert();
            });
            posting.always(function(data, status, xhr) {
                /*quitamos la animacion*/
                //RemoveAnimation("body");
            });
            break;
        case "GET":
            var getting = $.get(urljs + infojson.url + infojson.data);
            getting.done(function (data)
            {
                if(data.Estado)
                {
                    $(infojson.input).html("");
                    $(infojson.input).append('<option value="-1">Ninguno...</option>');
                    /*Recorremos el objeto objdata y agregamos los datos al ComboBox.*/
                    //for (var i = data.Resultado.length - 1; i >= 0; i--)
                   for (var i = 0;  i < data.Resultado.length; i++)
                    {
                        var option = $(document.createElement('option'));
                        option.text(data.Resultado[i][infojson.text]);
                        option.val(data.Resultado[i][infojson.val]);
                        $(infojson.input).append(option);
                    }
                    $(infojson.input).select2().trigger('change');
                }
                else
                {
                    $(infojson.input).html("");
                    $(infojson.input).append('<option value="-1">Ninguno...</option>');
                    if (infojson.data != '-1')
                    {
                        GenerarErrorAlerta(data.Mensaje, "error");
                        goAlert();
                    }
                }
            });

            getting.fail(function (data, status, xhr)
            {
                sendError(data);
                GenerarErrorAlerta("Error cargando los datos - , " + xhr, "error");
                goAlert();
            });
            getting.always(function(data, status, xhr)
            {
                /*quitamos la animacion*/
                //RemoveAnimation("body");
            });
            break;
        default:
            $(infojson.input).html("");
            $(infojson.input).append('<option value="-1">Ninguno...</option>');
            //RemoveAnimation("body");
            break;
    }
}
function LimpiarTabla(TableName) {
  $(TableName).dataTable().fnClearTable();
  $(TableName).dataTable().fnDestroy();
  $(TableName).DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "language": languageConf
  });
}

function LimpiarTablaSimple(TableName) {
  $(TableName).dataTable().fnClearTable();
  $(TableName).dataTable().fnDestroy();
  $(TableName).DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true,
      "scrollY": "300px",
      "language": languageConf,
      "dom": '<"pull-left"f><"pull-right"l>tip'
      // "dom":'<"search"f><"top"l>rt<"bottom"ip><"clear">'
  });
}

function LoadDataTable(data, tableID, columns) {
  $(tableID).dataTable().fnClearTable();
  $(tableID).dataTable().fnDestroy();
  $(tableID).DataTable({
      "aaData": data,
      "aoColumns": columns,
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "scrollX": true,
      "scrollY": "50vh",
      "language": languageConf
  });
  var table = $(tableID).DataTable().draw();
}
function LoadDataTableSimple(data, tableID, columns) {
  $(tableID).dataTable().fnClearTable();
  $(tableID).dataTable().fnDestroy();
  $(tableID).DataTable({
      "aaData": data,
      "aoColumns": columns,
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true,
      "scrollY": "300px",
      "language": languageConf,
      "dom": '<"pull-left"f><"pull-right"l>tip'
      // "dom":' <"search"f><"top"l>rt<"bottom"ip><"clear">'
  });
  var table = $(tableID).DataTable().draw();
}
function addOnTable(data, columnasOcultas) {
  principalTable.row.add(data).draw(false);
  if (columnasOcultas) {
    for (var i = 0; i < columnasOcultas.length; i++) {
      principalTable.column(columnasOcultas[i]).nodes().to$().removeClass('oculto');
      principalTable.column(columnasOcultas[i]).nodes().to$().addClass('oculto');
    }
  }
}
function removeOnTable(therow) {
  principalTable.row(therow).remove().draw();
}
EventAjax();

  $.validator.addMethod("password", function(value) {
   return /^[A-Za-z0-9\d=!\-@$%._*]*$/.test(value) // consists of only these
       && /[a-z]/.test(value) // has a lowercase letter
       && /\d/.test(value) // has a digit
       && /[A-Z]/.test(value) // has a capital letters
  });

    $.validator.addMethod("codigos", function(value) {
   return /^[A-Za-z0-9]*$/.test(value) // consists of only these
       && /[a-zA-Z]/.test(value) // has a lowercase letter
       && /[0-9]/.test(value) // has a capital letters
  }, "Este campo debe contener al menos una letra y un número solamente.");

  $.validator.addMethod( "Letrascontildes", function( value, element ) {
  return this.optional( element ) || /^[a-zñÑáéíóúÁÉÍÓÚ\s]+$/i.test( value );
}, "Por favor, ingrese solo letras.");

  $.validator.addMethod( "SinCaracteresEspeciales", function( value, element ) {
  return this.optional( element ) || /^[a-z0-9ñÑáéíóúÁÉÍÓÚ\s]+$/i.test( value );
}, "Por favor, ingrese solo letras o números.");

  $.validator.addMethod( "SinDosEspaciosSoloTexto", function( value, element ) {
  return this.optional( element ) || /^(([A-zñÑáéíóúÁÉÍÓÚ])+( ?[A-zñÑáéíóúÁÉÍÓÚ])*)$/.test( value ) || /^(([A-zñÑáéíóúÁÉÍÓÚ])+( ?[A-zñÑáéíóúÁÉÍÓÚ])*)\.$/.test( value );
}, "Cuidado, no puede ingresar mas de un espacio.");

  $.validator.addMethod( "SinDosEspaciosNumeroTexto", function( value, element ) {
  return this.optional( element ) || /^(([A-z0-9ñÑáéíóúÁÉÍÓÚ])+( ?[A-z0-9ñÑáéíóúÁÉÍÓÚ])*)$/.test( value ) || /^(([A-z0-9ñÑáéíóúÁÉÍÓÚ])+( ?[A-z0-9ñÑáéíóúÁÉÍÓÚ])*)\.$/.test( value );
}, "Cuidado, no puede ingresar mas de un espacio.");

  $.validator.addMethod( "NoIniciarConCaracteresEspeciales", function( value, element ) {
  //return this.optional( element ) || /^[A-Za-zñÑáéíóúÁÉÍÓÚ][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/i.test( value );
  return this.optional( element ) || /^([A-Za-zñÑáéíóúÁÉÍÓÚ][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*)/i.test( value );
}, "Cuidado, no puede iniciar con caracteres especiales ni numeros.");

  $.validator.addMethod( "url", function( value, element ) {
   return /^[A-Za-z\/]*$/.test(value) // solo letras y '/'
       && /[\/]/.test(value) // evalua si al menos contiene una pleca
  }, "Por favor, asegurese de que este campo cumpla con el formato: '/ejemplo'.");

function LimpiarSelect(select) {
  select.find("option")
      .remove()
      .end()
      .append("<option value=''>--Seleccionar--</option>")
      .val("");
}

function LimpiarSelectPicker(select) {
  select.find("option")
      .remove()
      .end()
      // .append("<option value=''>--Seleccionar--</option>")
      .val("");
}
