$(document).ready(function(){
	var type = urljs+"index.php/Mantenimientos/";
	var typeNum = 0;
	var nombTab = '';
	var tablaForm = $("#tablaForm");
	var matenimientoForm = $("#matenimientoForm");
	$("select").select2();

	LimpiarSelect($("#idtbl"));
	cargarTablas();
	$("#tblMantenimientos").dataTable();

	jQuery('.withoutTwoOrMoreSpaces').keyup(function () {
		this.value = this.value.replace(/([ ]{2,})|[[]\/]/, '');
	});

	tablaForm.validate({
		rules:{
			nombreTabla: {
				required: true,
				Letrascontildes: true
				// SinDosEspaciosSoloTexto: true
			},
			codigoTabla: {
				required: true,
				SinCaracteresEspeciales: true
				// SinDosEspaciosNumeroTexto: true
			},
			descripcion: {
				required: true
				// SinDosEspaciosNumeroTexto: true
			}
		},
		messages: {
			nombreTabla: {
				required: "Por favor, ingrese un valor."
			},
			codigoTabla: {
				required: "Por favor, ingrese un valor."
			},
			descripcion: {
				required: "Por favor, ingrese un valor."
			}
		}

	});

	matenimientoForm.validate({
		rules: {
			idtbl: {
				required: true
			},
			codmantenimiento: {
				required: true,
				SinCaracteresEspeciales: true
				// SinDosEspaciosNumeroTexto: true
			},
			mantenimiento: {
				required: true,
				Letrascontildes: true
				// SinDosEspaciosSoloTexto: true
			},
			valor: {
				required: true
			},
			descripcion_mant: {
				required: true
				// SinDosEspaciosNumeroTexto: true
			}

		},
		messages: {
			idtbl: {
				required: "Por favor, seleccione un valor."
			},
			codmantenimiento: {
				required: "Por favor, ingrese un valor."
			},
			mantenimiento: {
				required: "Por favor, ingrese un valor."
			},
			valor: {
				required: "Por favor, ingrese un valor."
			},
			descripcion_mant: {
				required: "Por favor, ingrese un valor."
			}

		},
		errorPlacement: function(error, element) {
			if ($(element).is("select")) {
				$(error).appendTo($(element).parent().parent());
				$(element).parent().css("border", "1px solid #B60E16");
			}
			else {
				$(error).insertAfter(element);
			}
		}
	});

	$("#nombreTabla").blur(function(){
		var nombreTabla = $.trim($("#nombreTabla").val());
		$("#nombreTabla").val(nombreTabla);
	});

	$("#codigoTabla").blur(function(){
		var codigoTabla = $.trim($("#codigoTabla").val());
		$("#codigoTabla").val(codigoTabla);
	});

	$("#descripcion").blur(function(){
		var descripcion = $.trim($("#descripcion").val());
		$("#descripcion").val(descripcion);
	});

	$("#codmantenimiento").blur(function(){
		var codmantenimiento = $.trim($("#codmantenimiento").val());
		$("#codmantenimiento").val(codmantenimiento);
	});

	$("#mantenimiento").blur(function(){
		var mantenimiento = $.trim($("#mantenimiento").val());
		$("#mantenimiento").val(mantenimiento);
	});

	$("#valor").blur(function(){
		var valor = $.trim($("#valor").val());
		$("#valor").val(valor);
	});

	$("#descripcion_mant").blur(function(){
		var descripcion_mant = $.trim($("#descripcion_mant").val());
		$("#descripcion_mant").val(descripcion_mant);
	});



	$('#idtbl').change(function(){
		$('#div_idtabla').css('border', '');
		$('#idtbl-error').remove();
	});

	$("#newMantenimiento").click(function() {
		typeNum = 1;
		nombTab = 'infoTabla';
		$("#container").show();
		$("#theHeader").html('Nuevo Mantenimiento');
		displayOrHide(false);
		//$('#enviarmant').addClass('btn-primary').removeClass('btn-success').html("Guardar");
		$('#enviarmant').addClass('btn btn-primary').removeClass('btn-success').html("<i class='fa fa-plus'></i>&nbsp; Agregar");
		$("#modalMantenimiento").modal();
	});

	$('#modalMantenimiento').on('hidden.bs.modal', function(e) {
		$('.nav-tabs a[href="#infotabla"]').tab('show');
		$(".nav li#infoMantenimiento").removeClass('active');
		$(".nav li#infoTabla").addClass('active');
		$("#infoMantenimiento").show();
		$('#infoTabla').show();
		// $("#idtbl").select2("val","");
		$(".mentenimientosinputs").val("");
		$('.mentenimientosinputs').removeClass('error');
		$('.error').remove();
		$('div').css('border', '');
		$("#codmantenimiento").prop('disabled', false);
		$("#codigoTabla").prop('disabled', false);
	});

	$('#anclaTabla').on('click', '.cargarInfo', function(e){
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var idtabla = row.eq(0).text();
		cargarMantenimientos(idtabla);
	});

	$('#displayTables').on( 'click', 'tr', function () {
		if ( $(this).hasClass('selected') ) {
			$(this).removeClass('selected');
		}
		else {
			$('#tablaTablasMant').dataTable().$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		nombTab = e.target.id;
		console.log(nombTab);
	});

	$('#displayTables').on('click', '.editTablas', function (e) {
		typeNum = 2;
		nombTab = 'infoTabla';
		$("#theHeader").html('Editar Tabla');
		$('.nav-tabs a[href="#infotabla"]').tab('show');
		$('#infoTabla').show();
		$('#infoMantenimiento').hide();
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#idtabla").val(row.eq(0).text());
		$("#codigoTabla").val(row.eq(1).text());
		$("#codigoTabla").prop('disabled', true);
		$("#nombreTabla").val(row.eq(2).text());
		$("#descripcion").val(row.eq(3).text());
		$("#valor").val(row.eq(4).text());
		displayOrHide(false);
		// $('#enviarmant').addClass('btn-success').removeClass('btn-danger').html("Editar");
		$("#enviarmant").show();
		$("#cancelar").show();
		$('#enviarmant').addClass('btn btn-success').html("<i class='fa fa-floppy-o'></i>&nbsp; Guardar");		
		$("#modalMantenimiento").modal();
	});

	$('#displayTable').on('click', '.editMantenimiento', function (e) {
		typeNum = 2;
		nombTab = 'infoMantenimiento';
		$("#container").hide();
		$('.nav-tabs a[href="#infomantenimientos"]').tab('show');
		$("#theHeader").html('Editar Mantenimiento');
		$('.nav-tabs a[href="#infomantenimientos"]').tab('show');
		$('#infoMantenimiento').show();
		$('#infoTabla').hide();
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#idmantenimiento").val(row.eq(0).text());
		$("#idtbl").select2('val', row.eq(1).text());
		$("#idtbl").prop("disabled", true);
		$("#codmantenimiento").val(row.eq(2).text());
		$("#codmantenimiento").prop('disabled', true);
		$("#mantenimiento").val(row.eq(3).text());
		$("#valor").val(row.eq(4).text());
		$("#descripcion_mant").val(row.eq(5).text());
		displayOrHide(false);
		// $('#enviarmant').addClass('btn-success').removeClass('btn-danger').html("Editar");
		$("#enviarmant").show();
		$("#cancelar").show();
		$('#enviarmant').addClass('btn btn-success').html("<i class='fa fa-floppy-o'></i>&nbsp; Guardar");		
		$("#modalMantenimiento").modal();
	});

	$("#enviarmant").click(function() {
		if (nombTab == 'infoTabla') {
			if (tablaForm.valid())
			tablaForm.submit();
		}
		else {
			if (matenimientoForm.valid())
			 matenimientoForm.submit();
		}
		});

	tablaForm.submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "tablasInsert";
		} else if (typeNum == 2) {
			actividadDir = "tablasUpdate";
		}
		var params = {};
		params.ID_TABLA = $("#idtabla").val();
		params.CODIGO_TABLA = $("#codigoTabla").val();
		params.NOMBRE = $("#nombreTabla").val();
		params.DESCRIPCION = $("#descripcion").val();
		$.post(type+actividadDir, params, function(data) {
			var json = jQuery.parseJSON(data);
			if (json.estado == 1){
			GenerarAlerta(json.mensaje, 'Mantenimiento', 1);
			} else{
			GenerarAlerta(json.mensaje, 'Alerta', 2);
			setTimeout(function(){$("#cerrarMant").click();});
			cargarTablas();
			}		
		});
	});

	$("#infoTabla").click(function() {
		$("#enviarmant").show();
		$("#cancelar").show();
	});

	$("#infoMantenimiento").click(function() {
		$("#enviarmant").hide();
		$("#cancelar").hide();
	});

	$("#agregarMan").click(function() {
		if (matenimientoForm.valid())
			matenimientoForm.submit();

		inputs = ['#codmantenimiento', '#mantenimiento', '#valor', '#descripcion_mant'];
		LimpiarInput(inputs, ['']);

		// $("#idtbl").select2("val","");

		// var params = {};
		// params.ID_MANTENIMIENTO = $("#idmantenimiento").val();
		// params.COD_MANTENIMIENTO = $("#codmantenimiento").val();
		// params.ID_TABLA = $("#idtbl").val();
		// params.NOMBRE = $("#mantenimiento").val();
		// params.VALOR = $("#valor").val();
		// params.DESCRIPCION = $("#descripcion_mant").val();

		// addRow(params, '#tblModulos');
		// console.log(params);
	});

	matenimientoForm.submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "mantenimientosInsert";
		} else if (typeNum == 2) {
			actividadDir = "mantenimientosUpdate";
		}
		var params = {};
		params.ID_MANTENIMIENTO = $("#idmantenimiento").val();
		params.COD_MANTENIMIENTO = $("#codmantenimiento").val();
		params.ID_TABLA = $("#idtbl").val();
		params.NOMBRE = $("#mantenimiento").val();
		params.VALOR = $("#valor").val();
		params.DESCRIPCION = $("#descripcion_mant").val();
		$.post(type+actividadDir, params, function(data) {
			var json = jQuery.parseJSON(data);
			// console.log(json);
			if (json.id != ""){
				console.log(json.id);
				cargarMantenimientoTabla(json.id);
			}

			if(typeNum != 1)
			{
				if (json.estado == 1){
				GenerarAlerta(json.mensaje, 'Mantenimiento', 1);
				} else{
				GenerarAlerta(json.mensaje, 'Alerta', 2);
				setTimeout(function(){$("#cerrarMant").click();});
				console.log(params.ID_TABLA);
				cargarMantenimientos(params.ID_TABLA);
				}
			}	
		});
	});

	function displayOrHide(isDisplaying) {
    	if(isDisplaying) {
     	 	$("#sure").show();
     	 	$(".greatInputContainer").hide();
    	} else {
      		$(".greatInputContainer").show();
      		$("#sure").hide();
    	}
  	}

  	function cargarMantenimientoTabla(id){
		var path = type + "MantenimientoTablaGet";
		var objeto = {};
		objeto.id = id;
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){			
			if (data != false) {
				// console.log(data);
				 addRow(data, "#tblMantenimientos");
			}
		});
	}

	//Este se repiten los datos cuando uno agrega una tabla y luego quiere agregar un módulo a esa tabla

	function cargarTablas(){
		var path = type + "tablasGet";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			LimpiarTablaSimple("#tablaTablasMant");
			// LimpiarSelect($("#idtbl"));
			$("#idtbl").select2("val","");
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var nodo = data[i]
					$("#idtbl").append('<option value="' + nodo.ID_MAINTENANCE_TABLE + '">' + nodo.TABLE_NAME + '</option>');
				}
				var columns = [{"mDataProp":"ID_MAINTENANCE_TABLE", "className":"hidden"},
							{"mDataProp":"TABLE_CODE", "className":"text-left cargarInfo"},
							{"mDataProp":"TABLE_NAME", "className":"text-left cargarInfo"},
							{"mDataProp":"DESCRIPTION", "className":"text-left cargarInfo"},
							{
	                         "targets": -1,
	                          "data": [{"mDataProp":""}],
	                          "defaultContent": "<span class='editTablas btn btn-success botonVED'><i class='icon-edit icon-white fa fa-pencil'></i></span>", "className": "text-center"
	                  		}];
				LoadDataTableSimple(data, '#tablaTablasMant', columns);
			}
		});
	}

	function cargarMantenimientos(idtabla){
		var path = type + "mantenimientosGet";
		var objeto = {};
		objeto.idtabla = idtabla;
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){
			LimpiarTablaSimple("#tablaMantenimientos");			
			if (data != false) {
				var columns = [{"mDataProp":"ID_MAINTENANCE", "className":"hidden"},
								{"mDataProp":"ID_MAINTENANCE_TABLE", "className":"hidden"},
								{"mDataProp":"MAINTENANCE_CODE", "className":"text-left cargarInfo"},
								{"mDataProp":"MAINTENANCE_NAME", "className":"text-left cargarInfo"},
								{"mDataProp":"VALUE", "className":"text-left cargarInfo"},
								{"mDataProp":"DESCRIPTION", "className":"text-left cargarInfo"},
								{
	                          	 "targets": -1,
	                          	 "data": [{"mDataProp":""}],
	                         	 "defaultContent": "<span class='editMantenimiento btn btn-success botonVED'><i class='icon-edit icon-white fa fa-pencil'></i></span><span class='deleteMantenimientoIndex btn btn-danger botonVED'><i class='icon-edit icon-white fa fa-trash-o'></i></span>", "className": "text-center"
	                 			 }];
				LoadDataTableSimple(data, '#tablaMantenimientos', columns);
			}
		});
	}

	function addRow(ArrayData, TableName){
                    var newRow = $(TableName).dataTable().fnAddData([
                               ArrayData['ID_MAINTENANCE'],
                               ArrayData['TABLE_NAME'],
                               ArrayData['MAINTENANCE_CODE'],
                               ArrayData['MAINTENANCE_NAME'],
                               ArrayData['VALUE'],
                               ArrayData['DESCRIPTION'],
                               "<span class='deleteMantenimiento btn btn-danger botonVED btn-sm'><i class='icon-edit icon-white fa fa-trash-o'></i></span>"
                               ]);
                               var theNode = $(TableName).dataTable().fnSettings().aoData[newRow[0]].nTr;
                               theNode.setAttribute('id', ArrayData['ID']);
                               $('td', theNode)[0].setAttribute( 'class', 'hidden id' );   
    }

    $('#tbodyMantenimientos').on('click', '.deleteMantenimiento', function (e) {
    	lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var idMantenimiento = row.eq(0).text();
		
        var path = type + "maintenanceDelete";
		var objeto = {};
		objeto.idMantenimiento = idMantenimiento;
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){
			if (data != false) {
				var tblMantenimientos = $('#tblMantenimientos').DataTable();
				tblMantenimientos.row(row).remove().draw();
			}
		});
        
        //$('#enviarTipoConstancia').addClass('btn-danger').removeClass('btn-primary').html("Eliminar");
	});

// 	$(document).ready(function() {
//   var oTable = $('#example').dataTable();
   
//   // Immediately remove the first row
//   oTable.fnDeleteRow( 0 );
// } );

	$('#displayTable').on('click', '.deleteMantenimientoIndex', function (e) {
    	$("#mantenimientoDelete").modal();
	    $("#theHeader2").html('Eliminar Mantenimiento');
	    displayOrHide(true);

    	lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var idMantenimiento = row.eq(0).text();
		var idTabla = row.eq(1).text();
		$("#nombre_span").html(row.eq(3).text());

		$("#eliminarMantenimiento").click(function(){
    	var path = type + "maintenanceDelete";
		var objeto = {};
		objeto.idMantenimiento = idMantenimiento;
		var posting = $.post(path, objeto);
		posting.done(function(data, status, xhr){			
				if (data.estado == 1){
				GenerarAlerta(data.mensaje, 'Mantenimiento', 1);
				} else{
				GenerarAlerta(data.mensaje, 'Alerta', 2);
				setTimeout(function(){$("#cerrarSure").click();});
				cargarMantenimientos(idTabla);
				}
			});
		});
	});
});