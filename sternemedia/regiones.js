$(document).ready(function (){
	var type = urljs+"index.php/regiones/";
	var typeNum = 0;
	var lastRow = null;
	var regionesForm = $('#regionesForm');
	var deptoForm = $("#deptoForm");
	var munForm = $("#munForm");
	$('#cod_pais').select2();
	$('#cod_depto').select2();
	$('#cod').select2();
	CargarRegiones();
	cargarPais();

	regionesForm.validate({
		rules: {
			nombrePais: {
				required: true,
				Letrascontildes: true
			}
		},
		messages: {
		}
	});

	deptoForm.validate({
		rules: {
			cod_pais: {
				required: true
			},
			nombre_departamento:{
				required: true,
				Letrascontildes: true
			}
		},
		messages:{
		},
		errorPlacement: function(error, element) {
			//si el elemento es un select
			if ($(element).is("select")) {
				// colocar el error al final del contenedor
				$(error).appendTo($(element).parent().parent());
				// resaltar el borde del contenedor del select
				$(element).parent().css("border", "1px solid #B60E16");
			}
			else {
				// colocar el error después del elemento
				$(error).insertAfter(element);
			}
		}
	});
	munForm.validate({
		rules: {
			cod: {
				required: true
			},
			cod_depto:{
				required: true
			},
			nombre_muni: {
				required: true,
				Letrascontildes: true
			}
		},
		messages:{
		},
		errorPlacement: function(error, element) {
			//si el elemento es un select
			if ($(element).is("select")) {
				// colocar el error al final del contenedor
				$(error).appendTo($(element).parent().parent());
				// resaltar el borde del contenedor del select
				$(element).parent().css("border", "1px solid #B60E16");
			}
			else {
				// colocar el error después del elemento
				$(error).insertAfter(element);
			}
		}
	});

	$('#cod_pais').change(function(){
		$('#div_cod_pais').css('border', '');
		$('#cod_pais-error').remove();
	});

	$('#cod').change(function(){
		$('#div_cod').css('border', '');
		$('#cod-error').remove();
	});

	$('#cod_depto').change(function(){
		$('#div_cod_depto').css('border', '');
		$('#cod_depto-error').remove();
	});

	$("#nombrePais").blur(function(){
		var nombrePais = $.trim($("#nombrePais").val());
		$("#nombrePais").val(nombrePais);
	});
	$("#nombre_departamento").blur(function(){
		var nombre_departamento = $.trim($("#nombre_departamento").val());
		$("#nombre_departamento").val(nombre_departamento);
	});
	$("#nombre_muni").blur(function(){
		var nombre_muni  = $.trim($("#nombre_muni").val());
		$("#nombre_muni").val(nombre_muni);
	});

	function displayOrHide(isDisplaying) {
    if(isDisplaying) {
      $("#sure").show();
      $(".greatInputContainer").hide();
    } else {
      $(".greatInputContainer").show();
      $("#sure").hide();
    }
  }
	$('#modalRegiones').on('hidden.bs.modal', function(e) {
		$('.nav-tabs a[href="#infoprincipalMenu"]').tab('show');
		$("#cod_pais").select2("val","");
		$("#cod").select2("val","");
		$("#cod_depto").select2("val", "");
		$(".regionesinputs").val("");
		$('.regionesinputs').removeClass('error');
		$('.error').remove();
		$('div').css('border', '');
	});
	$('#displayTable').on('click', '.editRegiones', function (e) {
		typeNum = 1;
		$("#theHeader").html('Editar Región');
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$("#codpais").val(row.eq(0).text());
		$("#coddepto").val(row.eq(1).text());
		$("#codmun").val(row.eq(2).text());
		$("#modalRegiones").attr("data-id-depto", row.eq(1).text());
		$("#cod_pais").select2("val",row.eq(0).text());
		$("#cod").select2("val",row.eq(0).text());
		$("#nombrePais").val(row.eq(3).text());
		$("#nombre_departamento").val(row.eq(4).text());
		$("#nombre_muni").val(row.eq(5).text());
		displayOrHide(false);
		$('#enviarRegion').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		$('#enviardepto').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		$('#enviarmun').addClass('btn-primary').removeClass('btn-danger').html("Editar");		
		$("#modalRegiones").modal();
	});
	
	$("#newRegion").click(function() {
		typeNum = 2;
		$("#theHeader").html('Nueva Región');
		displayOrHide(false);
		$('#enviarRegion').addClass('btn-primary').removeClass('btn-danger').html("Guardar");
		$('#enviardepto').addClass('btn-primary').removeClass('btn-danger').html("Guardar");
		$('#enviarmun').addClass('btn-primary').removeClass('btn-danger').html("Guardar");
		$("#modalRegiones").modal();
	});
	$("#enviarRegion").click(function() {
		if (regionesForm.valid())
			regionesForm.submit();
	});

	regionesForm.submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "paisUpdate";
		} else if (typeNum == 2) {
			actividadDir = "paisInsert";
		}
		var params = {};
		params.COD_PAIS = $("#codpais").val();
		params.NOMBRE = $("#nombrePais").val();
		//var paisName = JSON.stringify(params);
		$.post(type+actividadDir, params, function(data) {
			var json = jQuery.parseJSON(data);
			if (json.estado == 1){
			GenerarAlerta(json.mensaje, 'Regiones', 1);
			} else{
			GenerarAlerta(json.mensaje, 'Alerta', 2);
			CargarRegiones();
			cargarPais();
			setTimeout(function(){$("#cerrarRegion").click();});
			}		
		});
	});

	$("#newDepto").click(function() {
		typeNum = 2;
		$("#theHeader").html('Nueva Región');
		displayOrHide(false);
		$('#enviardepto').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$("#modalRegiones").modal();
	});

	$("#enviardepto").click(function() {
		if (deptoForm.valid())
			deptoForm.submit();
	});

	deptoForm.submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "deptoUpdate";
		} else if (typeNum == 2) {
			actividadDir = "deptoInsert";
		}
		var params = {};
		params.COD_DEPTO = $("#coddepto").val();
		params.NOMBRE = $("#nombre_departamento").val();
		params.COD_PAIS = $("#cod_pais").val();
		var deptoName = JSON.stringify(params);
		$.post(type+actividadDir, params, function(data) {
			var json = jQuery.parseJSON(data);
			if (json.estado == 1){
			GenerarAlerta(json.mensaje, 'Regiones', 1);
			} else{
			GenerarAlerta(json.mensaje, 'Alerta', 2);
			CargarRegiones();
			setTimeout(function(){$("#cerrarRegion").click();});
			}
		});
	});

	$("#newMunicipio").click(function() {
		typeNum = 2;
		$("#theHeader").html('Nueva Región');
		displayOrHide(false);
		$('#enviarmun').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$("#modalRegiones").modal();
	});

	$("#enviarmun").click(function() {
		if(munForm.valid())
			munForm.submit();
	});

	munForm.submit(function(event){
		event.preventDefault();
		var actividadDir = "";
		if (typeNum == 1) {
			actividadDir = "muniUpdate";
		} else if (typeNum == 2) {
			actividadDir = "muniInsert";
		}
		var params = {};
		params.COD_MUN = $("#codmun").val();
		params.NOMBRE = $("#nombre_muni").val();
		params.COD_DEPTO = $("#cod_depto").val();
		var muniName = JSON.stringify(params);
		$.post(type+actividadDir, params, function(data) {
			var json = jQuery.parseJSON(data);
			if (json.estado == 1){
			GenerarAlerta(json.mensaje, 'Regiones', 1);
			} else{
			GenerarAlerta(json.mensaje, 'Alerta', 2);
			CargarRegiones();
			setTimeout(function(){$("#cerrarRegion").click();});
			}
		});
	});

	$('#cod').change(function(){
		limpiarListaDepartamentos();
		// obtener el valor del país seleccionado
		var idPais = $(this).val();
		// si el valor eleccionado no es nulo
		if (idPais != "")
			// cargar los departamentos del país seleccionados
			cargardepto(idPais);
	});

	function limpiarListaDepartamentos()
	{
		$("#cod_depto")
				.find("option")
				.remove()
				.end()
				.append('<option value="">Seleccionar...</option>')
				.val("");
	}

	function limpiarListaPaises(id)
	{
		$(id)
				.find("option")
				.remove()
				.end()
				.append('<option value="">Seleccionar...</option>')
				.val("");
	}

	function cargardepto(id) {
		var metodo = type + 'getDeptosPais/' + id;
		$.get(metodo, null, function(data) {
			var deptos = $.parseJSON(data);
			for (var i = 0; i < deptos.length; i++) {
				var depto = deptos[i];
				$("#cod_depto").append('<option value="' + depto.COD_DEPTO + '">' + depto.NOMBRE + '</option>');
			}
			
			var idDepto = $("#modalRegiones").attr("data-id-depto");
			if (idDepto != ""){
				$("#cod_depto").select2("val",idDepto);
				$("#modalRegiones").attr("val","");
			}
		});
	}

	function cargarPais() {
		var metodo = type + 'paisesGet';
		var posting = $.post(metodo, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			limpiarListaPaises("#cod");
			limpiarListaPaises("#cod_pais");
			$("#cod_pais").select2("val","");
			$("#cod").select2("val","");
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var paises = data[i]
					$("#cod_pais").append('<option value="' + paises.COD_PAIS + '">' + paises.NOMBRE + '</option>');	
					$("#cod").append('<option value="' + paises.COD_PAIS + '">' + paises.NOMBRE + '</option>');

				}
			}
		});
	}

	function CargarRegiones() 
	{
		var path = type + "regionesGet";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function (data, status, xhr) {
			LimpiarTabla('#tablaRegiones');
			if (data != false) {
				var columns = [{ "mDataProp": "COD_PAIS", "className": "hidden id"},
                      {"mDataProp":"COD_DEPTO", "className":"hidden id"},
                      {"mDataProp":"COD_MUN", "className":"hidden id"},
                      {"mDataProp":"PAIS", "className":"text-center" },
                      {"mDataProp":"DEPARTAMENTO", "className":"text-center" },
                      {"mDataProp":"MUNICIPIO", "className":"text-center" },
                      {
	                          //Agregamos un button que servira para editar los registros.
	                          //<span class='editUserperNod btn btn-success botonVED' data-toggle='tooltip' data-placement='left' title='Ver usuarios por Nodos'><i class='icon-edit icon-white fa fa-tags'></i></span>
	                          //<span class='editAsignaciones btn btn-primary botonVED' data-toggle='tooltip' data-placement='right' title='Ver nodos por usuarios'><i class='icon-edit icon-white fa fa-users'></i></span> 
	                          "targets": -1,
	                          "data": [{"mDataProp":""}],
	                          "defaultContent": "<span class='editRegiones btn btn-primary botonVED'><i class='icon-edit icon-white fa fa-pencil'></i></span>", "className": "text-center"
	                  }];

			LoadDataTable(data, '#tablaRegiones', columns);
			}
		});
		posting.fail(function (data, status, xhr) {
			console.log(data);
		});
	}
});
