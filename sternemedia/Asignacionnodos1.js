$(document).ready(function(){
var type = urljs + "index.php/Asignacionnodos/";
var typeNum = 0;
var isNodoToUser = false;
var isEditNodo = false;
var selected = [];
var arraySelected = [];
var IdNodo;
var IdUser;
// $("#nodo").select2();
// $("#user").select2();
$('#guardarAsignaciones').prop('disabled', true);
var asignacionesForm = $('#asignacionesForm');
// cargarUsuarios();
CargarNodos();

asignacionesForm.validate({
	rules:{
		// user:{
		// 	required: true,
		// },
		// nodo:{
		// 	required: true,
		// },
		FechaInicial:{
			required: false,
		},
		FechaFinal:{
			required: false,
		},
		descripcion:{
			required: false,
		},
	},
	messages:{

	},
	errorPlacement: function(error, element) {
		//si el elemento es un select
		if ($(element).is("select")) {
			// colocar el error al final del contenedor
			$(error).appendTo($(element).parent().parent());
			// resaltar el borde del contenedor del select
			$(element).parent().css("border", "1px solid #B60E16");
		}
		else {
			// colocar el error después del elemento
			$(error).insertAfter(element);
		}
	}
});



	$(function(){
		$('#FechaInicial').datepicker({
			autoclose: true,
			language: 'es',
			startDate: new Date(),
			today:'today'
		});

		$('#FechaFinal').datepicker({
			autoclose: true,
			language: 'es',
			startDate: new Date(),
			today:'today'
		});
	});

	$('#nodo').change(function(){
		$('#divNodo').css('border', '');
		$('#nodo-error').remove();
		$('#user').select2('val','');
		var valnodo = $('#nodo').val();
		if (valnodo != '') {
			cargarUsuariosId($('#nodo').val());
		}else{
			$('#user').empty();	
		}
	});

	$('#user').change(function(){
		$('#divUser').css('border', '');
		$('#user-error').remove();
	});

	$('#FechaInicial').change(function(){
		$('#FechaInicial').removeClass('error');
		$('#FechaInicial-error').remove();
	});

	$('#FechaFinal').change(function(){
		$('#FechaFinal').removeClass('error');
		$('#FechaFinal-error').remove();
	});

	$('#descripcion').blur(function(){
		var descripcion = $.trim($('#descripcion').val());
		$('#descripcion').val(descripcion);
	});

  	$('#modalAsignacion').on('hidden.bs.modal', function(e) {
  		LimpiarModal();
		LimpiarTabla('#TblNodoId');
		LimpiarTabla('#TblUserId');
		// $('#guardarAsignaciones').prop('disabled', true);
		// $("#nodo").prop('disabled', true);
	});

	$('#newAsignacion').click(function(){
		typeNum = 1;
		isNodoToUser = true;
		table = $("#TblNodoId").dataTable();
		table2 = $("#TblUserId").dataTable();
    	LimpiarTablaSimple(table2);
		$('#theHeader').html('Nueva asignación de nodos.');
		$("#nodo").prop('disabled', false);
		$("#user").prop('disabled', false);
		table.$('tr.selected').removeClass('selected');
		cargarNodosId();
		$('#enviarAsignacion').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$('#modalAsignacion').modal();
		$('#displayTableUser').on( 'click', 'tr', function () {
			var id = this.id
			var index = $.inArray(id, selected);

        		if (index === -1) {
            		selected.push(id);
        		} else {
           			selected.splice(index, 1);
        		}

       		$(this).toggleClass('selected');
    	});
    	$('#displayTables').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
            	$(this).removeClass('selected');
        	}
       	 	else {
            	$('#TblNodoId').dataTable().$('tr.selected').removeClass('selected');
            	$(this).addClass('selected');
        	}
    	});
    	$('#displayTables').on('click', '.cargarInfo', function(e){
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var imei = row.eq(0).text();
		IdNodo = imei;
		cargarUsuariosId(imei);
	});
	});

	$('#newAsignacionUser').click(function(){
		typeNum = 1;
		isNodoToUser = false;
		table = $("#TblNodoId").dataTable();
		table2 = $("#TblUserId").dataTable();
    	LimpiarTablaSimple(table);
		$('#theHeader').html('Nueva asignación de nodos.');
		table2.$('tr.selected').removeClass('selected');
		cargarUsuarios();
		$('#enviarAsignacion').addClass('btn-primary').removeClass('btn-danger').html("Agregar");
		$('#modalAsignacion').modal();
		$('#displayTables').on( 'click', 'tr', function () {
			var id = this.id
			var index = $.inArray(id, selected);

        		if (index === -1) {
            		selected.push(id);
        		} else {
           			selected.splice(index, 1);
        		}

       		$(this).toggleClass('selected');
    	});
    	$('#displayTableUser').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
            	$(this).removeClass('selected');
        	}
       	 	else {
            	$('#TblUserId').dataTable().$('tr.selected').removeClass('selected');
            	$(this).addClass('selected');
        	}
    	});
    	$('#displayTableUser').on('click', '.cargarInfo', function(e){
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var user = row.eq(0).text();
		IdUser = user;
		cargarNodosUser(user);
	});
	});

	$('#enviarAsignacion').click(function(){
		// if (asignacionesForm.valid())
		// asignacionesForm.submit();
	});

	asignacionesForm.submit(function(event){
		event.preventDefault();
		// var fechasvalidas = validaCamposFecha($('#FechaInicial').val(), $('#FechaFinal').val());

		// if (fechasvalidas == false) {
		// 	GenerarAlerta('La fecha de inicio debe ser menor que la fecha de finalización','Asignacion',3);
		// } else {
			var actividad = "";
			if (typeNum == 1) {
				actividad = "AsignacionNodoInsert";
			} else if(typeNum == 2){
				actividad = "AsignacionNodoUpdate";
			}

			if (isNodoToUser == true) {
				DataTableSelected("#TblUserId");
				if (arraySelected.length != 0) {
				var fila;
				var arreglo = [];
				for (var i = arraySelected.length - 1; i >= 0; i--) {
				 	fila = arraySelected[i];
				 	var parametros = {};
				 	// parametros.auto_id = $('#auto_id').val();
				 	parametros.usuario = fila.U_ID;
				 	parametros.nodo = IdNodo;
				 	parametros.FechaInicial = $('#FechaInicial').val();
				 	parametros.FechaFinal = $('#FechaFinal').val();
				 	parametros.descripcion = $('#descripcion').val();
				 	arreglo.push(parametros);
				}

				var path = type + actividad;
				var dataArray = JSON.stringify(arreglo);
				var posting = $.post(path, {"arreglo": arreglo});
				posting.done(function (data, status, xhr) {
					var json = jQuery.parseJSON(data);
					if (json.estado == 1){
						GenerarAlerta(json.mensaje, 'Asignacion', 3);
					} else{
						if (data != false) {
							CargarNodos();
							setTimeout(function(){$("#cerrarModalAsignacion").click();}, 2000);
						}
					GenerarAlerta(json.mensaje, 'Asignacion', 2);
					}	
				});
				posting.fail(function (data, status, xhr) {
					console.log(data);
				// sendError(data);
				// GenerarErrorAlerta(xhr, "error");
				// goAlert();
				});
			
				posting.always(function (data, status, xhr) {
				// RemoveAnimation("body");
				//console.log(data);
				});
			// }
		} else {
			GenerarAlerta('Por favor, seleccione un elemento.', 'Asignacion', 3);
		}
			} else {
				DataTableSelected("#TblNodoId");
				if (arraySelected.length != 0) {
				var fila;
				var arreglo = [];
				for (var i = arraySelected.length - 1; i >= 0; i--) {
				 	fila = arraySelected[i];
				 	var parametros = {};
				 	// parametros.auto_id = $('#auto_id').val();
				 	parametros.usuario = IdUser;
				 	parametros.nodo = fila.IMEI;
				 	parametros.FechaInicial = $('#FechaInicial').val();
				 	parametros.FechaFinal = $('#FechaFinal').val();
				 	parametros.descripcion = $('#descripcion').val();
				 	arreglo.push(parametros);
				}

				var path = type + actividad;
				var dataArray = JSON.stringify(arreglo);
				var posting = $.post(path, {"arreglo": arreglo});
				posting.done(function (data, status, xhr) {
					var json = jQuery.parseJSON(data);
					if (json.estado == 1){
						GenerarAlerta(json.mensaje, 'Asignacion', 3);
					} else{
						if (data != false) {
							CargarNodos();
							setTimeout(function(){$("#cerrarModalAsignacion").click();}, 2000);
						}
					GenerarAlerta(json.mensaje, 'Asignacion', 2);
					}	
				});
				posting.fail(function (data, status, xhr) {
					console.log(data);
				// sendError(data);
				// GenerarErrorAlerta(xhr, "error");
				// goAlert();
				});
			
				posting.always(function (data, status, xhr) {
				// RemoveAnimation("body");
				//console.log(data);
				});
			// }
		} else {
			GenerarAlerta('Por favor, seleccione un elemento.', 'Asignacion', 3);
		}
			}
			// DataTableSelected();
			
	});

	$('#guardarAsig').click(function(){
		if (asignacionesForm.valid())
		asignacionesForm.submit();
	});

	// $('#displayTables').on('click', '.cargarInfo', function(e){
	// 	lastRow = $(this).closest('tr');
	// 	var row = lastRow.find("td");
	// 	var imei = row.eq(0).text();
	// 	IdNodo = imei;
	// 	cargarUsuariosId(imei);
	// });

	// $('#displayTables').on( 'click', 'tr', function () {
	// 	if ( $(this).hasClass('selected') ) {
 //            $(this).removeClass('selected');
 //        }
 //        else {
 //            $('#TblNodoId').dataTable().$('tr.selected').removeClass('selected');
 //            $(this).addClass('selected');
 //        }
 //    });

    $("#TblUserId").dataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "autoWidth": true,
      "info": false,
      "language": languageConf,
    	"rowCallBack": function(row, data){
    		if ($.inArray(data.DT_RowID, selected) !== -1) {
    			$(row).addClass('selected');
    		}
    	}

    });

    $("#TblNodoId").dataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "autoWidth": true,
      "info": false,
      "language": languageConf,
    	"rowCallBack": function(row, data){
    		if ($.inArray(data.DT_RowID, selected) !== -1) {
    			$(row).addClass('selected');
    		}
    	}

    });

	// $('#displayTableUser').on( 'click', 'tr', function () {
	// 	var id = this.id
	// 	var index = $.inArray(id, selected);

 //        	if (index === -1) {
 //            	selected.push(id);
 //        	} else {
 //           		selected.splice(index, 1);
 //        	}

 //       $(this).toggleClass('selected');
 //    });
 
    // $('#button').click( function () {
    //     table.row('.selected').remove().draw( false );
    // } );

	$('#displayTable').on('click', '.editAsignaciones', function(e){
		typeNum = 2;
		isEditNodo = true;
		cargarNodosId();
		$('#theHeader').html('Editar asignación de nodo.');
		$("#nodo").prop('disabled', true);
		$("#user").prop('disabled', false);
		$("#user").removeAttr('multiple');
      	lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var imei = row.eq(1).text();
		$('#auto_id').val(row.eq(0).text());
		$('#nodo').select2("val", imei);
		$('#user').select2("val", row.eq(2).text());
		$('#FechaInicial').val(row.eq(4).text());
		$('#FechaFinal').val(row.eq(5).text());
		$('#descripcion').val(row.eq(6).text());
		$('#enviarAsignacion').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		cargarNodosporUsuario(imei);
		$('#modalAsignacionNodos').modal();
	});

	$('#displayTable').on('click', '.editUserperNod', function(e){
		typeNum = 2;
		isEditNodo = false;
		cargarUsuarios();
		$('#theHeader').html('Editar asignación de nodo.');
		$("#nodo").prop('disabled', false);
		$("#user").removeAttr('multiple');
		$("#user").removeClass('selectpicker');
		$("#user").prop('disabled', true);
      	lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var user = row.eq(2).text();
		cargarNodosUser(user);
		$('#auto_id').val(row.eq(0).text());
		$('#nodo').select2("val", row.eq(1).text());
		$('#user').select2("val", user);
		$('#FechaInicial').val(row.eq(4).text());
		$('#FechaFinal').val(row.eq(5).text());
		$('#descripcion').val(row.eq(6).text());
		$('#enviarAsignacion').addClass('btn-primary').removeClass('btn-danger').html("Editar");
		cargarUsuariosporNodo(user);
		$('#modalAsignacionNodos').modal();
	});

	$('#displayTable2').on('click', '.editAsignacion', function(e){
		typeNum = 2;
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		$('div').animate({
      		scrollTop: $("#modalAsignacionNodos").offset().top
  		});
		var imei = row.eq(1).text();
		$('#auto_id').val(row.eq(0).text());
		$('#nodo').select2("val", imei);
		$('#user').select2("val", row.eq(2).text());
		$('#FechaInicial').val(row.eq(4).text());
		$('#FechaFinal').val(row.eq(5).text());
		$('#descripcion').val(row.eq(6).text());
	});

	$('#displayTable').on('click', '.deleteAsignacion', function(e){
		lastRow = $(this).closest('tr');
		var row = lastRow.find("td");
		var auto_id = row.eq(0).text();
		var imei = row.eq(1).text();
		var parametros = {};
		parametros.auto_id = auto_id;

		var path = type + 'AsignacionNodoDelete';
		var posting = $.post(path, parametros);
		posting.done(function (data, status, xhr) {
			var json = jQuery.parseJSON(data);
			if (data != false) {
				CargarNodos();
				// cargarNodosporUsuario(imei);
				// LimpiarModal();
				$('#guardarAsignaciones').prop('disabled', false);

			}
			GenerarAlerta(json.mensaje, 'Asignaciones', 3);
		});
		posting.fail(function (data, status, xhr) {
			console.log(data);
		});
		posting.always(function (data, status, xhr) {
			console.log(data);
		})
	});

	function CargarNodos() 
	{
		// /animacion de loading/
		// LoadAnimation("body");
		var path = type + "obtenerNodos";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function (data, status, xhr) {
			LimpiarTabla('#TblNodos');
			if (data != false) {
				var columns = [{ "mDataProp": "AUTO_ID", "className": "hidden id"},
                      {"mDataProp":"IMEI", "className":"text-center"},
                      {"mDataProp":"COD_US", "className":"hidden id"},
                      {"mDataProp":"USUARIO", "className":"text-center" },
                      {"mDataProp":"FECHA_INICIAL", "className":"text-center" },
                      {"mDataProp":"FECHA_FINAL", "className":"text-center" },
                      {"mDataProp":"DESCRIPCION", "className":"hidden id" },
                      {
	                          //Agregamos un button que servira para editar los registros.
	                          //<span class='editUserperNod btn btn-success botonVED' data-toggle='tooltip' data-placement='left' title='Ver usuarios por Nodos'><i class='icon-edit icon-white fa fa-tags'></i></span>
	                          //<span class='editAsignaciones btn btn-primary botonVED' data-toggle='tooltip' data-placement='right' title='Ver nodos por usuarios'><i class='icon-edit icon-white fa fa-users'></i></span> 
	                          "targets": -1,
	                          "data": [{"mDataProp":""}],
	                          "defaultContent": "<span class='deleteAsignacion btn btn-danger botonVED' data-toggle='tooltip' data-placement='right' title='Eliminar'><i class='icon-edit icon-white fa fa-trash'></i></span>", "className": "text-center"
	                  }];

			LoadDataTable(data, '#TblNodos', columns);
			}
		});
		posting.fail(function (data, status, xhr) {
			console.log(data);
			// sendError(data);
			//GenerarErrorAlerta(xhr, "error");
			// goAlert();
		});
		posting.always(function (data, status, xhr) {
			// RemoveAnimation("body");
			// console.log(data);
		})
	}

	function cargarNodosporUsuario(imei){
		var direccion = type + "obtenerNodosporUsuarios";
		obj = {}
		obj.IMEI = imei;
		var posting = $.post(direccion, obj);
		posting.done(function(data,status,xhr){
			LimpiarTablaSimple('#TblNodoId');
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var columns = [{"mDataProp":"IMEI" , "className":"text-center"}];
					LoadDataTableSimple(data, '#TblNodoId', columns);
				}
			}
		});
	}

	function cargarUsuariosporNodo(user){
		var direccion = type + "obtenerUsuariosporNodos";
		obj = {}
		obj.USER = user;
		var posting = $.post(direccion, obj);
		posting.done(function(data,status,xhr){
			LimpiarTabla('#TblNodo');
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					addRowNodo(data[i], "#TblNodo");
				}
			}
		});
	}

	function cargarNodosId(){
		var path = type + "obtenerNodosId";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function(data, status,xhr){
			if (data != false){
					LimpiarTablaSimple('#TblNodoId')
				for (var i = data.length - 1; i >= 0; i--) {
					var columns = [{"mDataProp":"IMEI", "className":"hidden"},
									{"mDataProp":"DESCRIPCION", "className":"text-left cargarInfo",
									"mRender": function(data, type, full){
										return full.IMEI +' - '+ full.DESCRIPCION;
									}
								}
					];
					LoadDataTableSimple(data, '#TblNodoId', columns);
				}
			}
		});
	}

	function cargarNodosUser(user){
		var path = type + "obtenerNodosUser";
		obj = {}
		obj.user = user;
		var posting = $.post(path, obj);
		posting.done(function(data, status, xhr){
			if (data != false){
					LimpiarTablaSimple('#TblNodoId')
				for (var i = data.length - 1; i >= 0; i--) {
					var columns = [{"mDataProp":"IMEI", "className":"hidden"},
									{"mDataProp":"DESCRIPCION", "className":"text-left cargarInfo",
									"mRender": function(data, type, full){
										return full.IMEI +', '+ full.DESCRIPCION;
									}
								}
					];
					LoadDataTableSimple(data, '#TblNodoId', columns);
				}
			}
		});
	}

	function cargarUsuariosId(nodo){
		var path = type + "obtenerUsuariosId";
		obj = {}
		obj.nodo = nodo;
		var posting = $.post(path, obj);
		posting.done(function(data, status, xhr){
			LimpiarTablaSimple("#TblUserId");	
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var columns = [{"mDataProp":"U_ID", "className":"hidden"},
								{"mDataProp":"NAME", "className":"text-left cargarInfo"}];
					LoadDataTableSimple(data, '#TblUserId', columns);					
				}
			}
		});
	}

	function cargarUsuarios(){
		var path = type + "obtenerUsuarios";
		var posting = $.post(path, {param1: 'value1'});
		posting.done(function(data, status, xhr){
			LimpiarTablaSimple("#TblUserId");			
			if (data != false) {
				for (var i = data.length - 1; i >= 0; i--) {
					var columns = [{"mDataProp":"U_ID", "className":"hidden"},
								{"mDataProp":"NAME", "className":"text-left cargarInfo"}];
					LoadDataTableSimple(data, '#TblUserId', columns);
				}
			}
		});
	}
		
	function addRowNodo(ArrayData, TableName){
		var newRow = $(TableName).dataTable().fnAddData([
			ArrayData['AUTO_ID'],
			ArrayData['IMEI'],
			ArrayData['COD_US'],
			ArrayData['USUARIO'],
			ArrayData['FECHA_INICIAL'],
			ArrayData['FECHA_FINAL'],
			ArrayData['DESCRIPCION'],
			"<span class='editAsignacion btn btn-success botonVED' data-toggle='tooltip' data-placement='right' title='Editar'><i class='icon-edit icon-white fa fa-pencil'></i></span><span class='deleteAsignacion btn btn-danger botonVED' data-toggle='tooltip' data-placement='right' title='Eliminar'><i class='icon-edit icon-white fa fa-trash'></i></span>"
		]);
		var theNode = $(TableName).dataTable().fnSettings().aoData[newRow[0]].nTr;
		theNode.setAttribute('id', ArrayData['AUTO_ID']);
		$('td', theNode)[0].setAttribute( 'class', 'hidden' );
		$('td', theNode)[2].setAttribute( 'class', 'hidden' );
		$('td', theNode)[6].setAttribute( 'class', 'hidden' );	
	}

	function addRowNodoId(ArrayData, TableName){
		var newRow = $(TableName).dataTable().fnAddData([
			ArrayData['IMEI']
		]);
		var theNode = $(TableName).dataTable().fnSettings().aoData[newRow[0]].nTr;
		theNode.setAttribute('id', ArrayData['IMEI']);	
	}

	function addRowNodoUser(ArrayData, TableName){
		var newRow = $(TableName).dataTable().fnAddData([
			ArrayData['U_ID'],
			ArrayData['NAME']
		]);
		var theNode = $(TableName).dataTable().fnSettings().aoData[newRow[0]].nTr;
		theNode.setAttribute('id', ArrayData['U_ID']);
		$('td', theNode)[0].setAttribute( 'class', 'hidden' );
	}
	function LimpiarModal(){
		// $('#nodo').select2('val','');
		// $('#user').select2('val','');
		$('#FechaInicial').datepicker('clearDates');
		$('#FechaFinal').datepicker('clearDates');
		// $('#auto_id').val('');
		$('.asignacionesinput').val('');
		$(".asignacionesinput").removeClass('error');
		// LimpiarTablaSimple("#TblNodoId");
		// LimpiarTablaSimple("#TblUserId");
		$('div').css('border', '');
		$(".error").remove();
	}

	function DataTableSelected(tableSelected){
		var table = $(tableSelected).DataTable();
		var rows = $(tableSelected).dataTable().fnSettings().fnRecordsTotal();
    	var total = table.rows('.selected').data().length;
    	arraySelected = [];
    	if (rows > 0){
    		if (total > 0){
    			table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
    				var mydata = this.data();
          			var str = this.node().className;
          			var res = str.split(" ");
          			for (var ii = res.length - 1; ii >= 0; ii--){
          				if(res[ii] == "selected"){
          					arraySelected.push(mydata);
          				}
          			}
    			})
    		}
    	}
    }

    function DataTableSelectedTblNodo(table){
		var table = $("#TblNodoId").DataTable();
		var rows = $("#TblNodoId").dataTable().fnSettings().fnRecordsTotal();
    	var total = table.rows('.selected').data().length;
    	arraySelected = [];
    	if (rows > 0){
    		if (total > 0){
    			table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
    				var mydata = this.data();
          			var str = this.node().className;
          			var res = str.split(" ");
          			for (var ii = res.length - 1; ii >= 0; ii--){
          				if(res[ii] == "selected"){
          					arraySelected.push(mydata);
          				}
          			}
    			})
    		}
    	}
    }



});