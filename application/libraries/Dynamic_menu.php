<?php class Dynamic_menu {
  private $ci;
  function __construct() {
    $this->ci =& get_instance();
  }
  function build_menu($uniqueid, $base_url, $table = 'VW_MENU') {
    $menu = array();
    $query = $this->ci->db->get_where($table, array('UNIQUE_ID' => $uniqueid));
    if ($query->num_rows() > 0) {
      $count = 1;
      foreach ($query->result() as $row) {
        $menu[$count]['id'] = $row->IDS;
        $menu[$count]['title'] = $row->TITLE;
        $menu[$count]['link'] = $row->LINK_TYPE;
        $menu[$count]['page'] = $row->PAGEID;
        $menu[$count]['module'] = $row->MODULE_NAME;
        $menu[$count]['moduleid'] = $row->MODULEID;
        $menu[$count]['url'] = $row->URL;
        $menu[$count]['uri'] = $row->URI;
        $menu[$count]['dyn_group'] = $row->DYN_GROUP_ID;
        $menu[$count]['position'] = $row->POSITION;
        $menu[$count]['target'] = $row->TARGET;
        $menu[$count]['parent'] = $row->PARENT_ID;
        $menu[$count]['is_parent'] = $row->IS_PARENT;
        $menu[$count]['show'] = $row->SHOW_MENU;
        $menu[$count]['icon'] = $row->ICON;
        $count++;
      }
    }
    $query->free_result();
    $html_out = "";
    for ($i = 1; $i <= count($menu); $i++) {
      if ($menu[$i]['show'] && $menu[$i]['parent'] == 0) {
        $html_out .= '<li class="treeview"><a href=""><i class="'.$menu[$i]['icon'].'"></i> <span>'.$menu[$i]['title'].'</span> <i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu">';
        $html_out .= $this->get_childs($menu, $menu[$i]['id']);
        $html_out .= '</ul>';
        $html_out .= '</li>'."\n";
      }
    }
    $html_out .= '<li class="treeview"><a href=""><i class="fa fa-info-circle"></i> <span>A cerca de</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu"><li><a href="#/main" data-toggle="modal" data-target="#versionModal"><span>Versión</span></a></li><li><a id="ayuda" href="' . $base_url . 'img/ST_Manual de Usuarios Quality Software_08062018 V1R0.pdf" target="_blank" class="ex">Ayuda</a></li>';
    $html_out .= "\t\t".'</ul>'."\n";
    $html_out .= "\t".'</div>'."\n";
    return $html_out;
  }
  function get_childs($menu, $parent_id) {
    $has_subcats = FALSE;
    $html_out = "";
    for ($i = 1; $i <= count($menu); $i++) {
      if ($menu[$i]['show'] && $menu[$i]['parent'] == $parent_id) {
        $has_subcats = TRUE;
        $html_out .= "\t\t\t\t\t\t".'<li>'.anchor($menu[$i]['url'], '<span>'.$menu[$i]['title'].'</span>');
      } // Posible Break
    }
    return ($has_subcats) ? $html_out : FALSE;
  }
} ?>
