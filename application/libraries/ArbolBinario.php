<?php
require_once('nodoarbolbinario.php');
class ArbolBinario {
  private $raiz;
  private $niveles = array();
  public function __construct() {
    $this->raiz = new nodoarbolbinario();
  }
  public function insertarHoja($value, $almacen, $subtree = false)
  {
    if (is_null($subtree)) {
      $subtree = $this->raiz;
    } elseif ($subtree == false) {
      $subtree = $this->raiz;
    }
    if ($subtree->getValor() == false) {
      $subtree->setValor($value);
      $subtree->setAlmacen($almacen);
    } else {
      if ($value > $subtree->getValor()) {
        if (is_null($subtree->getMayor())) {
          $nuevo = new nodoarbolbinario($value);
          $nuevo->setAlmacen($almacen);
          $subtree->setMayor($nuevo);
        } else {
          $this->insertarHoja($value, $almacen, $subtree->getMayor());
        }
      } elseif ($value < $subtree->getValor()) {
        if (is_null($subtree->getMenor())) {
          $nuevo = new nodoarbolbinario($value);
          $nuevo->setAlmacen($almacen);
          $subtree->setMenor($nuevo);
        } else {
          $this->insertarHoja($value, $almacen, $subtree->getMenor());
        }
      } else {
        $subtree->setAlmacen($almacen);
      }
    }
  }
  public function ArmarImpresion($subtree = false, $capa = 1)
  {
    $par = "";
    $derNulo = false;
    $izqNulo = false;
    if (is_null($subtree)) {
      $subtree = $this->raiz;
      $this->niveles[0] = $subtree->getValor();
    } elseif ($subtree == false) {
      $subtree = $this->raiz;
      $this->niveles[0] = $subtree->getValor();
    }
    if (empty($this->niveles[$capa])) {
      $this->niveles[$capa] = array();
    }
    if ($derNulo = (!is_null($subtree->getMenor()))) {
      $this->ArmarImpresion($subtree->getMenor(), $capa+1);
    }
    if ($izqNulo = (!is_null($subtree->getMayor()))) {
      $this->ArmarImpresion($subtree->getMayor(), $capa+1);
    }
    if ($derNulo&&$izqNulo) {
      $par = "(".$subtree->getMenor()->getValor().", ".$subtree->getMayor()->getValor().")";
      array_push($this->niveles[$capa],$par);
    } elseif($derNulo) {
      $par = "(".$subtree->getMenor()->getValor().", '')";
      array_push($this->niveles[$capa],$par);
    } elseif($izqNulo) {
      $par = "('', ".$subtree->getMayor()->getValor().")";
      array_push($this->niveles[$capa],$par);
    }
  }
  public function Imprimir()
  {
    $this->ArmarImpresion();
    echo "(".$this->niveles[0].")<br>";
    for ($i=1; $i < count($this->niveles); $i++) {
      for ($j=0; $j < count($this->niveles[$i]); $j++) {
        echo $this->niveles[$i][$j]." ";
      }
      echo "<br>";
    }
  }
  public function Buscar($value, $subtree = false)
  {
    if (is_null($subtree)) {
      $subtree = $this->raiz;
    } elseif ($subtree == false) {
      $subtree = $this->raiz;
    }
    if ($subtree->getValor() == $value) {
      return $subtree->getAllAlmacen();
    } else {
      if ($value > $subtree->getValor()) {
        if (!is_null($subtree->getMayor())) {
          return $this->Buscar($value, $subtree->getMayor());
        }
      } else {
        if (!is_null($subtree->getMenor())) {
          return $this->Buscar($value, $subtree->getMenor());
        }
      }
    }
    return false;
  }
}
?>
