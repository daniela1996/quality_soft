<?php defined('BASEPATH') OR exit('No direct script access allowed');
Class User extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function registro($url, $userid) {
    $this->db->where(array('URL' => $url, 'UNIQUE_ID' => $userid, 'SHOW_MENU' => 1));
    $query = $this->db->get("VW_MENU");
    return ($query->num_rows() == 1);
  }

  public function accionesPermitidas($url, $userid, $roleaction) {
    $this->db->where(array('URL' => $url, 'UNIQUE_ID' => $userid, 'SHOW_MENU' => 1));
    $query = $this->db->get("VW_MENU");
    if ($query->num_rows() == 1) {
      $moduloid = intval($query->result_array()[0]['MODULEID']);
      if (is_numeric($roleaction)) {
        $this->db->where(array('ROLEID' => $roleaction, 'IDMODULES' => $moduloid, 'UNIQUEID' => $userid, 'ESTADOUSUARIO' => 1));
        $query = $this->db->get("VW_ROLUSUARIO");
        return ($query->num_rows() > 0) ? true: false;
      } else {
        $this->db->where(array('CODEROLE' => $roleaction, 'IDMODULES' => $moduloid, 'UNIQUEID' => $userid, 'ESTADOUSUARIO' => 1));
        $query = $this->db->get("VW_ROLUSUARIO");
        return ($query->num_rows() > 0) ? true: false;
      }
    } else {
      return false;
    }
  }
  public function getUserByEmailAndPassword($emailOrNick, $password) {
    $result = $this->db->query("SELECT TU.*, count(*) OVER (PARTITION BY TU.EMAIL) countEmail FROM TBL_USERS TU WHERE TU.ESTADO = 1 AND (TU.EMAIL = '".$emailOrNick."' OR TU.NICKNAME = '".$emailOrNick."')");
    if ($result->num_rows() == 1) {
      $row = $result->row();
      $hash = $this->checkhashSSHA($row->SALT, $password);
      return ($row->ENCRYPTED_PASSWORD == $hash) ? $result->result() : false;
    } else {
      return false;
    }
  }
  public function getUser() {
    $query = $this->db->get('TBL_USERS');
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }
  public function getUsuarioPerfilParticular($userid) {
		$this->db->where('USERID',$userid);
		$query = $this->db->get("VW_USUARIOPERFIL");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
  public function getUltimoUsuarioPerfil($userid) {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(USERPROFILEID) ULTIMO FROM VW_USUARIOPERFIL WHERE USERID = ".$userid);
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
  public function getUltimoUsuarioRol($userid) {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(ROLEUSERID) ULTIMO FROM VW_ROLUSUARIO WHERE USERID = ".$userid);
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
  public function getUsuarioParticular($id) {
    $this->db->where('U_ID',$id);
    $query = $this->db->get("TBL_USERS");
    return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
  }
  public function getUsuarioIncompleto($perfilid) {
    $query = $this->db->query("SELECT US.*, NVL(UPF.ESTADO, 0) HABILITADO FROM TBL_USERS US LEFT OUTER JOIN TBL_USERS_PROFILES UPF ON(US.U_ID = UPF.USERID) AND US.ESTADO = 1 AND UPF.PROFILEID = ".$perfilid);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
  }
  public function getUserRolParticular($userid) {
    $this->db->where('USERID', $userid);
    $query = $this->db->get("VW_ROLUSUARIO");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
  public function getUserIncompleto($rolid) {
		$query = $this->db->query("SELECT US.U_ID, US.UNIQUE_ID, US.NAME, US.EMAIL, US.CREATED_AT, US.UPDATED_AT, US.PHONE, NVL(RUS.ESTADO, 0) HABILITADO FROM TBL_USERS US LEFT OUTER JOIN TBL_ROLE_USER RUS ON(US.U_ID = RUS.USERID) AND US.ESTADO = 1 AND RUS.ROLEID = ".$rolid);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
  public function userHasCuadrilla($userid, $cuadrillaid) {
		$this->db->where("USERID", $userid);
		$this->db->where("CUADRILLAID", $cuadrillaid);
		$query = $this->db->get("VW_USUARIOS_CUADRILLAS");
		return ($query->num_rows() > 0) ? true : false;
	}
  public function getPorNombre($nombre) {
    $query = $this->db->query("SELECT * FROM TBL_USERS WHERE ESTADO = 1 AND UPPER(NAME) LIKE UPPER('%".$nombre."%')");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }
  public function getUsuarioPorUniqueid($uniqueid) {
    $this->db->where('UNIQUE_ID', $uniqueid);
    $query = $this->db->get("TBL_USERS");
    return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
  }
  public function getUsuarioPerfilNuevo($userid, $limite) {
		$query = $this->db->query("SELECT * FROM (SELECT * FROM VW_USUARIOPERFIL WHERE USERID = ".$userid." ORDER BY USERPROFILEID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
  public function getUserRoleNuevo($userid, $limite) {
		$query = $this->db->query("SELECT * FROM (SELECT * FROM VW_ROLUSUARIO WHERE USERID = ".$userid." ORDER BY ROLEUSERID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
  public function getUsuarioNuevo($limite) {
		$query = $this->db->query("SELECT * FROM (SELECT U_ID, NAME, PHONE, EMAIL FROM TBL_USERS ORDER BY U_ID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
  public function getUltimoUsuario() {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(U_ID) ULTIMO FROM TBL_USERS");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
  public function insertUser($data) {
  $item = $this->db->query("SELECT * from TBL_USERS where upper(NICKNAME) = upper('".$data['NICK']."') or upper(EMAIL) = upper('".$data['EMAIL']."')");
    if ($item->num_rows() > 0 ) {
      return array('mensaje' => 'El Email o Nickname ingresado, ya existe.', 'estado' => 1);
    } else {
    if ($data['PHOTO']) {
      $instance = &get_instance();
  		$instance->load->database();
  		$conn = oci_connect($instance->db->username, $instance->db->password, $instance->db->hostname);
      if ((!(is_null($data["ENCRYPTED_PASSWORD"])))&&($data["ENCRYPTED_PASSWORD"] !== "")&&($data["ENCRYPTED_PASSWORD"] !== 0)) {
        $hash = $this->hashSSHA($data["ENCRYPTED_PASSWORD"]);
        $data['ENCRYPTED_PASSWORD'] = $hash["encrypted"];
        $data['SALT'] = $hash["salt"];
        $query = "INSERT INTO TBL_USERS (NAME, EMAIL, PHONE, NICKNAME, ENCRYPTED_PASSWORD, SALT, UNIQUE_ID, PHOTO, U_ID_UPDATE) VALUES ('".$data["NAME"]."', '".$data["EMAIL"]."', '".$data["PHONE"]."', '".$data["NICK"]."', '".$data["ENCRYPTED_PASSWORD"]."', '".$data["SALT"]."', '".$data["UNIQUE_ID"]."', EMPTY_BLOB(), '".$data["U_ID_UPDATE"]."') RETURNING PHOTO INTO :image" ;
      } else {
        $query = "INSERT INTO TBL_USERS (NAME, EMAIL, PHONE, NICKNAME, PHOTO, U_ID_UPDATE) VALUES ('".$data["NAME"]."', '".$data["EMAIL"]."', '".$data["PHONE"]."', '".$data["NICK"]."', EMPTY_BLOB(), '".$data["U_ID_UPDATE"]."') RETURNING PHOTO INTO :image";
      }
  		$parse = oci_parse($conn, $query);
  		$logo = base64_decode($data['PHOTO']);
  		$lob_a = oci_new_descriptor($conn, OCI_D_LOB);
  		oci_bind_by_name($parse, ":image", $lob_a, -1, OCI_B_BLOB);
  		oci_execute($parse, OCI_DEFAULT);
  		if($lob_a->save($logo)) {
  			oci_commit($conn);
  			$lob_a->free();
  			oci_close($conn);
  			$item = true;
  		} else {
  			$lob_a->free();
  			oci_close($conn);
  			$item = false;
  		}
    	return ($item) ? array('mensaje' => "Usuario agregado correctamente.", 'estado' => 2) : array('mensaje' => "Se presentó un error.", 'estado' => 2);
    } else {
      if ((!(is_null($data["ENCRYPTED_PASSWORD"])))&&($data["ENCRYPTED_PASSWORD"] !== "")&&($data["ENCRYPTED_PASSWORD"] !== 0)) {
        $hash = $this->hashSSHA($data["ENCRYPTED_PASSWORD"]);
        $data['ENCRYPTED_PASSWORD'] = $hash["encrypted"];
        $data['SALT'] = $hash["salt"];
        $item = $this->db->query("INSERT INTO TBL_USERS (NAME, EMAIL, PHONE, NICKNAME, ENCRYPTED_PASSWORD, SALT, UNIQUE_ID, U_ID_UPDATE) VALUES ('".$data["NAME"]."', '".$data["EMAIL"]."', '".$data["PHONE"]."', '".$data["NICK"]."', '".$data["ENCRYPTED_PASSWORD"]."', '".$data["SALT"]."', '".$data["UNIQUE_ID"]."', '".$data["U_ID_UPDATE"]."')");
        return ($item) ? array('mensaje' => "Usuario agregado correctamente.", 'estado' => 2) : array('mensaje' => "Se presentó un error.", 'estado' => 2);
      } else {
        $item = $this->db->query("INSERT INTO TBL_USERS (NAME, EMAIL, PHONE, NICKNAME, PHOTO, U_ID_UPDATE) VALUES ('".$data["NAME"]."', '".$data["EMAIL"]."', '".$data["PHONE"]."', '".$data["NICK"]."', '".$data["U_ID_UPDATE"]."')");
        return ($item) ? array('mensaje' => "Usuario agregado correctamente.", 'estado' => 2) : array('mensaje' => "Se presentó un error.", 'estado' => 2);
      }
    }
  }
  }
  public function updateUser($data) {
    $item = $this->db->query("SELECT * from TBL_USERS where u_id != '".$data["U_ID"]."' and (upper(NICKNAME) = upper('".$data["NICK"]."') or upper(EMAIL) = upper('".$data["EMAIL"]."'))");
        if ($item->num_rows() > 0 ) {
          return array('mensaje' => "El Email o Nickname ingresado, ya existe.", 'estado' => 1);
        } else {
          if ($data['PHOTO']) {
            $instance = &get_instance();
            $instance->load->database();
            $conn = oci_connect($instance->db->username, $instance->db->password, $instance->db->hostname);
            if ((!(is_null($data["ENCRYPTED_PASSWORD"])))&&($data["ENCRYPTED_PASSWORD"] !== "")&&($data["ENCRYPTED_PASSWORD"] !== 0)) {
              $hash = $this->hashSSHA($data["ENCRYPTED_PASSWORD"]);
              $data['ENCRYPTED_PASSWORD'] = $hash["encrypted"];
              $data['SALT'] = $hash["salt"];
              $query = "UPDATE TBL_USERS SET NAME = '".$data["NAME"]."', EMAIL = '".$data["EMAIL"]."', PHONE = '".$data["PHONE"]."', NICKNAME = '".$data["NICK"]."', ENCRYPTED_PASSWORD  = '".$data["ENCRYPTED_PASSWORD"]."', SALT = '".$data["SALT"]."', PHOTO = EMPTY_BLOB(), U_ID_UPDATE = '".$data["U_ID_UPDATE"]."' WHERE U_ID = '".$data["U_ID"]."' RETURNING PHOTO INTO :image";
            } else {
              $query = "UPDATE TBL_USERS SET NAME = '".$data["NAME"]."', EMAIL = '".$data["EMAIL"]."', PHONE = '".$data["PHONE"]."', NICKNAME = '".$data["NICK"]."', PHOTO = EMPTY_BLOB(), U_ID_UPDATE = '".$data["U_ID_UPDATE"]."' WHERE U_ID = '".$data["U_ID"]."' RETURNING PHOTO INTO :image";
            }
  		      $parse = oci_parse($conn, $query);
  		      $logo = base64_decode($data['PHOTO']);
  		      $lob_a = oci_new_descriptor($conn, OCI_D_LOB);
  		      oci_bind_by_name($parse, ":image", $lob_a, -1, OCI_B_BLOB);
  		      oci_execute($parse, OCI_DEFAULT);
  		      if($lob_a->save($logo)) {
  			     oci_commit($conn);
  			     $lob_a->free();
  			     oci_close($conn);
  			     $item = true;
  		      } else {
  			     $lob_a->free();
  			     oci_close($conn);
  			     $item = false;
  		      }
            return ($item) ? array('mensaje' => "Usuario actualizado correctamente.", 'estado' => 2) : array('mensaje' => "Se presentó un error.", 'estado' => 2);
          } else {
            if ((!(is_null($data["ENCRYPTED_PASSWORD"])))&&($data["ENCRYPTED_PASSWORD"] !== "")&&($data["ENCRYPTED_PASSWORD"] !== 0)) {
              $hash = $this->hashSSHA($data["ENCRYPTED_PASSWORD"]);
              $data['ENCRYPTED_PASSWORD'] = $hash["encrypted"];
              $data['SALT'] = $hash["salt"];
              $item = $this->db->query("UPDATE TBL_USERS SET NAME = '".$data["NAME"]."', EMAIL = '".$data["EMAIL"]."', PHONE = '".$data["PHONE"]."', NICKNAME = '".$data["NICK"]."', ENCRYPTED_PASSWORD  = '".$data["ENCRYPTED_PASSWORD"]."', SALT = '".$data["SALT"]."', U_ID_UPDATE = '".$data["U_ID_UPDATE"]."' WHERE U_ID = '".$data['U_ID']."'");
            return ($item) ? array('mensaje' => "Usuario actualizado correctamente.", 'estado' => 2) : array('mensaje' => "Se presentó un error.", 'estado' => 2);
            } else {
              $item = $this->db->query("UPDATE TBL_USERS SET NAME = '".$data["NAME"]."', EMAIL = '".$data["EMAIL"]."', PHONE = '".$data["PHONE"]."', NICKNAME = '".$data["NICK"]."', U_ID_UPDATE = '".$data["U_ID_UPDATE"]."' WHERE U_ID = '".$data['U_ID']."'");
            return ($item) ? array('mensaje' => "Usuario actualizado correctamente.", 'estado' => 2) : array('mensaje' => "Se presentó un error.", 'estado' => 2);
            }
          }
        }
  }
  private function hashSSHA($password) {
    $salt = sha1(rand());
    $salt = substr($salt, 0, 10);
    $encrypted = base64_encode(sha1($password.$salt, true).$salt);
    $hash = array("salt" => $salt, "encrypted" => $encrypted);
    return $hash;
  }
  private function checkhashSSHA($salt, $password) {
    $hash = base64_encode(sha1($password . $salt, true) . $salt);
    return $hash;
  }
} ?>
