<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Pruebamodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}

  public function getProyectos(){
    $query = $this->db->get('TBL_PROJECTS');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  public function getModulosPruebas($idModulo){
    $this->db->where('PROOF_STATUS', 1);
    $this->db->where('TIPO', 1);
    $this->db->where('ID_PROJECT_MODULES', $idModulo);
    $query = $this->db->get('VW_PROOF_BY_MODULES');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  //Capturar checkbox de incidencias guardados en la BD
  public function getArregloIncidencias($idPrueba){
    $this->db->where('ID_PROOF_CASE', $idPrueba);
    $query = $this->db->get('VW_PROOF_INCIDENCES');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  //Capturar evidencias guardadas en la BD
  public function getArregloEvidencias($idPrueba){
    $this->db->where('PROOF_EVIDENCE_STATUS', 1);
    $this->db->where('ID_PROOF_CASE', $idPrueba);    
    $query = $this->db->get('VW_PROOF_EVIDENCES');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  public function getArregloImagenes($idPrueba){
    $this->db->where('PROOF_EVIDENCE_STATUS', 1);
    $this->db->where('ID_PROOF_CASE', $idPrueba);    
    $query = $this->db->get('VW_EVIDENCES_PRUEBA');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  public function getArregloVideos($idPrueba){
    $this->db->where('PROOF_URL_STATUS', 1);
    $this->db->where('ID_PROOF_CASE', $idPrueba);    
    $query = $this->db->get('VW_VIDEOS_PRUEBA');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  //Carga Módulo dependiendo del proyecto
  public function getModuloProyecto($id){
    $this->db->where('STATUS_MODULE', 1);
    $this->db->where('ID_PROJECTS', $id);
    $query = $this->db->get('VW_PROJECT_MODULES');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  public function getPruebas() {
    $query = $this->db->query("SELECT * FROM TBL_MAINTENANCE WHERE MAINTENANCE_CODE LIKE 'CAS%' AND STATUS_MAINTENANCE = 1");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }

  public function getResultados() {
    $query = $this->db->query("SELECT * FROM TBL_MAINTENANCE WHERE MAINTENANCE_CODE LIKE 'RES%' AND STATUS_MAINTENANCE = 1");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }

  public function getIncidencias() {
    $query = $this->db->query("SELECT * FROM TBL_MAINTENANCE WHERE MAINTENANCE_CODE LIKE 'INC%' AND STATUS_MAINTENANCE = 1");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }

  public function getCriticidad() {
    $query = $this->db->query("SELECT * FROM TBL_MAINTENANCE WHERE MAINTENANCE_CODE LIKE 'CRI%' AND STATUS_MAINTENANCE = 1");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }

  public function getEstadoIncidencia() {
    $query = $this->db->query("SELECT * FROM TBL_MAINTENANCE WHERE MAINTENANCE_CODE LIKE 'ESTINC%' AND STATUS_MAINTENANCE = 1");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }

  public function insertPruebas($data, $incidencias, $evidences, $posiciones) {
      $item = false;
      $item = $this->db->query("SELECT PROOF_NAME from TBL_PROOF_CASE where ID_PROJECT_MODULES = ".$data['ID_PROJECT_MODULES']." AND (PROOF_NAME = '".$data['PROOF_NAME']."' or UPPER(PROOF_NAME) = UPPER('".$data['PROOF_NAME']."') or INITCAP(PROOF_NAME) = INITCAP('".$data['PROOF_NAME']."')) AND PROOF_STATUS = 1");
      if ($item->num_rows() > 0 ) {
        return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
      } else {
        if(!empty($data['CRITICITY']))
        {
            $item = $this->db->query("INSERT INTO TBL_PROOF_CASE (PROOF_NAME, RESULTS, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, CRITICITY, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT) VALUES ('".$data['PROOF_NAME']."', '".$data['RESULTS']."', to_date('".$data["INITIAL_ESTIMATED_DATE"]."', 'dd/mm/yyyy'), to_date('".$data["FINAL_ESTIMATED_DATE"]."' , 'dd/mm/yyyy'), '".$data['OBSERVATION']."', ".$data["ID_PROJECT_MODULES"].", '".$data['CASE_TYPE']."', '".$data['CRITICITY']."', ".$data['INCIDENCE_STATUS'].", '".$data['EXPECTED_RESULT']."', '".$data['OBTAINED_RESULT']."')");

          $query = $this->db->query("SELECT SQ_TBL_PROOF_CASE.CURRVAL FROM DUAL");
          $ultimoId = $query->row(0)->CURRVAL;  
              
            $item = $this->db->query("INSERT INTO TBL_PROOF_CASE_HISTORY (PROOF_NAME, PROOF_CYCLE, RESULTS, ACTUAL_DATE, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT) SELECT PROOF_NAME, PROOF_CYCLE, RESULTS, ACTUAL_DATE, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT FROM TBL_PROOF_CASE WHERE ID_PROJECT_MODULES = ".$data["ID_PROJECT_MODULES"]." AND ID_PROOF_CASE = ".$ultimoId."");


          $dataIncidencias = array();

          for ($i=0; $i < count($incidencias) ; $i++) {

            $query2 = $this->db->query("SELECT MAX(INCIDENCE_NUMBER) INCIDENCE_NUMBER FROM TBL_INCIDENCES_PROOF_CASE where ID_PROJECT_MODULE = ".$data['ID_PROJECT_MODULES']."");

            if ($query2->num_rows() > 0 ) {
              $numeroIncidencias = $query2->result_array()[0]['INCIDENCE_NUMBER'];
            }
            else{
              $numeroIncidencias = 1;
            }

              $numeroIncidencias += 1;
              $fila = $incidencias[$i];
              $dataIncidencias[] = array('ID_PROOF_CASE' => $ultimoId, 'ID_MAINTENANCE' => $fila['ID_MAINTENANCE'], 'INCIDENCE_NUMBER' => "$numeroIncidencias", 'ID_PROJECT_MODULE' => $data['ID_PROJECT_MODULES']);

              // log_message('error', $numeroIncidencias);
          }
        }
        else
        {
          $item = $this->db->query("INSERT INTO TBL_PROOF_CASE (PROOF_NAME, RESULTS, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, EXPECTED_RESULT, OBTAINED_RESULT) VALUES ('".$data['PROOF_NAME']."', '".$data['RESULTS']."', to_date('".$data["INITIAL_ESTIMATED_DATE"]."', 'dd/mm/yyyy'), to_date('".$data["FINAL_ESTIMATED_DATE"]."' , 'dd/mm/yyyy'), '".$data['OBSERVATION']."', ".$data["ID_PROJECT_MODULES"].", '".$data['CASE_TYPE']."', '".$data['EXPECTED_RESULT']."', '".$data['OBTAINED_RESULT']."')");
          $query = $this->db->query("SELECT SQ_TBL_PROOF_CASE.CURRVAL FROM DUAL");
          $ultimoId = $query->row(0)->CURRVAL;

          $item = $this->db->query("INSERT INTO TBL_PROOF_CASE_HISTORY (PROOF_NAME, PROOF_CYCLE, RESULTS, ACTUAL_DATE, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT) SELECT PROOF_NAME, PROOF_CYCLE, RESULTS, ACTUAL_DATE, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT FROM TBL_PROOF_CASE WHERE ID_PROJECT_MODULES = ".$data["ID_PROJECT_MODULES"]." AND ID_PROOF_CASE = ".$ultimoId."");
        }

        if (!empty($dataIncidencias))
        {
          $item = $this->db->insert_batch('TBL_INCIDENCES_PROOF_CASE', $dataIncidencias);
        }

        /*$item = $this->db->query("INSERT INTO TBL_RESULTS_IMAGES (ID_RESULTS_IMAGES, EXPECTED_RESULT, OBTAINED_RESULT, ID_PROOF_CASE) VALUES (1, '".$data['EXPECTED_RESULT']."', '".$data['OBTAINED_RESULT']."', ".$ultimoId.")");*/

        if (!empty($data))
        // if (!empty($data) && !empty($evidences))
        {
          for($i=0; $i < count($evidences); $i++)
          {
            // echo(substr($evidences[$i]['EVIDENCES'], 0, 10));

            if(substr($evidences[$i]['EVIDENCES'], 0, 10) != 'data:image')
            {
                $laEvidencia = str_replace("data:video/mp4;base64,", "", $evidences[$i]['EVIDENCES']);

                 // echo('Aquí---videooooooooooooooooooooooo CON DATA'.$evidences[$i]['EVIDENCES']);

                $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$ultimoId.", 0, ".$posiciones[$i]['POSITION_EVIDENCES'].") RETURNING EVIDENCES INTO :image";
                 
                // $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, EXPECTED_RESULT, OBTAINED_RESULT, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$ultimoId.", '".$evidences[$i]['EXPECTED_RESULT']."', '".$evidences[$i]['OBTAINED_RESULT']."', 0, ".$posiciones[$i]['POSITION_EVIDENCES'].") RETURNING EVIDENCES INTO :image";

                /*$item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, EXPECTED_RESULT, OBTAINED_RESULT, IMAGE_OR_VIDEO) VALUES (EMPTY_BLOB(), ".$ultimoId.", '".$evidences[$i]['EXPECTED_RESULT']."', '".$evidences[$i]['OBTAINED_RESULT']."', 0) RETURNING EVIDENCES INTO :image";*/
                
            }
            else
            {
                $laEvidencia = str_replace("data:image/png;base64,", "", str_replace("data:image/jpg;base64,", "", str_replace("data:image/jpeg;base64,", "", $evidences[$i]['EVIDENCES'])));

                $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$ultimoId.", 1, ".$posiciones[$i]['POSITION_EVIDENCES'].") RETURNING EVIDENCES INTO :image";

                // $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, EXPECTED_RESULT, OBTAINED_RESULT, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$ultimoId.", '".$evidences[$i]['EXPECTED_RESULT']."', '".$evidences[$i]['OBTAINED_RESULT']."', 1, ".$posiciones[$i]['POSITION_EVIDENCES'].") RETURNING EVIDENCES INTO :image";

                /*$item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, EXPECTED_RESULT, OBTAINED_RESULT, IMAGE_OR_VIDEO) VALUES (EMPTY_BLOB(), ".$ultimoId.", '".$evidences[$i]['EXPECTED_RESULT']."', '".$evidences[$i]['OBTAINED_RESULT']."', 1) RETURNING EVIDENCES INTO :image";*/
            }
            
            $instance = &get_instance();
            $instance->load->database();
            $conn = oci_connect($instance->db->username, $instance->db->password, $instance->db->hostname);
            $parse = oci_parse($conn, $item);
            $evidencia = base64_decode($laEvidencia);

            // echo('Aquí---videooooooooooooooooooooooo DECODE'.$laEvidencia);
            $lob_a = oci_new_descriptor($conn, OCI_D_LOB);
            oci_bind_by_name($parse, ":image", $lob_a, -1, OCI_B_BLOB);
            oci_execute($parse, OCI_DEFAULT);
            if($lob_a->save($evidencia)) {
              oci_commit($conn);
              $lob_a->free();
              oci_close($conn);
              $item = true;
            } else {
              $lob_a->free();
              oci_close($conn);
              $item = false;
            }
          }
        }

        /*if(!empty($evidences))
        {
          $dataPosiciones = array();

          for ($i=0; $i < count($posiciones) ; $i++) {

              $query2 = $this->db->query("SELECT SQ_TBL_PROOF_EVIDENCES.CURRVAL FROM DUAL");
              $ultimoId2 = $query2->row(0)->CURRVAL;

              $fila = $posiciones[$i];
              $dataPosiciones[] = array('ID_PROOF_EVIDENCES' => $ultimoId2, 'POSITION_EVIDENCES' => $fila['POSITION_EVIDENCES']);

              // log_message('error', $numeroIncidencias);
          }
        }

        if (!empty($dataPosiciones))
        {
          $item = $this->db->insert_batch('TBL_POSITION_EVIDENCES', $dataPosiciones);
        }*/
      }

      return ($item) ? array('mensaje' => "Nuevo registro agregado", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);
      // print_r($item);
  }

    public function updatePruebas($data, $idprueba, $incidencias, $evidences, $posiciones) {
      $item = false;
      $item = $this->db->query("SELECT PROOF_NAME from TBL_PROOF_CASE where ID_PROJECT_MODULES = ".$data['ID_PROJECT_MODULES']." and ID_PROOF_CASE != ".$idprueba." and (PROOF_NAME = '".$data['PROOF_NAME']."' or UPPER(PROOF_NAME) = UPPER('".$data['PROOF_NAME']."') or INITCAP(PROOF_NAME) = INITCAP('".$data['PROOF_NAME']."')) AND PROOF_STATUS = 1");
      
      if ($item->num_rows() > 0 ) {
        return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
      } else {
        if(!empty($data['CRITICITY']))
        {
            $item = $this->db->query("UPDATE TBL_PROOF_CASE SET PROOF_NAME = '".$data['PROOF_NAME']."', RESULTS = '".$data['RESULTS']."', INITIAL_ESTIMATED_DATE = to_date('".$data["INITIAL_ESTIMATED_DATE"]."', 'dd/mm/yyyy'), FINAL_ESTIMATED_DATE = to_date('".$data["FINAL_ESTIMATED_DATE"]."' , 'dd/mm/yyyy'), OBSERVATION = '".$data['OBSERVATION']."', ID_PROJECT_MODULES = ".$data["ID_PROJECT_MODULES"].", CASE_TYPE = '".$data['CASE_TYPE']."', CRITICITY = '".$data['CRITICITY']."', INCIDENCE_STATUS = ".$data['INCIDENCE_STATUS'].", EXPECTED_RESULT = '".$data['EXPECTED_RESULT']."', OBTAINED_RESULT = '".$data['OBTAINED_RESULT']."' WHERE ID_PROOF_CASE = ".$idprueba."");

            $item = $this->db->query("INSERT INTO TBL_PROOF_CASE_HISTORY (PROOF_NAME, PROOF_CYCLE, RESULTS, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT) SELECT PROOF_NAME, PROOF_CYCLE, RESULTS, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT FROM TBL_PROOF_CASE WHERE ID_PROJECT_MODULES = ".$data["ID_PROJECT_MODULES"]." AND ID_PROOF_CASE = ".$idprueba."");

            $dataIncidencias = array();

            for ($i=0; $i < count($incidencias) ; $i++) {

              $query2 = $this->db->query("SELECT MAX(INCIDENCE_NUMBER) INCIDENCE_NUMBER FROM TBL_INCIDENCES_PROOF_CASE where ID_PROJECT_MODULE = ".$data['ID_PROJECT_MODULES']."");

              if ($query2->num_rows() > 0 ) {
                $numeroIncidencias = $query2->result_array()[0]['INCIDENCE_NUMBER'];
              }
              else{
                $numeroIncidencias = 1;
              }

                $numeroIncidencias += 1;
                $fila = $incidencias[$i];
                $dataIncidencias[] = array('ID_PROOF_CASE' => $fila['ID_PROOF_CASE'], 'ID_MAINTENANCE' => $fila['ID_MAINTENANCE'], 'INCIDENCE_NUMBER' => "$numeroIncidencias", 'ID_PROJECT_MODULE' => $data['ID_PROJECT_MODULES']);

                // print_r($dataIncidencias);

                // log_message('error', $numeroIncidencias);
            }
        }
        else
        {
            $item = $this->db->query("UPDATE TBL_PROOF_CASE SET PROOF_NAME = '".$data['PROOF_NAME']."', RESULTS = '".$data['RESULTS']."', INITIAL_ESTIMATED_DATE = to_date('".$data["INITIAL_ESTIMATED_DATE"]."', 'dd/mm/yyyy'), FINAL_ESTIMATED_DATE = to_date('".$data["FINAL_ESTIMATED_DATE"]."' , 'dd/mm/yyyy'), OBSERVATION = '".$data['OBSERVATION']."', ID_PROJECT_MODULES = ".$data["ID_PROJECT_MODULES"].", CASE_TYPE = '".$data['CASE_TYPE']."', EXPECTED_RESULT = '".$data['EXPECTED_RESULT']."', OBTAINED_RESULT = '".$data['OBTAINED_RESULT']."' WHERE ID_PROOF_CASE = ".$idprueba."");

            $item = $this->db->query("INSERT INTO TBL_PROOF_CASE_HISTORY (PROOF_NAME, PROOF_CYCLE, RESULTS, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT) SELECT PROOF_NAME, PROOF_CYCLE, RESULTS, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT FROM TBL_PROOF_CASE WHERE ID_PROJECT_MODULES = ".$data["ID_PROJECT_MODULES"]." AND ID_PROOF_CASE = ".$idprueba."");
        }

        if (!empty($dataIncidencias))
        {
          // print_r($data);
          // print_r($dataIncidencias);
          $item = $this->db->query("DELETE FROM TBL_INCIDENCES_PROOF_CASE WHERE ID_PROOF_CASE = ".$idprueba);
          $item = $this->db->insert_batch('TBL_INCIDENCES_PROOF_CASE', $dataIncidencias);
        }

        if(!empty($data) && !empty($evidences))
        {
          //$item = $this->db->query("DELETE FROM TBL_PROOF_EVIDENCES WHERE ID_PROOF_CASE = ".$idprueba);

          // log_message('error', count($evidences));
          for($i=0; $i < count($evidences); $i++)
          {
                if(substr($evidences[$i]['EVIDENCES'], 0, 10) != 'data:image')
                {
                    $laEvidencia = str_replace("data:video/mp4;base64,", "", $evidences[$i]['EVIDENCES']);
                    // echo(count($laEvidencia).'Aquí---videooooooooooooooooooooooo');
                    /*$item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, EXPECTED_RESULT, OBTAINED_RESULT, IMAGE_OR_VIDEO) VALUES (EMPTY_BLOB(), ".$idprueba.", '".$evidences[$i]['EXPECTED_RESULT']."', '".$evidences[$i]['OBTAINED_RESULT']."', 0) RETURNING EVIDENCES INTO :image";*/

                    /*$item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$idprueba.", 0, ".$posiciones[$i]['POSITION_EVIDENCES'].") RETURNING EVIDENCES INTO :image";*/

                    $query3 = $this->db->query("SELECT EVIDENCES FROM TBL_PROOF_EVIDENCES WHERE ID_PROOF_CASE = ".$idprueba);

                    if($evidences[$i]['EVIDENCES'] != $query3)
                    {
                      $query4 = $this->db->query("SELECT MAX(POSITION_EVIDENCES) POSITION_EVIDENCES FROM TBL_PROOF_EVIDENCES WHERE ID_PROOF_CASE = ".$idprueba."");

                      $contadorPosition = $query4->result_array()[0]['POSITION_EVIDENCES'];

                      if ($query4->num_rows() > 0 ) {
                          $contadorPosition += 1;

                          $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$idprueba.", 0, ".$contadorPosition.") RETURNING EVIDENCES INTO :image";
                      }
                    }
                }
                else
                {
                    $laEvidencia = str_replace("data:image/png;base64,", "", str_replace("data:image/jpg;base64,", "", str_replace("data:image/jpeg;base64,", "", $evidences[$i]['EVIDENCES'])));

                    /*$item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, EXPECTED_RESULT, OBTAINED_RESULT, IMAGE_OR_VIDEO) VALUES (EMPTY_BLOB(), ".$idprueba.", '".$evidences[$i]['EXPECTED_RESULT']."', '".$evidences[$i]['OBTAINED_RESULT']."', 1) RETURNING EVIDENCES INTO :image";*/

                    // $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$idprueba.", 1, ".$posiciones[$i]['POSITION_EVIDENCES'].") RETURNING EVIDENCES INTO :image";

                    //$item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$idprueba.", 1, null) RETURNING EVIDENCES INTO :image";


                    // if($evidences[$i]['EVIDENCES'] == $query3)
                    // {
                    //   $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$idprueba.", 1, ".$posiciones[$i]['POSITION_EVIDENCES'].") RETURNING EVIDENCES INTO :image";
                    // }
                    // else
                    // {
                    //   $query4 = $this->db->query("SELECT MAX(POSITION_EVIDENCES) POSITION_EVIDENCES FROM TBL_PROOF_EVIDENCES WHERE ID_PROOF_CASE = ".$idprueba."");

                    //   $contadorPosition = $query2->result_array()[0]['POSITION_EVIDENCES'];

                    //   if ($query2->num_rows() > 0 ) {
                    //       $contadorPosition += 1;
                    //       $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$idprueba.", 1, ".$contadorPosition.") RETURNING EVIDENCES INTO :image";
                    //   }
                    // }
                    $query4 = $this->db->query("SELECT EVIDENCES FROM TBL_PROOF_EVIDENCES WHERE ID_PROOF_CASE = ".$idprueba);

                    if($evidences[$i]['EVIDENCES'] != $query4)
                    {
                      $query5 = $this->db->query("SELECT MAX(POSITION_EVIDENCES) POSITION_EVIDENCES FROM TBL_PROOF_EVIDENCES WHERE ID_PROOF_CASE = ".$idprueba."");

                      $contadorPosition = $query5->result_array()[0]['POSITION_EVIDENCES'];

                      if ($query5->num_rows() > 0 ) {
                          $contadorPosition += 1;

                          $item = "INSERT INTO TBL_PROOF_EVIDENCES (EVIDENCES, ID_PROOF_CASE, IMAGE_OR_VIDEO, POSITION_EVIDENCES) VALUES (EMPTY_BLOB(), ".$idprueba.", 1, ".$contadorPosition.") RETURNING EVIDENCES INTO :image";
                      }
                    }

                }

                $instance = &get_instance();
                $instance->load->database();
                $conn = oci_connect($instance->db->username, $instance->db->password, $instance->db->hostname);
                $parse = oci_parse($conn, $item);
                $evidencia = base64_decode($laEvidencia);
                // log_message('error', $laEvidencia);
                $lob_a = oci_new_descriptor($conn, OCI_D_LOB);
                oci_bind_by_name($parse, ":image", $lob_a, -1, OCI_B_BLOB);
                oci_execute($parse, OCI_DEFAULT);
                if($lob_a->save($evidencia)) {
                  oci_commit($conn);
                  $lob_a->free();
                  oci_close($conn);
                  $item = true;
                } else {
                  $lob_a->free();
                  oci_close($conn);
                  $item = false;
                }
          }
        }
          
      }

      return ($item) ? array('mensaje' => "Registro actualizado correctamente.", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);
    }

    public function updatePosition($data)
    {
      $item = $this->db->query("UPDATE TBL_PROOF_CASE SET PROOF_NAME = '".$data['PROOF_NAME']."', RESULTS = '".$data['RESULTS']."', INITIAL_ESTIMATED_DATE = to_date('".$data["INITIAL_ESTIMATED_DATE"]."', 'dd/mm/yyyy'), FINAL_ESTIMATED_DATE = to_date('".$data["FINAL_ESTIMATED_DATE"]."' , 'dd/mm/yyyy'), OBSERVATION = '".$data['OBSERVATION']."', ID_PROJECT_MODULES = ".$data["ID_PROJECT_MODULES"].", CASE_TYPE = '".$data['CASE_TYPE']."', POSITION_CASE = '".$data['POSITION_CASE']."', EXPECTED_RESULT = '".$data['EXPECTED_RESULT']."', OBTAINED_RESULT = '".$data['OBTAINED_RESULT']."' WHERE ID_PROOF_CASE = ".$data['ID_PROOF_CASE']);

      return $item;
    }

    public function updatePositionEvidence($data)
    {
      $item = $this->db->query("UPDATE TBL_PROOF_EVIDENCES SET EVIDENCES = '".$data['EVIDENCES']."', ACTUAL_DATE = '".$data['ACTUAL_DATE']."', ID_PROOF_CASE = '".$data['ID_PROOF_CASE']."', EXPECTED_RESULT = '".$data['EXPECTED_RESULT']."', OBTAINED_RESULT = ".$data["OBTAINED_RESULT"].", IMAGE_OR_VIDEO = '".$data['IMAGE_OR_VIDEO']."', PROOF_EVIDENCE_STATUS = '".$data['PROOF_EVIDENCE_STATUS']."', POSITION_EVIDENCES = '".$data['POSITION_EVIDENCES']."' WHERE ID_PROOF_EVIDENCES = ".$data['ID_PROOF_EVIDENCES']);

      return $item;
    }

    public function deletePrueba($idprueba) {
      $item = $this->db->query("UPDATE TBL_PROOF_CASE SET PROOF_STATUS = 0 WHERE ID_PROOF_CASE = ".$idprueba);
      return ($item) ? array('mensaje' => "Registro eliminado", 'estado' => 2) : array('mensaje' => "Error al eliminar registro", 'estado' => 2);
    }

  public function insertCiclos($data){

        // log_message('error', $data['ID_PROJECT_MODULE']);
        // $item = $this->db->query("SELECT ID_PROJECT_MODULE from TBL_MODULE_CYCLE where ID_PROJECT_MODULE = ".$data['ID_PROJECT_MODULE']."");

        // log_message('error', $item->result_array()[0]['ID_PROJECT_MODULE']);          

          $dataCiclos = array();
          
          // for ($i=0; $i < count($data) ; $i++) {
              // $fila = $data[$i];
              $this->db->where('ID_PROJECT_MODULE', $data['ID_PROJECT_MODULE']);
              $query = $this->db->get("TBL_MODULE_CYCLE");

              // log_message('error', $fila['ID_PROJECT_MODULE']);
              // $item = $this->db->query("SELECT ID_PROJECT_MODULE from TBL_MODULE_CYCLE where ID_PROJECT_MODULE = ".$data['ID_PROJECT_MODULE']."");

              $query2 = $this->db->query("SELECT NUMBER_CYCLE from TBL_MODULE_CYCLE where ID_PROJECT_MODULE = ".$data['ID_PROJECT_MODULE']."");

              // log_message('error', $query2->result_array()[0]['NUMBER_CYCLE']);

              $acumulador = $query2->result_array()[0]['NUMBER_CYCLE'];

              if ($query2->num_rows() > 0 ) {
                  $acumulador += 1;
                  $cicloid = $query->row(0)->ID_MODULE_CYCLE;
                  $moduloid = $query->row(1)->ID_PROJECT_MODULE;
                  $dataCiclos[] = array('ID_MODULE_CYCLE' => $cicloid, 'ID_PROJECT_MODULE' => $moduloid, 'NUMBER_CYCLE' => "$acumulador", 'GENERAL_OBSERVATION' => $data['GENERAL_OBSERVATION']);
                  // log_message('error', $cicloid);
                  // log_message('error', $moduloid);
                  // log_message('error', $acumulador);
                  // }
              }

        if (!empty($dataCiclos)) {
          $item = $this->db->update_batch('TBL_MODULE_CYCLE', $dataCiclos, 'ID_MODULE_CYCLE');
          $item = $this->db->query("UPDATE TBL_PROOF_CASE PROOF_CYCLE SET PROOF_CYCLE = ".$acumulador." WHERE ID_PROJECT_MODULES = ".$moduloid."");
          $item = $this->db->query("UPDATE TBL_PROOF_CASE STATUS_DEVELOPMENT SET STATUS_DEVELOPMENT = CASE WHEN STATUS_DEVELOPMENT = 0 THEN 1 ELSE 0 END WHERE ID_PROJECT_MODULES = ".$moduloid."");
          $item = $this->db->query("INSERT INTO TBL_PROOF_CASE_HISTORY (PROOF_NAME, PROOF_CYCLE, RESULTS, ACTUAL_DATE, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT) SELECT PROOF_NAME, PROOF_CYCLE, RESULTS, ACTUAL_DATE, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, OBSERVATION, ID_PROJECT_MODULES, CASE_TYPE, PROOF_STATUS, CRITICITY, DATE_PROGRAMMER_CORRECTION, DATE_TESTING_CORRECTION, IS_INCIDENCE, DETECTION_DATE, POSITION_CASE, ID_PROOF_CASE, STATUS_DEVELOPMENT, INCIDENCE_STATUS, EXPECTED_RESULT, OBTAINED_RESULT FROM TBL_PROOF_CASE WHERE ID_PROJECT_MODULES = ".$moduloid." AND PROOF_CYCLE = ".$acumulador."");
          $item = $this->db->query("UPDATE TBL_PROOF_CASE_HISTORY SET GENERAL_OBSERVATION = (SELECT (GENERAL_OBSERVATION) FROM TBL_MODULE_CYCLE WHERE ID_PROJECT_MODULE = ".$moduloid." AND NUMBER_CYCLE = ".$acumulador.") WHERE ID_PROJECT_MODULES = ".$moduloid." AND PROOF_CYCLE = ".$acumulador."");
          }

        return ($item) ? array('mensaje' => "Ciclo Terminado", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);        
    // }
  }

  public function insertPruebasClonacion($data){
    $item = $this->db->query("SELECT PROOF_NAME from TBL_PROOF_CASE where PROOF_NAME = '".$data['PROOF_NAME']."' and ID_PROJECT_MODULES = ".$data['ID_PROJECT_MODULES']." AND PROOF_STATUS = 1 ");
    // SELECT PROOF_NAME from TBL_PROOF_CASE where ID_PROJECT_MODULES = ".$data['ID_PROJECT_MODULES']." and ID_PROOF_CASE != ".$idprueba." and (PROOF_NAME = '".$data['PROOF_NAME']."' or UPPER(PROOF_NAME) = UPPER('".$data['PROOF_NAME']."') or INITCAP(PROOF_NAME) = INITCAP('".$data['PROOF_NAME']."')
      if ($item->num_rows() > 0 ) {
        return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
      } else {
        // log_message('error', $evidences['EVIDENCES']);
        $item = $this->db->query("INSERT INTO TBL_PROOF_CASE (PROOF_NAME, ID_PROJECT_MODULES, CASE_TYPE) VALUES ('".$data['PROOF_NAME']."', ".$data["ID_PROJECT_MODULES"].", '".$data['CASE_TYPE']."')");
            // $query = $this->db->query("SELECT SQ_TBL_PROOF_CASE.CURRVAL FROM DUAL");
            // $ultimoId = $query->row(0)->CURRVAL;

            // $item = $this->db->query("INSERT INTO TBL_PROOF_EVIDENCES (ID_PROOF_CASE, EXPECTED_RESULT) VALUES (".$ultimoId.", '".$data['EXPECTED_RESULT']."')");
      }

    return ($item) ? array('mensaje' => "Registro clonado correctamente", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);
  }

  public function deleteEvidencia($idevidencia) {
      $item = $this->db->query("UPDATE TBL_PROOF_EVIDENCES SET PROOF_EVIDENCE_STATUS = 0 WHERE ID_PROOF_EVIDENCES = ".$idevidencia);
      return ($item) ? array('mensaje' => "Registro eliminado", 'estado' => 2) : array('mensaje' => "Error al eliminar registro", 'estado' => 2);
    }

  public function deleteVideo($idvideo) {
    $item = $this->db->query("UPDATE TBL_URL_VIDEOS SET PROOF_URL_STATUS = 0 WHERE ID_PROOF_VIDEOS = ".$idvideo);
    return ($item) ? array('mensaje' => "Registro eliminado", 'estado' => 2) : array('mensaje' => "Error al eliminar registro", 'estado' => 2);
  }

  public function insertUrlVideo($data)
  {
    /*$query = $this->db->query("SELECT MAX(ID_PROOF_CASE)+1 ID_PROOF_CASE FROM TBL_URL_VIDEOS");
    $ultimoId = $query->row()->ID_PROOF_CASE;

    $query = $this->db->query("SELECT SQ_TBL_PROOF_CASE.NEXTVAL FROM DUAL");

    $ultimoId = $query->row(0)->NEXTVAL;

    $query = $this->db->query("SELECT MAX(ID_PROOF_CASE) ID_PROOF_CASE FROM TBL_PROOF_CASE");
    $ultimoId = $query->row(0)->ID_PROOF_CASE;*/

    $query = $this->db->query("SELECT MAX(ID_PROOF_CASE) ID_PROOF_CASE FROM TBL_PROOF_CASE");
    $ultimoId = $query->row(0)->ID_PROOF_CASE;
    
    $query = $this->db->query("INSERT INTO TBL_URL_VIDEOS (ID_PROOF_CASE, URL) VALUES (".$ultimoId.", '".$data['EVIDENCES']."')");

    if ($query) 
    {
      $Resultado = array('Estado' => true,'Mensaje'=>'Video subido exitosamente', 'Resultado' =>$query);
    }
    else
    {
      $Resultado = array('Estado' => false,'Mensaje'=>'Error, Video Fallido', 'Resultado' =>$query);
    }
    return $Resultado;
  }
}?>