<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Regionmodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function getRegiones() {
    	$query = $this->db->get('VW_REGIONES');
    	return ($query->num_rows() > 0) ? $query->result_array(): false;
  	}

  	public function insertPais($data) {
  		//verificar que el pais no exista el la base de datos
  		$item = $this->db->query("SELECT * from PAIS where upper(NOMBRE) = upper('".$data['NOMBRE']."')");
  		// si el pais existe muestre el error
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este país ya existe.", 'estado' => 1 );
  			} else {
  		// si no existe haga el insert
  			$item = $this->db->insert('PAIS', $data);
			return ($item) ? array('mensaje' => "Nuevo país agregado", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);	
  			}
  		
	}

	public function updatePais($codpais, $data){
		//verificar que el pais no exista el la base de datos
  		$item = $this->db->query("SELECT * from PAIS where cod_pais != ".$codpais." and  upper(NOMBRE) = upper('".$data['NOMBRE']."')");
  		// si el pais existe muestre el error 
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este país ya existe.", 'estado' => 1 );
  			} else {
  		// si no existe haga el update
  				$this->db->where('COD_PAIS', $codpais);
				$item = $this->db->update('PAIS', $data);
				return ($item) ? array('mensaje' => "País actualizado", 'estado' => 2) : array('mensaje' => "Error al actualizar", 'estado' => 2);
  			}
		}

	public function insertDepto($data) {
  		$item = $this->db->query("SELECT * from DEPARTAMENTO where upper(NOMBRE) = upper('".$data['NOMBRE']."') AND COD_PAIS = '".$data['COD_PAIS']."'");
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este departemento ya existe.", 'estado' => 1 );
  			} else {
				$item = $this->db->insert('DEPARTAMENTO', $data);
				return ($item) ? array("mensaje" => "Nuevo departamento agregado", 'estado' => 2) : array("mensaje" => "Se presento un Error", 'estado' => 2);
			}
	}

	public function updateDepto($coddepto,$data){
  		$item = $this->db->query("SELECT * from DEPARTAMENTO where COD_DEPTO != ".$coddepto." and upper(NOMBRE) = upper('".$data['NOMBRE']."') AND COD_PAIS = '".$data['COD_PAIS']."'");
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este departamento ya existe.", 'estado' => 1 );
  			} else {
				$this->db->where('COD_DEPTO', $coddepto);
				$item = $this->db->update('DEPARTAMENTO',$data);
				return ($item) ? array('mensaje' => "Departamento actualizado", 'estado' => 2) : array('mensaje' => "Error al actualizar", 'estado' => 2);
			}
	}

	public function insertMunicipio($data){
  		$item = $this->db->query("SELECT * from MUNICIPIO where upper(NOMBRE) = upper('".$data['NOMBRE']."') AND COD_DEPTO = '".$data['COD_DEPTO']."'");
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este municipio ya existe.", 'estado' => 1 );
  			} else {
			$item = $this->db->insert('MUNICIPIO', $data);
			return ($item) ?  array('mensaje' => "Nuevo municipio agregado", 'estado' => 2) : array('mensaje' => "Se presento un error", 'estado' => 2); 
			}
	}

	public function updateMuni($codmun, $data){
  		$item = $this->db->query("SELECT * from MUNICIPIO where COD_MUN !=".$codmun." and upper(NOMBRE) = upper('".$data['NOMBRE']."') AND COD_DEPTO = '".$data['COD_DEPTO']."'");
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este municipio ya existe.", 'estado' => 1 );
  			} else {
		$this->db->where('COD_MUN',$codmun);
		$item = $this->db->update('MUNICIPIO',$data);
		return ($item) ? array('mensaje' => "Municipio actualizado", 'estado' => 2) : array('mensaje' => "Error al actualizar", 'estado' => 2);
		}
	}

	public function getPais(){
		$query = $this ->db->get('PAIS');
	return ($query->num_rows() > 0) ? $query ->result_array(): false;
	}

	public function getDepto(){
		$query = $this->db->get('DEPARTAMENTO');
	return ($query->num_rows() > 0) ? $query ->result_array(): false;	
	}

	public function getDeptoId($id){
		$this->db->where('COD_PAIS', $id);
		$query = $this->db->get('DEPARTAMENTO');
	return $query->result_array();	
	}
} ?>
