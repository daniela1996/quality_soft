<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Rolmodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function getRol() {
		$query = $this->db->query("SELECT RM.*, MD.NAMEMODULE FROM TBL_ROLES_MENU RM INNER JOIN TBL_MODULES MD ON(RM.IDMODULES = MD.MODULEID) AND RM.STATUSROLE = 1");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getRolUser() {
		$query = $this->db->get("VW_ROLUSUARIO");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getRolUserParticular($rolid) {
		$this->db->where('ROLEID', $rolid);
		$query = $this->db->get("VW_ROLUSUARIO");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getRolIncompleto($userid) {
		$query = $this->db->query("SELECT RMN.ROLEID, RMN.CODEROLE, RMN.NAMEROLE, RMN.IDMODULES, RMN.DESCROLE, RMN.STATUSROLE, NVL(RUS.ESTADO, 0) HABILITADO FROM TBL_ROLES_MENU RMN LEFT OUTER JOIN TBL_ROLE_USER RUS ON(RMN.ROLEID = RUS.ROLEID) AND RMN.STATUSROLE = 1 AND RUS.USERID = ".$userid);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getRolParticular($rolid) {
		$query = $this->db->query("SELECT RM.*, MD.NAMEMODULE FROM TBL_ROLES_MENU RM INNER JOIN TBL_MODULES MD ON(RM.IDMODULES = MD.MODULEID) AND RM.STATUSROLE = 1 AND RM.ROLEID = ".$rolid);
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function getUltimoRolUsuario($roleid) {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(ROLEUSERID) ULTIMO FROM VW_ROLUSUARIO WHERE ROLEID = ".$roleid);
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function getRolUserNuevo($rolid, $limite) {
		$query = $this->db->query("SELECT * FROM (SELECT * FROM VW_ROLUSUARIO WHERE ROLEID = ".$rolid." ORDER BY ROLEUSERID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getRolNuevo($limite) {
		$query = $this->db->query("SELECT * FROM (SELECT RM.*, MD.NAMEMODULE FROM TBL_ROLES_MENU RM INNER JOIN TBL_MODULES MD ON(RM.IDMODULES = MD.MODULEID) ORDER BY RM.ROLEID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getUltimoRol() {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(ROLEID) ULTIMO FROM TBL_ROLES_MENU");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function insertRole($data) {
		 $item = $this->db->query("SELECT * from TBL_ROLES_MENU where upper(CODEROLE) = upper('".$data['CODEROLE']."')");
  		if ($item->num_rows() > 0 ) {
  			return array('mensaje' => "Este rol ya existe.", 'estado' => 1);
  		} else {
			$item = $this->db->insert('TBL_ROLES_MENU', $data);
			return ($item) ? array('mensaje' => "Nuevo Rol Agregado.", 'estado' => 2): array('mensaje' => "Se presento un Error.", 'estado' => 2);
		}
	}
	public function updateRole($rolid, $data) {
		$item = $this->db->query("SELECT * from TBL_ROLES_MENU where ROLEID != ('".$rolid."') AND (upper(CODEROLE) = upper('".$data['CODEROLE']."') OR upper(NAMEROLE) = upper('".$data['NAMEROLE']."'))");
  		if ($item->num_rows() > 0 ) {
  			return array('mensaje' => "Este rol ya existe.", 'estado' => 1);
  		} else {
			$this->db->where('ROLEID',$rolid);
			$item = $this->db->update('TBL_ROLES_MENU', $data);
			return ($item) ? array('mensaje' => "Rol Actualizado", 'estado' => 2): array('mensaje' => "Se presento un Error", 'estado' => 2);
		}
	}
	public function deleteRole($rolid) {
		$item = $this->db->query("UPDATE TBL_ROLES_MENU SET STATUSROLE = 0 WHERE ROLEID = ".$rolid);
		return ($item) ? "Rol Eliminado": "Se presento un Error";
	}
	public function insertRoleOUsuario($data) {
		$item = false;
		foreach($data as $eachData) {
			$this->db->where('USERID', $eachData["USERID"]);
			$this->db->where('ROLEID', $eachData["ROLEID"]);
			$query = $this->db->get("TBL_ROLE_USER");
			if($query->num_rows() > 0) {
				$item = $this->db->query("UPDATE TBL_ROLE_USER SET ESTADO = '1' WHERE USERID = ".$eachData["USERID"].", ROLEID = ".$eachData["ROLEID"]);
			} else {
				$item = $this->db->query("INSERT INTO TBL_ROLE_USER (USERID, ROLEID) VALUES (".$eachData["USERID"].", ".$eachData["ROLEID"].")");
			}
		}
		return ($item) ? "Asociación realizada con éxito.": "Se presento un error";
	}
	public function acceptDecline($roluserid, $estado) {
		$elEstado = ($estado == "A") ? 1 : 0;
		$item = $this->db->query("UPDATE TBL_ROLE_USER SET ESTADO = ".$elEstado." WHERE ROLEUSERID = ".$roluserid);
		return ($item) ? (($estado == "A") ? "Asociacion habilitada" : "Asociacion deshabilitada") : "Se presento un error";
	}
} ?>
