<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Bitacoraasignacionnodosmodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	public function getBitacora($fechainicial, $fechafinal){
		$query = "SELECT * FROM VW_BITACORA_LOGINTRANSACCION WHERE FECHA BETWEEN (to_date('".$fechainicial." 00:00:00', 'dd/mm/yyyy hh24:mi:ss')) AND (to_date('".$fechafinal." 23:59:59', 'dd/mm/yyyy hh24:mi:ss'))";
		$item = $this->db->query($query);
		return($item -> num_rows() > 0) ? $item -> result_array() : false;
	}
}?>
