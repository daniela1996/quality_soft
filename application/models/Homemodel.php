<?php

	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Homemodel extends CI_Model
	{

		function __construct()
		{
			parent::__construct();
		}

		public function getCallendarPo()
		{
			
	 		$query = $this->db->get('VW_CALENDAR_PO');

			if ($query->num_rows() > 0) 
			{
				return $query->result_array();	
			}
			else
			{
				return false;
			}
			
		}
		
		public function getCallendarPoBy($status)
		{
			
			$this->db->where('ESTADO',$status);
	 		$query = $this->db->get('VW_CALENDAR_PO');

			if ($query->num_rows() > 0) 
			{
				return $query->result_array();	
			}
			else
			{
				return false;
			}
			
		}
		
		public function getCallendarSolicitudes()
		{
			
	 		$query = $this->db->get('VW_CALENDAR_SOLICITUDES');

			if ($query->num_rows() > 0) 
			{
				return $query->result_array();	
			}
			else
			{
				return false;
			}
			
		}

	}
	
?>