<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Asignacionnodosmodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	public function insertAsignacionNodo($data){
		$item = false;
		$ultimoId = 0;
		$dataLogintransaccion = array();
		$dataTransaccional = array();
		$dataLogintransaccionUpdate = array();
		for ($i=0; $i < count($data) ; $i++) { 
			$fila = $data[$i];
			$this->db->where('COD_US', $fila['COD_US']);
			$this->db->where('IMEI', $fila['IMEI']);
			$query = $this->db->get("LOGINTRANSACCION");
			if ($query ->num_rows() > 0) {
				$autoid = $query->row(0)->AUTO_ID;
				$dataLogintransaccionUpdate[] = array('ESTADO' => 1, 'AUTO_ID' => $autoid, 'DESCRIPCION' => $fila['DESCRIPCION']);
			} else {
				$dataLogintransaccion[] = array('COD_US' => $fila['COD_US'],
								'IMEI' => $fila['IMEI'],
								'DESCRIPCION' => $fila['DESCRIPCION'],
								'COD_US_INSERT' => $fila['COD_US_INSERT']);
			}

			if ($fila["FECHA_INICIAL"] != "" && $fila["FECHA_FINAL"] != "") {
						$query2 = "INSERT INTO TBL_LOGINTRANSACCION_FECHAS (FECHA_INICIAL, FECHA_FINAL) VALUES ((to_date('".$fila["FECHA_INICIAL"]."', 'dd/mm/yyyy')), (to_date('".$fila["FECHA_FINAL"]."' , 'dd/mm/yyyy')))";
						$item2 = $this->db->query($query2);
						$item3 = $this->db->query("SELECT SQ_TBL_LOGINTRANSACCION_FECHAS.CURRVAL FROM DUAL");
						$ultimoId = $item3->row(0)->CURRVAL;
						$dataTransaccional[] = array('ID_FECHA' => $ultimoId,
													'IMEI' => $fila['IMEI'],
													'COD_US' => $fila['COD_US']);
			}
		}

	if (!empty($dataLogintransaccionUpdate)) {
		$item = $this->db->update_batch('LOGINTRANSACCION', $dataLogintransaccionUpdate, 'AUTO_ID');
	} 
	if (!empty($dataLogintransaccion)) {
		$item = $this->db->insert_batch('LOGINTRANSACCION', $dataLogintransaccion);
	}
	if (!empty($dataTransaccional)) {
			$item = $this->db->insert_batch('TBL_TRANSACCIONAL_F_L', $dataTransaccional);
	}
	return ($item) ? array('mensaje' => "Registros agregados correctamente.", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);
	}

	public function deleteAsignacionNodo($auto_id, $data, $array){
		$this->db->where('AUTO_ID', $auto_id);
		$item = $this->db->update('LOGINTRANSACCION', $data);
		
		$this->db->where('COD_US', $array['COD_US']);
		$this->db->where('IMEI', $array['IMEI']);
		$consulta = $this->db->get("TBL_TRANSACCIONAL_F_L");
		if ($consulta ->num_rows() > 0) {
			$idfecha = $consulta->row(0)->ID_FECHA;
			$id = $consulta->row(0)->ID;

			$this->db->where('ID', $id);
			$this->db->delete('TBL_TRANSACCIONAL_F_L');

			$this->db->where('ID_FECHA', $idfecha);
			$this->db->delete('TBL_LOGINTRANSACCION_FECHAS');
		}
		return ($item) ? array('mensaje' => "Registro eliminado", 'estado' => 2) : array('mensaje' => "Error al eliminar registro", 'estado' => 2);
	}

	public function getAsignacionNodo(){
		$this->db->where('ESTADO', '1');
		$query = $this->db->get('VW_LOGINTRANSACCION');
		return($query -> num_rows() > 0) ? $query -> result_array() : false;
	}

	public function getNodosporUsuarios($imei){
		$this->db->where('IMEI', $imei);
		$this->db->where('ESTADO', '1');
		$query = $this->db->get('VW_LOGINTRANSACCION');
		return($query -> num_rows() > 0) ? $query -> result_array() : false;
	}

	public function getUsuariosporNodos($user){
		$this->db->where('COD_US', $user);
		$this->db->where('ESTADO', '1');
		$query = $this->db->get('VW_LOGINTRANSACCION');
		return($query -> num_rows() > 0) ? $query -> result_array() : false;
	}

	public function getNodosId(){
		$query = $this->db->query('SELECT IMEI, DESCRIPCION FROM INFOANTENA');
		return($query -> num_rows() > 0) ? $query -> result_array() : false;
	}

	public function getNodosUser($user){
		$query = $this->db->query("SELECT I.IMEI, I.DESCRIPCION FROM INFOANTENA I WHERE I.IMEI NOT IN (SELECT L.IMEI FROM LOGINTRANSACCION L WHERE L.COD_US = '".$user."' AND L.ESTADO = 1)");
		return($query -> num_rows() > 0) ? $query -> result_array() : false;
	}

	public function getUserId($imei){
		$query = $this->db->query("SELECT U.U_ID, NAME FROM TBL_USERS U WHERE U.U_ID NOT IN (SELECT L.COD_US FROM LOGINTRANSACCION  L where L.IMEI = '".$imei."' AND L.ESTADO = 1)");
		return($query -> num_rows() > 0) ? $query -> result_array() : false;
	}
	public function getUser(){
		$query = $this->db->query("SELECT U_ID, NAME FROM TBL_USERS");
		return($query -> num_rows() > 0) ? $query -> result_array() : false;
	}
}?>