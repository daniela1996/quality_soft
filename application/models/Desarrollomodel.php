<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Desarrollomodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}

  public function getProyectos(){
    $query = $this->db->get('TBL_PROJECTS');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  public function getModulos($idProyecto){
    $this->db->where('STATUS_MODULE','1');
    $this->db->where('ID_PROJECTS', $idProyecto);
    $query = $this->db->get('TBL_PROJECT_MODULES');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  // public function getModuloProyecto($id){
  //   $this->db->where('ID_PROJECT_MODULES', $id);
  //   $query = $this->db->get('TBL_PROJECT_MODULES');
  //   return($query->num_rows() > 0) ? $query->result_array()[0] : false;
  // }

  public function getModuloProyecto($id){
    $this->db->where('ID_PROJECT_MODULES', $id);
    $query = $this->db->get('VW_PROJECT_MODULES');
    return($query->num_rows() > 0) ? $query->result_array()[0] : false;
  }

  public function getEmpresas(){
    $query = $this->db->get('TBL_BUSINESS_PROJECTS');
    return($query->num_rows() > 0) ? $query->result_array() : false;
  }

  public function getEstados() {
    $query = $this->db->query("SELECT * FROM TBL_MAINTENANCE WHERE MAINTENANCE_CODE LIKE 'EST%' AND STATUS_MAINTENANCE = 1");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }

  public function getPrioridades() {
    $query = $this->db->query("SELECT * FROM TBL_MAINTENANCE WHERE MAINTENANCE_CODE LIKE 'PRI%' AND STATUS_MAINTENANCE = 1");
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }

  public function getEncargados() {
    $this->db->where('ESTADO', 1);
    $this->db->select( 'U_ID, NAME');
    $query =  $this->db->get('TBL_USERS');
    return ($query->num_rows() > 0) ? $query->result_array(): false;
  }

  public function insertProjects($data, $usuarios) {
      $item = $this->db->query("SELECT * from TBL_PROJECTS where PROJECT_NAME = '".$data['PROJECT_NAME']."'");
      if ($item->num_rows() > 0 ) {
        return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
      } else {
        $item = $this->db->insert('TBL_PROJECTS', $data);

        $query = $this->db->query("SELECT SQ_TBL_PROJECTS.CURRVAL FROM DUAL");
        $ultimoId = $query->row(0)->CURRVAL;

        $dataUsuarios = array();

        for ($i=0; $i < count($usuarios); $i++) {
            $fila = $usuarios[$i];
            $dataUsuarios[] = array('U_ID' => $fila['U_ID'], 'ID_PROJECTS' => $ultimoId);
        }

        if (!empty($dataUsuarios))
        {
          $item = $this->db->insert_batch('TBL_WORK_TEAM', $dataUsuarios);
        }

        return ($item) ? array('mensaje' => "Nuevo registro agregado", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);  
      }
  }

  public function updateProjects($data, $idProyecto, $usuarios) {
      $item = $this->db->query("SELECT * from TBL_PROJECTS where ID_PROJECTS != ".$idProyecto." and PROJECT_NAME = '".$data['PROJECT_NAME']."'");
      if ($item->num_rows() > 0 ) {
        return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
      } else {
        $this->db->where('ID_PROJECTS', $idProyecto);
        $item = $this->db->update('TBL_PROJECTS', $data);

        $dataUsuarios = array();

        for ($i=0; $i < count($usuarios); $i++) {
            $fila = $usuarios[$i];
            $dataUsuarios[] = array('U_ID' => $fila['U_ID'], 'ID_PROJECTS' => $idProyecto);
        }

        if (!empty($dataUsuarios))
        {
          $item = $this->db->query("DELETE FROM TBL_WORK_TEAM WHERE ID_PROJECTS = ".$idProyecto);
          $item = $this->db->insert_batch('TBL_WORK_TEAM', $dataUsuarios);
        }

        return ($item) ? array('mensaje' => "Registro actualizado correctamente.", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);  
      }
  }

  public function insertModulos($data, $usuarios) {
      $item = $this->db->query("SELECT * from TBL_PROJECT_MODULES where PROJECT_MODULE_NAME = '".$data['PROJECT_MODULE_NAME']."' and ID_PROJECTS = '".$data["ID_PROJECTS"]."'");
      if ($item->num_rows() > 0 ) {
        return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
      } else {
        $item = $this->db->query("INSERT INTO TBL_PROJECT_MODULES (PROJECT_MODULE_NAME, DESCRIPTION, PRIORITY, ADVANCE_PERCENTAGE, ESTIMATED_TIME, REAL_TIME, DELAY_TIME, INITIAL_ESTIMATED_DATE, FINAL_ESTIMATED_DATE, INITIAL_REAL_DATE, FINAL_REAL_DATE, DEVELOPMENT_CYCLE, ID_PROJECTS) VALUES ('".$data['PROJECT_MODULE_NAME']."', '".$data['DESCRIPTION']."', '".$data['PRIORITY']."', '100%', '".$data['ESTIMATED_TIME']."', 10, 0, to_date('".$data["INITIAL_ESTIMATED_DATE"]."', 'dd/mm/yyyy'), to_date('".$data["FINAL_ESTIMATED_DATE"]."' , 'dd/mm/yyyy'), sysdate, sysdate, 1, ".$data["ID_PROJECTS"].")");

        $query = $this->db->query("SELECT SQ_TBL_PROJECT_MODULES.CURRVAL FROM DUAL");
        $ultimoId = $query->row(0)->CURRVAL;

        // $query = $this->db->query("SELECT SQ_TBL_PROJECTS.CURRVAL FROM DUAL");
        // $ultimoId = $query->row(0)->CURRVAL;

        $dataUsuarios = array();

        for ($i=0; $i < count($usuarios); $i++) {
            $fila = $usuarios[$i];
            $dataUsuarios[] = array('U_ID' => $fila['U_ID'], 'ID_PROJECTS_MODULE' => $ultimoId);
        }

        if (!empty($dataUsuarios))
        {
          $item = $this->db->insert_batch('TBL_MODULES_WORK_TEAM', $dataUsuarios);
        }

        return(array('id' => $ultimoId));
      // return ($item) ? array('mensaje' => "Nuevo registro agregado", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);  
      }
  }

    public function updateModulos($data, $idmodulo, $usuarios) {
      $item = $this->db->query("SELECT * from TBL_PROJECT_MODULES where ID_PROJECT_MODULES != ".$idmodulo." and PROJECT_MODULE_NAME = '".$data['PROJECT_MODULE_NAME']."' and ID_PROJECTS = ".$data['ID_PROJECTS']."");
      if ($item->num_rows() > 0 ) {
        return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
      } else {
        $item = $this->db->query("UPDATE TBL_PROJECT_MODULES SET PROJECT_MODULE_NAME = '".$data['PROJECT_MODULE_NAME']."', DESCRIPTION = '".$data['DESCRIPTION']."', MODULES_NUMBER = '".$data['MODULES_NUMBER']."', PRIORITY = '".$data['PRIORITY']."', ADVANCE_PERCENTAGE = '100%', ESTIMATED_TIME = '".$data['ESTIMATED_TIME']."', REAL_TIME = 10, DELAY_TIME = 0, INITIAL_ESTIMATED_DATE = to_date('".$data["INITIAL_ESTIMATED_DATE"]."', 'dd/mm/yyyy'), FINAL_ESTIMATED_DATE = to_date('".$data["FINAL_ESTIMATED_DATE"]."' , 'dd/mm/yyyy'), INITIAL_REAL_DATE = sysdate, FINAL_REAL_DATE = sysdate, DEVELOPMENT_CYCLE = 1, ID_PROJECTS = ".$data["ID_PROJECTS"]." WHERE ID_PROJECT_MODULES = ".$idmodulo."");

        // $this->db->where('ID_PROJECTS', $idProyecto);
        // $item = $this->db->update('TBL_PROJECTS', $data);

        $dataUsuarios = array();

        for ($i=0; $i < count($usuarios); $i++) {
            $fila = $usuarios[$i];
            $dataUsuarios[] = array('U_ID' => $fila['U_ID'], 'ID_PROJECTS_MODULE' => $idmodulo);
        }

        if (!empty($dataUsuarios))
        {
          $item = $this->db->query("DELETE FROM TBL_MODULES_WORK_TEAM WHERE ID_PROJECTS_MODULE = ".$idmodulo);
          $item = $this->db->insert_batch('TBL_MODULES_WORK_TEAM', $dataUsuarios);
        }

      return ($item) ? array('mensaje' => "Registro actualizado correctamente.", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);  
      }
    }

    public function deleteModulo($idmodulo) {
      $item = $this->db->query("UPDATE TBL_PROJECT_MODULES SET STATUS_MODULE = 0 WHERE ID_PROJECT_MODULES = ".$idmodulo);
      return ($item) ? array('mensaje' => "Registro eliminado", 'estado' => 2) : array('mensaje' => "Error al eliminar registro", 'estado' => 2);
    }

    //Capturar usuarios de un proyecto guardados en la BD
    public function getArregloUsuarios($idProyecto){
      $this->db->where('ID_PROJECTS', $idProyecto);
      $query = $this->db->get('VW_PROJECT_USERS');
      return($query->num_rows() > 0) ? $query->result_array() : false;
    }

    //Capturar usuarios de un modulo guardados en la BD
    public function getArregloUsuariosPorModulo($idModulo){
      $this->db->where('ID_PROJECTS_MODULE', $idModulo);
      $query = $this->db->get('VW_PROJECT_MODULE_USERS');
      return($query->num_rows() > 0) ? $query->result_array() : false;
    }
}?>