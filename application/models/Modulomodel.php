<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Modulomodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function getModulo() {
		$query = $this->db->query("SELECT MO.*, 0 HABILITADO FROM TBL_MODULES MO WHERE MO.STATUSMODULE = 1");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getModuloParticular($moduloid) {
		$this->db->where('MODULEID',$moduloid);
		$this->db->where('STATUSMODULE','1');
		$query = $this->db->get("TBL_MODULES");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function getModuloIncompleto($perfilid) {
		$query = $this->db->query("SELECT * FROM (SELECT MD.MODULEID, MD.CODEMODULE, MD.NAMEMODULE, MD.DESCMODULE, MD.DATES, MD.STATUSMODULE, NVL(PRM.ESTADO, 0) HABILITADO FROM TBL_MODULES MD LEFT OUTER JOIN TBL_PROF_MODULE PRM ON(MD.MODULEID = PRM.MODULEID AND PRM.PROFILEID = ".$perfilid.")) WHERE STATUSMODULE = 1");
		// ("SELECT MD.MODULEID, MD.CODEMODULE, MD.NAMEMODULE, MD.DESCMODULE, MD.DATES, MD.STATUSMODULE, NVL(PRM.ESTADO, 0) HABILITADO FROM TBL_MODULES MD LEFT OUTER JOIN TBL_PROF_MODULE PRM ON(MD.MODULEID = PRM.MODULEID) AND MD.STATUSMODULE = 1 AND PRM.PROFILEID = ".$perfilid);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getModuloPerfilParticular($moduloid) {
		$this->db->where('MODULEID',$moduloid);
		$query = $this->db->get("VW_MODULOPERFIL");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getModuloPerfilNuevo($moduloid, $limite) {
		$query = $this->db->query("SELECT * FROM (SELECT * FROM VW_MODULOPERFIL WHERE MODULEID = ".$moduloid." ORDER BY PROFMODULEID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getModuloNuevo($limite) {
		$query = $this->db->query("SELECT * FROM (SELECT * FROM TBL_MODULES ORDER BY MODULEID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getUltimoModuloPerfil($moduloid) {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(PROFMODULEID) ULTIMO FROM VW_MODULOPERFIL WHERE MODULEID = ".$moduloid);
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function getUltimoModulo() {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(MODULEID) ULTIMO FROM TBL_MODULES");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function insertModulo($data) {		
		 $item = $this->db->query("SELECT * from TBL_MODULES where upper(CODEMODULE) = upper('".$data['CODEMODULE']."') or upper(NAMEMODULE) = upper('".$data['NAMEMODULE']."') ");
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este módulo ya existe.", 'estado' => 1);
  			} else {
				$item = $this->db->insert('TBL_MODULES', $data);
				return ($item) ? array('mensaje' => "Nuevo Módulo Agregado", 'estado' => 2): array('mensaje' => "Se presento un Error", 'estado' => 2);
			}
	}
	public function updateModulo($moduloid, $data) {
		$item = $this->db->query("SELECT * from TBL_MODULES where MODULEID != ('".$moduloid."') AND (upper(CODEMODULE) = upper('".$data['CODEMODULE']."') or upper(NAMEMODULE) = upper('".$data['NAMEMODULE']."'))");
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este módulo ya existe.", 'estado' => 1);
  			} else {
				$this->db->where('MODULEID',$moduloid);
				$item = $this->db->update('TBL_MODULES', $data);
				return ($item) ? array('mensaje' => "Módulo Actualizado", 'estado' => 2 ): array('mensaje' => "Se presentó un Error", 'estado' => 2 );
			}
	}
	public function deleteModulo($moduloid) {
		$item = $this->db->query("UPDATE TBL_MODULES SET STATUSMODULE = 0 WHERE MODULEID = ".$moduloid);
		return ($item) ? array('mensaje' => "Módulo eliminado", 'estado' => 2 ): array('mensaje' => "Se presentó un Error", 'estado' => 2 );
	}
	public function getPerfilModulo() {
		$query = $this->db->get("VW_MODULOPERFIL");
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	public function insertPerfilOModulo($data) {
		$item = false;
		foreach($data as $eachData) {
			$this->db->where('MODULEID', $eachData["MODULEID"]);
			$this->db->where('PROFILEID', $eachData["PROFILEID"]);
			$query = $this->db->get("TBL_PROF_MODULE");
			if($query->num_rows() > 0) {
				$item = $this->db->query("UPDATE TBL_PROF_MODULE SET ESTADO = 1 WHERE MODULEID = ".$eachData["MODULEID"]." AND PROFILEID = ".$eachData["PROFILEID"]);
			} else {
				$item = $this->db->query("INSERT INTO TBL_PROF_MODULE (PROFILEID, MODULEID) VALUES (".$eachData["PROFILEID"].", ".$eachData["MODULEID"].")");
			}
		}
		return ($item) ? "Asociación realizada con éxito." : "Se presento un error";
	}
	public function acceptDecline($profmoduleid, $estado) {
		$elEstado = ($estado == "A") ? 1 : 0;
		$item = $this->db->query("UPDATE TBL_PROF_MODULE SET ESTADO = ".$elEstado." WHERE PROFMODULEID = ".$profmoduleid);
		return ($item) ? (($estado == "A") ? "Asociacion habilitada" : "Asociacion deshabilitada") : "Se presento un error";
	}
} ?>
