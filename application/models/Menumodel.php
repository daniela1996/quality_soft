<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Menumodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function getMenu() {
		$query = $this->db->query("SELECT DM.*, MO.NAMEMODULE FROM DYN_MENUS DM INNER JOIN TBL_MODULES MO ON(DM.MODULEID = MO.MODULEID)");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getMenuParticular($id) {
		$query = $this->db->query("SELECT DM.*, MO.NAMEMODULE FROM DYN_MENUS DM INNER JOIN TBL_MODULES MO ON(DM.MODULEID = MO.MODULEID) AND DM.ID = ".$id);
		return ($query->num_rows() > 0)? $query->result_array()[0]: false;
	}
	public function insertMenu($data) {
		if ($data['URL'] == '/'){
		 $item = $this->db->query("SELECT * from DYN_MENUS where upper(TITLE) = upper('".$data['TITLE']."')");	
		}else{
		 $item = $this->db->query("SELECT * from DYN_MENUS where upper(TITLE) = upper('".$data['TITLE']."') or upper(URL) = upper('".$data['URL']."')");
		}
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este menu ya existe.", 'estado' => 1);
  			} else {
				$item = $this->db->insert('DYN_MENUS', $data);
				return ($item) ? array('mensaje' => "Nuevo Menu Agregado", 'estado' => 2): array('mensaje' => "Se presento un Error" , 'estado' => 2);
			}
	}
	public function updateMenu($menuid, $data) {
		if ($data['URL'] == '/') {
			$item = $this->db->query("SELECT * from DYN_MENUS where upper(TITLE) = upper('".$data['TITLE']."') and ID != ('".$menuid."')");
		} else {
			$item = $this->db->query("SELECT * from DYN_MENUS where (upper(TITLE) = upper('".$data['TITLE']."') or upper(URL) = upper('".$data['URL']."')) and ID != ('".$menuid."')");
		}
  		if ($item->num_rows() > 0) {
  			return array('mensaje' => "Este menu ya existe.", 'estado' => 1);
  		} else {
			$this->db->where('ID',$menuid);
			$item = $this->db->update('DYN_MENUS', $data);
			return ($item) ? array('mensaje' =>"Menu Actualizado", 'estado' => 2): array('mensaje' =>"Se presento un Error", 'estado' => 2);
		}
	}
	public function deleteMenu($menuid, $estado) {
		$item = $this->db->query("UPDATE DYN_MENUS SET SHOW_MENU = '".$estado."' WHERE ID = ".$menuid);
		if ($estado == 0) {
			return ($item) ? array('mensaje' =>"Menu desactivado." , 'estado' => 3): array('mensaje' => "Se presento un error", 'estado'=> 2) ;
		} else {
			return ($item) ? array('mensaje' =>"Menu activado.", 'estado' => 3): array('mensaje' => "Se presento un error",'estado' => 3);
		}
	}
	public function getUltimoMenu() {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(ID) ULTIMO FROM DYN_MENUS");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function getMenuNuevo($limite) {
		$query = $this->db->query("SELECT * FROM (SELECT DM.*, MO.NAMEMODULE FROM DYN_MENUS DM INNER JOIN TBL_MODULES MO ON(DM.MODULEID = MO.MODULEID) ORDER BY DM.ID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
} ?>
