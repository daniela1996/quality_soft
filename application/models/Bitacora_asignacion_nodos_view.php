<?php $alertasid = array('errorBitacora', 'successBitacora', 'warningBitacora'); include('template/alert_template.php'); ?>
<section class="content-header">
	<h1>GPS: <small> Bitácora Asignación de Nodos</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Bitácora asignación de Nodos</h2>
				<div class="form-group col-md-3">
					<label>Fecha de Inicial:</label>
                    <div class='input-group date' id='datetimepicker2'>
                    	<input type='text' class="form-control asignacionesinput" placeholder="dd/mm/yyyy" id='FechaInicial' name = 'FechaInicial'/>
                    	<span class="input-group-addon">
                        	<span class="glyphicon glyphicon-calendar"></span>
                    	</span>
                	</div>
				</div>
				<div class="form-group col-md-3">
					<label>Fecha de Final:</label>
					<div class='input-group date' id='datetimepicker2'>
                    	<input type='text' class="form-control asignacionesinput" id='FechaFinal' name = 'FechaFinal' placeholder="dd/mm/yyyy" />
                    	<span class="input-group-addon">
                        	<span class="glyphicon glyphicon-calendar"></span>
                    	</span>
                	</div>
				</div>
				<div class="form-group col-md-2">
					<p style="line-height: 80px;"><span id="searchBitacora" class="btn btn-success"><i class='icon-edit icon-white fa fa-search'></i> Buscar</span></p> 
				</div>
			
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable" id = "TblBitacora">
					<thead>
						<tr>
							<th class="center cargarInfo">Usuario actualización</th>
							<th class="center cargarInfo">Nodo</th>
							<th class="center cargarInfo">Usuario asignado</th>
							<th class="center cargarInfo">Fecha</th>
							<th class="center cargarInfo">Acción realizada</th>
						</tr>
					</thead>
					<tbody class="tableViewer" id="anclaTabla">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>