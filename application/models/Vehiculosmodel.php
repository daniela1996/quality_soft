<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Vehiculosmodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function getVehiculos(){
		$query = $this->db->get('DETAILVEHICLE');
	return($query->num_rows() > 0) ? $query ->result_array(): false;
	}

	public function insertVehiculo($data){
		$item = $this->db->query("SELECT MOTOR from DETAILVEHICLE where upper(MOTOR) = upper('".$data['MOTOR']."') or upper(CHASSIS) = upper('".$data['CHASSIS']."') or upper(PLATE) = upper('".$data['PLATE']."')");
  		if ($item->num_rows() > 0 ) {
  			return array('mensaje' => "Este registro ya existe.", 'estado' => 1);
  		} else {
		$item = $this->db->insert('DETAILVEHICLE', $data);
		return($item) ? array('mensaje' => "Registro agregado correctamente", 'estado' => 2 ): array('mensaje' =>  "Error al agregar registros", 'estado' => 2);
		}
	}
	public function updateVehiculo($id,$data){
		$item = $this->db->query("SELECT MOTOR from DETAILVEHICLE where (upper(MOTOR) = upper('".$data['MOTOR']."') or upper(CHASSIS) = upper('".$data['CHASSIS']."') or upper(PLATE) = upper('".$data['PLATE']."')) and ID != ('".$id."')");
  		if ($item->num_rows() > 0) {
  			return array('mensaje' => "Este registro ya existe.", 'estado' => 1);
  		} else {
			$this->db->where('ID', $id);
			$item = $this->db->update('DETAILVEHICLE',$data);
			return($item) ? array('mensaje' => "Registro actualizado" ): array('mensaje' =>  "Error al actualizar");
		}
	}	
	public function getUsers(){
		$query = $this->db->query("SELECT * FROM DETAILVEHICLE WHERE ID IN (SELECT MAX(ID) FROM DETAILVEHICLE)");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}	
}?>