<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Gestiongpsmodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function getInfoantena(){
		$query = $this->db->get('VW_INFOANTENA');
		return($query->num_rows() > 0) ? $query ->result_array(): false;
	}

	public function insertGestion($data){
		$item = $this->db->query("SELECT COD_ANT from INFOANTENA where upper(COD_ANT) = upper('".$data['COD_ANT']."')");
  		if ($item->num_rows() > 0 ) {
  			return array('mensaje' => "Este registro ya existe.", 'estado' => 1);
  		} else {
			$item = $this->db->insert('INFOANTENA', $data);
			return($item) ? array('mensaje' => "Registro agregado correctamente", 'estado' => 2) : array('mensaje' => "Error al agregar registro", 'estado' => 2 );
		}
	}
	public function getUser(){
		$query = $this->db->get('TBL_USERS');
		return ($query->num_rows() > 0) ? $query ->result_array(): false;
	}
	public function updateGestion($autoid,$data){
		$item = $this->db->query("SELECT COD_ANT from INFOANTENA where upper(COD_ANT) = upper('".$data['COD_ANT']."') and AUTO_ID != ('".$autoid."')");
  		if ($item->num_rows() > 0) {
  			return array('mensaje' => "Este registro ya existe.", 'estado' => 1);
  		} else {
			$this->db->where('AUTO_ID', $autoid);
			$item = $this->db->update('INFOANTENA',$data);
			return($item) ? array('mensaje' =>  "Registro actualizado", 'estado' => 2) : array('mensaje' =>  "Error al actualizar", 'estado' => 2);
		}
	}
	public function ultimoDato(){
		$query = $this->db->query("SELECT * FROM VW_INFOANTENA WHERE AUTO_ID IN (SELECT MAX(AUTO_ID) FROM VW_INFOANTENA)");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}	
}?>