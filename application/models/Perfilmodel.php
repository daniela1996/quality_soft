<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Perfilmodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function getPerfil() {
		$query = $this->db->query("SELECT PRO.*, 0 HABILITADO FROM TBL_PROFILE PRO WHERE PRO.STATUS = 1");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getPerfilIncompleto($moduloid) {
		$query = $this->db->query("SELECT * FROM (SELECT PF.PROFILEID, PF.CODEPROFILE, PF.NAMEPROFILE, PF.DESCPROFILE, PF.DATES, PF.STATUS, NVL(PRM.ESTADO, 0) HABILITADO FROM TBL_PROFILE PF LEFT OUTER JOIN TBL_PROF_MODULE PRM ON(PF.PROFILEID = PRM.PROFILEID AND PRM.MODULEID = ".$moduloid.")) WHERE STATUS = 1");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getPerfilUIncompleto($userid) {
    $query = $this->db->query("SELECT PF.PROFILEID, PF.CODEPROFILE, PF.NAMEPROFILE, PF.DESCPROFILE, PF.DATES, PF.STATUS, NVL(UPF.ESTADO, 0) HABILITADO FROM TBL_PROFILE PF LEFT OUTER JOIN TBL_USERS_PROFILES UPF ON(PF.PROFILEID = UPF.PROFILEID) AND PF.STATUS = 1 AND UPF.USERID = ".$userid);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
  }
	public function getPerfilModuloParticular($perfilid) {
		$this->db->where('PROFILEID',$perfilid);
		$query = $this->db->get("VW_MODULOPERFIL");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getPerfilModuloNuevo($perfilid, $limite) {
		$query = $this->db->query("SELECT * FROM (SELECT * FROM VW_MODULOPERFIL WHERE PROFILEID = ".$perfilid." ORDER BY PROFMODULEID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getPerfilUsuarioNuevo($perfilid, $limite) {
		$query = $this->db->query("SELECT * FROM (SELECT * FROM VW_USUARIOPERFIL WHERE PROFILEID = ".$perfilid." ORDER BY USERPROFILEID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getPerfilNuevo($limite) {
		$query = $this->db->query("SELECT * FROM (SELECT * FROM TBL_PROFILE ORDER BY PROFILEID DESC) WHERE ROWNUM <= ".$limite);
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getUltimoPerfilModulo($perfilid) {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(PROFMODULEID) ULTIMO FROM VW_MODULOPERFIL WHERE PROFILEID = ".$perfilid);
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function getUltimoPerfilUsuario($perfilid) {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(USERPROFILEID) ULTIMO FROM VW_USUARIOPERFIL WHERE PROFILEID = ".$perfilid);
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function getUltimoPerfil() {
		$query = $this->db->query("SELECT COUNT(*) CONTEO, MAX(PROFILEID) ULTIMO FROM TBL_PROFILE");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function getPerfilUsuarioParticular($perfilid) {
		$this->db->where('PROFILEID',$perfilid);
		$query = $this->db->get("VW_USUARIOPERFIL");
		return ($query->num_rows() > 0) ? $query->result_array(): false;
	}
	public function getPerfilParticular($perfilid) {
		$this->db->where('STATUS','1');
		$this->db->where('PROFILEID',$perfilid);
		$query = $this->db->get("TBL_PROFILE");
		return ($query->num_rows() > 0) ? $query->result_array()[0]: false;
	}
	public function insertPerfil($data) {
 		$item = $this->db->query("SELECT * from TBL_PROFILE where upper(CODEPROFILE) = upper('".$data['CODEPROFILE']."') or upper(NAMEPROFILE) = upper('".$data['NAMEPROFILE']."')");
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este perfil ya existe.", 'estado' => 1);
  			} else {
			$item = $this->db->insert('TBL_PROFILE', $data);
			return ($item) ? array('mensaje' => "Nuevo Perfil Agregado", 'estado' => 2 ): array('mensaje' => "Se presento un Error", 'estado' => 2);
			}
	}
	public function updatePerfil($perfilid, $data) {
		$item = $this->db->query("SELECT * from TBL_PROFILE where PROFILEID != ('".$perfilid."') and (upper(CODEPROFILE) = upper('".$data['CODEPROFILE']."')  or upper(NAMEPROFILE) = upper('".$data['NAMEPROFILE']."'))");
  			if ($item->num_rows() > 0 ) {
  				return array('mensaje' => "Este perfil ya existe.", 'estado' => 1);
  			} else {
				$this->db->where('PROFILEID',$perfilid);
				$item = $this->db->update('TBL_PROFILE', $data);
				return ($item) ? array('mensaje' => "Perfil Actualizado", 'estado'=> 2): array('mensaje'=>"Se presento un Error", 'estado' => 2);
			}
	}
	public function deletePerfil($perfilid) {
		$item = $this->db->query("UPDATE TBL_PROFILE SET STATUS = 0 WHERE PROFILEID = ".$perfilid);
		return ($item) ? array('mensaje' => "Perfil eliminado", 'estado'=> 2): array('mensaje'=>"Se presento un error", 'estado' => 2);
	}
	public function getPerfilUsuario() {
		$query = $this->db->get("VW_USUARIOPERFIL");
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	public function insertPerfilOUsuario($data) {
		$item = false;
		foreach($data as $eachData) {
			$this->db->where('USERID', $eachData["USERID"]);
			$this->db->where('PROFILEID', $eachData["PROFILEID"]);
			$query = $this->db->get("TBL_USERS_PROFILES");
			if($query->num_rows() > 0) {
				$item = $this->db->query("UPDATE TBL_USERS_PROFILES SET ESTADO = '1' WHERE USERID = ".$eachData["USERID"]." AND PROFILEID = ".$eachData["PROFILEID"]);
			} else {
				$item = $this->db->query("INSERT INTO TBL_USERS_PROFILES (PROFILEID, USERID) VALUES (".$eachData["PROFILEID"].", ".$eachData["USERID"].")");
			}
		}
		return ($item) ? "Asociación realizada con éxito." : "Se presento un error";
	}
	public function acceptDecline($profuserid, $estado) {
		$elEstado = ($estado == "A") ? 1 : 0;
		$item = $this->db->query("UPDATE TBL_USERS_PROFILES SET ESTADO = ".$elEstado." WHERE USERPROFILEID = ".$profuserid);
		return ($item) ? (($estado == "A") ? "Asociacion habilitada" : "Asociacion deshabilitada") : "Se presento un error";
	}
} ?>
