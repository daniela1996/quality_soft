<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Reportemodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	public function getDetalleModulo($proyecto){
	    $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_MODULE_REPORT');

	    if ($query->num_rows() > 0) 
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	  public function getIncidenciasGlobales($proyecto){
	    $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_GLOBAL_INCIDENCES_REPORT');

	    if ($query->num_rows() > 0) 
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	  public function getIncidencias($proyecto){
	    $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_INCIDENCES_REPORT_NEW');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	  public function getReporteriaDiariaFecha(){
	    $query = $this->db->get('VW_DAILY_REPORT_DATE');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	  public function getReporteriaDiaria($proyecto){
	  	$this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_LAST_ACTUAL_PERCENTAGE_NEW');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	 //  public function getReporteriaDiaria($proyecto){
	 //  	$this->db->where('ID_PROJECTS', $proyecto);
	 //    $query = $this->db->get('VW_DAILY_REPORT');

	 //    if ($query->num_rows() > 0)
		// {
		// 	$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		// }
		// else
		// {
		// 	$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		// }

		// return $Resultado;
	 //  }

	  public function getIncidenciasTotales($proyecto){
	  	$this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_TOTAL_INCIDENCES');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	  public function getIncidenciasCorregidas($proyecto){
	    $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_INCIDENCES_CORRECTED');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	  public function getMatriz($proyecto){
	    // $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_GROUP_MTXPROJECT');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	  public function getDataCasos($proyecto){
	    // $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_DATA_PROOF_EVIDENCE');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }

	  public function getEvidenciasPruebas($proyecto){
	    // $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_EVIDENCE_PROOF');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		// print_r($Resultado);
		return $Resultado;

	  }

	  public function getVideosPruebas($proyecto){
	    // $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_VIDEOS_PROOF');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		// print_r($Resultado);
		return $Resultado;

	  }

	  public function getEvidenciasPruebasSinNulos($proyecto){
	    // $this->db->where('STATUS_MODULE', 1);
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_PROOF_EVIDENCE_WITHOUT_NULL');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		// print_r($Resultado);
		return $Resultado;

	  }

	  public function modulosResumen($proyecto){
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_MODULES_SUMMARY');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		// print_r($Resultado);
		return $Resultado;

	  }

	  public function proyectosResumen($proyecto){
	    $this->db->where('ID_PROJECTS', $proyecto);
	    $query = $this->db->get('VW_PROJECTS_SUMMARY');

	    if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		// print_r($Resultado);
		return $Resultado;

	  }

	  public function getCorreosPorProyectos($id_proyecto){
	    $query = $this->db->query("SELECT DISTINCT CORREO FROM VW_USUARIOS_X_PROYECTO WHERE ID_PROYECTO = ".$id_proyecto." ");
			$emails = false;
	    if ($query->num_rows() > 0) {
				$i = 0;
				foreach ($query->result_array() as $row) {
					$send[$i] = $row['CORREO'];
					$i++;
				}
				$emails = implode(', ', $send);
	      // $respuesta = array('Estado' => true, 'Mensaje' => 'Se cargaron los correos correctamente!', 'Resultado' => $query->result_array());
	    }
	    return $emails;
	  }

	  public function getProyectoCorreo($id_proyecto){
	    $query = $this->db->query("SELECT DISTINCT NOMBRE_PROYECTO FROM VW_USUARIOS_X_PROYECTO WHERE ID_PROYECTO = ".$id_proyecto." ");

	    /*if ($query->num_rows() > 0)
		{
			return $query;
		}*/

		if ($query->num_rows() > 0)
		{
			$Resultado = array('Estado' => true, 'Mensaje' => 'Se cargaron los datos correctamente!', 'Resultado' => $query->result_array());	
		}
		else
		{
			$Resultado = array('Estado' => false, 'Mensaje' => 'No se encontraron registros!', 'Resultado' => $query->result_array());
		}

		return $Resultado;
	  }
}?>