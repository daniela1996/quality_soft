<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Mantenimientosmodel extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	public function getTablas(){
		$query = $this->db->get('TBL_MAINTENANCE_TABLES');
		return($query->num_rows() > 0) ? $query->result_array() : false;
	}

	public function getMantenimientos($idtabla){
    $this->db->where('STATUS_MAINTENANCE','1');
		$this->db->where('ID_MAINTENANCE_TABLE', $idtabla);
		$query = $this->db->get('TBL_MAINTENANCE');
		return($query->num_rows() > 0) ? $query->result_array() : false;
	}

  public function getMantenimientosTablas($id){
    $this->db->where('ID_MAINTENANCE', $id);
    $query = $this->db->get('VW_MAINTENANCE_TABLES');
    return($query->num_rows() > 0) ? $query->result_array()[0] : false;
  }

	public function insertTablas($data) {
  		$item = $this->db->query("SELECT * from TBL_MAINTENANCE_TABLES where TABLE_NAME = '".$data['TABLE_NAME']."' OR TABLE_CODE = '".$data['TABLE_CODE']."'");
  		if ($item->num_rows() > 0 ) {
  			return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
  		} else {
  			$item = $this->db->insert('TBL_MAINTENANCE_TABLES', $data);
        return ($item) ? array('mensaje' => "Nuevo registro agregado", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);	
  		}
	}

	public function updateTablas($data, $idtabla) {
  		$item = $this->db->query("SELECT * from TBL_MAINTENANCE_TABLES where ID_MAINTENANCE_TABLE != ".$idtabla." and TABLE_NAME = '".$data['TABLE_NAME']."'");
  		if ($item->num_rows() > 0 ) {
  			return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
  		} else {
  			$this->db->where('ID_MAINTENANCE_TABLE', $idtabla);
  			$item = $this->db->update('TBL_MAINTENANCE_TABLES', $data);
			return ($item) ? array('mensaje' => "Registro actualizado correctamente.", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);	
  		}
	}

	public function insertMantenimientos($data) {
  		$item = $this->db->query("SELECT * from TBL_MAINTENANCE where MAINTENANCE_CODE = '".$data['MAINTENANCE_CODE']."' and ID_MAINTENANCE_TABLE = ".$data['ID_MAINTENANCE_TABLE']." and STATUS_MAINTENANCE = 1");
  		if ($item->num_rows() > 0 ) {
  			return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
  		} else {
  			$item = $this->db->insert('TBL_MAINTENANCE', $data);
        $query = $this->db->query("SELECT SQ_TBL_MAINTENANCE.CURRVAL FROM DUAL");
        $ultimoId = $query->row(0)->CURRVAL;
        return(array('id' => $ultimoId));
			//return ($item) ? array('mensaje' => "Nuevo registro agregado", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);	
  		}
	}

	public function updateMantenimientos($data, $idmantenimiento) {
  		$item = $this->db->query("SELECT * from TBL_MAINTENANCE where ID_MAINTENANCE != ".$idmantenimiento." and MAINTENANCE_NAME = '".$data['MAINTENANCE_NAME']."' and ID_MAINTENANCE_TABLE = ".$data['ID_MAINTENANCE_TABLE']." and STATUS_MAINTENANCE = 1");
  		if ($item->num_rows() > 0 ) {
  			return array('mensaje' => "Este registro ya existe.", 'estado' => 1 );
  		} else {
  			$this->db->where('ID_MAINTENANCE', $idmantenimiento);
  			$item = $this->db->update('TBL_MAINTENANCE', $data);
			return ($item) ? array('mensaje' => "Registro actualizado correctamente.", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);	
  		}
	}

  public function deleteMantenimiento($idmantenimiento) {
      $item = $this->db->query("UPDATE TBL_MAINTENANCE SET STATUS_MAINTENANCE = 0 WHERE ID_MAINTENANCE = ".$idmantenimiento);
      return ($item) ? array('mensaje' => "Registro actualizado correctamente.", 'estado' => 2 ) : array('mensaje' => "Se presento un error", 'estado'=> 2);
    }
}?>