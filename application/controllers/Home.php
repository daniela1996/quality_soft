<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

	function __construct()
	{

		parent::__construct();
		$this->load->model("homemodel");

	}

 function index()
 {
 	
	date_default_timezone_set('America/New_York');
	if($this->session->userdata('logged_in'))
	{
	$session= $session_data = $this->session->userdata('logged_in');
	$data['correctView'] = "home";
	$this->load->view('template/header.php', $session);
	$this->load->view('main_view', $session);//('main_view', $data);
	$this->load->view('template/footer.php', $data);

	}
	else
	{
	//If no session, redirect to login page
	redirect('', 'refresh');
	}
 }

	public function getData()
	{

		header('Content-type: application/json');

		$array = $this->input->post('custom_param1');

		$decoded = json_decode($array,true);

		$po = $this->homemodel->getCallendarPo();
		$calendar = array();
		$solicitudes = $this->homemodel->getCallendarSolicitudes();

		foreach($po as $fields)
		{

			if($fields["ESTADO"] == 0)
			{
				$calendar[] = array(
				'isPo' => "yes",
				'title' => 'PO: '.$fields['TIPO'],
				'start' => $fields['FECHA'],
				'backgroundColor' => "#C9302C",
				//'borderColor' => "#C9302C",
				'borderColor' => "#000000",
				'className' => 'poTag',
				'estado' => $fields["ESTADO"]);
			}
			if($fields["ESTADO"] == 1)
			{
				$calendar[] = array(
				'isPo' => "yes",
				'title' => 'PO: '.str_replace("Esperando enviar", "Pendiente", $fields['TIPO']),
				'start' => $fields['FECHA'],
				'backgroundColor' => "#F39C12",
				//'borderColor' => "#F39C12",
				'borderColor' => "#000000",
				'className' => 'poTag',
				'estado' => $fields["ESTADO"]);
			}
			if($fields["ESTADO"] == 2)
			{
				$calendar[] = array(
				'isPo' => "yes",
				'title' => 'PO: '.$fields['TIPO'],
				'start' => $fields['FECHA'],
				'backgroundColor' => "#00A65A",
				//'borderColor' => "#00A65A",
				'borderColor' => "#000000",
				'className' => 'poTag',
				'estado' => $fields["ESTADO"]);
			}

		}

		foreach($solicitudes as $fields)
		{

			if($fields["ESTADO"] == 0)
			{
				$calendar[] = array(
				'isPo' => "no",
				'title' => 'S: '.$fields['TIPO'],
				'start' => $fields['FECHA'],
				'backgroundColor' => "#C9302C",
				//'borderColor' => "#C9302C",
				'borderColor' => "#000000",
				'className' => 'solicitudTag',
				'estado' => $fields["ESTADO"]);
			}
			if($fields["ESTADO"] == 1)
			{
				$calendar[] = array(
				'isPo' => "no",
				'title' => 'S: '.str_replace("Esperando enviar", "Pendiente", $fields['TIPO']),
				'start' => $fields['FECHA'],
				'backgroundColor' => "#F39C12",
				//'borderColor' => "#F39C12",
				'borderColor' => "#000000",
				'className' => 'solicitudTag',
				'estado' => $fields["ESTADO"]);
			}
			if($fields["ESTADO"] == 2)
			{
				$calendar[] = array(
				'isPo' => "no",
				'title' => 'S: '.$fields['TIPO'],
				'start' => $fields['FECHA'],
				'backgroundColor' => "#00A65A",
				//'borderColor' => "#00A65A",
				'borderColor' => "#000000",
				'className' => 'solicitudTag',
				'estado' => $fields["ESTADO"]);
			}

		}

		echo json_encode($calendar);

	}

	public function getPoPendientes()
	{

		header('Content-type: application/json');

		$array = $this->input->post('custom_param1');

		$decoded = json_decode($array,true);

		$po = $this->homemodel->getCallendarPoBy(1);

		$calendar = array();

		foreach($po as $fields)
		{

			$calendar[] = array(
			'isPo' => "yes",
			'title' => 'PO: '.str_replace("Esperando enviar", "Pendiente", $fields['TIPO']),
			'start' => $fields['FECHA'],
			'backgroundColor' => "#F39C12",
			//'borderColor' => "#F39C12",
			'borderColor' => "#000000",
			'className' => 'poTag',
			'estado' => $fields["ESTADO"]);

		}

		echo json_encode($calendar);

	}

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('', 'refresh');
 }

}
