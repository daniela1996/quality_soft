<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Modulos extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('modulomodel');
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['modulos'] = $this->modulomodel->getModulo();
				$data['jsfile'] = "Modulos.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('modulos_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function moduloParticular() {
		header('Content-type: application/json');
		$array = $this->input->post('moduloName');
		$decoded = json_decode($array,true);
		$item = $this->modulomodel->getModuloParticular($decoded['MODULEID']);
		$this->output->set_output(json_encode($item));
	}
	public function modulosInsert() {
		header('Content-type: application/json');
		$array = $this->input->post('moduloName');
		$decoded = json_decode($array,true);
		$rows = array('CODEMODULE' => $decoded['CODEMODULE'],
									'NAMEMODULE' => $decoded['NAMEMODULE'],
									'DESCMODULE' => $decoded['DESCMODULE']);
		$ultimoRegistro = $this->modulomodel->getUltimoModulo();
		$item = $this->modulomodel->insertModulo($rows);
		$nuevosRegistros = $this->modulomodel->getUltimoModulo();
		$rowsRespuesta = $this->ultimasEntradas($ultimoRegistro, $nuevosRegistros);
		$this->output->set_output(json_encode(array($item, $rowsRespuesta, 1)));
	}
	public function moduloUpdate() {
		header('Content-type: application/json');
		$array = $this->input->post('moduloName');
		$decoded = json_decode($array,true);
		$rows = array('CODEMODULE' => $decoded['CODEMODULE'],
									'NAMEMODULE' => $decoded['NAMEMODULE'],
									'DESCMODULE' => $decoded['DESCMODULE']);
		$item = $this->modulomodel->updateModulo($decoded['MODULEID'], $rows);
		$perfilUpdated = $this->modulomodel->getModuloParticular($decoded['MODULEID']);
		$this->output->set_output(json_encode(array($item, $perfilUpdated, 2)));
	}
	public function moduloDelete() {
		header('Content-type: application/json');
		$array = $this->input->post('moduloName');
		$decoded = json_decode($array,true);
		$item = $this->modulomodel->deleteModulo($decoded['MODULEID']);
		$this->output->set_output(json_encode(array($item, false, 3)));
	}
	private function ultimasEntradas($ultimoRegistro, $nuevosRegistros) {
		$diferenciaAgregados = (intval($nuevosRegistros['CONTEO']) - intval($ultimoRegistro['CONTEO']));
		if ($diferenciaAgregados > 0) {
			return $this->modulomodel->getModuloNuevo($diferenciaAgregados);
		} else {
			return false;
		}
	}
} ?>
