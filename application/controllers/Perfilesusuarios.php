<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Perfilesusuarios extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model("perfilmodel");
		$this->load->model("user");
	}
	public function index() {
		$pu = $this->input->get('pu');
		$vl = $this->input->get('vl');
		$data['result'] = $this->perfilmodel->getPerfilUsuario();
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(),$session['uniqueid'])) {
				$data['jsfile'] = "Perfilesusuarios.js";
				$data['both'] = true;
				$data["idVal"] = (empty($pu)) ? 0: $vl;
				if (!((empty($pu))||(empty($vl)))) {
					if ($pu == 1) {
						$data['result'] = $this->perfilmodel->getPerfilUsuarioParticular($vl);
						$data['perfilUsuario'] = 1;
					}
					if ($pu == 2) {
						$data['result'] = $this->user->getUsuarioPerfilParticular($vl);
						$data['perfilUsuario'] = 2;
					}
					if(!empty($data['result'])) {
						if ($pu == 1) {
							$data['usuario'] = $this->user->getUsuarioIncompleto($vl);
						}
						if ($pu == 2) {
							$data['perfil'] = $this->perfilmodel->getPerfilUIncompleto($vl);
						}
					} else {
						$data['perfil'] = $this->perfilmodel->getPerfil();
						$data['usuario'] = $this->user->getUser();
					}
				} else {
					$data['both'] = false;
				}
				$this->load->view('template/header.php', $session);
				$this->load->view('perfiles_usuarios_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function perfilusuarioInsert() {
		header('Content-type: application/json');
		$array = $this->input->post('perfilusuarioName');
		$decoded = json_decode($array,true);
		$rows = array();
		foreach($decoded['dataArray'] as $fields) {
			$rows[] = array('PROFILEID' => $fields['PROFILES_IDPROFILE'],
											'USERID' => $fields['USERS_IDUSERS']);
		}
		$ultimoRegistro = (intval($decoded['PROCEDENCIA']) == 1) ? $this->user->getUltimoUsuarioPerfil($decoded['LLAVE']) : $this->perfilmodel->getUltimoPerfilUsuario($decoded['LLAVE']);
		$item = $this->perfilmodel->insertPerfilOUsuario($rows);
		$nuevosRegistros = (intval($decoded['PROCEDENCIA']) == 1) ? $this->user->getUltimoUsuarioPerfil($decoded['LLAVE']) : $this->perfilmodel->getUltimoPerfilUsuario($decoded['LLAVE']);
		$rowsRespuesta = $this->ultimasEntradas($ultimoRegistro, $nuevosRegistros, intval($decoded['PROCEDENCIA']), intval($decoded['LLAVE']));
		$this->output->set_output(json_encode(array($item, $rowsRespuesta)));
	}
	public function perfilusuarioAcceptDecline() {
		header('Content-type: application/json');
		$array = $this->input->post('perfilusuarioName');
		$decoded = json_decode($array,true);
		$item = $this->perfilmodel->acceptDecline($decoded['PROF_MODULEID'], $decoded['ESTADO']);
		$this->output->set_output(json_encode($item));
	}
	private function ultimasEntradas($ultimoRegistro, $nuevosRegistros, $procedencia, $llave) {
		$diferenciaAgregados = (intval($nuevosRegistros['CONTEO']) - intval($ultimoRegistro['CONTEO']));
		if ($diferenciaAgregados > 0) {
			if ($procedencia == 1) {
				return $this->user->getUsuarioPerfilNuevo($llave, $diferenciaAgregados);
			} else {
				return $this->perfilmodel->getPerfilUsuarioNuevo($llave, $diferenciaAgregados);
			}
		} else {
			return false;
		}
	}
} ?>
