<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Regiones extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('regionmodel');
		
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['jsfile'] = "regiones.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('regiones_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}

	public function regionesGet(){		
		header('Content-type: application/json');
		$item = $this->regionmodel->getRegiones();
		$this->output->set_output(json_encode($item));
	}

	public function paisesGet(){
		header('Content-type: application/json');
		$data = $this->regionmodel->getPais();
		echo json_encode($data);
	}

	public function paisInsert(){
		$nombre = $this->input->post("NOMBRE");
		$data = array('NOMBRE' => $nombre);
		$item = $this->regionmodel->insertPais($data);
		$this->output->set_output(json_encode($item));
	}

	public function paisUpdate(){
		$codpais = $this->input->post("COD_PAIS");
		$nombre = $this->input->post("NOMBRE");
		$data = array('NOMBRE' => $nombre);
		$item = $this->regionmodel->updatePais($codpais, $data);
		$this->output->set_output(json_encode($item));

	}
	public function selectPaises(){
		$item = $this->regionmodel->getPais();
	}

	public function deptoInsert(){
		$nombredepto = $this->input->post("NOMBRE");
		$codpais = $this ->input->post("COD_PAIS");
		$arreglo = array('NOMBRE' => $nombredepto,
						'COD_PAIS'=> $codpais);
		$item = $this->regionmodel->insertDepto($arreglo);
		$this->output->set_output(json_encode($item));
	}

	public function deptoUpdate(){
		$coddepto = $this ->input->post("COD_DEPTO");
		$codpais = $this ->input ->post("COD_PAIS");
		$nombredepto = $this->input->post("NOMBRE");
		$data = array('NOMBRE' => $nombredepto, 'COD_PAIS'=>$codpais);
		$item = $this->regionmodel->updateDepto($coddepto,$data);
		$this->output->set_output(json_encode($item));
	}
	public function muniInsert(){
		$nombremun = $this->input->post("NOMBRE");
		$coddepto = $this ->input->post("COD_DEPTO");
		$arreglo = array('NOMBRE' => $nombremun, 
						'COD_DEPTO' => $coddepto);
		$item = $this ->regionmodel->insertMunicipio($arreglo);
		$this->output->set_output(json_encode($item));
	}

	public function muniUpdate(){
		$codmun = $this->input->post('COD_MUN');
		$nombremun = $this->input->post('NOMBRE');
		$coddepto = $this->input->post('COD_DEPTO');
		$data = array('NOMBRE' => $nombremun, 'COD_DEPTO' => $coddepto);
		$item = $this->regionmodel->updateMuni($codmun,$data);
		$this->output->set_output(json_encode($item));
	}

	public function selectDeptos(){
		$item = $this ->regionmodel->selectDepto();
	}

	public function getDeptosPais($idPais){
		$deptos = $this->regionmodel->getDeptoId($idPais);
		echo json_encode($deptos);
	}

} ?>
