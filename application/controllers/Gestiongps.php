<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Gestiongps extends CI_Controller{
	function __construct(){
		parent::__construcT();
		$this->load->model('Gestiongpsmodel');
		$this->load->model('regionmodel');
		$this->load->model('Vehiculosmodel');
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['user'] = $this->Gestiongpsmodel->getUser();

				$data['pais'] = $this->regionmodel->getPais();
				$data['departamento'] = $this->regionmodel->getDepto();
				$data['jsfile'] = 'Gestiongps.js';

				$this->load->view('template/header.php', $session);
				$this->load->view('gestion_gps_view.php', $data);
				$this->load->view('modals_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}

	public function getGPS(){
		header('Content-type: application/json');
		$data = $this->Gestiongpsmodel->getInfoantena();
		echo json_encode($data);
	}

	public function gestionInsert(){
		$pais = $this->input->post("COD_PAIS");
		$departamento = $this->input->post("COD_DEPTO");
		$municipio = $this->input->post("MUNICIPIO");
		$cod_vehiculo = $this->input->post("COD_VEHICULO");
		$cuenta = $this->input->post("CUENTA");
		$celular = $this->input->post("CELULAR");
		$voltaje = $this->input->post("VOLTAJE");
		$voltajemin = $this->input->post("VOLTAJEMIN");
		$galones = $this->input->post("GALONES");
		$descripcion = $this->input->post("DESCRIPCION");

		$data = array('COD_DEPTO' => $departamento, 
						'COD_PAIS' => $pais,
						'CIUDAD' => $municipio,
						'COD_ANT' => $cod_vehiculo,
						'CUENTA' => $cuenta,
						'CELULAR'=>$celular,
						'DESCRIPCION' => $descripcion,
						'GALONS' => $galones,
						'VOLTAJE' => $voltaje,
						'VOLTAJEMIN' => $voltajemin);
		$item = $this->Gestiongpsmodel->insertGestion($data);
		$this->output->set_output(json_encode($item));
		
	}
	public function getUsers(){
		$item = $this->Gestiongpsmodel->getUser();
	}

	public function gestionUpdate(){
		$pais = $this->input->post("COD_PAIS");
		$departamento = $this->input->post("COD_DEPTO");
		$municipio = $this->input->post("MUNICIPIO");
		$cod_vehiculo = $this->input->post("COD_VEHICULO");
		$cuenta = $this->input->post("CUENTA");
		$celular = $this->input->post("CELULAR");
		$voltaje = $this->input->post("VOLTAJE");
		$voltajemin = $this->input->post("VOLTAJEMIN");
		$galones = $this->input->post("GALONES");
		$descripcion = $this->input->post("DESCRIPCION");
		$autoid = $this->input->post("AUTOID");
		$data = array('COD_DEPTO' => $departamento, 
						'COD_PAIS' => $pais,
						'CIUDAD' => $municipio,
						'COD_ANT' => $cod_vehiculo,
						'CUENTA' => $cuenta,
						'CELULAR'=>$celular,
						'DESCRIPCION' => $descripcion,
						'GALONS' => $galones,
						'VOLTAJE' => $voltaje,
						'VOLTAJEMIN' => $voltajemin);
		$item = $this->Gestiongpsmodel->updateGestion($autoid, $data);
		$this->output->set_output(json_encode($item));
	}

	public function vehiculosUpdate(){
		$id = $this->input->post("ID");
		$chasis = $this->input->post("CHASIS");
		$color = $this->input->post("COLOR");
		$compania = $this->input->post("COMPANIA");
		$marca = $this->input->post("MARCA");
		$modelo = $this->input->post("MODELO");
		$motor = $this->input->post("MOTOR");
		$plate = $this->input->post("PLATE");
		$year = $this->input->post("YEAR");
		$data = array('CHASSIS' => $chasis, 
						'COLOR' => $color,
						'COMPANY' => $compania,
						'MAKE' => $marca,
						'MODEL' => $modelo,
						'MOTOR' => $motor,
						'PLATE' => $plate,
						'YEAR' => $year);
		$item = $this->Vehiculosmodel->updateVehiculo($id, $data);
		$this->output->set_output(json_encode($item));
	}
}?>