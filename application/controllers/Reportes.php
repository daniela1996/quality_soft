<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Reportes extends CI_Controller 
	{

		function __construct()
		{
			
			parent::__construct();
			$this->load->model('reportemodel');
			$this->load->model('empresamodel');
			$this->load->model('contratistamodel');
			$this->load->model('proveedormodel');
			$this->load->model('monedamodel');
			$this->load->model('usuariomodel');
			$this->load->model('impuestomodel');
			$this->load->model('firmamodel');
			$this->load->model('inventariomodel');
			$this->load->library('excel');
			
		}

		public function index(){}
	
		function po($id)		
		{
			if($this->session->userdata('logged_in'))

			
			{
		 
				$data["reporte"] = $this->reportemodel->getReporte($id);
				
				if(!$data["reporte"]){
					$data["reporte"] = false;
					$data["reporteId"] = $id;
				}
				
				$this->load->helper(array('form', 'url'));
	        
				$this->load->view('reportes_view',$data);
			
				$html = $this->output->get_output();
			
				$this->load->library('dompdf_gen');
			
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream($id.".pdf", array("Attachment" => 0));

	        }
	        
			else
			
	   		{
	    
	     		redirect('login', 'refresh');
	     		
	   		}	
		}
		
		function preview()		
		{
			if($this->session->userdata('logged_in'))
			
			{
				
				$empresa = $this->empresamodel->getEmpresaLogoSello($this->input->post('empresa'));
				$firma = $this->firmamodel->getFirma($this->input->post('firma'))[0]["FIRMA"];
				$nombreEmpresa = $empresa[0]["NOMBRE_EMPRESA"];
				$logoEmpresa = $empresa[0]["LOGO"];
				$selloEmpresa = $empresa[0]["SELLO"];
				$direccionEmpresa = $empresa[0]["DIRECCION_EMPRESA"];
				$telefonoEmpresa = $empresa[0]["TELEFONO_EMPRESA"];
				$RTNEmpresa = $empresa[0]["RTN"];
				$cotizacion = $this->input->post('cotizacion');
				$contratista = $this->input->post('contratista');
				$proveedor = $this->input->post('proveedor');
				if($proveedor == "NO")
				{$proveedor = ""; $contratista = $this->contratistamodel->getContratistaLogo($contratista)[0]["NOMBRE_CONTRATISTA"];}
				else{$contratista = ""; $proveedor = $this->proveedormodel->getProveedorParticular($proveedor)[0]["NOMBRE_PROVEEDOR"];}
				$moneda = $this->monedamodel->getMonedaInica($this->input->post('moneda'))[0]["SIMBOLO"];
				$factura = $this->input->post('factura');
				$responsable = $this->usuariomodel->getUsuarioFoto($this->input->post('responsable'))[0]["NAME"];
				$autorizacion = $this->usuariomodel->getUsuarioFoto($this->input->post('autorizacion'))[0]["NAME"];
				$notas = $this->input->post('notas');
				$creador = $this->usuariomodel->getUsuarioFoto($this->input->post('creador'))[0]["NAME"];
				$subtotal = $this->input->post('subtotal');
				$impuesto = $this->impuestomodel->getImpuestoParticular($this->input->post('impuesto'))[0]["IMPUESTO"];
				$total = $this->input->post('total');
				
				$max = $this->input->post('indice');
				
				$codigos = $this->input->post('codigo');
				$descripcion = $this->input->post('descripcion');
				$descuento = $this->input->post('descuento');
				$cantidad = $this->input->post('cantidad');
				$unidades = $this->input->post('unidades');
				$precio = $this->input->post('unitario');
				
				$dataRepot = array();
				
				for($i = 0; $i < $max; $i++)
				{
					
					$dataRepot[$i] = array(
					"ITEM" => ($i+1),
					"CANTIDAD" => $cantidad[$i],
					"PO" => "".date("Y")."XXX",
					"FECHA_CREACION_PO" => "".date("d-M-Y")."",
					"CODIGO" => $codigos[$i],
					"DESCRIPCION" => $descripcion[$i],
					"DESCUENTO" => $descuento[$i],
					"PRECIO_UNITARIO" => $precio[$i],
					"NOMBRE_EMPRESA" => $nombreEmpresa,
					"RTN" => $RTNEmpresa,
					"LOGO" => $logoEmpresa,
					"SELLO" => $selloEmpresa,
					"FIRMA" => $firma,
					"DIRECCION_EMPRESA" => $direccionEmpresa,
					"TELEFONO_EMPRESA" => $telefonoEmpresa,
    				"NOMBRE_PROVEEDOR" => $proveedor,
    				"NOMBRE_CONTRATISTA" => $contratista,
    				"NUM_COTIZACION" => $cotizacion,
    				"NAME_RESPONSABLE" => $responsable,
    				"NOTAS" => $notas,
    				"SIMBOLO" => $moneda,
    				"SUBTOTAL" => $subtotal,
    				"IMPUESTO" => $impuesto,
    				"TOTAL" => $total
					);
					
				}
				
				$data["reporte"] = $dataRepot;
				
				if(!$data["reporte"]){
					$data["reporte"] = false;
					$data["reporteId"] = $id;
				}
				
				$this->load->helper(array('form', 'url'));
	        
				$this->load->view('reportes_view',$data);
			
				$html = $this->output->get_output();
			
				$this->load->library('dompdf_gen');
			
				$this->dompdf->load_html($html);
				$this->dompdf->render();
				$this->dompdf->stream($id.".pdf", array("Attachment" => 0));

	        }
	        
			else
			
	   		{
	    
	     		redirect('login', 'refresh');
	     		
	   		}	
		}

		public function getAllStock($p)
		{
			$file = APPPATH."files/IVN_CONSUMIBLES.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			//$obj = new PHPExcel();
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);
			$result = $this->reportemodel->getAllStock($p);
			$fila=2;

			if ($result['Estado']) 
			{
				$obj->getActiveSheet()->setCellValue('A1', "#");
				$obj->getActiveSheet()->setCellValue('B1', 'CÓDIGO');
				$obj->getActiveSheet()->setCellValue('C1', 'DESCRIPCIÓN');
				$obj->getActiveSheet()->setCellValue('D1', 'UNIDAD');
				$obj->getActiveSheet()->setCellValue('E1', 'TOTAL');
				

				// $styleArray = array(
				//     'borders' => array(
				//         'outline' => array(
				//             'style' => PHPExcel_Style_Border::BORDER_THICK,
				//             'color' => array('argb' => '000000'),
				//         ),
				//     ),
				//     'alignment' => array(
			 //        	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			 //    	),
				// );

				// $obj->getActiveSheet()->getStyle("A1:E1")->applyFromArray($styleArray, true);

				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{

					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['CODIGO']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['TOTAL']);
					

					$fila++;
				}
			}
			else
			{
				$obj->getActiveSheet()->setCellValue('A'.$fila, 'No hay Registros que mostrar!');
			}			

			//prepare download
			$filename='IVN_CONSUMIBLES_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
		}

		public function getAllStockWithProy($p, $idProy)
		{
			$file = APPPATH."files/IVN_CONSUMIBLES_BY_PROYECTO.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			//$obj = new PHPExcel();
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);
			$result = $this->reportemodel->getAllStockWithProy($p, $idProy);

			if ($result['Estado']) 
			{
				$obj->getActiveSheet()->setCellValue('A1', "#");
				$obj->getActiveSheet()->setCellValue('B1', "NOMBRE_PROYECTO");
				$obj->getActiveSheet()->setCellValue('C1', 'CÓDIGO');
				$obj->getActiveSheet()->setCellValue('D1', 'DESCRIPCIÓN');
				$obj->getActiveSheet()->setCellValue('E1', 'UNIDAD');
				$obj->getActiveSheet()->setCellValue('F1', 'NOMBRE_BODEGA');
				$obj->getActiveSheet()->setCellValue('G1', 'TOTAL');

				// $styleArray = array(
				//     'borders' => array(
				//         'outline' => array(
				//             'style' => PHPExcel_Style_Border::BORDER_THICK,
				//             'color' => array('argb' => '000000'),
				//         ),
				//     ),
				//     'alignment' => array(
			 //        	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			 //    	),
				// );

				// $obj->getActiveSheet()->getStyle("A1:E1")->applyFromArray($styleArray, true);

				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{

					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['CODIGO']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_BODEGA']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['TOTAL']);

					$fila++;
				}
			}
			else
			{
				$obj->getActiveSheet()->setCellValue('A2', 'No hay Registros que mostrar!');
			}			

			//prepare download
			$filename='IVN_CONSUMIBLES_BY_PROYECTO_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output'); 
		}

		public function getAllStockWithProyAndBodegas($p, $idProy, $idBod)
		{
			
			$file = APPPATH."files/IVN_CONSUMIBLES_BY_PROYECTO_BY_BODEGA.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);
			$result = $this->reportemodel->getAllStockWithProyAndBodegas($p, $idProy, $idBod);

			if ($result['Estado']) 
			{
				$obj->getActiveSheet()->setCellValue('A1', "#");
				$obj->getActiveSheet()->setCellValue('B1', "NOMBRE_PROYECTO");
				$obj->getActiveSheet()->setCellValue('C1', 'CÓDIGO');
				$obj->getActiveSheet()->setCellValue('D1', 'DESCRIPCIÓN');
				$obj->getActiveSheet()->setCellValue('E1', 'UNIDAD');
				$obj->getActiveSheet()->setCellValue('F1', 'NOMBRE_BODEGA');
				$obj->getActiveSheet()->setCellValue('G1', 'TOTAL');

		

				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{

					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['CODIGO']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_BODEGA']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['TOTAL']);

					$fila++;
				}
			}
			else
			{
				$obj->getActiveSheet()->setCellValue('A2', 'No hay Registros que mostrar!');
			}			

			//Preparamos la descarga
			$filename='IVN_CONSUMIBLES_BY_PROYECTO_BY_BODEGA_'.date('d-m-y').'.xlsx'; 
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			
			$objWriter->save('php://output'); 

		}//fin getAllStockWithProyAndBodegas

		public function getMovimientosItemCuadrilla($f1, $f2, $id){
			$item = $this->inventariomodel->getItemSalidasCuadrillas($id, $f1, $f2);
			//print_r($item);
			$file = APPPATH."files/INV_SALIDA_ITEM_CUADRILLA.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			//$obj = new PHPExcel();
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);

			if ($item['Estado']){
				$fila = 2;
				foreach($item['Resultado'] as $key => $value){
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['CODIGO_EQUIPO']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['NOMBRE_UNIDAD']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['CANTIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['FECHA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRECUADRILLA']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['USERNAME_RECIBE']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['TEAMLEADER']);
					$obj->getActiveSheet()->setCellValue('I'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('J'.$fila, $value['NOMBRE_BODEGA']);
					$obj->getActiveSheet()->setCellValue('K'.$fila, $value['OBSERVACION']);
					$fila++;
				}

				//prepare download
				$filename='IVN_SALIDA_'.date('d-m-y').'.xlsx'; //just some random filename
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
				#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
				$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
			}else{
				$obj->getActiveSheet()->setCellValue('A2', $item['Mensaje']);
			}
		}

		public function getMovimientosEntradas($id, $f1, $f2, $idProy, $idBod)
		{
			header('Content-type: application/json');
			//$data = $this->input->get('info');
			//$decoded = json_decode($data, true);
			$rows = array();
			
			$rows = array('FECHA_INICIO' => $f1, 
							  'FECHA_FINAL' => $f2,
							  'ID' => $id,
							  'ID_PROYECTO' => $idProy,
							  'ID_BODEGA' => $idBod);
			
			$item = $this->reportemodel->getEntradasMovimiento($rows);

			$file = APPPATH."files/INV_MOV.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			//$obj = new PHPExcel();
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);
			

			if ($item['Estado']){
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) {
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['NOMBRE_BODEGA']);
					$obj->getActiveSheet()->setCellValue('I'.$fila, $value['TIPO_ENTRADA']);

					$fila++;
				}
			}else{
				$obj->getActiveSheet()->setCellValue('A2', $item['Mensaje']);
			}

				$obj->setActiveSheetIndex(1);
				$result = $this->reportemodel->getSalidasMovimientoCuadrilla($rows);
				
			if ($item['Estado']) 
			{

				$obj->getActiveSheet()->setCellValue('A1', "#");
				$obj->getActiveSheet()->setCellValue('B1', 'FECHA_SALIDA');
				$obj->getActiveSheet()->setCellValue('C1', 'DESCRIPCION');
				$obj->getActiveSheet()->setCellValue('D1', 'UNIDAD');
				$obj->getActiveSheet()->setCellValue('E1', 'CANTIDAD_SALIDA');
				$obj->getActiveSheet()->setCellValue('F1', 'NOMBRE_PROYECTO');
				$obj->getActiveSheet()->setCellValue('G1', 'RESPONSABLE');
				$obj->getActiveSheet()->setCellValue('H1', 'NOMBRE_BODEGA');
				$obj->getActiveSheet()->setCellValue('I1', 'CUADRILLA');
				$obj->getActiveSheet()->setCellValue('J1', 'ORDEN_TRABAJO');
				$obj->getActiveSheet()->setCellValue('K1', 'CODIGO_PROYECTO');


				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['NOMBRE_BODEGA']);
					$obj->getActiveSheet()->setCellValue('I'.$fila, $value['NOMBRECUADRILLA']);
					$obj->getActiveSheet()->setCellValue('J'.$fila, $value['ID_ORDEN_TRABAJO']);
					$obj->getActiveSheet()->setCellValue('K'.$fila, $value['CODIGO_PROYECTO']);
					
					$fila++;
				}
			}
					
			else
			{
				$obj->getActiveSheet()->setCellValue('A2',$item['Mensaje']);
			}			

			//prepare download
			$filename='IVN_MOV_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
		}


		public function getMovimientosEntradasWithProyectos($id, $f1, $f2, $idProy)
		{
			header('Content-type: application/json');
			//$data = $this->input->get('info');
			//$decoded = json_decode($data, true);
			$rows = array();
			
				$rows = array('FECHA_INICIO' => $f1, 
							  'FECHA_FINAL' => $f2,
							  'ID' => $id);
			
			$item = $this->reportemodel->getEntradasMovimiento($rows);

			$file = APPPATH."files/INV_MOV.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			//$obj = new PHPExcel();
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);
			

			if ($item['Estado']) 
			{
				
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{

					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['NOMBRE_BODEGA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['TIPO_ENTRADA']);

					$fila++;
				}
			}
			else
			{
				$obj->getActiveSheet()->setCellValue('A2', $item['Mensaje']);
			}

				$obj->setActiveSheetIndex(1);
				$result = $this->reportemodel->getSalidasMovimientoCuadrilla($rows);
				
			if ($item['Estado']) 
			{

				$obj->getActiveSheet()->setCellValue('A1', "#");
				$obj->getActiveSheet()->setCellValue('B1', 'FECHA_SALIDA');
				$obj->getActiveSheet()->setCellValue('C1', 'DESCRIPCION');
				$obj->getActiveSheet()->setCellValue('D1', 'UNIDAD');
				$obj->getActiveSheet()->setCellValue('E1', 'CANTIDAD_SALIDA');
				$obj->getActiveSheet()->setCellValue('F1', 'NOMBRE_PROYECTO');
				$obj->getActiveSheet()->setCellValue('G1', 'RESPONSABLE');
				$obj->getActiveSheet()->setCellValue('H1', 'NOMBRE_BODEGA');
				$obj->getActiveSheet()->setCellValue('I1', 'CUADRILLA');


				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['NOMBRE_BODEGA']);
					$obj->getActiveSheet()->setCellValue('I'.$fila, $value['NOMBRECUADRILLA']);
					
					$fila++;
				}
			}
					
			else
			{
				$obj->getActiveSheet()->setCellValue('A2',$item['Mensaje']);
			}			

			//prepare download
			$filename='IVN_MOV_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
		}

		public function getRptEntradasBodega($idProy, $idBod, $idTipoEntrada, $f1, $f2)
		{
			header('Content-Type:application/json');

			//$rows = array();
			$rows = array('ID_PROYECTO' => $idProy, 
							'ID_BODEGA' => $idBod,
							'ID_TIPO_ENTRADA' => $idTipoEntrada,
							'FECHA_INICIO' => $f1,
							'FECHA_FINAL' => $f2
							);
			$item = $this->reportemodel->getRptEntradasBodega($rows);

			if ($item['Estado']){
				$file = APPPATH."files/INV_ENTRADAS_BODEGA.xlsx";
				$obj = PHPExcel_IOFactory::load($file);	
				//$obj = new PHPExcel();
				//$obj->getActiveSheet()->setShowGridlines(true);
				$obj->setActiveSheetIndex(0);
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{

					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['CODIGO_INTERNO']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['TIPO_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['FECHA_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('I'.$fila, $value['COMENTARIOS']);

					$fila++;
				}
			}

			/*	$obj->setActiveSheetIndex(1);
				$result = $this->reportemodel->getSalidasMovimiento($rows);
				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					
					$fila++;
				}
			}*/
					
			/*else
			{
				$obj->getActiveSheet()->setCellValue('A2','No hay Registros que mostrar!');
			}*/			

			//prepare download
			$filename='INV_ENTRADAS_BODEGA_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');
		}


		public function getRptSalidasBodega($idProy, $idBod, $idTipoSalida, $idCuadrillas , $f1, $f2)
		{
			header('Content-Type:application/json');

			//$rows = array();
			$rows = array('ID_PROYECTO' => $idProy, 
							'ID_BODEGA' => $idBod,
							'ID_TIPO_SALIDA' => $idTipoSalida, 
							'CUADRILLAID' => $idCuadrillas,
							'FECHA_INICIO' => $f1,
							'FECHA_FINAL' => $f2
							);

			$item = $this->reportemodel->getRptSalidaBodega($rows);

			$file = APPPATH."files/INV_SALIDAS_BODEGA.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			//$obj = new PHPExcel();
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);
			

			if ($item['Estado']) 
			{
				
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{

					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['CODIGO_INTERNO']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['TIPO_SALIDA']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['RESPONSABLE']);
					$obj->getActiveSheet()->setCellValue('I'.$fila, $value['NOMBRETECNICO']);
					$obj->getActiveSheet()->setCellValue('J'.$fila, $value['NOMBRECUADRILLA']);

					$fila++;
				}
			}

			/*	$obj->setActiveSheetIndex(1);
				$result = $this->reportemodel->getSalidasMovimiento($rows);
				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					
					$fila++;
				}
			}*/
					
			else
			{
				$obj->getActiveSheet()->setCellValue('A2','No hay Registros que mostrar!');
			}			

			//prepare download
			$filename='INV_SALIDAS_BODEGA_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');
		}

		public function getRptDevolucionesDetalle($id, $f1, $f2)
		{
			header('Content-Type:application/json');

			$rows = array();
			$rows = array(		'ID' => $id,
								'FECHA_INICIO' => $f1, 
							  	'FECHA_FINAL' => $f2,
						 );

			$item = $this->reportemodel->getRptDevolucionesDetalle($rows);

			$file = APPPATH."files/INV_DEVOLUCIONES.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			//$obj = new PHPExcel();
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);
			

			if ($item['Estado']) 
			{
				
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{

					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['CODIGO_INTERNO']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['NOMBRE_UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['NOMBRECUADRILLA']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['TEAMLEADER']);
					$obj->getActiveSheet()->setCellValue('I'.$fila, $value['TEAMLEADER_ACTUAL']);
					$obj->getActiveSheet()->setCellValue('J'.$fila, $value['ENCARGADO']);
					$obj->getActiveSheet()->setCellValue('K'.$fila, $value['FECHA']);

					$fila++;
				}
			}

			/*	$obj->setActiveSheetIndex(1);
				$result = $this->reportemodel->getSalidasMovimiento($rows);
				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					
					$fila++;
				}
			}*/
					
			else
			{
				$obj->getActiveSheet()->setCellValue('A1','No hay Registros que mostrar!');
			}			

			//prepare download
			$filename='INV_DEVOLUCIONES_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');
		}
		
		public function getMovimientosSalida($p)
		{
			$file = APPPATH."files/INV_MOV.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			//$obj = new PHPExcel();
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(1);
			$result = $this->reportemodel->getSalidasMovimiento($p);

			if ($result['Estado']) 
			{
				$obj->getActiveSheet()->setCellValue('A1', "#");
				$obj->getActiveSheet()->setCellValue('B1', 'FECHA_SALIDA');
				$obj->getActiveSheet()->setCellValue('C1', 'DESCRIPCION');
				$obj->getActiveSheet()->setCellValue('D1', 'UNIDAD');
				$obj->getActiveSheet()->setCellValue('E1', 'CANTIDAD_SALIDA');
				$obj->getActiveSheet()->setCellValue('F1', 'NOMBRE_PROYECTO');
				$obj->getActiveSheet()->setCellValue('G1', 'RESPONSABLE');
				
				

				// $styleArray = array(
				//     'borders' => array(
				//         'outline' => array(
				//             'style' => PHPExcel_Style_Border::BORDER_THICK,
				//             'color' => array('argb' => '000000'),
				//         ),
				//     ),
				//     'alignment' => array(
			 //        	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			 //    	),
				// );

				// $obj->getActiveSheet()->getStyle("A1:E1")->applyFromArray($styleArray, true);

				$fila = 2;
				foreach ($result['Resultado'] as $key => $value) 
				{

					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					$fila++;
				}
			}
			else
			{
				$obj->getActiveSheet()->setCellValue('A1','No hay Registros que mostrar!');
			}			

			//prepare download
			/*$filename='IVN_MOV_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!*/
		}

		public function test($id, $f1, $f2)
		{
			header('Content-type: application/json');
			//$data = $this->input->post('info');
			$decoded = json_decode($data, true);
			$rows = array();
			foreach ($decoded['dataArray'] as $key => $value) 
			{
				$rows = array('FECHA_INICIO' => $value['INICIO'], 
							  'FECHA_FINAL' => $value['FINAL'],
							  'ID' => $value['ID']);
			}
			$item = $this->reportemodel->getMovByItemIn($rows);
			//echo json_encode($item);

			$file = APPPATH."files/INV_MOVBYITEMS.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);

			// ENTRADAS				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['TIPO_ENTRADA']);
					
					$fila++;
				}
			}

			/*$item = $this->reportemodel->getMovByItemOut($rows);
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(1);

			// SALIDAS				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					//$obj->getActiveSheet()->setCellValue('H'.$fila, $value['TIPO_ENTRADA']);
					
					$fila++;
				}
			}*/

			else
			{
				$obj->getActiveSheet()->setCellValue('A1','No hay Registros que mostrar!');
			}			

			//prepare download
			$filename='INV_MOVBYITEMS_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');
		}

		public function getMovimientosByItem($id, $f1, $f2)
		{
			header('Content-type: application/json');
			//$data = $this->input->get('info');
			//$decoded = json_decode($data, true);
			$rows = array();
			
				$rows = array('FECHA_INICIO' => $f1, 
							  'FECHA_FINAL' => $f2,
							  'ID' => $id);
			
			$item = $this->reportemodel->getMovByItemIn($rows);
			//echo json_encode($item);
		
			$file = APPPATH."files/INV_MOVBYITEMS.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);

			// ENTRADAS				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_ENTRADA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					$obj->getActiveSheet()->setCellValue('H'.$fila, $value['TIPO_ENTRADA']);
					
					$fila++;
				}
			}
			else
			{
				$obj->getActiveSheet()->setCellValue('A2','No hay Registros que mostrar!');
			}

			$item = $this->reportemodel->getMovByItemOut($rows);
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(1);

			// SALIDAS				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					//$obj->getActiveSheet()->setCellValue('H'.$fila, $value['TIPO_ENTRADA']);
					
					$fila++;
				}
			}

			else
			{
				$obj->getActiveSheet()->setCellValue('A2','No hay Registros que mostrar!');
			}			

			//prepare download
			$filename='INV_MOVBYITEMS_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');

		}

		public function getCatalogos()
		{
			header('Content-type: application/json');
			//$data = $this->input->post('info');
			//$decoded = json_decode($data, true);
			$rows = array();
			/*foreach ($decoded['dataArray'] as $key => $value) 
			{
				$rows = array('ID_ARTICULOS_PROVEE_CODIGO' => $value['ID_ARTICULOS_PROVEE_CODIGO']);
			}*/
			$item = $this->reportemodel->getCatalogos();
			//echo json_encode($item);

			$file = APPPATH."files/INV_CATALOGO.xlsx";
			$obj = PHPExcel_IOFactory::load($file);	
			
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(0);

			// ENTRADAS				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['NOMBRE']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['CODIGO']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['NOMBRE_UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['DESCRIPCION']);
					
					$fila++;
				}
			}

			/*$item = $this->reportemodel->getMovByItemOut($rows);
			$obj->getActiveSheet()->setShowGridlines(true);
			$obj->setActiveSheetIndex(1);

			// SALIDAS				
			if ($item['Estado']) 
			{
				$fila = 2;
				foreach ($item['Resultado'] as $key => $value) 
				{
					$obj->getActiveSheet()->setCellValue('A'.$fila, $value['ITEM']);
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['FECHA_SALIDA']);
					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['DESCRIPCION']);
					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['UNIDAD']);
					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['CANTIDAD_SALIDA']);
					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['NOMBRE_PROYECTO']);
					$obj->getActiveSheet()->setCellValue('G'.$fila, $value['RESPONSABLE']);
					//$obj->getActiveSheet()->setCellValue('H'.$fila, $value['TIPO_ENTRADA']);
					
					$fila++;
				}
			}*/

			else
			{
				$obj->getActiveSheet()->setCellValue('A2','No hay Registros que mostrar!');
			}			

			//prepare download
			$filename='INV_CATALOGO_'.date('d-m-y').'.xlsx'; //just some random filename
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
			#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
			$objWriter->save('php://output');
		}

	}
