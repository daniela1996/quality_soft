<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Asignacionnodos extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Asignacionnodosmodel');
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['jsfile'] = "Asignacionnodos.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('Asignacion_nodos_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}

	public function AsignacionNodoInsert(){
		$session = $this->session->userdata('logged_in');
		$user = intval($session['userid']);	

		$array = $this->input->post("arreglo");
		$filas = array();

		for ($i=0; $i < count($array) ; $i++) { 
			$fields = $array[$i];
			$filas[] = array('COD_US' => $fields['usuario'],
						'IMEI' => $fields['nodo'],
						'DESCRIPCION' => $fields['descripcion'],
						'FECHA_INICIAL' => $fields['FechaInicial'],
						'FECHA_FINAL' => $fields['FechaFinal'],
						'COD_US_INSERT' => $user);
		}
		$item = $this->Asignacionnodosmodel->insertAsignacionNodo($filas);
		$this->output->set_output(json_encode($item));
	}

	public function obtenerNodos(){
		header('Content-type: application/json');
		$item = $this->Asignacionnodosmodel->getAsignacionNodo();
		echo json_encode($item);
	}

	public function obtenerNodosporUsuarios(){
		header('Content-type: application/json');
		$imei = $this ->input->post('IMEI');
		$item = $this->Asignacionnodosmodel->getNodosporUsuarios($imei);
		echo json_encode($item);
	}

	public function obtenerUsuariosporNodos(){
		header('Content-type: application/json');
		$user = $this ->input->post('USER');
		$item = $this->Asignacionnodosmodel->getUsuariosporNodos($user);
		echo json_encode($item);
	}

	public function obtenerNodosId(){
		header('Content-type: application/json');
		$item = $this->Asignacionnodosmodel->getNodosId();
		echo json_encode($item);
	}
	
	public function obtenerNodosUser(){
		header('Content-type: application/json');
		$user = $this ->input->post('user');
		$item = $this->Asignacionnodosmodel->getNodosUser($user);
		echo json_encode($item);
	}


	public function obtenerUsuariosId(){
		header('Content-type: application/json');
		$imei = $this ->input->post('nodo');
		$item = $this->Asignacionnodosmodel->getUserId($imei);
		echo json_encode($item);
	}

	public function obtenerUsuarios(){
		header('Content-type: application/json');
		$item = $this->Asignacionnodosmodel->getUser();
		echo json_encode($item);
	}

	public function AsignacionNodoDelete(){
		$session = $this->session->userdata('logged_in');
		$user = intval($session['userid']);	
		$auto_id = $this->input->post("auto_id");
		$imei = $this->input->post("imei");
		$cod_us = $this->input->post("cod_us");
		$estado = 0;
		$data = array('ESTADO' => $estado, 
					'COD_US_INSERT' => $user);
		$array = array('IMEI' => $imei,
					'COD_US' => $cod_us);
		$item = $this->Asignacionnodosmodel->deleteAsignacionNodo($auto_id, $data, $array);
		$this->output->set_output(json_encode($item));
	}


} ?>