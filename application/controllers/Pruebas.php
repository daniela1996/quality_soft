<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pruebas extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Pruebamodel');
		
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['jsfile'] = "Pruebas.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('prueba_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}

	public function proyectosGet(){
		header('Content-Type: application/json');
		$item = $this->Pruebamodel->getProyectos();
		echo json_encode($item);
	}

	public function casosModulosGet(){
		header('Content-Type: application/json');
		$idModulo = $this->input->post('idModulo');
		$item = $this->Pruebamodel->getModulosPruebas($idModulo);
		echo json_encode($item);
	}

	public function arregloIncidenciasGet(){
		header('Content-Type: application/json');
		$idPrueba = $this->input->post('idPrueba');
		$item = $this->Pruebamodel->getArregloIncidencias($idPrueba);
		echo json_encode($item);
	}

	public function arregloEvidenciasGet() 
	{
		header('Content-Type: application/json');
		// ini_set('memory_limit', '-1');
		// set_time_limit(0);
		 // header('Content-Type: application/mp4');
		// header("Content-Type: video/mp4");
		$idPrueba = $this->input->post('idPrueba');
		$item = $this->Pruebamodel->getArregloEvidencias($idPrueba);

		if ($item != false)
		{
			for ($i=0; $i < count($item); $i++) {
				$item[$i]['EVIDENCES'] = base64_encode($item[$i]['EVIDENCES']->load());
				// print_r($item[$i]['EVIDENCES']);
			}
				// header("Content-Length: ".filesize($item.length));
				// readfile($item.length);
		}
		echo json_encode($item);
	}

	public function arregloImagenesVideosGet() 
	{
		header('Content-Type: application/json');
		// ini_set('memory_limit', '-1');
		// set_time_limit(0);
		 // header('Content-Type: application/mp4');
		// header("Content-Type: video/mp4");
		$idPrueba = $this->input->post('idPrueba');
		$item = $this->Pruebamodel->getArregloImagenes($idPrueba);
		$item1 = $this->Pruebamodel->getArregloVideos($idPrueba);

		if ($item != false)
		{
				// print_r(count($item));
			for ($i=0; $i < count($item); $i++) {
				$item[$i]['EVIDENCES'] = base64_encode($item[$i]['EVIDENCES']->load());
			}
				// header("Content-Length: ".filesize($item.length));
				// readfile($item.length);
		}

		if ($item && $item1) {
			$merge = array_merge($item, $item1);
			echo json_encode($merge);		
		}else if ($item)
		{
			echo json_encode($item);
		}else if ($item1)
		{
			echo json_encode($item1);
		}
	}

	public function pruebasGet(){
		header('Content-Type: application/json');
		$item = $this->Pruebamodel->getPruebas();
		echo json_encode($item);
	}

	public function resultadosGet(){
		header('Content-Type: application/json');
		$item = $this->Pruebamodel->getResultados();
		echo json_encode($item);
	}

	public function estadoIncidenciasGet(){
		header('Content-Type: application/json');
		$item = $this->Pruebamodel->getEstadoIncidencia();
		echo json_encode($item);
	}

	public function incidenciaGet(){
		header('Content-Type: application/json');
		$item = $this->Pruebamodel->getIncidencias();
		echo json_encode($item);
	}

	public function criticidadGet(){
		header('Content-Type: application/json');
		$item = $this->Pruebamodel->getCriticidad();
		echo json_encode($item);
	}

	//Carga Módulo dependiendo del proyecto
	public function ModuloProyectoGet(){
		header('Content-Type: application/json');
		$id = $this->input->post('id');
		$item = $this->Pruebamodel->getModuloProyecto($id);
		echo json_encode($item);
	}

	private function parse_json_request()
	{
		$postData = $this->security->xss_clean($this->input->raw_input_stream);
		return json_decode($postData, true);
	}

	public function proofsInsert(){
		// header('Content-type: application/json');
		// header("Content-Type: text/html;charset=utf-8");
		$array = $this->input->post("data");
		// $info = $this->parse_json_request();
		// echo count($info);
		// return;
		$filas = array();
		$filasEvidencias = array();
		$filasPosiciones = array();
		$info = json_decode($array, true);

		// if (is_array($info['incidences']) || is_object($info['incidences']))
		// {
		/*if(!empty($info['incidences']))
		{*/
			foreach ($info['incidences'] as $value) {
				// echo $value.'</br>';
				$filas[] = array('ID_PROOF_CASE' => $info['IDPRUEBA'],
								 'ID_MAINTENANCE' => $value);
			}
		//}
		// }

		$data = array(
					'ID_PROOF_CASE' => $info['IDPRUEBA'],
					'PROOF_NAME' => $info['CASO'],
					'CASE_TYPE' => $info['TIPOCASO'],
					'INITIAL_ESTIMATED_DATE' => $info['INICIAL'],
					'FINAL_ESTIMATED_DATE' => $info['FINAL'],
					'RESULTS' => $info['RESULTADOS'],
					'OBSERVATION' => $info['OBSERVACION'],
					'ID_PROJECT_MODULES' => $info['IDMODULO'],
					// 'PROOF_CYCLE' => $info['CICLO'],
					'CRITICITY' => $info['CRITICIDAD'],
					'INCIDENCE_STATUS' => $info['ESTADOINCIDENCIA'],
					'EXPECTED_RESULT' => $info['ESPERADO'],
					'OBTAINED_RESULT' => $info['OBTENIDO']);

		/*$filasEvidencias = array(
								'ID_PROOF_CASE' => $info['IDPRUEBA'],
								'EXPECTED_RESULT' => $info['ESPERADO'],
								'OBTAINED_RESULT' => $info['OBTENIDO']
								// 'POSITION_EVIDENCE' => $info['INDICE']
									);*/

		// if (is_array($info['evidences']) || is_object($info['evidences']))
		// {
		/*if(!empty($info['evidences']))
		{*/
			foreach ($info['evidences'] as $key => $value) {
				// echo $value.'</br>';
				$filasEvidencias[] = array(
										'EVIDENCES' => $value/*,
										'ID_PROOF_CASE' => $info['IDPRUEBA'],
										'EXPECTED_RESULT' => $info['ESPERADO'],
										'OBTAINED_RESULT' => $info['OBTENIDO']*/
										// 'POSITION_EVIDENCE' => $info['INDICE']
									);
			}
		//}
		// }
		
		// print_r($filas);
		// print_r($filasEvidencias);
		if(!empty($info['positions']))
		{
			foreach ($info['positions'] as $key => $value) {
				$filasPosiciones[] = array('POSITION_EVIDENCES' => $value);
			}
		}

			// print_r($info);
			// print_r($data);
			// print_r($info['evidences']);
			// print_r($info['positions']);
		
		$item = $this->Pruebamodel->insertPruebas($data, $filas, $filasEvidencias, $filasPosiciones);
		// $this->Pruebamodel->historialInsertPruebas($data, $filas, $filasEvidencias);
		$this->output->set_output(json_encode($item));
		// print_r(json_encode($item));
	}

	public function proofsUpdate(){
		//header('Content-type: application/json');
		$array = $this->input->post("data");
		$filas = array();
		$filasEvidencias = array();
		$filasPosiciones = array();
		$info = json_decode($array, true);

		/*if($info['incidences']!=null)
		{*/
			foreach ($info['incidences'] as $key => $value) {
			// echo $value.'</br>';
			$filas[] = array('ID_PROOF_CASE' => $info['IDPRUEBA'],
							 'ID_MAINTENANCE' => $value);
			}
		//}

		$data = array(
					'ID_PROOF_CASE' => $info['IDPRUEBA'],
					'PROOF_NAME' => $info['CASO'],
					'CASE_TYPE' => $info['TIPOCASO'],
					'INITIAL_ESTIMATED_DATE' => $info['INICIAL'],
					'FINAL_ESTIMATED_DATE' => $info['FINAL'],
					'RESULTS' => $info['RESULTADOS'],
					'OBSERVATION' => $info['OBSERVACION'],
					'ID_PROJECT_MODULES' => $info['IDMODULO'],
					// 'PROOF_CYCLE' => $info['CICLO'],
					'CRITICITY' => $info['CRITICIDAD'],
					'INCIDENCE_STATUS' => $info['ESTADOINCIDENCIA'],
					'EXPECTED_RESULT' => $info['ESPERADO'],
					'OBTAINED_RESULT' => $info['OBTENIDO']);

		/*if($info['evidences']!=null)
		{*/
			foreach ($info['evidences'] as $key => $value) {
				// echo $value.'</br>';
				$filasEvidencias[] = array(
										'EVIDENCES' => $value/*,
										'ID_PROOF_CASE' => $info['IDPRUEBA'],
										'EXPECTED_RESULT' => $info['ESPERADO'],
										'OBTAINED_RESULT' => $info['OBTENIDO']*/);
			}
		//}

		if(!empty($info['positions']))
		{
			foreach ($info['positions'] as $key => $value) {
					$filasPosiciones[] = array('POSITION_EVIDENCES' => $value);
				}
		}

		/*$var = end($filasPosiciones);
		print_r($var);*/

		// print_r($filasEvidencias);
		// print_r($filasPosiciones);


		$item = $this->Pruebamodel->updatePruebas($data, $info['IDPRUEBA'], $filas, $filasEvidencias, $filasPosiciones);
		// $this->Pruebamodel->historialInsertPruebas($data, $filas, $filasEvidencias);
		$this->output->set_output(json_encode($item));
	}

	public function proofPosition() {
		header('Content-type: application/json');
		$array = $this->input->post("data");
		$info = json_decode($array, true);

		$data = array(
					'ID_PROOF_CASE' => $info['ID_PROOF_CASE'],
					'PROOF_NAME' => $info['PROOF_NAME'],
					'CASE_TYPE' => $info['ID_CASE_TYPE'],
					'INITIAL_ESTIMATED_DATE' => $info['INITIAL_ESTIMATED_DATE'],
					'FINAL_ESTIMATED_DATE' => $info['FINAL_ESTIMATED_DATE'],
					'RESULTS' => $info['ID_RESULTS'],
					'OBSERVATION' => $info['OBSERVATION'],
					'ID_PROJECT_MODULES' => $info['ID_PROJECT_MODULES'],
					// 'PROOF_CYCLE' => $info['PROOF_CYCLE'],
					'POSITION_CASE' => $info['POSITION_CASE'],
					'EXPECTED_RESULT' => $info['EXPECTED_RESULT'],
					'OBTAINED_RESULT' => $info['OBTAINED_RESULT']);

		//print_r($data);
		$item = $this->Pruebamodel->updatePosition($data);
		$this->output->set_output(json_encode($item));
	}

	public function evidencePosition() {
		header('Content-type: application/json');
		$array = $this->input->post("data");
		$info = json_decode($array, true);

		$data = array(
					'ID_PROOF_EVIDENCES' => $info['ID_PROOF_EVIDENCES'],
					'EVIDENCES' => $info['EVIDENCES'],
					'ACTUAL_DATE' => $info['ACTUAL_DATE'],
					'ID_PROOF_CASE' => $info['ID_PROOF_CASE'],
					'EXPECTED_RESULT' => $info['EXPECTED_RESULT'],
					'OBTAINED_RESULT' => $info['OBTAINED_RESULT'],
					'IMAGE_OR_VIDEO' => $info['IMAGE_OR_VIDEO'],
					'PROOF_EVIDENCE_STATUS' => $info['PROOF_EVIDENCE_STATUS'],
					'POSITION_EVIDENCES' => $info['POSITION_EVIDENCES']
					);

		//print_r($data);
		$item = $this->Pruebamodel->updatePositionEvidence($data);
		$this->output->set_output(json_encode($item));
	}

	public function proofDelete() {
		header('Content-type: application/json');
		$idprueba = $this->input->post('idPrueba');
		$item = $this->Pruebamodel->deletePrueba($idprueba);
		echo json_encode($item);
	}

	public function ciclosInsert(){
		$modulo = $this->input->post("modulo");
		$observacion = $this->input->post("observacion");
		$filas = array('ID_PROJECT_MODULE' => $modulo,
						'GENERAL_OBSERVATION' => $observacion);
		
		$item = $this->Pruebamodel->insertCiclos($filas);
		$this->output->set_output(json_encode($item));
	}

	public function proofsClonacionInsert(){
		// header('Content-type: application/json');
		$array = $this->input->post("data");
		$filasEvidencias = array();
		$info = json_decode($array, true);

		$data = array(
					// 'ID_PROOF_CASE' => $info['IDPRUEBA'],
					'PROOF_NAME' => $info['CASO'],
					'CASE_TYPE' => $info['TIPOCASO'],
					'EXPECTED_RESULT' => $info['ESPERADO'],
					// 'EVIDENCES' => $info['evidences'],
					// 'INITIAL_ESTIMATED_DATE' => $info['INICIAL'],
					// 'FINAL_ESTIMATED_DATE' => $info['FINAL'],
					// 'RESULTS' => $info['RESULTADOS'],
					// 'OBSERVATION' => $info['OBSERVACION'],

					'ID_PROJECT_MODULES' => $info['IDMODULO']);
					// 'PROOF_CYCLE' => $info['CICLO'],
					// 'CRITICITY' => $info['CRITICIDAD']);

		// foreach ($info['evidences'] as $key => $value) {
		// 	// echo $value.'</br>';
		// 	$filasEvidencias[] = array('EVIDENCES' => $value);
		// }

		// foreach ($info['evidences'] as $key => $value) {
		// 		// echo $value.'</br>';
		// 		$filasEvidencias[] = array(
		// 								// 'EVIDENCES' => $value,
		// 								'ID_PROOF_CASE' => $info['IDPRUEBA'],
		// 								'EXPECTED_RESULT' => $info['ESPERADO']);
		// 								// 'OBTAINED_RESULT' => $info['OBTENIDO']);
		// 	}
		
		// print_r($data);
		
		$item = $this->Pruebamodel->insertPruebasClonacion($data);
		//$this->Pruebamodel->historialInsertPruebas($data, $filas, $filasEvidencias);
		$this->output->set_output(json_encode($item));
	}

	public function proofEvidenceDelete() {
		header('Content-type: application/json');
		$idEvidencia = $this->input->get('idEvidencia');
		$item = $this->Pruebamodel->deleteEvidencia($idEvidencia);
		echo json_encode($item);
	}

	public function proofVideoDelete() {
		header('Content-type: application/json');
		$idVideo = $this->input->get('idVideo');
		$item = $this->Pruebamodel->deleteVideo($idVideo);
		echo json_encode($item);
	}

	public function addupload()
    {

		$dir = "upload_videos/";
		foreach ($_FILES as $video) {

			$n=$video['tmp_name'];
			// echo ($video['tmp_name']);
			// echo ($video['name']);

			// $fecha = date("2012-07-08 11:14:15.889342");
			$t = microtime(true);
			$micro = sprintf("%06d",($t - floor($t)) * 1000000);
			$d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
			$fecha = $d->format("Y-m-d H-i-s-u");
			$type = ".mp4";
			// echo $fecha->getTimestamp();

			// move_uploaded_file( $n, $dir.$video['name']);
			move_uploaded_file($n, $dir.$fecha.$type);
			// $url = array('EVIDENCES' => $dir.$video['name']);
			$url = array('EVIDENCES' => $dir.$fecha.$type);
			$item = $this->Pruebamodel->insertUrlVideo($url);
		   echo json_encode($item);

		}
    }
}?>