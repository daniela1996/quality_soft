<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Perfiles extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('perfilmodel');
		$this->load->model('user');
	}
	public function index() {
		if($session = $this->session->userdata('logged_in')) {
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['perfiles'] = $this->perfilmodel->getPerfil();
				$data['jsfile'] = "Perfiles.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('perfiles_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function perfilParticular() {
		header('Content-type: application/json');
		$array = $this->input->post('perfilName');
		$decoded = json_decode($array, true);
		$item = $this->perfilmodel->getPerfilParticular($decoded['PROFILEID']);
		$this->output->set_output(json_encode($item));
	}
	public function perfilInsert() {
		header('Content-type: application/json');
		$array = $this->input->post('perfilName');
		$decoded = json_decode($array,true);
		$rows = array('CODEPROFILE' => $decoded['CODEPROFILE'],
									'NAMEPROFILE' => $decoded['NAMEPROFILE'],
									'DESCPROFILE' => $decoded['DESCPROFILE']);
		$ultimoRegistro = $this->perfilmodel->getUltimoPerfil();
		$item = $this->perfilmodel->insertPerfil($rows);
		$nuevosRegistros = $this->perfilmodel->getUltimoPerfil();
		$rowsRespuesta = $this->ultimasEntradas($ultimoRegistro, $nuevosRegistros);
		$this->output->set_output(json_encode(array($item, $rowsRespuesta, 1)));
	}
	public function perfilUpdate() {
		header('Content-type: application/json');
		$array = $this->input->post('perfilName');
		$decoded = json_decode($array,true);
		$rows = array('CODEPROFILE' => $decoded['CODEPROFILE'],
									'NAMEPROFILE' => $decoded['NAMEPROFILE'],
									'DESCPROFILE' => $decoded['DESCPROFILE']);
		$item = $this->perfilmodel->updatePerfil($decoded['PROFILEID'], $rows);
		$perfilUpdated = $this->perfilmodel->getPerfilParticular($decoded['PROFILEID']);
		$this->output->set_output(json_encode(array($item, $perfilUpdated, 2)));
	}
	public function perfilDelete() {
		header('Content-type: application/json');
		$array = $this->input->post('perfilName');
		$decoded = json_decode($array,true);
		$item = $this->perfilmodel->deletePerfil($decoded['PROFILEID']);
		$this->output->set_output(json_encode(array($item, false, 3)));
	}
	private function ultimasEntradas($ultimoRegistro, $nuevosRegistros) {
		$diferenciaAgregados = (intval($nuevosRegistros['CONTEO']) - intval($ultimoRegistro['CONTEO']));
		if ($diferenciaAgregados > 0) {
			return $this->perfilmodel->getPerfilNuevo($diferenciaAgregados);
		} else {
			return false;
		}
	}
} ?>
