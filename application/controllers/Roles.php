<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Roles extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('rolmodel');
		$this->load->model('modulomodel');
	}
	function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(),$session['uniqueid'])) {
				$data['result'] = $this->rolmodel->getRol();
				$data['modulo'] = $this->modulomodel->getModulo();
				$data['jsfile'] = "Roles.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('roles_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function rolInsert() {
		header('Content-type: application/json');
		$array = $this->input->post('rolesName');
		$decoded = json_decode($array,true);
		$rows = array('CODEROLE' => $decoded['CODIGOROL'],
									'NAMEROLE' => $decoded['NOMBREROL'],
									'IDMODULES' => $decoded['MODULOIDROL'],
									'DESCROLE' => $decoded['DESCROL']);
		$ultimoRegistro = $this->rolmodel->getUltimoRol();
		$item = $this->rolmodel->insertRole($rows);
		$nuevosRegistros = $this->rolmodel->getUltimoRol();
		$rowsRespuesta = $this->ultimasEntradas($ultimoRegistro, $nuevosRegistros);
		$this->output->set_output(json_encode(array($item, $rowsRespuesta, 1)));
	}
	public function rolUpdate() {
		header('Content-type: application/json');
		$array = $this->input->post('rolesName');
		$decoded = json_decode($array,true);
		$rows = array('CODEROLE' => $decoded['CODIGOROL'],
									'NAMEROLE' => $decoded['NOMBREROL'],
									'IDMODULES' => $decoded['MODULOIDROL'],
									'DESCROLE' => $decoded['DESCROL']);
		$item = $this->rolmodel->updateRole($decoded['ROLID'], $rows);
		$perfilUpdated = $this->rolmodel->getRolParticular($decoded['ROLID']);
		$this->output->set_output(json_encode(array($item, $perfilUpdated, 2)));
	}
	public function rolDelete() {
		header('Content-type: application/json');
		$array = $this->input->post('rolesName');
		$decoded = json_decode($array,true);
		$item = $this->rolmodel->deleteRole($decoded['ROLID']);
		$this->output->set_output(json_encode(array($item, false, 3)));
	}
	private function ultimasEntradas($ultimoRegistro, $nuevosRegistros) {
		$diferenciaAgregados = (intval($nuevosRegistros['CONTEO']) - intval($ultimoRegistro['CONTEO']));
		if ($diferenciaAgregados > 0) {
			return $this->rolmodel->getRolNuevo($diferenciaAgregados);
		} else {
			return false;
		}
	}
} ?>
