<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bitacoraasignacionnodos extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Bitacoraasignacionnodosmodel');
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['jsfile'] = "Bitacoraasignacionnodos.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('Bitacora_asignacion_nodos_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function obtenerBitacora(){
		header('Content-type: application/json');
		$FechaInicial = $this ->input->post('FechaInicial');
		$FechaFinal  = $this ->input->post('FechaFinal');
		$item = $this->Bitacoraasignacionnodosmodel->getBitacora($FechaInicial, $FechaFinal);
		echo json_encode($item);
	}
}?>