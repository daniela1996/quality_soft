<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class VerifyLogin extends CI_Controller {

	 function __construct()
	 {
	   	parent::__construct();
		$this->load->model('user','',TRUE);
	 }

	 function index()
	 {


       $this->load->helper(array('form', 'url'));

		 if( !ini_get('date.timezone') )
			{
			    date_default_timezone_set('America/New_York');
			}
	   //This method will have the credentials validation
       $this->load->library('form_validation');




	   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

	   $this->form_validation->set_error_delimiters('<div class="alert alert-info">','</div>');
	  //$this->form_validation->set_message('required', 'usuario o clave invalidos.');
	   $this->form_validation->set_message('rule', 'Error Message');
//echo $query -> num_rows();

	   	if($this->form_validation->run() == FALSE)
		{
		    // echo 'Field validation failed.  User redirected to login page';
	     	$this->load->view('login_view');
	    }
		else
		{
			//Go to private area
			redirect('home', 'refresh');
		}

	}

	 function check_database($password)
	 {
	   //Field validation succeeded.  Validate against database
	   $username = $this->input->post('username');

	   //query the database
	   $result = $this->user->getUserByEmailAndPassword($username, $password); //login($username, $password);


	   if($result)
	   {
	     $sess_array = array();
	     foreach($result as $row)
	     {
	       $sess_array = array(

                    'userid'   => $row->U_ID,
                    'fname'    => $row->NAME,
                    'salt'    => $row->SALT,
                    'dates'    => $row->CREATED_AT,
					'email'    => $row->EMAIL,
					'celular'  => $row->PHONE,
					'image'  => ((is_null($row->PHOTO)) ? false : base64_encode($row->PHOTO->load())),
					'level'  => $row->TIPO,
                    'username' => $row->UPDATED_AT, //$row->username
					'uniqueid' => $row->UNIQUE_ID,
					'status' => $row->ESTADO,
					'validated'=> true
	       );
	       $this->session->set_userdata('logged_in', $sess_array);
	     }
	     return TRUE;
	   }
	   else
	   {
	   	  $this->form_validation->set_error_delimiters('<div class="alert alert-error">', '</div>');
	     $this->form_validation->set_message('check_database', 'usuario o clave invalidos.');

	     return false;
	   }
	 }
	}
