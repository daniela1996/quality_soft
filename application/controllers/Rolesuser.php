<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rolesuser extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model("rolmodel");
		$this->load->model("user");
	}
	function index() {
		$ru = $this->input->get('ru');
		$vl = $this->input->get('vl');
		$data['result'] = $this->rolmodel->getRolUser();
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(),$session['uniqueid'])) {
				$data['jsfile'] = "RoleUser.js";
				$data['both'] = true;
				$data["idVal"] = (empty($ru)) ? 0: $vl;
				if (!((empty($ru))||(empty($vl)))) {
					if($ru == 1) {
						$data['result'] = $this->rolmodel->getRolUserParticular($vl);
						$data['roleUsuario'] = 1;
					}
					if($ru == 2) {
						$data['result'] = $this->user->getUserRolParticular($vl);
						$data['roleUsuario'] = 2;
					}
					if(!empty($data['result'])) {
						if ($ru == 1) {
							$data['user'] = $this->user->getUserIncompleto($vl);
						}
						if ($ru == 2) {
							$data['role'] = $this->rolmodel->getRolIncompleto($vl);
						}
					} else {
						$data['role'] = $this->rolmodel->getRol();
						$data['user'] = $this->user->getUser();
					}
				} else {
					$data['both'] = false;
				}
				$this->load->view('template/header.php', $session);
				$this->load->view('role_user_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function roleusuarioInsert() {
		header('Content-type: application/json');
		$array = $this->input->post('rolesusuariosName');
		$decoded = json_decode($array,true);
		$rows = array();
		foreach($decoded['dataArray'] as $fields) {
			$rows[] = array('USERID' => $fields['USERS_IDUSERS'],
											'ROLEID' => $fields['ROLES_IDROLES']);
		}
		$ultimoRegistro = (intval($decoded['PROCEDENCIA']) == 1) ? $this->user->getUltimoUsuarioRol($decoded['LLAVE']) : $this->rolmodel->getUltimoRolUsuario($decoded['LLAVE']);
		$item = $this->rolmodel->insertRoleOUsuario($rows);
		$nuevosRegistros = (intval($decoded['PROCEDENCIA']) == 1) ? $this->user->getUltimoUsuarioRol($decoded['LLAVE']): $this->rolmodel->getUltimoRolUsuario($decoded['LLAVE']);
		$rowsRespuesta = $this->ultimasEntradas($ultimoRegistro, $nuevosRegistros, intval($decoded['PROCEDENCIA']), intval($decoded['LLAVE']));
		$this->output->set_output(json_encode(array($item, $rowsRespuesta)));
	}
	public function roleusuarioAcceptDecline() {
		header('Content-type: application/json');
		$array = $this->input->post('rolesusuariosName');
		$decoded = json_decode($array,true);
		$item = $this->rolmodel->acceptDecline($decoded['ROLE_USERID'], $decoded['ESTADO']);
		$this->output->set_output(json_encode($item));
	}
	private function ultimasEntradas($ultimoRegistro, $nuevosRegistros, $procedencia, $llave) {
		$diferenciaAgregados = (intval($nuevosRegistros['CONTEO']) - intval($ultimoRegistro['CONTEO']));
		if ($diferenciaAgregados > 0) {
			if ($procedencia == 1) {
				return $this->user->getUserRoleNuevo($llave, $diferenciaAgregados);
			} else {
				return $this->rolmodel->getRolUserNuevo($llave, $diferenciaAgregados);
			}
		} else {
			return false;
		}
	}
} ?>
