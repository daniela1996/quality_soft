<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ReportePrueba extends CI_Controller 
{
	function __construct()
	{
		
		parent::__construct();
		$this->load->model('ReporteModel');
		$this->load->model('Pruebamodel');
		$this->load->library('excel');
		
	}

	public function index()
	{
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['jsfile'] = "Reportes.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('reportes_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}

	public function detalleModulos($proyecto)
	{
		$results = $this->ReporteModel->getReporteriaDiariaFecha();
		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$name_project = $value['NOMBRE_PROYECTO'];
			}
		}

		$file = APPPATH."files/Reporte_Detalle_Modulos.xlsx";
		$obj = PHPExcel_IOFactory::load($file);	
		//$obj = new PHPExcel();
		$obj->getActiveSheet()->setShowGridlines(false);
		// $obj->setActiveSheetIndex(0);

		if ($results['Estado'])
		{
			$obj->getActiveSheet()->setCellValue('M3', 'Fecha:');

			$fila = 3;
			foreach ($results['Resultado'] as $key => $value)
			{				
				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['DIA']." ".$value['NUMERO_DIA']." de ".$value['MES']."de ".$value['AÑO']);

				$fila++;
			}
		}

		$obj->getActiveSheet()->setCellValue('B7', 'Nombre del Proyecto');

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C7', 'Prioridad de Módulos');

			$obj->getActiveSheet()->getStyle('C7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D7', 'Módulos');

			$obj->getActiveSheet()->getStyle('D7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E7', 'Estado del Proyecto');

			$obj->getActiveSheet()->getStyle('E7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F7', 'Fecha de Inicio de Desarrollo de Módulo');

			$obj->getActiveSheet()->getStyle('F7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G7', 'Fecha Final de Desarrollo de Módulo');

			$obj->getActiveSheet()->getStyle('G7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H7', 'Porcentaje de Avance');

			$obj->getActiveSheet()->getStyle('H7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I7', 'Ciclo de Desarrollo');

			$obj->getActiveSheet()->getStyle('I7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J7', 'Correcciones Pendientes');

			$obj->getActiveSheet()->getStyle('J7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K7', 'Correcciones Realizadas');

			$obj->getActiveSheet()->getStyle('K7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L7', 'Total de Correcciones');

			$obj->getActiveSheet()->getStyle('L7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M7', 'Porcentaje Incidencias Corregidas');

			$obj->getActiveSheet()->getStyle('M7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N7', 'Desarrollador Asignado');

			$obj->getActiveSheet()->getStyle('N7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('O7', 'Observaciones (Bitácora de Eventos)');

			$obj->getActiveSheet()->getStyle('O7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

		$result = $this->ReporteModel->getDetalleModulo($proyecto);
		
		$fila = 8;

		if ($result['Estado']) 
		{
			// $fila = 8;
			foreach ($result['Resultado'] as $key => $value) 
			{				
				for($col = 'A'; $col !== 'Q'; $col++) {
						    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
						}

				$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$fila, $value['PRIORITY']);

				$obj->getActiveSheet()->getStyle('C'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$fila, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$fila, $value['STATUS_PROJECT']);

				$obj->getActiveSheet()->getStyle('E'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$fila, $value['REAL_INICIAL']);

				$obj->getActiveSheet()->getStyle('F'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$fila, $value['REAL_FINAL']);

				$obj->getActiveSheet()->getStyle('G'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$fila, $value['PORCENTAJE_AVANCE']);

				$obj->getActiveSheet()->getStyle('H'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['NUMERO_PORCENTAJE_AVANCE'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('H'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //ROJO
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['NUMERO_PORCENTAJE_AVANCE'] >= 51 && $value['NUMERO_PORCENTAJE_AVANCE'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('H'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('H'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //VERDE
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$fila, $value['PROOF_CYCLE']);

				$obj->getActiveSheet()->getStyle('I'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$fila, $value['INCIDENTES_PENDIENTES']);

				$obj->getActiveSheet()->getStyle('J'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$fila, $value['INCIDENTES_CORREGIDOS']);

				$obj->getActiveSheet()->getStyle('K'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['TOTAL_CORRECCIONES']);

				$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$fila, $value['PORCENTAJE_CORREGIDO']);

				$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['PORCENTAJE_CORREGIDO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //ROJO
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['PORCENTAJE_CORREGIDO'] >= 51 && $value['PORCENTAJE_CORREGIDO'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //VERDE
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('O'.$fila, $value['OBSERVACION_GENERAL']);

				$obj->getActiveSheet()->getStyle('O'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$fila++;
				// $projectname = $value['PROJECT_NAME'];
			}
		}
		else
		{
			$obj->getActiveSheet()->setCellValue('B'.$fila, 'No hay Registros que mostrar!');
		}

		//prepare download
		// $filename='Reporte_Detalle_Modulos'.date('d-m-y').'/'.$projectname.'.xlsx'; //just some random filename
		$filename='Reporte_Detalle_Modulos'.date('d-m-y').'-'.$name_project.'.xlsx'; //just some random filename
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
	}

	public function incidencias($proyecto)
	{
		$results = $this->ReporteModel->getReporteriaDiariaFecha();
		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$name_project = $value['NOMBRE_PROYECTO'];
			}
		}

		$file = APPPATH."files/Reporte_Incidencias.xlsx";
		$obj = PHPExcel_IOFactory::load($file);	
		//$obj = new PHPExcel();
		$obj->getActiveSheet()->setShowGridlines(false);
		$obj->setActiveSheetIndex(0);
		
		if ($results['Estado'])
		{
			$obj->getActiveSheet()->setCellValue('K3', 'Fecha:');

			$fila = 3;
			foreach ($results['Resultado'] as $key => $value)
			{				
				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['DIA']." ".$value['NUMERO_DIA']." de ".$value['MES']."de ".$value['AÑO']);

				$fila++;
			}
		}

		$obj->getActiveSheet()->setCellValue('B7', 'Nombre del Proyecto');

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C7', 'Módulos');

			$obj->getActiveSheet()->getStyle('C7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D7', 'Nro Incidencia');

			$obj->getActiveSheet()->getStyle('D7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E7', 'Descripción');

			$obj->getActiveSheet()->getStyle('E7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F7', 'Tipo de Incidencia');

			$obj->getActiveSheet()->getStyle('F7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G7', 'Estatus de Incidencia');

			$obj->getActiveSheet()->getStyle('G7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H7', 'Criticidad');

			$obj->getActiveSheet()->getStyle('H7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I7', 'Ciclo de Detección');

			$obj->getActiveSheet()->getStyle('I7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J7', 'Responsable');

			$obj->getActiveSheet()->getStyle('J7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K7', 'Fecha de Detección');

			$obj->getActiveSheet()->getStyle('K7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L7', 'Fecha de Corrección');

			$obj->getActiveSheet()->getStyle('L7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M7', 'Ciclo de Corrección');

			$obj->getActiveSheet()->getStyle('M7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N7', 'Observaciones');

			$obj->getActiveSheet()->getStyle('N7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

		$result = $this->ReporteModel->getIncidenciasGlobales($proyecto);

		$fila=8;

		if ($result['Estado']) 
		{
			// $fila = 8;
			foreach ($result['Resultado'] as $key => $value) 
			{				
				for($col = 'A'; $col !== 'P'; $col++) {
						    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
						}

				$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$fila, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$fila, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$fila, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$fila, $value['INCIDENCE_NUMBER']);

				$obj->getActiveSheet()->getStyle('D'.$fila, $value['INCIDENCE_NUMBER'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$fila, $value['PROOF_NAME']);

				$obj->getActiveSheet()->getStyle('E'.$fila, $value['PROOF_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$fila, $value['TIPO_INCIDENCIA']);

				$obj->getActiveSheet()->getStyle('F'.$fila, $value['TIPO_INCIDENCIA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$fila, $value['INCIDENCE_STATUS']);

				$obj->getActiveSheet()->getStyle('G'.$fila, $value['INCIDENCE_STATUS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$fila, $value['CRITICITY_NAME']);

				$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['CRITICITY_NAME'] == 'Stopper')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROJO
					            'color' => array('rgb' => 'FF0000')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Crítica')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROSADO
					            'color' => array('rgb' => 'FF8080')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Alta')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ANARANJADO
					            'color' => array('rgb' => 'FF9900')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Media')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Baja')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO TENUE
					            'color' => array('rgb' => 'FFFF99')
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$fila, $value['PROOF_CYCLE_DETECTION']);

				$obj->getActiveSheet()->getStyle('I'.$fila, $value['PROOF_CYCLE_DETECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$fila, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('J'.$fila, $value['DESARROLLADOR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$fila, $value['DETECTION_DATE']);

				$obj->getActiveSheet()->getStyle('K'.$fila, $value['DETECTION_DATE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['DATE_TESTING_CORRECTION']);

				$obj->getActiveSheet()->getStyle('L'.$fila, $value['DATE_TESTING_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$fila, $value['PROOF_CYCLE_CORRECTION']);

				$obj->getActiveSheet()->getStyle('M'.$fila, $value['PROOF_CYCLE_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['OBSERVATION']);

				$obj->getActiveSheet()->getStyle('N'.$fila, $value['OBSERVATION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$fila++;
				// $projectname = $value['PROJECT_NAME'];				
			}
		}
		else
		{
			$obj->getActiveSheet()->setCellValue('B'.$fila, 'No hay Registros que mostrar!');
		}		

		//prepare download
		// $filename='Reporte_Incidencias'.date('d-m-y').'/'.$projectname.'.xlsx'; //just some random filename
		$filename='Reporte_Incidencias'.date('d-m-y').'-'.$name_project.'.xlsx'; //just some random filename
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
		$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
	}
	
	public function reporteriaDiaria($proyecto)
	{
		$result = $this->ReporteModel->getReporteriaDiariaFecha();
		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$name_project = $value['NOMBRE_PROYECTO'];
			}
		}
		
		$file = APPPATH."files/Reporteria_Diaria.xlsx";
		$obj = PHPExcel_IOFactory::load($file);	
		//$obj = new PHPExcel();
		$obj->getActiveSheet()->setShowGridlines(false);
		$obj->setActiveSheetIndex(0);

		if ($result['Estado'])
		{
			$obj->getActiveSheet()->setCellValue('K4', 'Fecha:');

			$fila = 4;
			foreach ($result['Resultado'] as $key => $value)
			{				
				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['DIA']." ".$value['NUMERO_DIA']." de ".$value['MES']."de ".$value['AÑO']);

				$fila++;
			}
		}

		$result1 = $this->ReporteModel->getReporteriaDiaria($proyecto);

		if ($result1['Estado']) 
		{
			$obj->getActiveSheet()->setCellValue('B11', 'Proyecto');
			$obj->getActiveSheet()->setCellValue('C11', 'Módulo');
			$obj->getActiveSheet()->setCellValue('D11', 'Estatus');
			$obj->getActiveSheet()->setCellValue('E11', 'Ciclo de Pruebas');
			$obj->getActiveSheet()->setCellValue('F11', 'Casos de pruebas de matriz completa');
			$obj->getActiveSheet()->setCellValue('G11', 'Casos de Pruebas por Módulo');
			$obj->getActiveSheet()->setCellValue('H11', 'Ejecutados (testing)');
			$obj->getActiveSheet()->setCellValue('I11', 'Exitosos');
			$obj->getActiveSheet()->setCellValue('J11', 'Incidentes Pendientes');
			$obj->getActiveSheet()->setCellValue('K11', 'Pendientes de Ejecutar');
			$obj->getActiveSheet()->setCellValue('L11', 'Porcentaje completado de priorizados en fecha anterior');
			$obj->getActiveSheet()->setCellValue('M11', 'Porcentaje completado de priorizados de hoy');
			$obj->getActiveSheet()->setCellValue('N11', 'Porcentaje de avance diario');

			$fila = 12;
			foreach ($result1['Resultado'] as $key => $value)
			{
				for($col = 'A'; $col !== 'P'; $col++) {
					    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
					}

				$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$fila, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$fila, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$fila, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$fila, $value['STATUS_PROJECT']);

				$obj->getActiveSheet()->getStyle('D'.$fila, $value['STATUS_PROJECT'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$fila, $value['PROOF_CYCLE']);

				$obj->getActiveSheet()->getStyle('E'.$fila, $value['PROOF_CYCLE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$fila, $value['MATRIZ_COMPLETA']);

				$obj->getActiveSheet()->getStyle('F'.$fila, $value['MATRIZ_COMPLETA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$fila, $value['PRUEBAS_POR_MODULO']);

				$obj->getActiveSheet()->getStyle('G'.$fila, $value['PRUEBAS_POR_MODULO'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$fila, $value['EJECUTADOS']);

				$obj->getActiveSheet()->getStyle('H'.$fila, $value['EJECUTADOS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('I'.$fila, $value['EXITOSOS']);

				$obj->getActiveSheet()->getStyle('I'.$fila, $value['EXITOSOS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$fila, $value['INC_PENDIENTES']);

				$obj->getActiveSheet()->getStyle('J'.$fila, $value['INC_PENDIENTES'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				// Pendientes de Ejecutar
				$obj->getActiveSheet()->setCellValue('K'.$fila, $value['PENDIENTES_EJECUTAR']);

				$obj->getActiveSheet()->getStyle('K'.$fila, $value['PENDIENTES_EJECUTAR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['PORCENTAJEPREVIO'].'%');

				$obj->getActiveSheet()->getStyle('L'.$fila, $value['PORCENTAJEPREVIO'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				//Colores a los porcentajes de ayer
				if($value['PORCENTAJEPREVIO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //ROJO
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if(($value['PORCENTAJEPREVIO'] >= 51) && ($value['PORCENTAJEPREVIO'] <= 90))
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //VERDE
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('M'.$fila, $value['PORCENTAJEACUMULATIVO'].'%');

				$obj->getActiveSheet()->getStyle('M'.$fila, $value['PORCENTAJEACUMULATIVO'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['PORCENTAJEACUMULATIVO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['PORCENTAJEACUMULATIVO'] >= 51 && $value['PORCENTAJEACUMULATIVO'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['NUMERO_PORCENTAJE'].'%');

				$obj->getActiveSheet()->getStyle('N'.$fila, $value['NUMERO_PORCENTAJE'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['NUMERO_PORCENTAJE'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['NUMERO_PORCENTAJE'] >= 51 && $value['NUMERO_PORCENTAJE'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				/* CON VISTA VW_DAILY_REPORT
				if($value['PORCENTAJEPREVIO']==0)
				{
					$obj->getActiveSheet()->setCellValue('L'.$fila, $value['PORCENTAJEPREVIO'].'%');

					$obj->getActiveSheet()->getStyle('L'.$fila, $value['PORCENTAJEPREVIO'].'%')->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
				}
				elseif ($value['PORCENTAJEACTUAL'] == 0) {
					$obj->getActiveSheet()->setCellValue('L'.$fila, $value['PORCENTAJEPREVIO'].'%');

					$obj->getActiveSheet()->getStyle('L'.$fila, $value['PORCENTAJEPREVIO'].'%')->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->setCellValue('L'.$fila, $value['PORCENTAJEACTUAL'].'%');

					$obj->getActiveSheet()->getStyle('L'.$fila, $value['PORCENTAJEACTUAL'].'%')->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
				}

				//Colores a los porcentajes de ayer
				if($value['PORCENTAJEACTUAL'] <= 50 || $value['PORCENTAJEPREVIO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //ROJO
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if(($value['PORCENTAJEACTUAL'] >= 51 || $value['PORCENTAJEPREVIO'] >= 51) && ($value['PORCENTAJEACTUAL'] <= 90 || $value['PORCENTAJEPREVIO'] <= 90))
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //VERDE
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('M'.$fila, $value['TOTAL'].'%');

				$obj->getActiveSheet()->getStyle('M'.$fila, $value['TOTAL'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['TOTAL'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['TOTAL'] >= 51 && $value['TOTAL'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['PORCENTAJE_DIARIO'].'%');

				$obj->getActiveSheet()->getStyle('N'.$fila, $value['PORCENTAJE_DIARIO'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['PORCENTAJE_DIARIO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['PORCENTAJE_DIARIO'] >= 51 && $value['PORCENTAJE_DIARIO'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}*/

				$fila++;
			}

			$filaActual = $fila+1;
			$obj->getActiveSheet()->setCellValue('B'.$filaActual, '2.- Estatus de Incidencias:  (incidencias totales)');
		}
		else
		{
			$filas = 12;
			$obj->getActiveSheet()->setCellValue('B'.$filas, 'No hay Registros que mostrar!');
		}

		$result2 = $this->ReporteModel->getIncidenciasTotales($proyecto);

		if ($result2['Estado']) 
		{
			$filaActual2 = $filaActual + 2;
			$filaActual3 = $filaActual2 + 1;
			$obj->getActiveSheet()->setCellValue('B7', 'Reporte de avance diario  de  Proyecto:');

			$obj->getActiveSheet()->getStyle('B'.$filaActual2.':F'.$filaActual2)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        ),
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        ),
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B'.$filaActual2, 'Reportados');

			$obj->getActiveSheet()->setCellValue('C'.$filaActual2, 'Corregidos');

			$obj->getActiveSheet()->setCellValue('D'.$filaActual2, 'Incidentes Pendientes');

			$obj->getActiveSheet()->setCellValue('E'.$filaActual2, 'Cerrados por Limitantes Técnicas');

			$obj->getActiveSheet()->setCellValue('F'.$filaActual2, 'Casos En Pruebas');

			$fila = 7;
			foreach ($result2['Resultado'] as $key => $value)
			{
						
				$obj->getActiveSheet()->setCellValue('D'.'7', $value['PROJECT_NAME']);
				$obj->getActiveSheet()->setCellValue('B'.$filaActual3, $value['REPORTADOS']);

				$obj->getActiveSheet()->getStyle('B'.$filaActual3, $value['REPORTADOS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaActual3, $value['CORREGIDOS']);

				$obj->getActiveSheet()->getStyle('C'.$filaActual3, $value['CORREGIDOS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaActual3, $value['INCIDENTES_PENDIENTES']);

				$obj->getActiveSheet()->getStyle('D'.$filaActual3, $value['INCIDENTES_PENDIENTES'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaActual3, $value['LIMITANTE_TECNICA']);

				$obj->getActiveSheet()->getStyle('E'.$filaActual3, $value['LIMITANTE_TECNICA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaActual3, $value['CASOS_EN_PRUEBAS']);

				$obj->getActiveSheet()->getStyle('F'.$filaActual3, $value['CASOS_EN_PRUEBAS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$fila++;
			}

			$filaActual4 = $filaActual3+2;
			$obj->getActiveSheet()->setCellValue('B'.$filaActual4, '3.- Detalle de Incidencias Reportadas pendientes:');
		}
		elseif($filaActual4!=null)
		{
			$obj->getActiveSheet()->setCellValue('B'.$filaActual4, 'No hay Registros que mostrar!');
		}

		$result3 = $this->ReporteModel->getIncidencias($proyecto);
		// $obj->getActiveSheet()->setCellValue('A1', $result3['Estado']);

		/*if ($result3['Estado'])
		{
			$obj->getActiveSheet()->setCellValue('A1', 'SI TRAE');
		}else{
			$obj->getActiveSheet()->setCellValue('B1', 'NO TRAE');
		}	*/

		$filaActual5 = $filaActual4 + 2;
			$filaActual6 = $filaActual5 + 1;
			$obj->getActiveSheet()->getStyle('B'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B'.$filaActual5, 'Nombre Proyecto');

			$obj->getActiveSheet()->getStyle('C'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C'.$filaActual5, 'Módulo');

			$obj->getActiveSheet()->getStyle('D'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D'.$filaActual5, 'Nro Incidencia');

			$obj->getActiveSheet()->getStyle('E'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E'.$filaActual5, 'Descripción');

			$obj->getActiveSheet()->getStyle('F'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F'.$filaActual5, 'Tipo de incidencia');

			$obj->getActiveSheet()->getStyle('G'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G'.$filaActual5, 'Estatus de incidencia');

			$obj->getActiveSheet()->getStyle('H'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H'.$filaActual5, 'Criticidad');

			$obj->getActiveSheet()->getStyle('I'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('I'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('I'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I'.$filaActual5, 'Ciclo de detección');

			$obj->getActiveSheet()->getStyle('J'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('J'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('J'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J'.$filaActual5, 'Responsable');

			$obj->getActiveSheet()->getStyle('K'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('K'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('K'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K'.$filaActual5, 'Fecha de detección');

			$obj->getActiveSheet()->getStyle('L'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('L'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('L'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L'.$filaActual5, 'Fecha de corrección');

			$obj->getActiveSheet()->getStyle('M'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('M'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('M'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M'.$filaActual5, 'Ciclo de corrección esperado');

			$obj->getActiveSheet()->getStyle('N'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('N'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('N'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N'.$filaActual5, 'Observaciones');

		if ($result3['Estado'])
		{
			// $obj->getActiveSheet()->getColumnDimension('B')->setWidth(20);

			// $fila = 12;
			foreach ($result3['Resultado'] as $key => $value)
			{

				$obj->getActiveSheet()->setCellValue('B'.$filaActual6, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$filaActual6, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaActual6, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$filaActual6, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaActual6, $value['INCIDENCE_NUMBER']);

				$obj->getActiveSheet()->getStyle('D'.$filaActual6, $value['INCIDENCE_NUMBER'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaActual6, $value['PROOF_NAME']);

				$obj->getActiveSheet()->getStyle('E'.$filaActual6, $value['PROOF_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaActual6, $value['TIPO_INCIDENCIA']);

				$obj->getActiveSheet()->getStyle('F'.$filaActual6, $value['TIPO_INCIDENCIA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$filaActual6, $value['INCIDENCE_STATUS']);

				$obj->getActiveSheet()->getStyle('G'.$filaActual6, $value['INCIDENCE_STATUS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$filaActual6, $value['CRITICITY_NAME']);

				$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['CRITICITY_NAME'] == 'Stopper')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROJO
					            'color' => array('rgb' => 'FF0000')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Crítica')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROSADO
					            'color' => array('rgb' => 'FF8080')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Alta')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ANARANJADO
					            'color' => array('rgb' => 'FF9900')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Media')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Baja')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO TENUE
					            'color' => array('rgb' => 'FFFF99')
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$filaActual6, $value['PROOF_CYCLE_DETECTION']);

				$obj->getActiveSheet()->getStyle('I'.$filaActual6, $value['PROOF_CYCLE_DETECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$filaActual6, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('J'.$filaActual6, $value['DESARROLLADOR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$filaActual6, $value['DETECTION_DATE']);

				$obj->getActiveSheet()->getStyle('K'.$filaActual6, $value['DETECTION_DATE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$filaActual6, $value['DATE_TESTING_CORRECTION']);

				$obj->getActiveSheet()->getStyle('L'.$filaActual6, $value['DATE_TESTING_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$filaActual6, $value['PROOF_CYCLE_CORRECTION']);

				$obj->getActiveSheet()->getStyle('M'.$filaActual6, $value['PROOF_CYCLE_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('N'.$filaActual6, $value['OBSERVATION']);

				$obj->getActiveSheet()->getStyle('N'.$filaActual6, $value['OBSERVATION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$filaActual6++;
			}

			$filaActual7 = $filaActual6+2;
			$obj->getActiveSheet()->setCellValue('B'.$filaActual7, '4.- Detalle de Incidencias Corregidas/Cerradas Por Limitante Técnica:');
		}
		else//if($filaActual7!=null)
		{
			$obj->getActiveSheet()->setCellValue('B'.$filaActual6, 'No hay Registros que mostrar!');

			$filaActual7 = $filaActual6+2;
			$obj->getActiveSheet()->setCellValue('B'.$filaActual7, '4.- Detalle de Incidencias Corregidas/Cerradas Por Limitante Técnica:');
		}

		$result4 = $this->ReporteModel->getIncidenciasCorregidas($proyecto);

		$filaActual8 = $filaActual7 + 2;
			$obj->getActiveSheet()->getStyle('B'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B'.$filaActual8, 'Nombre Proyecto');

			$obj->getActiveSheet()->getStyle('C'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C'.$filaActual8, 'Módulo');

			$obj->getActiveSheet()->getStyle('D'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D'.$filaActual8, 'Nro Incidencia');

			$obj->getActiveSheet()->getStyle('E'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E'.$filaActual8, 'Descripción');

			$obj->getActiveSheet()->getStyle('F'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F'.$filaActual8, 'Tipo de incidencia');

			$obj->getActiveSheet()->getStyle('G'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G'.$filaActual8, 'Estatus de incidencia');

			$obj->getActiveSheet()->getStyle('H'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H'.$filaActual8, 'Criticidad');

			$obj->getActiveSheet()->getStyle('I'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('I'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('I'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I'.$filaActual8, 'Ciclo de detección');

			$obj->getActiveSheet()->getStyle('J'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('J'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('J'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J'.$filaActual8, 'Responsable');

			$obj->getActiveSheet()->getStyle('K'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('K'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('K'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K'.$filaActual8, 'Fecha de detección');

			$obj->getActiveSheet()->getStyle('L'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('L'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('L'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L'.$filaActual8, 'Fecha de corrección');

			$obj->getActiveSheet()->getStyle('M'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('M'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('M'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M'.$filaActual8, 'Ciclo de corrección');

			$obj->getActiveSheet()->getStyle('N'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('N'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('N'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N'.$filaActual8, 'Observaciones');

		$filaActual9 = $filaActual8 + 1;
		if ($result4['Estado'])
		{
			// $obj->getActiveSheet()->getColumnDimension('B')->setWidth(20);

			// $fila = 12;
			foreach ($result4['Resultado'] as $key => $value)
			{

				$obj->getActiveSheet()->setCellValue('B'.$filaActual9, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$filaActual9, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaActual9, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$filaActual9, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaActual9, $value['INCIDENCE_NUMBER']);

				$obj->getActiveSheet()->getStyle('D'.$filaActual9, $value['INCIDENCE_NUMBER'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaActual9, $value['PROOF_NAME']);

				$obj->getActiveSheet()->getStyle('E'.$filaActual9, $value['PROOF_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaActual9, $value['TIPO_INCIDENCIA']);

				$obj->getActiveSheet()->getStyle('F'.$filaActual9, $value['TIPO_INCIDENCIA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$filaActual9, $value['INCIDENCE_STATUS']);

				$obj->getActiveSheet()->getStyle('G'.$filaActual9, $value['INCIDENCE_STATUS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$filaActual9, $value['CRITICITY_NAME']);

				$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['CRITICITY_NAME'] == 'Stopper')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROJO
					            'color' => array('rgb' => 'FF0000')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Crítica')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROSADO
					            'color' => array('rgb' => 'FF8080')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Alta')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ANARANJADO
					            'color' => array('rgb' => 'FF9900')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Media')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Baja')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO TENUE
					            'color' => array('rgb' => 'FFFF99')
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$filaActual9, $value['PROOF_CYCLE_DETECTION']);

				$obj->getActiveSheet()->getStyle('I'.$filaActual9, $value['PROOF_CYCLE_DETECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$filaActual9, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('J'.$filaActual9, $value['DESARROLLADOR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$filaActual9, $value['DETECTION_DATE']);

				$obj->getActiveSheet()->getStyle('K'.$filaActual9, $value['DETECTION_DATE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$filaActual9, $value['DATE_TESTING_CORRECTION']);

				$obj->getActiveSheet()->getStyle('L'.$filaActual9, $value['DATE_TESTING_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$filaActual9, $value['PROOF_CYCLE_CORRECTION']);

				$obj->getActiveSheet()->getStyle('M'.$filaActual9, $value['PROOF_CYCLE_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('N'.$filaActual9, $value['OBSERVATION']);

				$obj->getActiveSheet()->getStyle('N'.$filaActual9, $value['OBSERVATION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$filaActual9++;
				// $projectname = $value['PROJECT_NAME'];
			}
		}
		else
		{
			$obj->getActiveSheet()->setCellValue('B'.$filaActual9, 'No hay Registros que mostrar!');
		}

		//prepare download
		// $filename='Reporteria_Diaria'.date('d-m-y').'/'.$projectname.'.xlsx'; //just some random filename
		$filename='Reporteria_Diaria'.date('d-m-y').'-'.$name_project.'.xlsx'; //just some random filename
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		#$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
		$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
	}

	public function matrizCompleta($proyecto)
	{
		$file = APPPATH."files/Reporte_Matriz_Casos_Pruebas.xlsx";
		$obj = PHPExcel_IOFactory::load($file);	
		//$obj = new PHPExcel();
		$obj->getActiveSheet()->setShowGridlines(false);

		$result0 = $this->ReporteModel->proyectosResumen($proyecto);
		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$name_project = $value['NOMBRE_PROYECTO'];
			}
		}

		if($result0['Estado'])
		{
			$obj->setActiveSheetIndex(0);
			$obj->getActiveSheet(0)->setShowGridlines(false);

			$obj->getActiveSheet()->setCellValue('B7', 'Actualizado Por:');

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B8', 'Fecha:');

			$obj->getActiveSheet()->getStyle('B8')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B8')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B8')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B9', 'Cantidad de Casos de Prueba:');

			$obj->getActiveSheet()->getStyle('B9')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B9')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B9')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B10', 'Módulo');

			$obj->getActiveSheet()->getStyle('B10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C10', 'Ciclo de Prueba');

			$obj->getActiveSheet()->getStyle('C10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D10', 'Fecha de Inicio');

			$obj->getActiveSheet()->getStyle('D10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E10', 'Fecha Final');

			$obj->getActiveSheet()->getStyle('E10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F10', 'Analista');

			$obj->getActiveSheet()->getStyle('F10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G10', 'Casos Ejecutados');

			$obj->getActiveSheet()->getStyle('G10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H10', 'Observaciones');

			$obj->getActiveSheet()->getStyle('H10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			foreach ($result0['Resultado'] as $key => $value) 
			{
				$obj->getActiveSheet()->setCellValue('B4', $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B4')->applyFromArray(
				    array(
				        'font' => array(
				            'bold' => true,
				            // 'color' => array('rgb' => 'FFFFFF'),
				            'size' => 15,
				            'name' => 'Calibri'
				        )
				    )
				);

				$session = $this->session->userdata('logged_in');
				$obj->getActiveSheet()->setCellValue('C7', $session['fname']);
				$obj->setActiveSheetIndex()->mergeCells('C7:H7');

				$obj->getActiveSheet()->getStyle('C7:H7')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        ),
				        'alignment' => array(
				            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C8', $value['PROJECT_DATE_CREATION']);
				$obj->setActiveSheetIndex()->mergeCells('C8:H8');

				$obj->getActiveSheet()->getStyle('C8:H8')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        ),
				        'alignment' => array(
				            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C9', $value['CANTIDADCASOSPROYECTOS']);
				$obj->setActiveSheetIndex()->mergeCells('C9:H9');

				$obj->getActiveSheet()->getStyle('C9:H9')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        ),
				        'alignment' => array(
				            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				        )
				    )
				);
			}
		}

		$resultModulos = $this->ReporteModel->modulosResumen($proyecto);
		$filaModulo = 11;

		if($resultModulos['Estado'])
		{
			foreach ($resultModulos['Resultado'] as $key => $value) 
			{
				for($col = 'A'; $col !== 'J'; $col++) {
				    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

				$obj->getActiveSheet()->setCellValue('B'.$filaModulo, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaModulo, $value['CICLOPRUEBA']);

				$obj->getActiveSheet()->getStyle('C'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaModulo, $value['INITIAL_REAL_DATE']);

				$obj->getActiveSheet()->getStyle('D'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaModulo, $value['FINAL_REAL_DATE']);

				$obj->getActiveSheet()->getStyle('E'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaModulo, $session['fname']);

				$obj->getActiveSheet()->getStyle('F'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$filaModulo, $value['CANTIDADCASOSMODULOS']);

				$obj->getActiveSheet()->getStyle('G'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$filaModulo, $value['OBSERVACION']);

				$obj->getActiveSheet()->getStyle('H'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);				

				$filaModulo++;
			}
		}


		$obj->setActiveSheetIndex(1);

		for($col = 'A'; $col !== 'P'; $col++) {
				    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

		$obj->getActiveSheet()->setCellValue('B2', 'Volver a Casos de Prueba');

			$obj->getActiveSheet()->getStyle('B2')->applyFromArray(
			    array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    )
			);

			$obj->getActiveSheet()->getCell('B2')->getHyperlink()->setUrl("sheet://'CASOS DE PRUEBAS'!A1");

			$obj->getActiveSheet()->getStyle('B5:N5')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B5', 'Nombre del Proyecto');

			$obj->getActiveSheet()->getStyle('B5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C5', 'Módulos');

			$obj->getActiveSheet()->getStyle('C5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D5', 'Nro Incidencia');

			$obj->getActiveSheet()->getStyle('D5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E5', 'Descripción');

			$obj->getActiveSheet()->getStyle('E5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F5', 'Tipo de Incidencia');

			$obj->getActiveSheet()->getStyle('F5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G5', 'Estatus de Incidencia');

			$obj->getActiveSheet()->getStyle('G5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H5', 'Criticidad');

			$obj->getActiveSheet()->getStyle('H5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I5', 'Ciclo de Detección');

			$obj->getActiveSheet()->getStyle('I5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J5', 'Responsable');

			$obj->getActiveSheet()->getStyle('J5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K5', 'Fecha de Detección');

			$obj->getActiveSheet()->getStyle('K5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L5', 'Fecha de Corrección');

			$obj->getActiveSheet()->getStyle('L5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M5', 'Ciclo de Corrección');

			$obj->getActiveSheet()->getStyle('M5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N5', 'Observaciones');

			$obj->getActiveSheet()->getStyle('N5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

		$resultIncidencias = $this->ReporteModel->getIncidenciasGlobales($proyecto);

		$filaIncidencias=6;

		if ($resultIncidencias['Estado']) 
		{
			// $obj->setActiveSheetIndex(1);

			foreach ($resultIncidencias['Resultado'] as $key => $value) 
			{				
				for($col = 'A'; $col !== 'P'; $col++) {
				    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

				$obj->getActiveSheet()->setCellValue('B'.$filaIncidencias, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$filaIncidencias, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaIncidencias, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$filaIncidencias, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaIncidencias, $value['INCIDENCE_NUMBER']);

				$obj->getActiveSheet()->getStyle('D'.$filaIncidencias, $value['INCIDENCE_NUMBER'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaIncidencias, $value['PROOF_NAME']);

				$obj->getActiveSheet()->getStyle('E'.$filaIncidencias, $value['PROOF_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaIncidencias, $value['TIPO_INCIDENCIA']);

				$obj->getActiveSheet()->getStyle('F'.$filaIncidencias, $value['TIPO_INCIDENCIA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$filaIncidencias, $value['INCIDENCE_STATUS']);

				$obj->getActiveSheet()->getStyle('G'.$filaIncidencias, $value['INCIDENCE_STATUS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$filaIncidencias, $value['CRITICITY_NAME']);

				$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['CRITICITY_NAME'] == 'Stopper')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROJO
					            'color' => array('rgb' => 'FF0000')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Crítica')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROSADO
					            'color' => array('rgb' => 'FF8080')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Alta')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ANARANJADO
					            'color' => array('rgb' => 'FF9900')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Media')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Baja')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO TENUE
					            'color' => array('rgb' => 'FFFF99')
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$filaIncidencias, $value['PROOF_CYCLE_DETECTION']);

				$obj->getActiveSheet()->getStyle('I'.$filaIncidencias, $value['PROOF_CYCLE_DETECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$filaIncidencias, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('J'.$filaIncidencias, $value['DESARROLLADOR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$filaIncidencias, $value['DETECTION_DATE']);

				$obj->getActiveSheet()->getStyle('K'.$filaIncidencias, $value['DETECTION_DATE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$filaIncidencias, $value['DATE_TESTING_CORRECTION']);

				$obj->getActiveSheet()->getStyle('L'.$filaIncidencias, $value['DATE_TESTING_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$filaIncidencias, $value['PROOF_CYCLE_CORRECTION']);

				$obj->getActiveSheet()->getStyle('M'.$filaIncidencias, $value['PROOF_CYCLE_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('N'.$filaIncidencias, $value['OBSERVATION']);

				$obj->getActiveSheet()->getStyle('N'.$filaIncidencias, $value['OBSERVATION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$filaIncidencias++;
				// $projectname = $value['PROJECT_NAME'];
			}
		}
		else
		{
			$obj->getActiveSheet()->setCellValue('B'.$filaIncidencias, 'No hay Registros que mostrar!');
		}

		$result = $this->ReporteModel->getMatriz($proyecto);
		
		$fila = 6;

		// for($col = 'A'; $col !== 'H'; $col++) {
		//     $obj->getActiveSheet()->getColumnDimension()->setAutoSize(true);
		// }

		if ($result['Estado']) 
		{
			$obj->setActiveSheetIndex(2);
			$obj->getActiveSheet(2)->setShowGridlines(false);

			$obj->getActiveSheet()->setCellValue('B5', 'Casos de Prueba Aplicados');

			$obj->getActiveSheet()->setCellValue('B2', 'Volver a Resumen');
			$obj->getActiveSheet()->setCellValue('F2', 'Volver a Incidentes');

			$obj->getActiveSheet()->getStyle('B2:F2')->applyFromArray(
			    array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    )
			);

			$obj->getActiveSheet()->getCell('B2')->getHyperlink()->setUrl("sheet://'RESUMEN'!A1");
			$obj->getActiveSheet()->getCell('F2')->getHyperlink()->setUrl("sheet://'INCIDENTES'!A1");

			$obj->getActiveSheet()->getStyle('B5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C5', 'Tipo de Caso');

			$obj->getActiveSheet()->getStyle('C5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D5', 'Resultados');

			$obj->getActiveSheet()->getStyle('D5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E5', 'Evidencias');

			$obj->getActiveSheet()->getStyle('E5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F5', 'Observaciones');

			$obj->getActiveSheet()->getStyle('F5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			foreach ($result['Resultado'] as $key => $value) 
			{
				for($col = 'A'; $col !== 'H'; $col++) {
				    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

				if($value['TIPO'] == '0')
				{
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROJECT_MODULE_NAME']);
					
					$obj->getActiveSheet()->getStyle('B'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('B'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('C'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
					
					$obj->getActiveSheet()->getStyle('C'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
					
					$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('E'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
					
					$obj->getActiveSheet()->getStyle('E'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('F'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
					
					$obj->getActiveSheet()->getStyle('F'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$fila++;
				}//if
				else
				{

					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROOF_NAME']);

					$obj->getActiveSheet()->getStyle('B'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['TIPO_DE_CASO']);

					$obj->getActiveSheet()->getStyle('C'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['RESULTADO']);

					$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					if($value['RESULTADO'] == 'Con Incidentes')//ROJO
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (1ERA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '7B241C'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (2DA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'FADBD8')
						        )
						    )
						);
					}
					elseif($value['RESULTADO'] == 'Satisfactorio')//VERDE
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (9NA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '196F3D'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (10MA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'D5F5E3')
						        )
						    )
						);
					}
					elseif($value['RESULTADO'] == 'Con Observaciones')//AMARILLO
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (11VA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '9A7D0A'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (11VA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'FCF3CF')
						        )
						    )
						);
					}
					elseif($value['RESULTADO'] == 'Pendiente de Probar')//AZUL
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '1A5276'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'D4E6F1')
						        )
						    )
						);
					}
					elseif($value['RESULTADO'] == 'No Aplica')//GRIS
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (16TA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '5F6A6A'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (19NA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'E5E8E8')
						        )
						    )
						);
					}
					// elseif($value['RESULTADO'] == 'Pendiente de Probar')//ANARANJADO
					// {
					// 	$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
					// 	    array(
					// 	    	//TEXTO
					// 	        'font' => array(
					// 	            'bold' => false,
					// 	            //https://htmlcolorcodes.com/es/ (13VA COLUMNA, PENULTIMO)
					// 	            'color' => array('rgb' => '873600'),
					// 	            'size' => 11,
					// 	            'name' => 'Calibri'
					// 	        )
					// 	    )
					// 	);

					// 	$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
					// 	    array(
					// 	    	//BACKGROUND
					// 	        'fill' => array(
					// 	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					// 	            //https://htmlcolorcodes.com/es/ (12VA COLUMNA, 2DA FILA)
					// 	            'color' => array('rgb' => 'FDEBD0')
					// 	        )
					// 	    )
					// 	);
					// }

					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['EVIDENCIAS']);

					$obj->getActiveSheet()->getStyle('E'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->getCell('E'.$fila)->getHyperlink()->setUrl("sheet://'".$value['EVIDENCIAS']."'!A1");

					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['OBSERVATION']);

					$obj->getActiveSheet()->getStyle('F'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$fila++;

				}//else

			}//foreach

			$result1 = $this->ReporteModel->getEvidenciasPruebasSinNulos($proyecto);

			$index=3;
		}//ifresult
		// else
		// {
		// 	$obj->getActiveSheet()->setCellValue('B'.$fila, 'No hay Registros que mostrar!');
		// }

		// $obj->setActiveSheetIndex(1);
		$result = $this->ReporteModel->getDataCasos($proyecto);
		$filaHoja2 = 5;
		$filaHoja3 = 7;

		$result2 = $this->ReporteModel->getEvidenciasPruebas($proyecto);
		$result3 = $this->ReporteModel->getVideosPruebas($proyecto);

		$indexVideo = 3;
		// $filaHoja4 = 26;

		foreach ($result1['Resultado'] as $key => $value1) 
		{
			foreach ($result['Resultado'] as $key => $value) 
			{
				if($value1['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
				{
					$filaHoja3 = 7;
					$obj->createSheet();
					$obj->setActiveSheetIndex($index);
					$obj->getActiveSheet()->setShowGridlines(false);

					$obj->getActiveSheet()->setTitle($value1['EVIDENCIAS']);
					$index++;
				

					for($col = 'A'; $col !== 'H'; $col++) {
					    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
					}

					$obj->getActiveSheet()->setCellValue('B2', 'Volver a Resumen');
					$obj->getActiveSheet()->setCellValue('C2', 'Volver a Casos de Prueba');
					$obj->getActiveSheet()->setCellValue('D2', 'Volver a Incidentes');

					$obj->getActiveSheet()->getStyle('B2:D2')->applyFromArray(
					    array(
					        'alignment' => array(
					            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					        )
					    )
					);

					$obj->getActiveSheet()->getCell('B2')->getHyperlink()->setUrl("sheet://'RESUMEN'!A1");
					$obj->getActiveSheet()->getCell('C2')->getHyperlink()->setUrl("sheet://'CASOS DE PRUEBAS'!A1");
					$obj->getActiveSheet()->getCell('D2')->getHyperlink()->setUrl("sheet://'INCIDENTES'!A1");

					$obj->getActiveSheet()->setCellValue('B4', 'Descripción');

					$obj->getActiveSheet()->getStyle('B4')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => '003366')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('B4')->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFFFFF'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('C4', 'Resultado Esperado');

					$obj->getActiveSheet()->getStyle('C4')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => '003366')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('C4')->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFFFFF'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('D4', 'Resultado Obtenido');

					$obj->getActiveSheet()->getStyle('D4')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => '003366')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('D4')->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFFFFF'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('B'.$filaHoja2, $value['PROOF_NAME']);

					$obj->getActiveSheet()->getStyle('B'.$filaHoja2)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('C'.$filaHoja2, $value['EXPECTED_RESULT']);

					$obj->getActiveSheet()->getStyle('C'.$filaHoja2)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('D'.$filaHoja2, $value['OBTAINED_RESULT']);

					$obj->getActiveSheet()->getStyle('D'.$filaHoja2)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					/*foreach ($result2['Resultado'] as $key => $value2) 
					{
						if($value2['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
						{
							if($value2['EVIDENCES'] != NULL)
							{	
								$laImagen = imagecreatefromstring($value2['EVIDENCES']->load());
								$enX = imagesx($laImagen);
								$enY = imagesy($laImagen);
								
								$dimensiones = imagecreatetruecolor($enX, $enY);
								imagecopyresampled($dimensiones, $laImagen, 0, 0, 0, 0, $enX, $enY, $enX, $enY);
								$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
								$objDrawing->setImageResource($dimensiones);
								$objDrawing->setWorksheet($obj->getActiveSheet());
								$objDrawing->setCoordinates('B'.$filaHoja3);
								
								$calculo = round($enY/20);
								$filaHoja3+=$calculo+2;
							}
						}
					}*/

					$filaaas = 1;
					// $ultimaColumna = $obj->setActiveSheetIndex()->getHighestColumn();
					// $indexVideo = $index-2;
					// $indexVideo = 3;

					// $row = 1; 
					 // $lastColumn = 'E';
					// $lastColumn++; 
					/*for ($column = 'B'; $column != $lastColumn; $column++) {
						$columna = $column;
						// $obj->setActiveSheetIndex()->setCellValue($column.$filaaas, $column);

					    // $cell = $worksheet->getCell($column.$row); 
					    // Do what you want with the cell 
					}*/

					foreach ($result3['Resultado'] as $key => $value3) 
					{
						// $obj->setActiveSheetIndex($index)->setCellValue('A'.$filaaas, $value['ID_PROOF_CASE']);
						// $column = 'B';

						/*for ($n=0; $n<6; $n++) {
						    if($value3['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
							{
						    		++$column;
							}
						}*/
						
								if($value3['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
								{
									if($value3['URL'] != 'NULL')
									{
										$obj->setActiveSheetIndex($index-1)->setCellValue('B'.$filaHoja3, "VIDEO".$filaaas);
										$obj->getActiveSheet()->getCell('B'.$filaHoja3)->getHyperlink()->setUrl(base_url().$value3['URL']);
										$indexVideo+=1;
										// $column++;
										$filaHoja3+=1;
										$filaaas+=1;

									}
								}
							/*for ($column = 'B'; $column != $ultimaColumna; $column++) {
								if($value3['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
								{
									// $obj->getProperties()->setCreator($value3['URL']);
								}
						}*/
					}

					$filaImagen = $filaHoja3+1;

					foreach ($result2['Resultado'] as $key => $value2) 
					{
						if($value2['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
						{
							if($value2['EVIDENCES'] != NULL)
							{	
								$laImagen = imagecreatefromstring($value2['EVIDENCES']->load());
								$enX = imagesx($laImagen);
								$enY = imagesy($laImagen);
								
								$dimensiones = imagecreatetruecolor($enX, $enY);
								imagecopyresampled($dimensiones, $laImagen, 0, 0, 0, 0, $enX, $enY, $enX, $enY);
								$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
								$objDrawing->setImageResource($dimensiones);
								$objDrawing->setWorksheet($obj->getActiveSheet());
								$objDrawing->setCoordinates('B'.$filaImagen);
								
								$calculo = round($enY/20);
								$filaImagen+=$calculo+2;
							}
						}
					}
					
				}
			}
		}

		// else
		// {
		// 	$obj->getActiveSheet()->setCellValue('B'.$fila, 'No hay Registros que mostrar!');
		// }

		//Eliminar la última hoja que genera automaticamente
		if($obj->getSheetByName('Worksheet'))
		{	
			$obj->setActiveSheetIndexByName('Worksheet');
			$sheetIndex = $obj->getActiveSheetIndex();
			$obj->removeSheetByIndex($sheetIndex);
		}
		elseif ($obj->getSheetByName('Worksheet 1')) {
			$obj->setActiveSheetIndexByName('Worksheet 1');
			$sheetIndex = $obj->getActiveSheetIndex();
			$obj->removeSheetByIndex($sheetIndex);
		}

		$obj->setActiveSheetIndex(0);

		//prepare download
		// $filename='Reporte_Matriz_Casos_Pruebas'.date('d-m-y').'/'.$projectname.'.xlsx'; //just some random filename
		$filename='Reporte_Matriz_Casos_Pruebas'.date('d-m-y').'-'.$name_project.'.xlsx'; //just some random filename
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		// $objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
		$objWriter->save('php://output');  //send it to user, of course you can save it to disk also!
	}

	public function setemail_Modulo($proyecto){
		// $email="d.valladares@netgogroup.com, d.valladares@sternemedia.com, j.marin@netgogroup.com";
		$email = $this->ReporteModel->getCorreosPorProyectos($proyecto);

		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$project_name = $value['NOMBRE_PROYECTO'];
			}
		}

		$subject="Reporte de Modulos"."-".$project_name;
		$message="<p>Correo de pruebas usando rutas dinamicas PHP!</p>";
		$file_project = $this->detalleModulossend($proyecto);
		$this->send($email,$subject,$message, $file_project);
		unlink($file_project);
	}

	public function setemail_Incidencias($proyecto){
		// $email="d.valladares@netgogroup.com, d.valladares@sternemedia.com, j.marin@netgogroup.com";
		$email = $this->ReporteModel->getCorreosPorProyectos($proyecto);

		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$project_name = $value['NOMBRE_PROYECTO'];
			}
		}
		
		$subject="Reporte de incidencias"."-".$project_name;
		$message="<p>Correo de pruebas usando rutas dinamicas PHP!</p>";
		$file_project = $this->incidenciassend($proyecto);
		$this->send($email, $subject, $message, $file_project);
		unlink($file_project);
	}

	public function setemail_Diaria($proyecto){
		// $email="d.valladares@netgogroup.com, d.valladares@sternemedia.com, j.marin@netgogroup.com";
		$email = $this->ReporteModel->getCorreosPorProyectos($proyecto);

		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$project_name = $value['NOMBRE_PROYECTO'];
			}
		}

		$subject="Reporteria diaria"."-".$project_name;
		$message="<p>Correo de pruebas usando rutas dinamicas PHP!</p>";
		$file_project = $this->reporteriaDiariasend($proyecto);
		$this->send($email,$subject,$message, $file_project);
		unlink($file_project);
	}

	public function setemail_Matriz($proyecto){
		// $email="d.valladares@netgogroup.com, d.valladares@sternemedia.com, j.marin@netgogroup.com";
		$email = $this->ReporteModel->getCorreosPorProyectos($proyecto);

		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$project_name = $value['NOMBRE_PROYECTO'];
			}
		}

		$subject="Reporte matriz de casos de pruebas"."-".$project_name;
		$message="<p>Correo de pruebas usando rutas dinamicas PHP!</p>";
		$file_project = $this->matrizCompletasend($proyecto);
		$this->send($email,$subject,$message, $file_project);
		unlink($file_project);
	}

	public function send($email,$subject,$message, $pathfile){
	// public function send($email,$subject,$message){
			// Set SMTP Configuration
			$emailConfig = [
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => 'notificaciones.netgogroup@gmail.com',
					'smtp_pass' => '$%netgo123',
					'mailtype' => 'html',
					'charset' => 'iso-8859-1'
			];

			// Set your email information
			$from = [
					'email' => 'notificaciones.netgogroup@gmail.com',
					'name' => 'notificaciones'
			];

			$to = $email;
			//$subject = 'Your gmail subject here';
			$message = '';
			//$message =  $this->load->view('correo_ingresobodega',[],true);
			// Load CodeIgniter Email library
			$this->load->library('email', $emailConfig);
			// Sometimes you have to set the new line character for better result
			$this->email->set_newline("\r\n");
			// Set email preferences
			$this->email->from($from['email'], $from['name']);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($message);
			if (file_exists($pathfile)){
				$this->email->attach($pathfile);
				// $this->email->attach(base_url("files/template/survey-template.xlsx"));
			}
			// Ready to send email and check whether the email was successfully sent
			if (!$this->email->send()){
					// Raise error message
					show_error($this->email->print_debugger());
			} else{
					// Show success notification or other things here
					echo 'Success to send email';
			}
	}

	public function detalleModulossend($proyecto)
	{
		$results = $this->ReporteModel->getReporteriaDiariaFecha();
		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$name_project = $value['NOMBRE_PROYECTO'];
			}
		}

		$file = APPPATH."files/Reporte_Detalle_Modulos.xlsx";
		$obj = PHPExcel_IOFactory::load($file);	
		//$obj = new PHPExcel();
		$obj->getActiveSheet()->setShowGridlines(false);
		// $obj->setActiveSheetIndex(0);

		if ($results['Estado'])
		{
			$obj->getActiveSheet()->setCellValue('M3', 'Fecha:');

			$fila = 3;
			foreach ($results['Resultado'] as $key => $value)
			{				
				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['DIA']." ".$value['NUMERO_DIA']." de ".$value['MES']."de ".$value['AÑO']);

				$fila++;
			}
		}

		$obj->getActiveSheet()->setCellValue('B7', 'Nombre del Proyecto');

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C7', 'Prioridad de Módulos');

			$obj->getActiveSheet()->getStyle('C7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D7', 'Módulos');

			$obj->getActiveSheet()->getStyle('D7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E7', 'Estado del Proyecto');

			$obj->getActiveSheet()->getStyle('E7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F7', 'Fecha de Inicio de Desarrollo de Módulo');

			$obj->getActiveSheet()->getStyle('F7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G7', 'Fecha Final de Desarrollo de Módulo');

			$obj->getActiveSheet()->getStyle('G7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H7', 'Porcentaje de Avance');

			$obj->getActiveSheet()->getStyle('H7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I7', 'Ciclo de Desarrollo');

			$obj->getActiveSheet()->getStyle('I7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J7', 'Correcciones Pendientes');

			$obj->getActiveSheet()->getStyle('J7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K7', 'Correcciones Realizadas');

			$obj->getActiveSheet()->getStyle('K7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L7', 'Total de Correcciones');

			$obj->getActiveSheet()->getStyle('L7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M7', 'Porcentaje Incidencias Corregidas');

			$obj->getActiveSheet()->getStyle('M7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N7', 'Desarrollador Asignado');

			$obj->getActiveSheet()->getStyle('N7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('O7', 'Observaciones (Bitácora de Eventos)');

			$obj->getActiveSheet()->getStyle('O7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

		$result = $this->ReporteModel->getDetalleModulo($proyecto);
		
		$fila = 8;

		if ($result['Estado']) 
		{
			// $fila = 8;
			foreach ($result['Resultado'] as $key => $value) 
			{				
				for($col = 'A'; $col !== 'Q'; $col++) {
						    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
						}

				$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$fila, $value['PRIORITY']);

				$obj->getActiveSheet()->getStyle('C'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$fila, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$fila, $value['STATUS_PROJECT']);

				$obj->getActiveSheet()->getStyle('E'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$fila, $value['REAL_INICIAL']);

				$obj->getActiveSheet()->getStyle('F'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$fila, $value['REAL_FINAL']);

				$obj->getActiveSheet()->getStyle('G'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$fila, $value['PORCENTAJE_AVANCE']);

				$obj->getActiveSheet()->getStyle('H'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['NUMERO_PORCENTAJE_AVANCE'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('H'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //ROJO
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['NUMERO_PORCENTAJE_AVANCE'] >= 51 && $value['NUMERO_PORCENTAJE_AVANCE'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('H'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('H'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //VERDE
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$fila, $value['PROOF_CYCLE']);

				$obj->getActiveSheet()->getStyle('I'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$fila, $value['INCIDENTES_PENDIENTES']);

				$obj->getActiveSheet()->getStyle('J'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$fila, $value['INCIDENTES_CORREGIDOS']);

				$obj->getActiveSheet()->getStyle('K'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['TOTAL_CORRECCIONES']);

				$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$fila, $value['PORCENTAJE_CORREGIDO']);

				$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['PORCENTAJE_CORREGIDO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //ROJO
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['PORCENTAJE_CORREGIDO'] >= 51 && $value['PORCENTAJE_CORREGIDO'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //VERDE
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('O'.$fila, $value['OBSERVACION_GENERAL']);

				$obj->getActiveSheet()->getStyle('O'.$fila)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$fila++;
				// $projectname = $value['PROJECT_NAME'];
			}
		}
		else
		{
			$obj->getActiveSheet()->setCellValue('B'.$fila, 'No hay Registros que mostrar!');
		}

		//prepare download
		// $filename='Reporte_Detalle_Modulos'.date('d-m-y').'-'.$projectname.'('.rand(0, 100).').xlsx'; //just some random filename
		$filename='Reporte_Detalle_Modulos'.date('d-m-y').'-'.$name_project.'.xlsx'; //just some random filename
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		$objWriter->save(APPPATH."reportes/"."".$filename);  //send it to user, of course you can save it to disk also!
		return APPPATH."reportes/"."".$filename;
	}

	public function incidenciassend($proyecto)
	{
		$results = $this->ReporteModel->getReporteriaDiariaFecha();
		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$name_project = $value['NOMBRE_PROYECTO'];
			}
		}

		$file = APPPATH."files/Reporte_Incidencias.xlsx";
		$obj = PHPExcel_IOFactory::load($file);	
		//$obj = new PHPExcel();
		$obj->getActiveSheet()->setShowGridlines(false);
		$obj->setActiveSheetIndex(0);
		
		if ($results['Estado'])
		{
			$obj->getActiveSheet()->setCellValue('K3', 'Fecha:');

			$fila = 3;
			foreach ($results['Resultado'] as $key => $value)
			{				
				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['DIA']." ".$value['NUMERO_DIA']." de ".$value['MES']."de ".$value['AÑO']);

				$fila++;
			}
		}

		$obj->getActiveSheet()->setCellValue('B7', 'Nombre del Proyecto');

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C7', 'Módulos');

			$obj->getActiveSheet()->getStyle('C7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D7', 'Nro Incidencia');

			$obj->getActiveSheet()->getStyle('D7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E7', 'Descripción');

			$obj->getActiveSheet()->getStyle('E7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F7', 'Tipo de Incidencia');

			$obj->getActiveSheet()->getStyle('F7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G7', 'Estatus de Incidencia');

			$obj->getActiveSheet()->getStyle('G7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H7', 'Criticidad');

			$obj->getActiveSheet()->getStyle('H7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I7', 'Ciclo de Detección');

			$obj->getActiveSheet()->getStyle('I7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J7', 'Responsable');

			$obj->getActiveSheet()->getStyle('J7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K7', 'Fecha de Detección');

			$obj->getActiveSheet()->getStyle('K7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L7', 'Fecha de Corrección');

			$obj->getActiveSheet()->getStyle('L7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M7', 'Ciclo de Corrección');

			$obj->getActiveSheet()->getStyle('M7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N7', 'Observaciones');

			$obj->getActiveSheet()->getStyle('N7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

		$result = $this->ReporteModel->getIncidenciasGlobales($proyecto);

		$fila=8;

		if ($result['Estado']) 
		{
			// $fila = 8;
			foreach ($result['Resultado'] as $key => $value) 
			{				
				for($col = 'A'; $col !== 'P'; $col++) {
						    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
						}

				$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$fila, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$fila, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$fila, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$fila, $value['INCIDENCE_NUMBER']);

				$obj->getActiveSheet()->getStyle('D'.$fila, $value['INCIDENCE_NUMBER'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$fila, $value['PROOF_NAME']);

				$obj->getActiveSheet()->getStyle('E'.$fila, $value['PROOF_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$fila, $value['TIPO_INCIDENCIA']);

				$obj->getActiveSheet()->getStyle('F'.$fila, $value['TIPO_INCIDENCIA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$fila, $value['INCIDENCE_STATUS']);

				$obj->getActiveSheet()->getStyle('G'.$fila, $value['INCIDENCE_STATUS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$fila, $value['CRITICITY_NAME']);

				$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['CRITICITY_NAME'] == 'Stopper')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROJO
					            'color' => array('rgb' => 'FF0000')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Crítica')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROSADO
					            'color' => array('rgb' => 'FF8080')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Alta')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ANARANJADO
					            'color' => array('rgb' => 'FF9900')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Media')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Baja')
				{
					$obj->getActiveSheet()->getStyle('H'.$fila, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO TENUE
					            'color' => array('rgb' => 'FFFF99')
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$fila, $value['PROOF_CYCLE_DETECTION']);

				$obj->getActiveSheet()->getStyle('I'.$fila, $value['PROOF_CYCLE_DETECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$fila, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('J'.$fila, $value['DESARROLLADOR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$fila, $value['DETECTION_DATE']);

				$obj->getActiveSheet()->getStyle('K'.$fila, $value['DETECTION_DATE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['DATE_TESTING_CORRECTION']);

				$obj->getActiveSheet()->getStyle('L'.$fila, $value['DATE_TESTING_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$fila, $value['PROOF_CYCLE_CORRECTION']);

				$obj->getActiveSheet()->getStyle('M'.$fila, $value['PROOF_CYCLE_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['OBSERVATION']);

				$obj->getActiveSheet()->getStyle('N'.$fila, $value['OBSERVATION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$fila++;
				// $projectname = $value['PROJECT_NAME'];				
			}
		}
		else
		{
			$obj->getActiveSheet()->setCellValue('B'.$fila, 'No hay Registros que mostrar!');
		}

		//prepare download
		// $filename='Reporte_Incidencias'.date('d-m-y').'-'.$projectname.'('.rand(0, 100).').xlsx'; //just some random filename
		$filename='Reporte_Incidencias'.date('d-m-y').'-'.$name_project.'.xlsx'; //just some random filename
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		$objWriter->save(APPPATH."reportes/"."".$filename);  //send it to user, of course you can save it to disk also!
		return APPPATH."reportes/"."".$filename;
	}

	public function reporteriaDiariasend($proyecto)
	{
		$result = $this->ReporteModel->getReporteriaDiariaFecha();
		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$name_project = $value['NOMBRE_PROYECTO'];
			}
		}
		
		$file = APPPATH."files/Reporteria_Diaria.xlsx";
		$obj = PHPExcel_IOFactory::load($file);	
		//$obj = new PHPExcel();
		$obj->getActiveSheet()->setShowGridlines(false);
		$obj->setActiveSheetIndex(0);

		if ($result['Estado'])
		{
			$obj->getActiveSheet()->setCellValue('K4', 'Fecha:');

			$fila = 4;
			foreach ($result['Resultado'] as $key => $value)
			{				
				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['DIA']." ".$value['NUMERO_DIA']." de ".$value['MES']."de ".$value['AÑO']);

				$fila++;
			}
		}

		$result1 = $this->ReporteModel->getReporteriaDiaria($proyecto);

		if ($result1['Estado']) 
		{
			$obj->getActiveSheet()->setCellValue('B11', 'Proyecto');
			$obj->getActiveSheet()->setCellValue('C11', 'Módulo');
			$obj->getActiveSheet()->setCellValue('D11', 'Estatus');
			$obj->getActiveSheet()->setCellValue('E11', 'Ciclo de Pruebas');
			$obj->getActiveSheet()->setCellValue('F11', 'Casos de pruebas de matriz completa');
			$obj->getActiveSheet()->setCellValue('G11', 'Casos de Pruebas por Módulo');
			$obj->getActiveSheet()->setCellValue('H11', 'Ejecutados (testing)');
			$obj->getActiveSheet()->setCellValue('I11', 'Exitosos');
			$obj->getActiveSheet()->setCellValue('J11', 'Incidentes Pendientes');
			$obj->getActiveSheet()->setCellValue('K11', 'Pendientes de Ejecutar');
			$obj->getActiveSheet()->setCellValue('L11', 'Porcentaje completado de priorizados en fecha anterior');
			$obj->getActiveSheet()->setCellValue('M11', 'Porcentaje completado de priorizados de hoy');
			$obj->getActiveSheet()->setCellValue('N11', 'Porcentaje de avance diario');

			$fila = 12;
			foreach ($result1['Resultado'] as $key => $value)
			{
				for($col = 'A'; $col !== 'P'; $col++) {
					    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
					}

				$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$fila, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$fila, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$fila, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$fila, $value['STATUS_PROJECT']);

				$obj->getActiveSheet()->getStyle('D'.$fila, $value['STATUS_PROJECT'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$fila, $value['PROOF_CYCLE']);

				$obj->getActiveSheet()->getStyle('E'.$fila, $value['PROOF_CYCLE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$fila, $value['MATRIZ_COMPLETA']);

				$obj->getActiveSheet()->getStyle('F'.$fila, $value['MATRIZ_COMPLETA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$fila, $value['PRUEBAS_POR_MODULO']);

				$obj->getActiveSheet()->getStyle('G'.$fila, $value['PRUEBAS_POR_MODULO'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$fila, $value['EJECUTADOS']);

				$obj->getActiveSheet()->getStyle('H'.$fila, $value['EJECUTADOS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('I'.$fila, $value['EXITOSOS']);

				$obj->getActiveSheet()->getStyle('I'.$fila, $value['EXITOSOS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$fila, $value['INC_PENDIENTES']);

				$obj->getActiveSheet()->getStyle('J'.$fila, $value['INC_PENDIENTES'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				// Pendientes de Ejecutar
				$obj->getActiveSheet()->setCellValue('K'.$fila, $value['PENDIENTES_EJECUTAR']);

				$obj->getActiveSheet()->getStyle('K'.$fila, $value['PENDIENTES_EJECUTAR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$fila, $value['PORCENTAJEPREVIO'].'%');

				$obj->getActiveSheet()->getStyle('L'.$fila, $value['PORCENTAJEPREVIO'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				//Colores a los porcentajes de ayer
				if($value['PORCENTAJEPREVIO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //ROJO
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if(($value['PORCENTAJEPREVIO'] >= 51) && ($value['PORCENTAJEPREVIO'] <= 90))
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //VERDE
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('M'.$fila, $value['PORCENTAJEACUMULATIVO'].'%');

				$obj->getActiveSheet()->getStyle('M'.$fila, $value['PORCENTAJEACUMULATIVO'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['PORCENTAJEACUMULATIVO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['PORCENTAJEACUMULATIVO'] >= 51 && $value['PORCENTAJEACUMULATIVO'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['NUMERO_PORCENTAJE'].'%');

				$obj->getActiveSheet()->getStyle('N'.$fila, $value['NUMERO_PORCENTAJE'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['NUMERO_PORCENTAJE'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['NUMERO_PORCENTAJE'] >= 51 && $value['NUMERO_PORCENTAJE'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				/* CON VISTA VW_DAILY_REPORT
				if($value['PORCENTAJEPREVIO']==0)
				{
					$obj->getActiveSheet()->setCellValue('L'.$fila, $value['PORCENTAJEPREVIO'].'%');

					$obj->getActiveSheet()->getStyle('L'.$fila, $value['PORCENTAJEPREVIO'].'%')->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
				}
				elseif ($value['PORCENTAJEACTUAL'] == 0) {
					$obj->getActiveSheet()->setCellValue('L'.$fila, $value['PORCENTAJEPREVIO'].'%');

					$obj->getActiveSheet()->getStyle('L'.$fila, $value['PORCENTAJEPREVIO'].'%')->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->setCellValue('L'.$fila, $value['PORCENTAJEACTUAL'].'%');

					$obj->getActiveSheet()->getStyle('L'.$fila, $value['PORCENTAJEACTUAL'].'%')->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
				}

				//Colores a los porcentajes de ayer
				if($value['PORCENTAJEACTUAL'] <= 50 || $value['PORCENTAJEPREVIO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //ROJO
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if(($value['PORCENTAJEACTUAL'] >= 51 || $value['PORCENTAJEPREVIO'] >= 51) && ($value['PORCENTAJEACTUAL'] <= 90 || $value['PORCENTAJEPREVIO'] <= 90))
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('L'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            //VERDE
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('M'.$fila, $value['TOTAL'].'%');

				$obj->getActiveSheet()->getStyle('M'.$fila, $value['TOTAL'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['TOTAL'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['TOTAL'] >= 51 && $value['TOTAL'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('M'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('N'.$fila, $value['PORCENTAJE_DIARIO'].'%');

				$obj->getActiveSheet()->getStyle('N'.$fila, $value['PORCENTAJE_DIARIO'].'%')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['PORCENTAJE_DIARIO'] <= 50)
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FF0000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else if($value['PORCENTAJE_DIARIO'] >= 51 && $value['PORCENTAJE_DIARIO'] <= 90)
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFCC00'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}
				else
				{
					$obj->getActiveSheet()->getStyle('N'.$fila)->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => '008000'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);
				}*/

				$fila++;
			}

			$filaActual = $fila+1;
			$obj->getActiveSheet()->setCellValue('B'.$filaActual, '2.- Estatus de Incidencias:  (incidencias totales)');
		}
		else
		{
			$filas = 12;
			$obj->getActiveSheet()->setCellValue('B'.$filas, 'No hay Registros que mostrar!');
		}

		$result2 = $this->ReporteModel->getIncidenciasTotales($proyecto);

		if ($result2['Estado']) 
		{
			$filaActual2 = $filaActual + 2;
			$filaActual3 = $filaActual2 + 1;
			$obj->getActiveSheet()->setCellValue('B7', 'Reporte de avance diario  de  Proyecto:');

			$obj->getActiveSheet()->getStyle('B'.$filaActual2.':F'.$filaActual2)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        ),
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        ),
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B'.$filaActual2, 'Reportados');

			$obj->getActiveSheet()->setCellValue('C'.$filaActual2, 'Corregidos');

			$obj->getActiveSheet()->setCellValue('D'.$filaActual2, 'Incidentes Pendientes');

			$obj->getActiveSheet()->setCellValue('E'.$filaActual2, 'Cerrados por Limitantes Técnicas');

			$obj->getActiveSheet()->setCellValue('F'.$filaActual2, 'Casos En Pruebas');

			$fila = 7;
			foreach ($result2['Resultado'] as $key => $value)
			{
						
				$obj->getActiveSheet()->setCellValue('D'.'7', $value['PROJECT_NAME']);
				$obj->getActiveSheet()->setCellValue('B'.$filaActual3, $value['REPORTADOS']);

				$obj->getActiveSheet()->getStyle('B'.$filaActual3, $value['REPORTADOS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaActual3, $value['CORREGIDOS']);

				$obj->getActiveSheet()->getStyle('C'.$filaActual3, $value['CORREGIDOS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaActual3, $value['INCIDENTES_PENDIENTES']);

				$obj->getActiveSheet()->getStyle('D'.$filaActual3, $value['INCIDENTES_PENDIENTES'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaActual3, $value['LIMITANTE_TECNICA']);

				$obj->getActiveSheet()->getStyle('E'.$filaActual3, $value['LIMITANTE_TECNICA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaActual3, $value['CASOS_EN_PRUEBAS']);

				$obj->getActiveSheet()->getStyle('F'.$filaActual3, $value['CASOS_EN_PRUEBAS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$fila++;
			}

			$filaActual4 = $filaActual3+2;
			$obj->getActiveSheet()->setCellValue('B'.$filaActual4, '3.- Detalle de Incidencias Reportadas pendientes:');
		}
		elseif($filaActual4!=null)
		{
			$obj->getActiveSheet()->setCellValue('B'.$filaActual4, 'No hay Registros que mostrar!');
		}

		$result3 = $this->ReporteModel->getIncidencias($proyecto);
		// $obj->getActiveSheet()->setCellValue('A1', $result3['Estado']);

		/*if ($result3['Estado'])
		{
			$obj->getActiveSheet()->setCellValue('A1', 'SI TRAE');
		}else{
			$obj->getActiveSheet()->setCellValue('B1', 'NO TRAE');
		}	*/

		$filaActual5 = $filaActual4 + 2;
			$filaActual6 = $filaActual5 + 1;
			$obj->getActiveSheet()->getStyle('B'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B'.$filaActual5, 'Nombre Proyecto');

			$obj->getActiveSheet()->getStyle('C'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C'.$filaActual5, 'Módulo');

			$obj->getActiveSheet()->getStyle('D'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D'.$filaActual5, 'Nro Incidencia');

			$obj->getActiveSheet()->getStyle('E'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E'.$filaActual5, 'Descripción');

			$obj->getActiveSheet()->getStyle('F'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F'.$filaActual5, 'Tipo de incidencia');

			$obj->getActiveSheet()->getStyle('G'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G'.$filaActual5, 'Estatus de incidencia');

			$obj->getActiveSheet()->getStyle('H'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H'.$filaActual5, 'Criticidad');

			$obj->getActiveSheet()->getStyle('I'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('I'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('I'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I'.$filaActual5, 'Ciclo de detección');

			$obj->getActiveSheet()->getStyle('J'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('J'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('J'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J'.$filaActual5, 'Responsable');

			$obj->getActiveSheet()->getStyle('K'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('K'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('K'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K'.$filaActual5, 'Fecha de detección');

			$obj->getActiveSheet()->getStyle('L'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('L'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('L'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L'.$filaActual5, 'Fecha de corrección');

			$obj->getActiveSheet()->getStyle('M'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('M'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('M'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M'.$filaActual5, 'Ciclo de corrección esperado');

			$obj->getActiveSheet()->getStyle('N'.$filaActual5)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('N'.$filaActual5)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('N'.$filaActual5)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N'.$filaActual5, 'Observaciones');

		if ($result3['Estado'])
		{
			// $obj->getActiveSheet()->getColumnDimension('B')->setWidth(20);

			// $fila = 12;
			foreach ($result3['Resultado'] as $key => $value)
			{

				$obj->getActiveSheet()->setCellValue('B'.$filaActual6, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$filaActual6, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaActual6, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$filaActual6, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaActual6, $value['INCIDENCE_NUMBER']);

				$obj->getActiveSheet()->getStyle('D'.$filaActual6, $value['INCIDENCE_NUMBER'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaActual6, $value['PROOF_NAME']);

				$obj->getActiveSheet()->getStyle('E'.$filaActual6, $value['PROOF_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaActual6, $value['TIPO_INCIDENCIA']);

				$obj->getActiveSheet()->getStyle('F'.$filaActual6, $value['TIPO_INCIDENCIA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$filaActual6, $value['INCIDENCE_STATUS']);

				$obj->getActiveSheet()->getStyle('G'.$filaActual6, $value['INCIDENCE_STATUS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$filaActual6, $value['CRITICITY_NAME']);

				$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['CRITICITY_NAME'] == 'Stopper')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROJO
					            'color' => array('rgb' => 'FF0000')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Crítica')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROSADO
					            'color' => array('rgb' => 'FF8080')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Alta')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ANARANJADO
					            'color' => array('rgb' => 'FF9900')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Media')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Baja')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual6, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO TENUE
					            'color' => array('rgb' => 'FFFF99')
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$filaActual6, $value['PROOF_CYCLE_DETECTION']);

				$obj->getActiveSheet()->getStyle('I'.$filaActual6, $value['PROOF_CYCLE_DETECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$filaActual6, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('J'.$filaActual6, $value['DESARROLLADOR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$filaActual6, $value['DETECTION_DATE']);

				$obj->getActiveSheet()->getStyle('K'.$filaActual6, $value['DETECTION_DATE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$filaActual6, $value['DATE_TESTING_CORRECTION']);

				$obj->getActiveSheet()->getStyle('L'.$filaActual6, $value['DATE_TESTING_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$filaActual6, $value['PROOF_CYCLE_CORRECTION']);

				$obj->getActiveSheet()->getStyle('M'.$filaActual6, $value['PROOF_CYCLE_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('N'.$filaActual6, $value['OBSERVATION']);

				$obj->getActiveSheet()->getStyle('N'.$filaActual6, $value['OBSERVATION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$filaActual6++;
			}

			$filaActual7 = $filaActual6+2;
			$obj->getActiveSheet()->setCellValue('B'.$filaActual7, '4.- Detalle de Incidencias Corregidas/Cerradas Por Limitante Técnica:');
		}
		else//if($filaActual7!=null)
		{
			$obj->getActiveSheet()->setCellValue('B'.$filaActual6, 'No hay Registros que mostrar!');

			$filaActual7 = $filaActual6+2;
			$obj->getActiveSheet()->setCellValue('B'.$filaActual7, '4.- Detalle de Incidencias Corregidas/Cerradas Por Limitante Técnica:');
		}

		$result4 = $this->ReporteModel->getIncidenciasCorregidas($proyecto);

		$filaActual8 = $filaActual7 + 2;
			$obj->getActiveSheet()->getStyle('B'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B'.$filaActual8, 'Nombre Proyecto');

			$obj->getActiveSheet()->getStyle('C'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C'.$filaActual8, 'Módulo');

			$obj->getActiveSheet()->getStyle('D'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D'.$filaActual8, 'Nro Incidencia');

			$obj->getActiveSheet()->getStyle('E'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E'.$filaActual8, 'Descripción');

			$obj->getActiveSheet()->getStyle('F'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F'.$filaActual8, 'Tipo de incidencia');

			$obj->getActiveSheet()->getStyle('G'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G'.$filaActual8, 'Estatus de incidencia');

			$obj->getActiveSheet()->getStyle('H'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H'.$filaActual8, 'Criticidad');

			$obj->getActiveSheet()->getStyle('I'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('I'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('I'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I'.$filaActual8, 'Ciclo de detección');

			$obj->getActiveSheet()->getStyle('J'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('J'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('J'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J'.$filaActual8, 'Responsable');

			$obj->getActiveSheet()->getStyle('K'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('K'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('K'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K'.$filaActual8, 'Fecha de detección');

			$obj->getActiveSheet()->getStyle('L'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('L'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('L'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L'.$filaActual8, 'Fecha de corrección');

			$obj->getActiveSheet()->getStyle('M'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('M'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('M'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M'.$filaActual8, 'Ciclo de corrección');

			$obj->getActiveSheet()->getStyle('N'.$filaActual8)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('N'.$filaActual8)->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('N'.$filaActual8)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N'.$filaActual8, 'Observaciones');

		$filaActual9 = $filaActual8 + 1;
		if ($result4['Estado'])
		{
			// $obj->getActiveSheet()->getColumnDimension('B')->setWidth(20);

			// $fila = 12;
			foreach ($result4['Resultado'] as $key => $value)
			{

				$obj->getActiveSheet()->setCellValue('B'.$filaActual9, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$filaActual9, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaActual9, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$filaActual9, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaActual9, $value['INCIDENCE_NUMBER']);

				$obj->getActiveSheet()->getStyle('D'.$filaActual9, $value['INCIDENCE_NUMBER'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaActual9, $value['PROOF_NAME']);

				$obj->getActiveSheet()->getStyle('E'.$filaActual9, $value['PROOF_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaActual9, $value['TIPO_INCIDENCIA']);

				$obj->getActiveSheet()->getStyle('F'.$filaActual9, $value['TIPO_INCIDENCIA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$filaActual9, $value['INCIDENCE_STATUS']);

				$obj->getActiveSheet()->getStyle('G'.$filaActual9, $value['INCIDENCE_STATUS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$filaActual9, $value['CRITICITY_NAME']);

				$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['CRITICITY_NAME'] == 'Stopper')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROJO
					            'color' => array('rgb' => 'FF0000')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Crítica')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROSADO
					            'color' => array('rgb' => 'FF8080')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Alta')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ANARANJADO
					            'color' => array('rgb' => 'FF9900')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Media')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Baja')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaActual9, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO TENUE
					            'color' => array('rgb' => 'FFFF99')
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$filaActual9, $value['PROOF_CYCLE_DETECTION']);

				$obj->getActiveSheet()->getStyle('I'.$filaActual9, $value['PROOF_CYCLE_DETECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$filaActual9, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('J'.$filaActual9, $value['DESARROLLADOR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$filaActual9, $value['DETECTION_DATE']);

				$obj->getActiveSheet()->getStyle('K'.$filaActual9, $value['DETECTION_DATE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$filaActual9, $value['DATE_TESTING_CORRECTION']);

				$obj->getActiveSheet()->getStyle('L'.$filaActual9, $value['DATE_TESTING_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$filaActual9, $value['PROOF_CYCLE_CORRECTION']);

				$obj->getActiveSheet()->getStyle('M'.$filaActual9, $value['PROOF_CYCLE_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('N'.$filaActual9, $value['OBSERVATION']);

				$obj->getActiveSheet()->getStyle('N'.$filaActual9, $value['OBSERVATION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$filaActual9++;
				// $projectname = $value['PROJECT_NAME'];
			}
		}
		else
		{
			$obj->getActiveSheet()->setCellValue('B'.$filaActual9, 'No hay Registros que mostrar!');
		}

		//prepare download
		// $filename='Reporteria_Diaria'.date('d-m-y').'-'.$projectname.'('.rand(0, 100).').xlsx'; //just some random filename
		$filename='Reporteria_Diaria'.date('d-m-y').'-'.$name_project.'.xlsx'; //just some random filename
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		$objWriter->save(APPPATH."reportes/"."".$filename);  //send it to user, of course you can save it to disk also!
		return APPPATH."reportes/"."".$filename;
	}

	public function matrizCompletasend($proyecto)
	{
		$file = APPPATH."files/Reporte_Matriz_Casos_Pruebas.xlsx";
		$obj = PHPExcel_IOFactory::load($file);	
		//$obj = new PHPExcel();
		$obj->getActiveSheet()->setShowGridlines(false);

		$result0 = $this->ReporteModel->proyectosResumen($proyecto);
		$project = $this->ReporteModel->getProyectoCorreo($proyecto);

		if ($project['Estado'])
		{
			foreach ($project['Resultado'] as $key => $value)
			{
				$name_project = $value['NOMBRE_PROYECTO'];
			}
		}

		if($result0['Estado'])
		{
			$obj->setActiveSheetIndex(0);
			$obj->getActiveSheet(0)->setShowGridlines(false);

			$obj->getActiveSheet()->setCellValue('B7', 'Actualizado Por:');

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B7')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B8', 'Fecha:');

			$obj->getActiveSheet()->getStyle('B8')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B8')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B8')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B9', 'Cantidad de Casos de Prueba:');

			$obj->getActiveSheet()->getStyle('B9')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B9')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B9')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B10', 'Módulo');

			$obj->getActiveSheet()->getStyle('B10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('B10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C10', 'Ciclo de Prueba');

			$obj->getActiveSheet()->getStyle('C10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('C10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D10', 'Fecha de Inicio');

			$obj->getActiveSheet()->getStyle('D10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('D10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E10', 'Fecha Final');

			$obj->getActiveSheet()->getStyle('E10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('E10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F10', 'Analista');

			$obj->getActiveSheet()->getStyle('F10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('F10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G10', 'Casos Ejecutados');

			$obj->getActiveSheet()->getStyle('G10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('G10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H10', 'Observaciones');

			$obj->getActiveSheet()->getStyle('H10')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H10')->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			            	'style' => PHPExcel_Style_Border::BORDER_THIN,
			            	'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);

			$obj->getActiveSheet()->getStyle('H10')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			foreach ($result0['Resultado'] as $key => $value) 
			{
				$obj->getActiveSheet()->setCellValue('B4', $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B4')->applyFromArray(
				    array(
				        'font' => array(
				            'bold' => true,
				            // 'color' => array('rgb' => 'FFFFFF'),
				            'size' => 15,
				            'name' => 'Calibri'
				        )
				    )
				);

				$session = $this->session->userdata('logged_in');
				$obj->getActiveSheet()->setCellValue('C7', $session['fname']);
				$obj->setActiveSheetIndex()->mergeCells('C7:H7');

				$obj->getActiveSheet()->getStyle('C7:H7')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        ),
				        'alignment' => array(
				            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C8', $value['PROJECT_DATE_CREATION']);
				$obj->setActiveSheetIndex()->mergeCells('C8:H8');

				$obj->getActiveSheet()->getStyle('C8:H8')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        ),
				        'alignment' => array(
				            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C9', $value['CANTIDADCASOSPROYECTOS']);
				$obj->setActiveSheetIndex()->mergeCells('C9:H9');

				$obj->getActiveSheet()->getStyle('C9:H9')->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        ),
				        'alignment' => array(
				            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				        )
				    )
				);
			}
		}

		$resultModulos = $this->ReporteModel->modulosResumen($proyecto);
		$filaModulo = 11;

		if($resultModulos['Estado'])
		{
			foreach ($resultModulos['Resultado'] as $key => $value) 
			{
				for($col = 'A'; $col !== 'J'; $col++) {
				    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

				$obj->getActiveSheet()->setCellValue('B'.$filaModulo, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaModulo, $value['CICLOPRUEBA']);

				$obj->getActiveSheet()->getStyle('C'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaModulo, $value['INITIAL_REAL_DATE']);

				$obj->getActiveSheet()->getStyle('D'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaModulo, $value['FINAL_REAL_DATE']);

				$obj->getActiveSheet()->getStyle('E'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaModulo, $session['fname']);

				$obj->getActiveSheet()->getStyle('F'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$filaModulo, $value['CANTIDADCASOSMODULOS']);

				$obj->getActiveSheet()->getStyle('G'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$filaModulo, $value['OBSERVACION']);

				$obj->getActiveSheet()->getStyle('H'.$filaModulo)->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);				

				$filaModulo++;
			}
		}


		$obj->setActiveSheetIndex(1);

		for($col = 'A'; $col !== 'P'; $col++) {
				    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

		$obj->getActiveSheet()->setCellValue('B2', 'Volver a Casos de Prueba');

			$obj->getActiveSheet()->getStyle('B2')->applyFromArray(
			    array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    )
			);

			$obj->getActiveSheet()->getCell('B2')->getHyperlink()->setUrl("sheet://'CASOS DE PRUEBAS'!A1");

			$obj->getActiveSheet()->getStyle('B5:N5')->applyFromArray(
			    array(
			        'font' => array(
			            'bold' => true,
			            'color' => array('rgb' => 'FFFFFF'),
			            'size' => 11,
			            'name' => 'Calibri'
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('B5', 'Nombre del Proyecto');

			$obj->getActiveSheet()->getStyle('B5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C5', 'Módulos');

			$obj->getActiveSheet()->getStyle('C5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D5', 'Nro Incidencia');

			$obj->getActiveSheet()->getStyle('D5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E5', 'Descripción');

			$obj->getActiveSheet()->getStyle('E5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F5', 'Tipo de Incidencia');

			$obj->getActiveSheet()->getStyle('F5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('G5', 'Estatus de Incidencia');

			$obj->getActiveSheet()->getStyle('G5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('H5', 'Criticidad');

			$obj->getActiveSheet()->getStyle('H5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('I5', 'Ciclo de Detección');

			$obj->getActiveSheet()->getStyle('I5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('J5', 'Responsable');

			$obj->getActiveSheet()->getStyle('J5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('K5', 'Fecha de Detección');

			$obj->getActiveSheet()->getStyle('K5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('L5', 'Fecha de Corrección');

			$obj->getActiveSheet()->getStyle('L5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('M5', 'Ciclo de Corrección');

			$obj->getActiveSheet()->getStyle('M5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('N5', 'Observaciones');

			$obj->getActiveSheet()->getStyle('N5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

		$resultIncidencias = $this->ReporteModel->getIncidenciasGlobales($proyecto);

		$filaIncidencias=6;

		if ($resultIncidencias['Estado']) 
		{
			// $obj->setActiveSheetIndex(1);

			foreach ($resultIncidencias['Resultado'] as $key => $value) 
			{				
				for($col = 'A'; $col !== 'P'; $col++) {
				    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

				$obj->getActiveSheet()->setCellValue('B'.$filaIncidencias, $value['PROJECT_NAME']);

				$obj->getActiveSheet()->getStyle('B'.$filaIncidencias, $value['PROJECT_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('C'.$filaIncidencias, $value['PROJECT_MODULE_NAME']);

				$obj->getActiveSheet()->getStyle('C'.$filaIncidencias, $value['PROJECT_MODULE_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('D'.$filaIncidencias, $value['INCIDENCE_NUMBER']);

				$obj->getActiveSheet()->getStyle('D'.$filaIncidencias, $value['INCIDENCE_NUMBER'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('E'.$filaIncidencias, $value['PROOF_NAME']);

				$obj->getActiveSheet()->getStyle('E'.$filaIncidencias, $value['PROOF_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('F'.$filaIncidencias, $value['TIPO_INCIDENCIA']);

				$obj->getActiveSheet()->getStyle('F'.$filaIncidencias, $value['TIPO_INCIDENCIA'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('G'.$filaIncidencias, $value['INCIDENCE_STATUS']);

				$obj->getActiveSheet()->getStyle('G'.$filaIncidencias, $value['INCIDENCE_STATUS'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('H'.$filaIncidencias, $value['CRITICITY_NAME']);

				$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				if($value['CRITICITY_NAME'] == 'Stopper')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROJO
					            'color' => array('rgb' => 'FF0000')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Crítica')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ROSADO
					            'color' => array('rgb' => 'FF8080')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Alta')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //ANARANJADO
					            'color' => array('rgb' => 'FF9900')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Media')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO
					            'color' => array('rgb' => 'FFCC00')
					        )
					    )
					);
				}
				else if($value['CRITICITY_NAME'] == 'Baja')
				{
					$obj->getActiveSheet()->getStyle('H'.$filaIncidencias, $value['CRITICITY_NAME'])->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //AMARILLO TENUE
					            'color' => array('rgb' => 'FFFF99')
					        )
					    )
					);
				}

				$obj->getActiveSheet()->setCellValue('I'.$filaIncidencias, $value['PROOF_CYCLE_DETECTION']);

				$obj->getActiveSheet()->getStyle('I'.$filaIncidencias, $value['PROOF_CYCLE_DETECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('J'.$filaIncidencias, $value['DESARROLLADOR']);

				$obj->getActiveSheet()->getStyle('J'.$filaIncidencias, $value['DESARROLLADOR'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('K'.$filaIncidencias, $value['DETECTION_DATE']);

				$obj->getActiveSheet()->getStyle('K'.$filaIncidencias, $value['DETECTION_DATE'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('L'.$filaIncidencias, $value['DATE_TESTING_CORRECTION']);

				$obj->getActiveSheet()->getStyle('L'.$filaIncidencias, $value['DATE_TESTING_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('M'.$filaIncidencias, $value['PROOF_CYCLE_CORRECTION']);

				$obj->getActiveSheet()->getStyle('M'.$filaIncidencias, $value['PROOF_CYCLE_CORRECTION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$obj->getActiveSheet()->setCellValue('N'.$filaIncidencias, $value['OBSERVATION']);

				$obj->getActiveSheet()->getStyle('N'.$filaIncidencias, $value['OBSERVATION'])->applyFromArray(
				    array(
				        'borders' => array(
				            'allborders' => array(
				            	'style' => PHPExcel_Style_Border::BORDER_THIN,
				            	'color' => array('rgb' => '000000')
				            )
				        )
				    )
				);

				$filaIncidencias++;
				// $projectname = $value['PROJECT_NAME'];
			}
		}
		else
		{
			$obj->getActiveSheet()->setCellValue('B'.$filaIncidencias, 'No hay Registros que mostrar!');
		}

		$result = $this->ReporteModel->getMatriz($proyecto);
		
		$fila = 6;

		// for($col = 'A'; $col !== 'H'; $col++) {
		//     $obj->getActiveSheet()->getColumnDimension()->setAutoSize(true);
		// }

		if ($result['Estado']) 
		{
			$obj->setActiveSheetIndex(2);
			$obj->getActiveSheet(2)->setShowGridlines(false);

			$obj->getActiveSheet()->setCellValue('B5', 'Casos de Prueba Aplicados');

			$obj->getActiveSheet()->setCellValue('B2', 'Volver a Resumen');
			$obj->getActiveSheet()->setCellValue('F2', 'Volver a Incidentes');

			$obj->getActiveSheet()->getStyle('B2:F2')->applyFromArray(
			    array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    )
			);

			$obj->getActiveSheet()->getCell('B2')->getHyperlink()->setUrl("sheet://'RESUMEN'!A1");
			$obj->getActiveSheet()->getCell('F2')->getHyperlink()->setUrl("sheet://'INCIDENTES'!A1");

			$obj->getActiveSheet()->getStyle('B5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('C5', 'Tipo de Caso');

			$obj->getActiveSheet()->getStyle('C5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('D5', 'Resultados');

			$obj->getActiveSheet()->getStyle('D5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('E5', 'Evidencias');

			$obj->getActiveSheet()->getStyle('E5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			$obj->getActiveSheet()->setCellValue('F5', 'Observaciones');

			$obj->getActiveSheet()->getStyle('F5')->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => array('rgb' => '003366')
			        )
			    )
			);

			foreach ($result['Resultado'] as $key => $value) 
			{
				for($col = 'A'; $col !== 'H'; $col++) {
				    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
				}

				if($value['TIPO'] == '0')
				{
					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROJECT_MODULE_NAME']);
					
					$obj->getActiveSheet()->getStyle('B'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('B'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('C'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
					
					$obj->getActiveSheet()->getStyle('C'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
					
					$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('E'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
					
					$obj->getActiveSheet()->getStyle('E'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('F'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);
					
					$obj->getActiveSheet()->getStyle('F'.$fila)->applyFromArray(
					    array(
					    	//BACKGROUND
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 5TA FILA)
					            'color' => array('rgb' => '5499C7')
					        )
					    )
					);

					$fila++;
				}//if
				else
				{

					$obj->getActiveSheet()->setCellValue('B'.$fila, $value['PROOF_NAME']);

					$obj->getActiveSheet()->getStyle('B'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('C'.$fila, $value['TIPO_DE_CASO']);

					$obj->getActiveSheet()->getStyle('C'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('D'.$fila, $value['RESULTADO']);

					$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					if($value['RESULTADO'] == 'Con Incidentes')//ROJO
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (1ERA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '7B241C'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (2DA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'FADBD8')
						        )
						    )
						);
					}
					elseif($value['RESULTADO'] == 'Satisfactorio')//VERDE
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (9NA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '196F3D'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (10MA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'D5F5E3')
						        )
						    )
						);
					}
					elseif($value['RESULTADO'] == 'Con Observaciones')//AMARILLO
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (11VA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '9A7D0A'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (11VA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'FCF3CF')
						        )
						    )
						);
					}
					elseif($value['RESULTADO'] == 'Pendiente de Probar')//AZUL
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '1A5276'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (5TA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'D4E6F1')
						        )
						    )
						);
					}
					elseif($value['RESULTADO'] == 'No Aplica')//GRIS
					{
						$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
						    array(
						    	//TEXTO
						        'font' => array(
						            'bold' => false,
						            //https://htmlcolorcodes.com/es/ (16TA COLUMNA, PENULTIMO)
						            'color' => array('rgb' => '5F6A6A'),
						            'size' => 11,
						            'name' => 'Calibri'
						        )
						    )
						);

						$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
						    array(
						    	//BACKGROUND
						        'fill' => array(
						            'type' => PHPExcel_Style_Fill::FILL_SOLID,
						            //https://htmlcolorcodes.com/es/ (19NA COLUMNA, 2DA FILA)
						            'color' => array('rgb' => 'E5E8E8')
						        )
						    )
						);
					}
					// elseif($value['RESULTADO'] == 'Pendiente de Probar')//ANARANJADO
					// {
					// 	$obj->getActiveSheet()->getStyle('D'.$fila)->applyFromArray(
					// 	    array(
					// 	    	//TEXTO
					// 	        'font' => array(
					// 	            'bold' => false,
					// 	            //https://htmlcolorcodes.com/es/ (13VA COLUMNA, PENULTIMO)
					// 	            'color' => array('rgb' => '873600'),
					// 	            'size' => 11,
					// 	            'name' => 'Calibri'
					// 	        )
					// 	    )
					// 	);

					// 	$obj->getActiveSheet()->getStyle('D'.$fila, $value['RESULTADO'])->applyFromArray(
					// 	    array(
					// 	    	//BACKGROUND
					// 	        'fill' => array(
					// 	            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					// 	            //https://htmlcolorcodes.com/es/ (12VA COLUMNA, 2DA FILA)
					// 	            'color' => array('rgb' => 'FDEBD0')
					// 	        )
					// 	    )
					// 	);
					// }

					$obj->getActiveSheet()->setCellValue('E'.$fila, $value['EVIDENCIAS']);

					$obj->getActiveSheet()->getStyle('E'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->getCell('E'.$fila)->getHyperlink()->setUrl("sheet://'".$value['EVIDENCIAS']."'!A1");

					$obj->getActiveSheet()->setCellValue('F'.$fila, $value['OBSERVATION']);

					$obj->getActiveSheet()->getStyle('F'.$fila)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$fila++;

				}//else

			}//foreach

			$result1 = $this->ReporteModel->getEvidenciasPruebasSinNulos($proyecto);

			$index=3;
		}//ifresult
		// else
		// {
		// 	$obj->getActiveSheet()->setCellValue('B'.$fila, 'No hay Registros que mostrar!');
		// }

		// $obj->setActiveSheetIndex(1);
		$result = $this->ReporteModel->getDataCasos($proyecto);
		$filaHoja2 = 5;
		$filaHoja3 = 7;

		$result2 = $this->ReporteModel->getEvidenciasPruebas($proyecto);
		$result3 = $this->ReporteModel->getVideosPruebas($proyecto);

		$indexVideo = 3;
		// $filaHoja4 = 26;

		foreach ($result1['Resultado'] as $key => $value1) 
		{
			foreach ($result['Resultado'] as $key => $value) 
			{
				if($value1['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
				{
					$filaHoja3 = 7;
					$obj->createSheet();
					$obj->setActiveSheetIndex($index);
					$obj->getActiveSheet()->setShowGridlines(false);

					$obj->getActiveSheet()->setTitle($value1['EVIDENCIAS']);
					$index++;
				

					for($col = 'A'; $col !== 'H'; $col++) {
					    $obj->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
					}

					$obj->getActiveSheet()->setCellValue('B2', 'Volver a Resumen');
					$obj->getActiveSheet()->setCellValue('C2', 'Volver a Casos de Prueba');
					$obj->getActiveSheet()->setCellValue('D2', 'Volver a Incidentes');

					$obj->getActiveSheet()->getStyle('B2:D2')->applyFromArray(
					    array(
					        'alignment' => array(
					            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					        )
					    )
					);

					$obj->getActiveSheet()->getCell('B2')->getHyperlink()->setUrl("sheet://'RESUMEN'!A1");
					$obj->getActiveSheet()->getCell('C2')->getHyperlink()->setUrl("sheet://'CASOS DE PRUEBAS'!A1");
					$obj->getActiveSheet()->getCell('D2')->getHyperlink()->setUrl("sheet://'INCIDENTES'!A1");

					$obj->getActiveSheet()->setCellValue('B4', 'Descripción');

					$obj->getActiveSheet()->getStyle('B4')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => '003366')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('B4')->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFFFFF'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('C4', 'Resultado Esperado');

					$obj->getActiveSheet()->getStyle('C4')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => '003366')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('C4')->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFFFFF'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('D4', 'Resultado Obtenido');

					$obj->getActiveSheet()->getStyle('D4')->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => '003366')
					        )
					    )
					);

					$obj->getActiveSheet()->getStyle('D4')->applyFromArray(
					    array(
					        'font' => array(
					            'bold' => true,
					            'color' => array('rgb' => 'FFFFFF'),
					            'size' => 11,
					            'name' => 'Calibri'
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('B'.$filaHoja2, $value['PROOF_NAME']);

					$obj->getActiveSheet()->getStyle('B'.$filaHoja2)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('C'.$filaHoja2, $value['EXPECTED_RESULT']);

					$obj->getActiveSheet()->getStyle('C'.$filaHoja2)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					$obj->getActiveSheet()->setCellValue('D'.$filaHoja2, $value['OBTAINED_RESULT']);

					$obj->getActiveSheet()->getStyle('D'.$filaHoja2)->applyFromArray(
					    array(
					        'borders' => array(
					            'allborders' => array(
					            	'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	'color' => array('rgb' => '000000')
					            )
					        )
					    )
					);

					/*foreach ($result2['Resultado'] as $key => $value2) 
					{
						if($value2['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
						{
							if($value2['EVIDENCES'] != NULL)
							{	
								$laImagen = imagecreatefromstring($value2['EVIDENCES']->load());
								$enX = imagesx($laImagen);
								$enY = imagesy($laImagen);
								
								$dimensiones = imagecreatetruecolor($enX, $enY);
								imagecopyresampled($dimensiones, $laImagen, 0, 0, 0, 0, $enX, $enY, $enX, $enY);
								$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
								$objDrawing->setImageResource($dimensiones);
								$objDrawing->setWorksheet($obj->getActiveSheet());
								$objDrawing->setCoordinates('B'.$filaHoja3);
								
								$calculo = round($enY/20);
								$filaHoja3+=$calculo+2;
							}
						}
					}*/

					$filaaas = 1;
					// $ultimaColumna = $obj->setActiveSheetIndex()->getHighestColumn();
					// $indexVideo = $index-2;
					// $indexVideo = 3;

					// $row = 1; 
					 // $lastColumn = 'E';
					// $lastColumn++; 
					/*for ($column = 'B'; $column != $lastColumn; $column++) {
						$columna = $column;
						// $obj->setActiveSheetIndex()->setCellValue($column.$filaaas, $column);

					    // $cell = $worksheet->getCell($column.$row); 
					    // Do what you want with the cell 
					}*/

					foreach ($result3['Resultado'] as $key => $value3) 
					{
						// $obj->setActiveSheetIndex($index)->setCellValue('A'.$filaaas, $value['ID_PROOF_CASE']);
						// $column = 'B';

						/*for ($n=0; $n<6; $n++) {
						    if($value3['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
							{
						    		++$column;
							}
						}*/
						
								if($value3['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
								{
									if($value3['URL'] != 'NULL')
									{
										$obj->setActiveSheetIndex($index-1)->setCellValue('B'.$filaHoja3, "VIDEO".$filaaas);
										$obj->getActiveSheet()->getCell('B'.$filaHoja3)->getHyperlink()->setUrl(base_url().$value3['URL']);
										$indexVideo+=1;
										// $column++;
										$filaHoja3+=1;
										$filaaas+=1;

									}
								}
							/*for ($column = 'B'; $column != $ultimaColumna; $column++) {
								if($value3['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
								{
									// $obj->getProperties()->setCreator($value3['URL']);
								}
						}*/
					}

					$filaImagen = $filaHoja3+1;

					foreach ($result2['Resultado'] as $key => $value2) 
					{
						if($value2['ID_PROOF_CASE'] == $value['ID_PROOF_CASE'])
						{
							if($value2['EVIDENCES'] != NULL)
							{	
								$laImagen = imagecreatefromstring($value2['EVIDENCES']->load());
								$enX = imagesx($laImagen);
								$enY = imagesy($laImagen);
								
								$dimensiones = imagecreatetruecolor($enX, $enY);
								imagecopyresampled($dimensiones, $laImagen, 0, 0, 0, 0, $enX, $enY, $enX, $enY);
								$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
								$objDrawing->setImageResource($dimensiones);
								$objDrawing->setWorksheet($obj->getActiveSheet());
								$objDrawing->setCoordinates('B'.$filaImagen);
								
								$calculo = round($enY/20);
								$filaImagen+=$calculo+2;
							}
						}
					}
					
				}
			}
		}

		// else
		// {
		// 	$obj->getActiveSheet()->setCellValue('B'.$fila, 'No hay Registros que mostrar!');
		// }

		//Eliminar la última hoja que genera automaticamente
		if($obj->getSheetByName('Worksheet'))
		{	
			$obj->setActiveSheetIndexByName('Worksheet');
			$sheetIndex = $obj->getActiveSheetIndex();
			$obj->removeSheetByIndex($sheetIndex);
		}
		elseif ($obj->getSheetByName('Worksheet 1')) {
			$obj->setActiveSheetIndexByName('Worksheet 1');
			$sheetIndex = $obj->getActiveSheetIndex();
			$obj->removeSheetByIndex($sheetIndex);
		}

		$obj->setActiveSheetIndex(0);

		//prepare download
		// $filename='Reporte_Matriz_Casos_Pruebas'.date('d-m-y').'-'.$projectname.'('.rand(0, 100).').xlsx'; //just some random filename
		$filename='Reporte_Matriz_Casos_Pruebas'.date('d-m-y').'-'.$name_project.'.xlsx'; //just some random filename
		$objWriter = PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
		$objWriter->save(APPPATH."reportes/"."".$filename);  //send it to user, of course you can save it to disk also!
		return APPPATH."reportes/"."".$filename;
	}
}?>