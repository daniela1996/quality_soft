<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Usuarios extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('user');
		
	}
	function index() {
		if($session = $this->session->userdata('logged_in')) {
			if ($this->user->registro('/'.$this->router->fetch_class(),$session['uniqueid'])) {
				$data['result'] = $this->user->getUser();
				$data['jsfile'] = "Usuarios.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('usuarios_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function userParticularGet() {
		header('Content-type: application/json');
		$array = $this->input->post('usuarioName');
		$decoded = json_decode($array,true);
		$item = $this->user->getUsuarioParticular($decoded['IDUSUARIO']);
		$item["PHOTO"] = (is_null($item["PHOTO"])) ? false : base64_encode($item["PHOTO"]->load());
		$this->output->set_output(json_encode($item));
	}
	public function confirmarUsuario() {
		// header('Content-type: application/json');
		// $array = $this->input->post('confirmar');
		// $decoded = json_decode($array,true);
		// $result = $this->user->getUserByEmailAndPassword($decoded['user'], $decoded['pass']);
		// $session = $this->session->userdata('logged_in');
		// if ($result) {
		// 	if ($session['email'] === $decoded['user']) {
		// 		$this->output->set_output(json_encode(array("Solicitud Aceptada", true)));
		// 	} else {
		// 		$this->output->set_output(json_encode(array("El usuario ingresado no corresponde a la sesión en la plataforma.", false)));
		// 	}
		// } else {
		// 	$this->output->set_output(json_encode(array("El usuario o la contraseña no coinciden.", false)));
		// }
	}
	public function usuarioPorNombre() {
		header('Content-type: application/json');
		$array = $this->input->post('usuarioName');
		$decoded = json_decode($array,true);
		$item = $this->user->getPorNombre($decoded['NAME']);
		$this->output->set_output(json_encode($item));
	}
	public function usuariosInsert() {
		header('Content-type: application/json');
		$array = $this->input->post('usuarioName');
		$session = $this->session->userdata('logged_in');
		$user = intval($session['userid']);	
		$decoded = json_decode($array,true);
		$rows = array('NAME' => $decoded['NOMBRE_USUARIO'],
									'EMAIL' => $decoded['EMAIL_USUARIO'],
									'PHONE' => $decoded['CELULAR_USUARIO'],
									'NICK' => $decoded['NICKNAME_USUARIO'],
									'PHOTO' => $decoded['FOTO_USUARIO'],
									'UNIQUE_ID' => uniqid('', true),
									'ENCRYPTED_PASSWORD' => $decoded['CLAVE_USUARIO'],
									'U_ID_UPDATE' => $user);
		$ultimoRegistro = $this->user->getUltimoUsuario();
		$item = $this->user->insertUser($rows);
		$nuevosRegistros = $this->user->getUltimoUsuario();
		$rowsRespuesta = $this->ultimasEntradas($ultimoRegistro, $nuevosRegistros);
		$this->output->set_output(json_encode(array($item, $rowsRespuesta, 1)));
	}
	public function usuariosUpdate() {
		header('Content-type: application/json');
		$array = $this->input->post('usuarioName');
		$session = $this->session->userdata('logged_in');
		$user = intval($session['userid']);	
		$decoded = json_decode($array,true);
		$rows = array('U_ID' => $decoded['ID_USUARIO'],
									'NAME' => $decoded['NOMBRE_USUARIO'],
									'EMAIL' => $decoded['EMAIL_USUARIO'],
									'PHONE' => $decoded['CELULAR_USUARIO'],
									'NICK' => $decoded['NICKNAME_USUARIO'],
									'PHOTO' => $decoded['FOTO_USUARIO'],
									'ENCRYPTED_PASSWORD' => $decoded['CLAVE_USUARIO'],
									'U_ID_UPDATE' => $user);
		$item = $this->user->updateUser($rows);
		/*if ($item) {
			$item = $item."".$this->user->insertCuadrillaUsers($decoded['ID_USUARIO'], $decoded['CUADRILLAS']);
		}*/
		$perfilUpdated = $this->user->getUsuarioParticular($decoded['ID_USUARIO']);
		$perfilUpdated["PHOTO"] = (is_null($perfilUpdated["PHOTO"])) ? false : base64_encode($perfilUpdated["PHOTO"]->load());
		$this->output->set_output(json_encode(array($item, $perfilUpdated, 2)));
	}
	private function ultimasEntradas($ultimoRegistro, $nuevosRegistros) {
		$diferenciaAgregados = (intval($nuevosRegistros['CONTEO']) - intval($ultimoRegistro['CONTEO']));
		if ($diferenciaAgregados > 0) {
			return $this->user->getUsuarioNuevo($diferenciaAgregados);
		} else {
			return false;
		}
	}
} ?>
