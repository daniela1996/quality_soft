<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mantenimientos extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Mantenimientosmodel');
		
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['jsfile'] = "Mantenimientos.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('mantenimientos_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}

	public function tablasGet(){
		header('Content-Type: application/json');
		$item = $this->Mantenimientosmodel->getTablas();
		echo json_encode($item);
	}

	public function mantenimientosGet(){
		header('Content-Type: application/json');
		$idtabla = $this->input->post('idtabla');
		$item = $this->Mantenimientosmodel->getMantenimientos($idtabla);
		echo json_encode($item);
	}

	public function MantenimientoTablaGet(){
		header('Content-Type: application/json');
		$id = $this->input->post('id');
		$item = $this->Mantenimientosmodel->getMantenimientosTablas($id);
		echo json_encode($item);
	}

	public function tablasInsert(){
		$codigo = $this->input->post("CODIGO_TABLA");
		//$idtabla = $this->input->post("ID_TABLA");
		$nombre = $this->input->post("NOMBRE");
		$descripcion = $this->input->post("DESCRIPCION");
		$data = array( //'ID_MAINTENANCE_TABLE' => $idtabla,
					'TABLE_CODE' => $codigo,
					'TABLE_NAME' => $nombre,
					'DESCRIPTION' => $descripcion);
		$item = $this->Mantenimientosmodel->insertTablas($data);
		$this->output->set_output(json_encode($item));
	}

	public function tablasUpdate(){
		$idtabla = $this->input->post("ID_TABLA");
		// $codigo = $this->input->post("CODIGO_TABLA");
		$nombre = $this->input->post("NOMBRE");
		$descripcion = $this->input->post("DESCRIPCION");
		$data = array(//'ID_MAINTENANCE_TABLE' => $idtabla,
					'TABLE_NAME' => $nombre,
					'DESCRIPTION' => $descripcion);
		$item = $this->Mantenimientosmodel->updateTablas($data, $idtabla);
		$this->output->set_output(json_encode($item));
	}

	public function mantenimientosInsert(){
		$idtabla = $this->input->post("ID_TABLA");
		$cod_mantenimiento = $this->input->post("COD_MANTENIMIENTO");
		$nombre = $this->input->post("NOMBRE");
		$descripcion = $this->input->post("DESCRIPCION");
		$valor = $this->input->post("VALOR");
		$data = array('MAINTENANCE_CODE' => $cod_mantenimiento,
					'MAINTENANCE_NAME' => $nombre,
					'ID_MAINTENANCE_TABLE' => $idtabla,
					'VALUE' => $valor,
					'DESCRIPTION' => $descripcion);
		$item = $this->Mantenimientosmodel->insertMantenimientos($data);
		$this->output->set_output(json_encode($item));
	}

	public function mantenimientosUpdate(){
		$idmantenimiento = $this->input->post("ID_MANTENIMIENTO");
		#$cod_mantenimiento = $this->input->post("COD_MANTENIMIENTO");
		$idtabla = $this->input->post("ID_TABLA");
		$nombre = $this->input->post("NOMBRE");
		$descripcion = $this->input->post("DESCRIPCION");
		$valor = $this->input->post("VALOR");
		$data = array('MAINTENANCE_NAME' => $nombre,
					#'COD_MANTENIMIENTO' => $cod_mantenimiento,
					'ID_MAINTENANCE_TABLE' => $idtabla,
					'VALUE' => $valor,
					'DESCRIPTION' => $descripcion);
		$item = $this->Mantenimientosmodel->updateMantenimientos($data, $idmantenimiento);
		$this->output->set_output(json_encode($item));
	}

	public function maintenanceDelete() {
		header('Content-type: application/json');
		$idmantenimiento = $this->input->post('idMantenimiento');
		$item = $this->Mantenimientosmodel->deleteMantenimiento($idmantenimiento);
		echo json_encode($item);
	}
}?>