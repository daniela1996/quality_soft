<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Perfilesmodulos extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model("perfilmodel");
		$this->load->model("modulomodel");
	}
	public function index() {
		$pm = $this->input->get('pm');
		$vl = $this->input->get('vl');
		$data['result'] = $this->modulomodel->getPerfilModulo();
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(),$session['uniqueid'])) {
				$data['jsfile'] = "Perfilesmodulos.js";
				$data['both'] = true;
				$data["idVal"] = (empty($pm)) ? 0: $vl;
				if (!((empty($pm))||(empty($vl)))) {
					if ($pm == 1) {
						$data['result'] = $this->perfilmodel->getPerfilModuloParticular($vl);
						$data['perfilModulo'] = 1;
					}
					if ($pm == 2) {
						$data['result'] = $this->modulomodel->getModuloPerfilParticular($vl);
						$data['perfilModulo'] = 2;
					}
					if(!empty($data['result'])) {
						if ($pm == 1) {
							$data['modulo'] = $this->modulomodel->getModuloIncompleto($vl);
						}
						if ($pm == 2) {
							$data['perfil'] = $this->perfilmodel->getPerfilIncompleto($vl);
						}
					} else {
						$data['perfil'] = $this->perfilmodel->getPerfil();
						$data['modulo'] = $this->modulomodel->getModulo();
					}
				} else {
					$data['both'] = false;
				}
				$this->load->view('template/header.php', $session);
				$this->load->view('perfiles_modulos_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function perfilmoduloInsert() {
		header('Content-type: application/json');
		$array = $this->input->post('perfilmoduloName');
		$decoded = json_decode($array,true);
		$rows = array();
		foreach($decoded['dataArray'] as $fields) {
			$rows[] = array('PROFILEID' => $fields['PROFILES_IDPROFILE'],
											'MODULEID' => $fields['MODULES_IDMODULES']);
		}
		$ultimoRegistro = (intval($decoded['PROCEDENCIA']) == 1) ? $this->modulomodel->getUltimoModuloPerfil($decoded['LLAVE']) : $this->perfilmodel->getUltimoPerfilModulo($decoded['LLAVE']);
		$item = $this->modulomodel->insertPerfilOModulo($rows);
		$nuevosRegistros = (intval($decoded['PROCEDENCIA']) == 1) ? $this->modulomodel->getUltimoModuloPerfil($decoded['LLAVE']): $this->perfilmodel->getUltimoPerfilModulo($decoded['LLAVE']);
		$rowsRespuesta = $this->ultimasEntradas($ultimoRegistro, $nuevosRegistros, intval($decoded['PROCEDENCIA']), intval($decoded['LLAVE']));
		$this->output->set_output(json_encode(array($item, $rowsRespuesta)));
	}
	public function perfilmoduloAcceptDecline() {
		header('Content-type: application/json');
		$array = $this->input->post('perfilmoduloName');
		$decoded = json_decode($array,true);
		$item = $this->modulomodel->acceptDecline($decoded['PROF_MODULEID'], $decoded['ESTADO']);
		$this->output->set_output(json_encode($item));
	}
	private function ultimasEntradas($ultimoRegistro, $nuevosRegistros, $procedencia, $llave) {
		$diferenciaAgregados = (intval($nuevosRegistros['CONTEO']) - intval($ultimoRegistro['CONTEO']));
		if ($diferenciaAgregados > 0) {
			if ($procedencia == 1) {
				return $this->modulomodel->getModuloPerfilNuevo($llave, $diferenciaAgregados);
			} else {
				return $this->perfilmodel->getPerfilModuloNuevo($llave, $diferenciaAgregados);
			}
		} else {
			return false;
		}
	}
} ?>
