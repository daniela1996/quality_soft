<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Vehiculos extends CI_Controller{
	function __construct(){
		parent::__construcT();
		$this->load->model('Vehiculosmodel');
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['jsfile'] = 'Vehiculos.js';

				$this->load->view('template/header.php', $session);
				$this->load->view('vehiculos_view.php', $data);
				$this->load->view('modals_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}

	public function vehiculosGet(){
		header('Content-type: application/json');
		$data = $this->Vehiculosmodel->getVehiculos();
		echo json_encode($data);
	}
	
	public function vehiculosInsert(){
		$chasis = $this->input->post("CHASIS");
		$color = $this->input->post("COLOR");
		$compania = $this->input->post("COMPANIA");
		$marca = $this->input->post("MARCA");
		$modelo = $this->input->post("MODELO");
		$motor = $this->input->post("MOTOR");
		$plate = $this->input->post("PLATE");
		$year = $this->input->post("YEAR");
		$imei = $this->input->post("IMEI");

		$data = array('CHASSIS' => $chasis, 
						'COLOR' => $color,
						'COMPANY' => $compania,
						'MAKE' => $marca,
						'MODEL' => $modelo,
						'MOTOR' => $motor,
						'PLATE'=>$plate,
						'YEAR' => $year,
						'IMEI' => $imei);
		$item = $this->Vehiculosmodel->insertVehiculo($data);
		$this->output->set_output(json_encode($item));		
	}

	public function vehiculosUpdate(){
		$id = $this->input->post("ID");
		$chasis = $this->input->post("CHASIS");
		$color = $this->input->post("COLOR");
		$compania = $this->input->post("COMPANIA");
		$marca = $this->input->post("MARCA");
		$modelo = $this->input->post("MODELO");
		$motor = $this->input->post("MOTOR");
		$plate = $this->input->post("PLATE");
		$year = $this->input->post("YEAR");
		$data = array('CHASSIS' => $chasis, 
						'COLOR' => $color,
						'COMPANY' => $compania,
						'MAKE' => $marca,
						'MODEL' => $modelo,
						'MOTOR' => $motor,
						'PLATE' => $plate,
						'YEAR' => $year);
		$item = $this->Vehiculosmodel->updateVehiculo($id, $data);
		$this->output->set_output(json_encode($item));
	}
}?>