<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Desarrollo extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Desarrollomodel');
		
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['jsfile'] = "Desarrollo.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('desarrollo_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}

	public function proyectosGet(){
		header('Content-Type: application/json');
		$item = $this->Desarrollomodel->getProyectos();
		echo json_encode($item);
	}

	public function modulosGet(){
		header('Content-Type: application/json');
		$idProyecto = $this->input->post('idProyecto');
		$item = $this->Desarrollomodel->getModulos($idProyecto);
		echo json_encode($item);
	}

	public function ModuloProyectoGet(){
		header('Content-Type: application/json');
		$id = $this->input->post('id');
		$item = $this->Desarrollomodel->getModuloProyecto($id);
		echo json_encode($item);
	}

	public function empresasGet(){
		header('Content-Type: application/json');
		$item = $this->Desarrollomodel->getEmpresas();
		echo json_encode($item);
	}

	public function estadosGet(){
		header('Content-Type: application/json');
		$item = $this->Desarrollomodel->getEstados();
		echo json_encode($item);
	}

	public function prioridadGet(){
		header('Content-Type: application/json');
		$item = $this->Desarrollomodel->getPrioridades();
		echo json_encode($item);
	}

	public function encargadosGet(){
		header('Content-Type: application/json');
		$item = $this->Desarrollomodel->getEncargados();
		echo json_encode($item);
		// print_r(json_encode($item));
	}

	// FUNCIONAL ACTUALMENTE SIN SELECTPICKER
	// public function projectsInsert(){
	// 	$codigo = $this->input->post("IDPROYECTO");
	// 	//$idtabla = $this->input->post("ID_TABLA");
	// 	$nombre = $this->input->post("NOMBRE");
	// 	$descripcion = $this->input->post("DESCRIPCION");
	// 	$estado = $this->input->post("ESTADO");
	// 	$session = $this->session->userdata('logged_in');
	// 	//log_message('error', $session);
	// 	$empresa = $this->input->post("EMPRESA");
	// 	$data = array( //'ID_MAINTENANCE_TABLE' => $idtabla,
	// 				'ID_PROJECTS' => $codigo,
	// 				'PROJECT_NAME' => $nombre,
	// 				'DESCRIPTION' => $descripcion,
	// 				'STATUS_PROJECT' => $estado,
	// 				'U_ID' => $session['userid'],
	// 				'ID_BUSINESS_PROJECTS' => $empresa);
	// 	$item = $this->Desarrollomodel->insertProjects($data);
	// 	$this->output->set_output(json_encode($item));
	// }

	public function projectsInsert(){
		// $codigo = $this->input->post("IDPROYECTO");
		// //$idtabla = $this->input->post("ID_TABLA");
		// $nombre = $this->input->post("NOMBRE");
		// $descripcion = $this->input->post("DESCRIPCION");
		// $estado = $this->input->post("ESTADO");
		
		// INCORPORAR SELECTPICKER
		$array = $this->input->post("data");
		$filas = array();
		$info = json_decode($array, true);
		$session = $this->session->userdata('logged_in');
		
		//log_message('error', $session);
		// $empresa = $this->input->post("EMPRESA");
		$data = array( //'ID_MAINTENANCE_TABLE' => $idtabla,
					'ID_PROJECTS' => $info['IDPROYECTO'],
					'PROJECT_NAME' => $info['NOMBRE'],
					'DESCRIPTION' => $info['DESCRIPCION'],
					'STATUS_PROJECT' => $info['ESTADO'],
					'U_ID' => $session['userid'],
					'ID_BUSINESS_PROJECTS' => $info['EMPRESA']);


		foreach ($info['usuarios'] as $key => $value) {
			// echo $value.'</br>';
			$filas[] = array('U_ID' => $value);
		}

		// print_r($info['usuarios']);
		$item = $this->Desarrollomodel->insertProjects($data, $filas);
		$this->output->set_output(json_encode($item));
	}

	//ACTUALMENTE FUNCIONAL SIN SELECTPICKER
	// public function projectsUpdate(){
	// 	$idproyecto = $this->input->post("IDPROYECTO");
	// 	// $codigo = $this->input->post("CODIGO_TABLA");
	// 	$nombre = $this->input->post("NOMBRE");
	// 	$descripcion = $this->input->post("DESCRIPCION");
	// 	$estado = $this->input->post("ESTADO");
	// 	$session = $this->session->userdata('logged_in');
	// 	//log_message('error', $session);
	// 	$empresa = $this->input->post("EMPRESA");
	// 	$data = array(//'ID_MAINTENANCE_TABLE' => $idtabla,
	// 				'PROJECT_NAME' => $nombre,
	// 				'DESCRIPTION' => $descripcion,
	// 				'STATUS_PROJECT' => $estado,
	// 				'U_ID' => $session['userid'],
	// 				'ID_BUSINESS_PROJECTS' => $empresa);
	// 	$item = $this->Desarrollomodel->updateProjects($data, $idproyecto);
	// 	$this->output->set_output(json_encode($item));
	// }

	public function projectsUpdate(){
		// $idproyecto = $this->input->post("IDPROYECTO");
		// // $codigo = $this->input->post("CODIGO_TABLA");
		// $nombre = $this->input->post("NOMBRE");
		// $descripcion = $this->input->post("DESCRIPCION");
		// $estado = $this->input->post("ESTADO");
		// $session = $this->session->userdata('logged_in');
		// //log_message('error', $session);
		// $empresa = $this->input->post("EMPRESA");
		// $data = array(//'ID_MAINTENANCE_TABLE' => $idtabla,
		// 			'PROJECT_NAME' => $nombre,
		// 			'DESCRIPTION' => $descripcion,
		// 			'STATUS_PROJECT' => $estado,
		// 			'U_ID' => $session['userid'],
		// 			'ID_BUSINESS_PROJECTS' => $empresa);

		// INCORPORAR SELECTPICKER
		$array = $this->input->post("data");
		$filas = array();
		$info = json_decode($array, true);
		$session = $this->session->userdata('logged_in');

		$data = array( //'ID_MAINTENANCE_TABLE' => $idtabla,
					'ID_PROJECTS' => $info['IDPROYECTO'],
					'PROJECT_NAME' => $info['NOMBRE'],
					'DESCRIPTION' => $info['DESCRIPCION'],
					'STATUS_PROJECT' => $info['ESTADO'],
					'U_ID' => $session['userid'],
					'ID_BUSINESS_PROJECTS' => $info['EMPRESA']);

		foreach ($info['usuarios'] as $key => $value) {
			// echo $value.'</br>';
			$filas[] = array('U_ID' => $value);
		}

		$item = $this->Desarrollomodel->updateProjects($data, $info['IDPROYECTO'], $filas);
		$this->output->set_output(json_encode($item));
	}

	// SIN SELECTPICKER
	// public function modulosInsert(){
	// 	$idProyecto = $this->input->post("IDPROYECTO");
	// 	$nombre = $this->input->post("NOMBRE");
	// 	$descripcion = $this->input->post("DESCRIPCION");
	// 	$prioridad = $this->input->post("PRIORIDAD");
	// 	$inicial = $this->input->post("INICIAL");
	// 	$final = $this->input->post("FINAL");
	// 	$destimados = $this->input->post("ESTIMADOS");
	// 	//$valor = $this->input->post("VALOR");
	// 	$data = array(
	// 				'ID_PROJECTS' => $idProyecto,
	// 				'PROJECT_MODULE_NAME' => $nombre,
	// 				//'VALOR' => $valor,
	// 				'DESCRIPTION' => $descripcion,
	// 				'PRIORITY' => $prioridad,
	// 				'INITIAL_ESTIMATED_DATE' => $inicial,
	// 				'FINAL_ESTIMATED_DATE' => $final,
	// 				'ESTIMATED_TIME' => $destimados);
	// 	$item = $this->Desarrollomodel->insertModulos($data);
	// 	$this->output->set_output(json_encode($item));
	// }

	public function modulosInsert(){
		// $codigo = $this->input->post("IDPROYECTO");
		// //$idtabla = $this->input->post("ID_TABLA");
		// $nombre = $this->input->post("NOMBRE");
		// $descripcion = $this->input->post("DESCRIPCION");
		// $estado = $this->input->post("ESTADO");
		
		// INCORPORAR SELECTPICKER
		$array = $this->input->post("data");
		$filas = array();
		$info = json_decode($array, true);
		$session = $this->session->userdata('logged_in');
		
		//log_message('error', $session);
		// $empresa = $this->input->post("EMPRESA");
		$data = array( //'ID_MAINTENANCE_TABLE' => $idtabla,
					'ID_PROJECTS' => $info['IDPROYECTO'],
					'PROJECT_MODULE_NAME' => $info['NOMBRE'],
					'DESCRIPTION' => $info['DESCRIPCION'],
					'PRIORITY' => $info['PRIORIDAD'],
					'INITIAL_ESTIMATED_DATE' => $info['INICIAL'],
					'FINAL_ESTIMATED_DATE' => $info['FINAL'],
					'ESTIMATED_TIME' => $info['ESTIMADOS']);


		foreach ($info['usuarios'] as $key => $value) {
			// echo $value.'</br>';
			$filas[] = array('U_ID' => $value);
		}

		// print_r($info['usuarios']);
		$item = $this->Desarrollomodel->insertModulos($data, $filas);
		$this->output->set_output(json_encode($item));
	}

	// SIN SELECTPICKER
	// public function modulosUpdate(){
	// 	$idmodulo = $this->input->post("IDMODULO");
	// 	#$cod_mantenimiento = $this->input->post("COD_MANTENIMIENTO");
	// 	$idProyecto = $this->input->post("IDPROYECTO");
	// 	$nombre = $this->input->post("NOMBRE");
	// 	$descripcion = $this->input->post("DESCRIPCION");
	// 	$prioridad = $this->input->post("PRIORIDAD");
	// 	$inicial = $this->input->post("INICIAL");
	// 	$final = $this->input->post("FINAL");
	// 	$destimados = $this->input->post("ESTIMADOS");
	// 	//$valor = $this->input->post("VALOR");
	// 	$data = array('PROJECT_MODULE_NAME' => $nombre,
	// 				#'COD_MANTENIMIENTO' => $cod_mantenimiento,
	// 				'ID_PROJECTS' => $idProyecto,
	// 				//'VALOR' => $valor,
	// 				'DESCRIPTION' => $descripcion,
	// 				'PRIORITY' => $prioridad,
	// 				'INITIAL_ESTIMATED_DATE' => $inicial,
	// 				'FINAL_ESTIMATED_DATE' => $final,
	// 				'ESTIMATED_TIME' => $destimados);
	// 	$item = $this->Desarrollomodel->updateModulos($data, $idmodulo);
	// 	$this->output->set_output(json_encode($item));
	// }

	// INCORPORAR SELECTPICKER
	public function modulosUpdate(){
		$array = $this->input->post("data");
		$filas = array();
		$info = json_decode($array, true);
		$session = $this->session->userdata('logged_in');

		$data = array( //'ID_MAINTENANCE_TABLE' => $idtabla,
					'ID_PROJECTS' => $info['IDPROYECTO'],
					'PROJECT_MODULE_NAME' => $info['NOMBRE'],
					'DESCRIPTION' => $info['DESCRIPCION'],
					'PRIORITY' => $info['PRIORIDAD'],
					'INITIAL_ESTIMATED_DATE' => $info['INICIAL'],
					'FINAL_ESTIMATED_DATE' => $info['FINAL'],
					'ESTIMATED_TIME' => $info['ESTIMADOS'],
					'MODULES_NUMBER' => $info['NUMEROMODULO']);

		foreach ($info['usuarios'] as $key => $value) {
			// echo $value.'</br>';
			$filas[] = array('U_ID' => $value);
		}

		$item = $this->Desarrollomodel->updateModulos($data, $info['IDMODULO'], $filas);
		$this->output->set_output(json_encode($item));
	}

	public function moduleDelete() {
		header('Content-type: application/json');
		$idmodulo = $this->input->post('idModulo');
		$item = $this->Desarrollomodel->deleteModulo($idmodulo);
		echo json_encode($item);
	}

	public function arregloUsuariosGet(){
		header('Content-Type: application/json');
		$idProyecto = $this->input->post('idProyecto');
		$item = $this->Desarrollomodel->getArregloUsuarios($idProyecto);
		echo json_encode($item);
		// print_r($item);
	}

	public function arregloUsuariosGetByModule(){
		header('Content-Type: application/json');
		$idModulo = $this->input->post('idModulo');
		$item = $this->Desarrollomodel->getArregloUsuariosPorModulo($idModulo);
		echo json_encode($item);
		// print_r($item);
	}
}?>