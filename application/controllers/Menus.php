<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menus extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('menumodel');
		$this->load->model('modulomodel');
	}
	public function index() {
		if($this->session->userdata('logged_in')) {
			$session = $this->session->userdata('logged_in');
			if ($this->user->registro('/'.$this->router->fetch_class(), $session['uniqueid'])) {
				$data['result'] = $this->menumodel->getMenu();
				$data['modulos'] = $this->modulomodel->getModulo();
				$data['jsfile'] = "Menu.js";
				$this->load->view('template/header.php', $session);
				$this->load->view('menu_view.php', $data);
				$this->load->view('template/footer.php', $data);
			} else {
				$this->load->view('template/header.php', $session);
				$this->load->view('404_view');
				$this->load->view('template/footer.php');
			}
		} else {
			redirect('', 'refresh');
		}
	}
	public function menuParticularGet() {
		header('Content-type: application/json');
		$array = $this->input->post('menuName');
		$decoded = json_decode($array,true);
		$item = $this->menumodel->getMenuParticular($decoded['MENUID']);
		$this->output->set_output(json_encode($item));
	}
	public function menusInsert() {
		header('Content-type: application/json');
		$array = $this->input->post('menuName');
		$decoded = json_decode($array,true);
		$rows = array('TITLE' => $decoded['TITLE'],
									'URL' => $decoded['URL'],
									'PARENT_ID' => $decoded['PARENT_ID'],
									'MODULEID' => $decoded['MODULEID'],
									'ICON' => $decoded['ICON']);
		$ultimoRegistro = $this->menumodel->getUltimoMenu();
		$item = $this->menumodel->insertMenu($rows);
		$nuevosRegistros = $this->menumodel->getUltimoMenu();
		$rowsRespuesta = $this->ultimasEntradas($ultimoRegistro, $nuevosRegistros);
		$this->output->set_output(json_encode(array($item, $rowsRespuesta, 1)));
	}
	public function menusUpdate() {
		header('Content-type: application/json');
		$array = $this->input->post('menuName');
		$decoded = json_decode($array,true);
		$rows = array('TITLE' => $decoded['TITLE'],
					  'URL' => $decoded['URL'],
					  'PARENT_ID' => $decoded['PARENT_ID'],
					  'MODULEID' => $decoded['MODULEID'],
					  'ICON' => $decoded['ICON']);
		$item = $this->menumodel->updateMenu($decoded['ID'], $rows);
		$menuUpdated = $this->menumodel->getMenuParticular($decoded['ID']);
		$this->output->set_output(json_encode(array($item, $menuUpdated, 2)));
	}
	public function menusDelete() {
		header('Content-type: application/json');
		$array = $this->input->post('menuName');
		$decoded = json_decode($array,true);
		$item = $this->menumodel->deleteMenu($decoded['ID'], $decoded['SHOW_MENU']);
		$menuUpdated = $this->menumodel->getMenuParticular($decoded['ID']);
		$this->output->set_output(json_encode(array($item, $menuUpdated, 3)));
	}
	private function ultimasEntradas($ultimoRegistro, $nuevosRegistros) {
		$diferenciaAgregados = (intval($nuevosRegistros['CONTEO']) - intval($ultimoRegistro['CONTEO']));
		if ($diferenciaAgregados > 0) {
			return $this->menumodel->getMenuNuevo($diferenciaAgregados);
		} else {
			return false;
		}
	}
} ?>
