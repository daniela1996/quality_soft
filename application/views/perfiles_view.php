<section class="content-header">
	<h1>Usuarios:<small> Perfiles</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Perfiles</h2>
			<div class="filtroDePreguntas">
				<span id="newPerfil" class="btn btn-success"><i class='icon-edit icon-white fa fa-plus'></i> Agregar Perfil</span>
			</div>
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable">
					<thead>
						<tr>
							<th class="oculto"></th>
							<th>Código</th>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Fecha</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody id="anclaTabla">
						<?php if($perfiles) {
							foreach($perfiles as $fila) { ?>
							<tr>
								<td class="oculto"><?=$fila['PROFILEID']?></td>
								<td><?=$fila['CODEPROFILE']?></td>
								<td><?=$fila['NAMEPROFILE']?></td>
								<td><?=$fila['DESCPROFILE']?></td>
								<td><?=$fila['DATES']?></td>
								<td>
									<a href="perfilesusuarios?pu=1&vl=<?=$fila['PROFILEID']?>">
										<span class='btn purple botonVED' data-toggle="tooltip" data-placement="right" title="Crear perfiles por Usuarios">
											<i class='icon-edit icon-white fa fa-user'></i>
										</span>
									</a>
									<a href="perfilesmodulos?pm=1&vl=<?=$fila['PROFILEID']?>">
										<span class='btn btn-warning botonVED'data-toggle="tooltip" data-placement="right" title="Verificar orden de perfiles">
											<i class='icon-edit icon-white fa fa-list'></i>
										</span>
									</a>
									<span class='editPerfil btn btn-success botonVED' data-toggle="tooltip" data-placement="right" title="Editar perfil">
										<i class='icon-edit icon-white fa fa-pencil'></i>
									</span>
									<span class='deletePerfil btn btn-danger botonVED' data-toggle="tooltip" data-placement="right" title="Eliminar perfil">
										<i class='icon-edit icon-white fa fa-trash-o'></i>
									</span>
								</td>
							</tr>
						<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="modalPerfiles" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
			</div>
			<?php $alertasid = array('errorPerfiles', 'successPerfiles', 'warningPerfiles'); include('template/alert_template.php'); ?>
			<div class="modal-body">
				<div class="box-body">
					<div class="form-group">
						<input type="hidden" class="form-control profileinputs" id="id_perfil">
					</div>
					<div id="sure" class="form-group">
						<p>¿Esta seguro que desea eliminar el Perfil: <span id="nombre_span"></span>?</p>
					</div>
					<form id="perfilesForm">
					<div class="greatInputContainer">
						<div class="row">
							<div class="form-group col-md-12">
								<label>Código:</label>
								<input type="text" class="form-control profileinputs letrassinespacios" id="codigo_perfil" name="codigo_perfil">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<label>Nombre:</label>
								<input type="text" class="form-control profileinputs letras" id="nombreperfil" name="nombreperfil">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<label>Descripción:</label>
								<input type="text" class="form-control profileinputs" id="descripcion_perfil" name="descripcion_perfil">
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-xs-4">
					<button type="button" id="enviarPerfil" class="btn btn-primary btn-block btn-flat">Guardar</button>
				</div>
				<div class="col-xs-4">
					<button type="button" id="cerrarPerfil" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
