<section class="content-header">
	<h1>Usuarios: <small>Usuarios</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Usuarios</h2>
			<div class="filtroDePreguntas">
				<span id="newUsuario" class="btn btn-success"><i class='icon-edit icon-white fa fa-plus'></i> Agregar Usuario</span>
			</div>
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable">
					<thead>
						<tr>
							<th class="oculto"></th>
							<th>Nombre</th>
							<th>Celular</th>
							<th>Correo</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody class="tableViewer" id="anclaTabla">
						<?php if ($result) {
							foreach($result as $fila) { ?>
								<tr>
									<td class="oculto"><?=$fila['U_ID']?></td>
									<td class="center cargarInfo"><?=$fila['NAME']?></td>
									<td class="center cargarInfo"><?=$fila['PHONE']?></td>
									<td class="center cargarInfo"><?=$fila['EMAIL']?></td>
									<td>
										<a href="rolesuser?ru=2&vl=<?php echo $fila['U_ID'];?>">
											<span id='linkRoles' class='btn bg-orange botonVED' data-toggle="tooltip" data-placement="right" title="Asignar rol">
												<i class='icon-edit icon-white fa fa-arrow-circle-o-right'></i>
											</span>
										</a>
										<a href="perfilesusuarios?pu=2&vl=<?php echo $fila['U_ID'];?>">
											<span id='linkPerfil' class='btn purple botonVED' data-toggle="tooltip" data-placement="right" title="Asignar perfiles">
												<i class='icon-edit icon-white fa fa-users'></i>
											</span>
										</a>
										<span class=' btn btn-primary botonVED editUsuario' data-toggle="tooltip" data-placement="right" title="Editar usuario">
											<i class='icon-edit icon-white fa fa-pencil'></i>
										</span>
									</td>
								</tr>
						<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="modalUsuarios" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modalEnsanchado" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
			</div>
			<?php $alertasid = array('errorUsuarios', 'successUsuarios', 'warningUsuarios'); include('template/alert_template.php'); ?>
			<ul class="nav nav-tabs ligeroNav">
        		<li id="infoprincipal" class="active"><a data-toggle="tab" href="#infoprincipalMenu">Usuario</a></li>
				<li id="fotousuario"><a data-toggle="tab" href="#fotousuarioMenu">Fotografía</a></li>
      		</ul>
			<div class="modal-body" style="max-height: 440px; overflow-y: auto; overflow-x: hidden;">
				<div class="box-body">
					<div class="tab-content">
						<div id="infoprincipalMenu" class="tab-pane fade in active modalTabMargin">
							<div id="usuarioInfo" class="displayBasicInfo"></div>
						<form id="usuariosForm">
							<div class="greatInputContainer">
								<input type="hidden" class="form-control" id="id_usuario">
								<div class="row">
									<div class="form-group col-md-6">
										<label>Nombre:</label>
										<input type="text" class="form-control usersinputs letras" id="nombre_usuario" name="nombre_usuario">
									</div>
									<div class="form-group col-md-6">
										<label>Celular:</label>
										<input type="text" class="form-control usersinputs" id="celular_usuario" name="celular_usuario">
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label>Email:</label>
										<input type="email" class="form-control usersinputs" id="email_usuario" name="email_usuario">
									</div>
									<div class="form-group col-md-6">
										<label>Nickname:</label>
										<input type="text" class="form-control usersinputs letrassinespacios" id="nick_usuario" name="nick_usuario">
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label>Clave Ingreso Sistema:</label>
										<input type="password" class="form-control usersinputs" id="clave_usuario" name="clave_usuario">
									</div>
								</div>
							</div>
						</form>
						</div>
						
						<div id="fotousuarioMenu" class="tab-pane fade">
							<h3><span>Fotografía</span></h3>
							<div class="clear"></div>
							<span id="imagendelusuario" class="btn btn-success">Cargar Imagen</span>
							<div id="foto_ticket" class="imagenAlterable">
								<div class="helper"></div>
								<img id="imagenSelecta" src="https://dummyimage.com/1000x1000/a1acb2/000000.jpg&text=Nueva+imagen+de+perfil" class="fotodeltecnico"/>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-xs-4">
					<button type="button" id="enviarUsuario" class="btn btn-primary btn-block btn-flat">Guardar</button>
				</div>
				<div class="col-xs-4">
					<button type="button" id="cerrarUsuario" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
