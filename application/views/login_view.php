<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inicio de Sesion</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url();?>/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= base_url();?>/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?= base_url();?>/plugins/iCheck/square/blue.css">
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b> </b></a>
      </div>
      <div class="login-box-body">
        <p class="login-box-msg">Ingrese su usuario y contraseña</p>
        <h5><div id="msg-error" class="text-center"></div></h5>
     			<?php $attributes = array('id' => 'formLogin'); ?>
				 <?= validation_errors(); ?>
				 <?= form_open('verifylogin', $attributes); ?>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="username" name="username" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <!-- <div class="col-xs-6"> -->
              <!-- <div class="checkbox icheck"> -->
                <!-- <label> -->
                  <!-- <input type="checkbox"> Remember Me -->
                <!-- </label> -->
              <!-- </div> -->
            <!-- </div> -->
            <div class="col-xs-12">
              <button id="btnenviar" type="submit" class="btn btn-primary btn-block btn-flat">Iniciar Sesion</button>
            </div>
          </div>
        </form>
        <div class="hidden">
          <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
          </div>
          <a href="#">I forgot my password</a><br>
          <a href="register.html" class="text-center">Register a new membership</a>
        </div>
      </div>
    </div>
    <script src="<?= base_url();?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?= base_url();?>/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url();?>/plugins/iCheck/icheck.min.js"></script>
    <script src="<?= base_url(); ?>sternemedia/Global.js"></script>
  </body>
</html>
