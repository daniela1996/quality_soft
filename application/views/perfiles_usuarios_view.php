<section class="content-header">
	<h1>Usuarios: <small>Perfiles por Usuarios</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<?php if($both) { ?>
				<h2 class="displayoncenter aligntitle"><?= (($perfilUsuario == 1) ? "Usuarios por perfil": "Perfiles de Usuario"); ?></h2>
				<h4 class="displayoncenter">
					<?php $perUsInfo = ($perfilUsuario == 1) ?$result[0]['NAMEPROFILE'] : $result[0]['USERNAME']; ?>
					<?=$perUsInfo?>
				</h4>
				<div class="filtroDePreguntas">
					<?php if($result) { ?>
						<span id='asociarPerfilUsuario' class='btn btn-success'><i class='icon-edit icon-white fa fa-plus'></i> Asignar</span>
					<?php } else { ?>
						<span id='asociarPerfilUsuario' class='btn btn-success'>
							<?php $perUsInfo = ($perfilUsuario == 1) ? " Asignar Usuarios" : " Asignar Perfiles"; ?>
							<i class='icon-edit icon-white fa fa-plus'></i><?=$perUsInfo?>
						</span>
					<?php } ?>
				</div>
			<?php } else { ?>
				<h2 class="displayoncenter aligntitle">Orden por Ambos</h2>
			<?php } ?>
			<div class="displayoncenter">
				<table class="table table-striped table-bordered estiloTabla datatable">
					<thead>
						<tr>
							<th class="oculto"></th>
							<?php if(($both == false)||($perfilUsuario == 1)) { ?>
								<th class="oculto"></th>
								<th class="oculto">Perfil</th>
								<th class="oculto"></th>
								<th>Usuario</th>
							<?php } else { ?>
								<th class="oculto"></th>
								<th class="oculto">Usuario</th>
								<th class="oculto"></th>
								<th>Perfil</th>
							<?php } ?>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody id="anclaTabla">
						<?php if($result) {
							foreach($result as $fila) { ?>
							<tr>
								<td class="oculto"><?=$fila['USERPROFILEID']?></td>
								<?php if(($both == false)||($perfilUsuario == 1)) { ?>
									<td class="oculto"><?=$fila['PROFILEID']?></td>
									<td class="oculto"><?=$fila['NAMEPROFILE']?></td>
									<td class="oculto"><?=$fila['USERID']?></td>
									<td><?=$fila['USERNAME']?></td>
								<?php } else { ?>
									<td class="oculto"><?=$fila['USERID']?></td>
									<td class="oculto"><?=$fila['USERNAME']?></td>
									<td class="oculto"><?=$fila['PROFILEID']?></td>
									<td><?=$fila['NAMEPROFILE']?></td>
								<?php } ?>
								<td>
								<?php if($fila['USRPROSTATUS'] == 0) { ?>
									<span class='asociacion btn btn-success botonVED'>Habilitar</span>
								<?php } else { ?>
									<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>
								<?php } ?>
								</td>
							</tr>
						<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<?php if ($both) { ?>
	<div class="modal fade" id="modalPerfilesUsuarios" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span id="theHeader">Nueva Asociación</span></h4>
				</div>
				<?php $alertasid = array('errorPerfilesUsuarios', 'successPerfilesUsuarios', 'warningPerfilesUsuarios'); include('template/alert_template.php'); ?>
				<div class="modal-body">
					<div class="box-body">
						<div class="form-group">
							<input type="hidden" class="form-control" id="id_perfil_usuario">
							<p id="profileOrUser" hidden="true"><?=$idVal;?></p>
						</div>
						<?php if($perfilUsuario == 2) { ?>
						<div id="perfil_container" class="form-group">
							<label>Perfil:</label>
							<select class="form-control selectBuscar" id="perfil_perus" name = "perfil_perus">
								<option value="0">Seleccione un Perfil</option>
								<?php foreach($perfil as $fila) { ?>
									<?= ($fila['HABILITADO'] == 1) ? '<option value="'.$fila['PROFILEID'].'" disabled>'.$fila['NAMEPROFILE'].'</option>': '<option value="'.$fila['PROFILEID'].'">'.$fila['NAMEPROFILE'].'</option>'; ?>
								<?php } ?>
							</select>
						</div>
						<?php } else { ?>
						<div id="usuario_container" class="form-group">
							<label>Usuario:</label>
							<select class="form-control selectBuscar" id="user_perus" name = "user_perus">
								<option value="0">Seleccione un Usuario</option>
								<?php foreach($usuario as $fila) { ?>
									<?= ($fila['HABILITADO'] == 1) ? '<option value="'.$fila['U_ID'].'" disabled>'.$fila['NAME'].'</option>': '<option value="'.$fila['U_ID'].'">'.$fila['NAME'].'</option>'; ?>
								<?php } ?>
							</select>
						</div>
						<?php } ?>
						<div id="addContainer">
							<div id="addLeft" class="leftContainer"></div>
							<div id="addRight" class="rightContainer"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-xs-4">
						<button type="button" id="enviarPerus" class="btn btn-primary btn-block btn-flat">
							Guardar
						</button>
					</div>
					<div class="col-xs-4">
						<button type="button" id="cerrarPerus" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
