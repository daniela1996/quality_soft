<section class="content-header">
	<h1>Usuarios:<small> Menús</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Menús</h2>
			<div class="filtroDePreguntas">
				<span id="newMenu" class="btn btn-success"><i class='icon-edit icon-white fa fa-plus'></i> Agregar Menú</span>
			</div>
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable">
					<thead>
						<tr>
							<th class="oculto"></th>
							<th>Título</th>
							<th>Nombre Módulo</th>
							<th>URL</th>
							<th class="oculto">Grupo Dinámico</th>
							<th class="oculto"></th>
							<th class="oculto"></th>
							<th class="oculto"></th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody id="anclaTabla">
						<?php if($result) {
							foreach($result as $fila) { ?>
							<tr>
								<td class="oculto"><?=$fila['ID']?></td>
								<td><?=$fila['TITLE']?></td>
								<td><?=$fila['NAMEMODULE']?></td>
								<td><?=$fila['URL']?></td>
								<td class="oculto"><?=$fila['DYN_GROUP_ID']?></td>
								<td class="oculto"><?=$fila['PARENT_ID']?></td>
								<td class="oculto"><?=$fila['MODULEID']?></td>
								<td class="oculto"><?=$fila['ICON']?></td>

								<td>
									<span class='editMenu btn btn-success botonVED' data-toggle="tooltip" data-placement="right" title="Editar menú">
										<i class='icon-edit icon-white fa fa-pencil'></i>
									</span>
									<?php if($fila['SHOW_MENU'] == 0) { ?>
										<span class='activarMenu btn btn-info botonVED' data-toggle="tooltip" data-placement="right" title="Activar">
											<i class='icon-edit icon-white fa fa-check-square-o'></i>
										</span>
									<?php } else { ?>
										<span class='deleteMenu btn btn-danger botonVED' data-toggle="tooltip" data-placement="right" title="Desactivar">
											<i class='icon-edit icon-white fa fa-minus-square-o'></i>
										</span>
									<?php } ?>
								</td>
							</tr>
						<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalMenu" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
				</div>
				<div class="modal-body">
				<?php $alertasid = array('errorMenu', 'successMenu', 'warningMenu'); include('template/alert_template.php'); ?>
					<div class="box-body">
						
						<input type="hidden" class="form-control menuinputs" id="id_menu">
						<div id="sure" class="form-group">
							<p id= "pregunta"></p>
						</div>
						<Form id="menuForm">
						<div class="greatInputContainer">
							<div id="titulo_container" class="form-group">
								<label>Título:</label>
								<input type="text" class="form-control menuinputs" id="titulo_menu" name="titulo_menu">
							</div>
							<div id="padre_container" class="form-group">
								<label>Padre:</label>
								<select class="form-control menuinputs" id="padre_menu" name="padre_menu">
									<option value="0">Sin Padre</option>
									<?php foreach ($result as $fila) { if($fila['PARENT_ID'] == 0) { ?>
										<?= '<option value="'.$fila['ID'].'">'.$fila['TITLE'].'</option>' ?>
									<?php } } ?>
								</select>
							</div>
							<div id="url_container" class="form-group">
								<label>URL:</label>
								<input type="text" class="form-control menuinputs" id="url_menu" name="url_menu">
							</div>
							<div id="modulo_container" class="form-group">
								<label>Módulo:</label>
								<div id="div_modulo_menu">
									<select class="form-control menuinputs" id="modulo_menu" name="modulo_menu">
										<option value="">Seleccione...</option>
										<?php foreach($modulos as $fila) { ?>
											<?= '<option value="'.$fila['MODULEID'].'">'.$fila['NAMEMODULE'].'</option>' ?>
										<?php } ?>
									</select>
								</div>
							</div>
							<div id="icono_container" class="form-group" >
								<label>Ícono:</label>
								<input type="text" class="form-control menuinputs" id="icono_menu" name="icono_menu">
							</div>
						</div>
					</Form>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-xs-4">
						<button type="button" id="enviarMenu" class="btn btn-primary btn-block btn-flat">Guardar</button>
					</div>
					<div class="col-xs-4">
						<button type="button" id="cerrarMenu" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
