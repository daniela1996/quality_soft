<section class="content-header">
	<h1>Calendario<small>Control panel</small></h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!--<div class="col-md-3">
			<div class="box box-solid">
				<div class="box-header with-border">
					<h4 class="box-title">Filtrar Por:</h4>
				</div>
				<div class="box-body">
				<!-- the events -->
				<!--	<div id="default" style="cursor:pointer;" class="bg-light-blue filterBotton">Todos</div>
					<div id="pendientePO" class="bg-yellow filterBotton poTag">Ordenes Pendientes</div>
					<div id="aceptadaPO" class="bg-green filterBotton poTag">Ordenes Emitidas</div>
					<div id="rechazadaPO" class="bg-red filterBotton poTag">Ordenes Rechazadas</div>
					<div id="pendienteS" class="bg-yellow filterBotton solicitudTag">Solicitudes Por Confirmar</div>
					<div id="aceptadaS" class="bg-green filterBotton solicitudTag">Solicitudes Aceptadas</div>
					<div id="rechazadaS" class="bg-red filterBotton solicitudTag">Solicitudes Rechazadas</div>					
				</div> /.box-body -->
			<!--</div> /. box -->
		<!-- </div> /.col --> 
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body no-padding">
					<!-- THE CALENDAR -->
					<div id="calendar"></div>
				</div><!-- /.box-body -->
			</div><!-- /. box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!--  hasta aca/.content -->

<div class="modal fade" id="modalDepartamento" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
			</div>
			<div class="modal-body">
				<div class="box-body">
					<div id="informacion" class="form-group"></div>
				</div><!-- /.box-body -->
			</div>
			<div class="modal-footer">
				<div class="col-xs-4">
					<button type="button" id="cerrarCalendario" class="btn btn-default btn-block btn-flat" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="../sternemedia/Home.js"></script>