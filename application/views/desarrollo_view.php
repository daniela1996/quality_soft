<section class="content-header">
			<h1>Desarrollo: <small> Desarrollo</small></h1>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-body">
					<h2 class="displayoncenter aligntitle">Desarrollo</h2>
					<div class="filtroDePreguntas">
						<span id="newMantenimiento" class="btn btn-primary"><i class='icon-edit icon-white fa fa-plus'></i> Nuevo</span>
					</div>
				<div class="row">
				
				<div class="col-md-6">
					<div class="panel panel-default">
						<div align="center" class="panel-heading">Proyectos</div>
						<div class="displayoncenter" id="displayTables">
							<table id = "tablaTablasMant" class="table table-striped table-bordered estiloTabla dataTableSimple">
								<thead>
									<tr>
										<th class="oculto"></th>
										<th>Nombre</th>
										<th>Descripción</th>
										<th class="oculto">Estado</th>
										<th class="oculto"></th>
										<th>Acción</th>
									</tr>
								</thead>
								<tbody class="tableViewer" id="anclaTabla">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div align="center" class="panel-heading">Módulos</div>
						<div class="displayoncenter" id="displayTable">
							<table id = "tablaMantenimientos" class="table table-striped table-bordered estiloTabla dataTableSimple">
								<thead>
									<tr>
										<th class="oculto"></th>
										<th class="oculto"></th>
										<th>Nombre</th>
										<th>Descripción</th>
										<th class="oculto"></th>
										<th class="oculto"></th>
										<th class="oculto"></th>
										<th class="oculto"></th>
										<th>Acción</th>
									</tr>
								</thead>
								<tbody class="tableViewer" id="anclaTabla">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="modalMantenimiento" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
					</div>
					<?php $alertasid = array('errorMantenimiento', 'successMantenimiento', 'warningMantenmiento'); include('template/alert_template.php'); ?>
					<div id="tabs">
					<ul class="nav nav-tabs ligeroNav">
		        		<li id="infoTabla" class="active"><a id="infoTabla" data-toggle="tab" href="#infotabla">Proyectos</a></li>
						<li id="infoMantenimiento"><a id="infoMantenimiento" data-toggle="tab" href="#infomantenimientos">Módulos</a></li>
		     	 	</ul>
					<div class="modal-body">
						<div class="box-body">
							<div class="tab-content">
								<div id="infotabla" class="tab-pane fade in active modalTabMargin">
									<div class="greatInputContainer">
										<form id="tablaForm">
											<input type="hidden" class="form-control mentenimientosinputs" id="idProyecto">
											
											<div class="col-md-6">
												<div class="form-group">
													<label>Nombre Proyecto:</label>
													<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="20" id="nombreProyecto" name="nombreProyecto">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Descripción:</label>
													<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="250" id="descripcion" name="descripcion">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Estado Proyecto:</label>
													<div id="div_estadoProyecto">
														<select type="text" class="form-control mentenimientosinputs isSelect" id="estadoProyecto" name="estadoProyecto">
															<option disabled="disabled" value="">-- Seleccionar --</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Empresa:</label>
													<div id="div_empresa">
														<select type="text" class="form-control mentenimientosinputs isSelect" id="empresa" name="empresa">
															<option disabled="disabled" value="">-- Seleccionar --</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Encargado(s):</label>
													<div id="div_encargado">
														<select id="encargado" class="selectpicker form-control" data-live-search="true" data-style="btn-select" multiple>
															<option disabled="disabled" value="">-- Seleccionar --</option>
														</select>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								
								
									<div id="infomantenimientos" class="tab-pane fade">
										<form id="matenimientoForm">
										<input type="hidden" class="form-control mentenimientosinputs" id="idmodulo">
										<input type="hidden" class="form-control mentenimientosinputs" id="numero_modulo">
										
										<div class="col-md-6">
											<div class="form-group">
												<label>Nombre Proyecto:</label>
												<div id="div_idtabla">
													<select type="text" class="form-control mentenimientosinputs isSelect" id="idproject" name="idproject">
														<option disabled="disabled" value="">-- Seleccionar --</option>
													</select>
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label>Nombre Módulo:</label>
												<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="50" id="modulo" name="modulo">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Descripción:</label>
												<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="250" id="descripcion_mod" name="descripcion_mod">
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label>Prioridad:</label>
												<div id="div_prioridad">
													<select type="text" class="form-control mentenimientosinputs isSelect" id="prioridad" name="prioridad">
														<option disabled="disabled" value="">-- Seleccionar --</option>
													</select>
												</div>
											</div>
										</div>

										<div id="div_initialdate" class="col-md-6">
											<div class="form-group">
										 		<label for="initialdate" class="control-label">Fecha Estimada Inicial:</label>
										  		<input id="initialdate" name="initialdate" type="text" class="form-control fechas mentenimientosinputs" autocomplete="off">
											</div>
										</div>

										<div id="div_finaldate" class="col-md-6">
											<div class="form-group">
											  <label for="finaldate" class="control-label">Fecha Estimada Final:</label>
											  <input id="finaldate" name="finaldate" type="text" class="form-control fechas mentenimientosinputs" autocomplete="off">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Encargado(s):</label>
												<div id="div_encargadoModulo">
													<select id="encargadoModulo" class="selectpicker form-control" data-live-search="true" data-style="btn-select" multiple>
														<option disabled="disabled" value="">-- Seleccionar --</option>
													</select>
												</div>
											</div>
										</div>
										<div id="container">
											<div align="right" class="col-xs-12">
												<button id="agregarMod" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; Agregar</button>
												<button type="button" id="cerrarMant" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-ban"></i> Cancelar</button>
											</div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-md-12">
												<div class="panel panel-default">
													<table id = "tblModulos" class="table table-striped table-bordered estiloTabla dataTableSimple">
												        <thead>
												            <tr>
												            	<th class="oculto">id</th>
												                <th>Nombre Proyecto</th>
												                <th>Nombre Módulo</th>
												                <th>Descripción</th>
												                <th>Prioridad</th>
												                <th>Fecha Estimada Inicial</th>
												                <th>Fecha Estimada Final</th>
												                <th>Acciones</th>
												            </tr>
												        </thead>
												        <tbody id="tbodyModulos">
												            
												        </tbody>
												    </table>
												</div>
											</div>
										</div>

										</form>
									</div>
								</div>
							</div>
					</div>
					</div>
					<div class="modal-footer">
							<div class="col-xs-12">
								<button type="button" id="enviarmant"></button>&nbsp;
								<button type="button" id="cancelar" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-ban"></i> Cancelar</button>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modalView" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title" id="myModalLabel"><span id="theHeader2"></span></h4>
		        </div>
		        <div class="modal-body">
		        	<div class="col-md-6">
		        		<p id="info_Proyecto"><b>Nombre Proyecto: </b><span id="VER_Proyecto"></span></p>
		        	</div>
		        	<div class="col-md-6">
						<p id="info_Modulo"><b>Nombre Módulo: </b><span id="VER_Modulo"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Número de Módulos: </b><span id="VER_Numero_Modulos"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Prioridad: </b><span id="VER_Prioridad"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Porcentaje de Avance: </b><span id="VER_Porcentaje_Avance"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Tiempo Estimado: </b><span id="VER_Tiempo_Estimado"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Tiempo Real: </b><span id="VER_Tiempo_Real"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Tiempo de Retraso: </b><span id="VER_Tiempo_Retraso"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Fecha Estimada de Inicio: </b><span id="VER_Fecha_Inicio"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Fecha Estimada Final: </b><span id="VER_Fecha_Final"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Fecha Real de Inicio: </b><span id="VER_Fecha_Inicio_Real"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Fecha Real Final: </b><span id="VER_Fecha_Final_Real"></span></p>
					</div>
					<div class="col-md-6">
						<p id="info_Modulo"><b>Ciclo de Desarrollo: </b><span id="VER_Ciclo"></span></p>
					</div>
		        </div>
		        <div class="modal-footer">
		          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
		          <button type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-times-circle"></i> Cerrar</button>
		        </div>
		      </div>
		      
		    </div>
  		</div>
  		<div class="modal fade" id="modalDelete" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title" id="myModalLabel"><span id="theHeader3"></span></h4>
		        </div>
		        <div class="modal-body">
		        	<div id="sure" class="form-group">
						<p>¿Está seguro que desea eliminar el Módulo: <span id="nombre_span"></span>?</p>
					</div>
		        </div>
		        <div class="modal-footer">
		          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
		          <button id= "eliminarModulo" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Eliminar</button>
		          <button id= "cerrarSure" type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-times-circle"></i> Cerrar</button>
		        </div>
		      </div>
		      
		    </div>
  		</div>

<style type="text/css">
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}
</style>