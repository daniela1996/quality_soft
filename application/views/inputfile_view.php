<!DOCTYPE html>
<!-- release v4.4.8, copyright 2014 - 2018 Kartik Visweswaran -->
<!--suppress JSUnresolvedLibraryURL -->
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Krajee JQuery Plugins - &copy; Kartik</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- <link href="../css/fileinput.css" media="all" rel="stylesheet" type="text/css"/> -->
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/fileinput/css/fileinput.css" media="all">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>
    <!-- <link href="../themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/> -->
    <link href="<?= base_url();?>plugins/fileinput/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>
    
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <!-- <script src="../js/plugins/sortable.js" type="text/javascript"></script> -->
    <!-- <script src="../js/fileinput.js" type="text/javascript"></script> -->
    <!-- <script src="../js/locales/fr.js" type="text/javascript"></script> -->
    <!-- <script src="../js/locales/es.js" type="text/javascript"></script> -->
    <!-- <script src="../themes/explorer-fa/theme.js" type="text/javascript"></script> -->
    <!-- <script src="../themes/fa/theme.js" type="text/javascript"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script> -->


    <script type="text/javascript" src="<?= base_url();?>plugins/fileinput/js/plugins/sortable.js"></script>
    <script type="text/javascript" src="<?= base_url();?>plugins/fileinput/js/fileinput.js"></script>
    <script type="text/javascript" src="<?= base_url();?>plugins/fileinput/js/locales/fr.js"></script>
    <script type="text/javascript" src="<?= base_url();?>plugins/fileinput/js/locales/es.js"></script>
    <script src="<?= base_url();?>plugins/fileinput/themes/explorer-fa/themes.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>



    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script> -->
</head>
<body>
<div class="container kv-main">
    <div class="page-header">
        <h1>Bootstrap File Input Example
            <small><a href="https://github.com/kartik-v/bootstrap-fileinput-samples"><i
                    class="glyphicon glyphicon-download"></i> Download Sample Files</a></small>
        </h1>
    </div>
    <form enctype="multipart/form-data">
        <!-- <div class="file-loading">
            <input id="kv-explorer" type="file" multiple>
        </div>
        <br>
        <div class="file-loading">
            <input id="file-0a" class="file" type="file" multiple data-min-file-count="1">
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-default">Reset</button> -->
    </form>
    <hr>
    <form enctype="multipart/form-data">
        <label for="file-0b">Test invalid input type</label>
        <div class="file-loading">
            <input id="file-0b" name="file-0b" class="file" type="text" multiple data-min-file-count="1">
        </div>
        <script>
            $(document).on('ready', function () {
                $("#file-0b").fileinput();
            });
        </script>
    </form>
    <hr>
    <form enctype="multipart/form-data">
        <div class="file-loading">
            <input id="file-0c" class="file" type="file" multiple data-min-file-count="3">
        </div>
        <hr>
        <div class="form-group">
            <div class="file-loading">
                <input id="file-0d" class="file" type="file">
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="file-loading">
                <input id="file-1" type="file" multiple class="file" data-overwrite-initial="false" data-min-file-count="2">
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="file-loading">
                <input id="file-2" type="file" class="file" readonly data-show-upload="false">
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="file-loading">
                <label>Preview File Icon</label>
                <input id="file-3" type="file" multiple>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="file-loading">
                <input id="file-4" type="file" class="file" data-upload-url="#">
            </div>
        </div>
        <hr>
        <div class="form-group">
            <button class="btn btn-warning" type="button">Disable Test</button>
            <button class="btn btn-info" type="reset">Refresh Test</button>
            <button class="btn btn-primary">Submit</button>
            <button class="btn btn-default" type="reset">Reset</button>
        </div>
        <hr>
        <div class="form-group">
            <div class="file-loading">
                <input type="file" class="file" id="test-upload" multiple>
            </div>
            <div id="errorBlock" class="help-block"></div>
        </div>
        <hr>
        <div class="form-group">
            <div class="file-loading">
                <input id="file-5" class="file" type="file" multiple data-preview-file-type="any" data-upload-url="#">
            </div>
        </div>
    </form>


<script>
    $('#file-fr').fileinput({
        theme: 'fa',
        language: 'fr',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif']
    });
    $('#file-es').fileinput({
        theme: 'fa',
        language: 'es',
        uploadUrl: '#',
        allowedFileExtensions: ['jpg', 'png', 'gif']
    });
    $("#file-0").fileinput({
        theme: 'fa',
        'allowedFileExtensions': ['jpg', 'png', 'gif']
    });
    $("#file-1").fileinput({
        theme: 'fa',
        uploadUrl: '#', // you must set a valid URL here else you will get an error
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
    });
    
     $(".file").on('fileselect', function(event, n, l) {
     alert('File Selected. Name: ' + l + ', Num: ' + n);
     });
     

    $("#file-3").fileinput({
        // uploadUrl: "/file-upload-batch/1",
        // uploadAsync: false,
        // minFileCount: 2,
        // maxFileCount: 5,
        // overwriteInitial: false,
        initialPreview: [
            // VIDEO DATA
           // "http://kartik-v.github.io/bootstrap-fileinput-samples/samples/Desert.jpg"//,
            "http://localhost:8000/qualitysoftware/videos/bebe.mp4",
             "http://kartik-v.github.io/bootstrap-fileinput-samples/samples/Desert.jpg"
        ],
        initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        // initialPreviewDownloadUrl: 'http://localhost:8000/qualitysoftware/videos/{filename}', // includes the dynamic `filename` tag to be replaced for each config
        initialPreviewConfig: [ 
            {
                type: "video", 
                // type: "image", 
                size: 375000,
                filetype: "video/mp4"
              //  caption: "bebe.mp4", 
                // url: "/qualitysoftware/videos/",
                // key: 3,
                // downloadUrl: 'http://localhost:8000/qualitysoftware/videos/bebe.mp4', // override url
             //   filename: 'bebe.mp4'
            },

                {
                    type: "image", 
                size: 3000,
                 filetype: "image"
                // url:

                // caption: "bebe.mp4", 
                // url: "/qualitysoftware/videos/",
                // key: 3,
                // downloadUrl: 'http://localhost:8000/qualitysoftware/videos/bebe.mp4', // override url
                //filename: 'bebe.mp4'
            }
        ]//,
        // purifyHtml: true, // this by default purifies HTML data for preview
        // uploadExtraData: {
        //     img_key: "1000",
        //     img_keywords: "happy, places"
        // }
    // }).on('filesorted', function(e, params) {
    //     console.log('File sorted params', params);
    // }).on('fileuploaded', function(e, params) {
    //     console.log('File uploaded params', params);
    });

    $("#file-4").fileinput({
        theme: 'fa',
        uploadExtraData: {kvId: '10'}
    });
    $(".btn-warning").on('click', function () {
        var $el = $("#file-4");
        if ($el.attr('disabled')) {
            $el.fileinput('enable');
        } else {
            $el.fileinput('disable');
        }
    });
    $(".btn-info").on('click', function () {
        $("#file-4").fileinput('refresh', {previewClass: 'bg-info'});
    });
    
     $('#file-4').on('fileselectnone', function() {
     alert('Huh! You selected no files.');
     });
     $('#file-4').on('filebrowse', function() {
     alert('File browse clicked for #file-4');
     });
     
    $(document).ready(function () {
        $("#test-upload").fileinput({
            'theme': 'fa',
            'showPreview': false,
            'allowedFileExtensions': ['jpg', 'png', 'gif'],
            'elErrorContainer': '#errorBlock'
        });
        $("#kv-explorer").fileinput({
            'theme': 'explorer-fa',
            'uploadUrl': '#',
            overwriteInitial: false,
            initialPreviewAsData: true,
            initialPreview: [
                "http://lorempixel.com/1920/1080/nature/1",
                "http://lorempixel.com/1920/1080/nature/2",
                "http://lorempixel.com/1920/1080/nature/3"
            ],
            initialPreviewConfig: [
                {size: 329892, width: "120px", url: "{$url}", key: 1},
                {size: 872378, width: "120px", url: "{$url}", key: 2},
                {size: 632762, width: "120px", url: "{$url}", key: 3}
            ]
        });
        
         $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
         alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
         });
         
    });
</script>
</html>