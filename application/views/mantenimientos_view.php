		 <section class="content-header">
			<h1>Mantenimientos: <small> Mantenimentos</small></h1>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-body">
					<h2 class="displayoncenter aligntitle">Mantenimientos</h2>
					<div class="filtroDePreguntas">
						<span id="newMantenimiento" class="btn btn-primary"><i class='icon-edit icon-white fa fa-plus'></i> Nuevo</span>
					</div>
				<div class="row">
				
				<div class="col-md-6">
					<div class="panel panel-default">
						<div align="center" class="panel-heading">TABLAS DE MANTENIMIENTOS</div>
						<div class="displayoncenter" id="displayTables">
							<table id = "tablaTablasMant" class="table table-striped table-bordered estiloTabla dataTableSimple">
								<thead>
									<tr>
										<th class="oculto"></th>
										<th>Código</th>
										<th>Nombre</th>
										<th>Descripción</th>
										<th>Acción</th>
									</tr>
								</thead>
								<tbody class="tableViewer" id="anclaTabla">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div align="center" class="panel-heading">MANTENIMIENTOS</div>
						<div class="displayoncenter" id="displayTable">
							<table id = "tablaMantenimientos" class="table table-striped table-bordered estiloTabla dataTableSimple">
								<thead>
									<tr>
										<th class="oculto"></th>
										<th class="oculto"></th>
										<th>Código</th>
										<th>Nombre</th>
										<th>Valor</th>
										<th>Descripción</th>
										<th>Acción</th>
									</tr>
								</thead>
								<tbody class="tableViewer" id="anclaTabla">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="modalMantenimiento" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg modalEnsanchado" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
					</div>
					<?php $alertasid = array('errorMantenimiento', 'successMantenimiento', 'warningMantenmiento'); include('template/alert_template.php'); ?>
					<div id="tabs">
					<ul class="nav nav-tabs ligeroNav">
		        		<li id="infoTabla" class="active"><a id="infoTabla" data-toggle="tab" href="#infotabla">Tablas</a></li>
						<li id="infoMantenimiento"><a id="infoMantenimiento" data-toggle="tab" href="#infomantenimientos">Mantenimientos</a></li>
		     	 	</ul>
					<div class="modal-body">
						<div class="box-body">
							<div class="tab-content">
								<div id="infotabla" class="tab-pane fade in active modalTabMargin">
									<div class="greatInputContainer">
										<form id="tablaForm">
											<input type="hidden" class="form-control mentenimientosinputs" id="idtabla">
											<div class="col-md-6">
												<div class="form-group">
													<label>Código Tabla:</label>
													<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="20" id="codigoTabla" name="codigoTabla">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Nombre Tabla:</label>
													<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="30" id="nombreTabla" name="nombreTabla">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Descripción:</label>
													<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="250" id="descripcion" name="descripcion">
												</div>
											</div>
										</form>
									</div>
								</div>
								
								<div id="infomantenimientos" class="tab-pane fade">
									<form id="matenimientoForm">
									<input type="hidden" class="form-control mentenimientosinputs" id="idmantenimiento">
									
									<div class="col-md-6">
										<div class="form-group">
											<label>Nombre Tabla:</label>
											<div id="div_idtabla">
												<select type="text" class="form-control mentenimientosinputs" id="idtbl" name="idtbl">
													<option disabled="disabled" value="">-- Seleccionar --</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Código Registro:</label>
											<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="20"id="codmantenimiento" name="codmantenimiento">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Nombre Registro:</label>
											<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="20" id="mantenimiento" name="mantenimiento">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Valor:</label>
											<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="20"id="valor" name="valor">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Descripción:</label>
											<input type="text" class="form-control mentenimientosinputs withoutTwoOrMoreSpaces" maxlength="250" id="descripcion_mant" name="descripcion_mant">
										</div>
									</div>
									<div id="container">
											<div align="right" class="col-xs-12">
												<button id="agregarMan" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; Agregar</button>
											<button type="button" id="cerrarMant" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-ban"></i> Cancelar</button>
											</div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-xs-12"></div>
											<div class="col-md-12">
												<div class="panel panel-default">
													<table id = "tblMantenimientos" class="table table-striped table-bordered estiloTabla dataTableSimple">
												        <thead>
												            <tr>
												            	<th class="oculto">id</th>
												                <th>Nombre Tabla</th>
												                <th>Código Registro</th>
												                <th>Nombre Registro</th>
												                <th>Valor</th>
												                <th>Descripción</th>
												                <th>Acciones</th>
												            </tr>
												        </thead>
												        <tbody id="tbodyMantenimientos">
												            
												        </tbody>
												    </table>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					</div>
					<div class="modal-footer">
							<div class="col-xs-12">
								<button type="button" id="enviarmant"></button>&nbsp;
								<button type="button" id="cancelar" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-ban"></i> Cancelar</button>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="mantenimientoDelete" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title" id="myModalLabel"><span id="theHeader2"></span></h4>
		        </div>
		        <div class="modal-body">
		        	<div id="sure" class="form-group">
						<p>¿Está seguro que desea eliminar el Mantenimiento: <span id="nombre_span"></span>?</p>
					</div>
		        </div>
		        <div class="modal-footer">
		          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
		          <button id= "eliminarMantenimiento" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Eliminar</button>
		          <button id= "cerrarSure" type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-times-circle"></i> Cerrar</button>
		        </div>
		      </div>
		      
		    </div>
  		</div>