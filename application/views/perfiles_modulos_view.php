<section class="content-header">
	<h1>Usuarios: <small>Perfiles por Módulos</small></h1>
</section>
<section class="content">
	<div class="box">
		
		<div class="box-body">
			<?php if($both) { ?>
				<h2 class="displayoncenter aligntitle"><?= (($perfilModulo == 1) ? "Asignar por Perfiles": "Orden por Módulos");?></h2>
				<div class="filtroDePreguntas">
					<?php if($result) { ?>
						<span id='asociarPerfilModulo' class='btn btn-success'>
							<?php $perModInfo = ($perfilModulo == 1) ? " Asignar Módulo a ".$result[0]['NAMEPROFILE'] : " Asignar Perfil a ".$result[0]['NAMEMODULE']; ?>
							<i class='icon-edit icon-white fa fa-plus'></i><?=$perModInfo?>
						</span>
					<?php } else { ?>
						<span id='asociarPerfilModulo' class='btn btn-success'>
							<?php $perModInfo = ($perfilModulo == 1) ? " Asignar Módulos" : " Asignar Perfiles"; ?>
							<i class='icon-edit icon-white fa fa-plus'></i><?=$perModInfo?>
						</span>
					<?php } ?>
				</div>
			<?php } else { ?>
				<h2 class="displayoncenter aligntitle">Orden por Ambos</h2>
			<?php } ?>
			<div class="displayoncenter">
				<table class="table table-striped table-bordered estiloTabla datatable">
					<thead>
						<tr>
							<th class="oculto"></th>
							<?php if(($both == false)||($perfilModulo == 1)) { ?>
								<th class="oculto"></th>
								<th class="oculto">Perfil</th>
								<th class="oculto"></th>
								<th>Módulo</th>
							<?php }else{ ?>
								<th class="oculto"></th>
								<th class="oculto">Módulo</th>
								<th class="oculto"></th>
								<th>Perfil</th>
							<?php } ?>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody id="anclaTabla">
						<?php if($result) {
							foreach($result as $fila) { ?>
							<tr>
								<td class="oculto"><?=$fila['PROFMODULEID']?></td>
								<?php if(($both == false)||($perfilModulo == 1)) { ?>
									<td class="oculto"><?=$fila['PROFILEID']?></td>
									<td class="oculto"><?=$fila['NAMEPROFILE']?></td>
									<td class="oculto"><?=$fila['MODULEID']?></td>
									<td><?=$fila['NAMEMODULE']?></td>
								<?php } else { ?>
									<td class="oculto"><?=$fila['MODULEID']?></td>
									<td class="oculto"><?=$fila['NAMEMODULE']?></td>
									<td class="oculto"><?=$fila['PROFILEID']?></td>
									<td><?=$fila['NAMEPROFILE']?></td>
								<?php } ?>
								<td>
								<?php if($fila['PROFMODSTATUS'] == 0) { ?>
									<span class='asociacion btn btn-success botonVED'>Habilitar</span>
								<?php } else { ?>
									<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>
								<?php } ?>
								</td>
							</tr>
						<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<?php if ($both) { ?>
	<div class="modal fade" id="modalPerfilesModulos" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span id="theHeader">Nueva Asociación</span></h4>
				</div>
				<?php $alertasid = array('errorPerfilesModulos', 'successPerfilesModulos', 'warningPerfilesModulos'); include('template/alert_template.php'); ?>
				<div class="modal-body">
					<div class="box-body">
						<div class="form-group">
							<input type="hidden" class="form-control" id="id_perfil_modulo">
							<p id="profileOrModule" hidden="true"><?= $idVal; ?></p>
						</div>
						<?php if($perfilModulo == 2) { ?>
						<div id="perfil_container" class="form-group">
							<label>Perfil:</label>
							<select class="form-control selectBuscar" id="perfil_permod">
								<option value="0">Seleccione un Perfil</option>
								<?php foreach($perfil as $fila) { ?>
									<?= ($fila['HABILITADO'] == 1) ? '<option value="'.$fila['PROFILEID'].'" disabled>'.$fila['NAMEPROFILE'].'</option>': '<option value="'.$fila['PROFILEID'].'">'.$fila['NAMEPROFILE'].'</option>'; ?>
								<?php } ?>
							</select>
						</div>
						<?php } else { ?>
						<div id="modulo_container" class="form-group">
							<label>Módulo:</label>
							<select class="form-control selectBuscar" id="modulo_permod">
								<option value="0">Seleccione un Módulo</option>
								<?php foreach($modulo as $fila) { ?>
									<?= ($fila['HABILITADO'] == 1) ? '<option value="'.$fila['MODULEID'].'" disabled>'.$fila['NAMEMODULE'].'</option>': '<option value="'.$fila['MODULEID'].'">'.$fila['NAMEMODULE'].'</option>'; ?>
								<?php } ?>
							</select>
						</div>
						<?php } ?>
						<div id="addContainer">
							<div id="addLeft" class="leftContainer"></div>
							<div id="addRight" class="rightContainer"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-xs-4">
						<button type="button" id="enviarPermod" class="btn btn-primary btn-block btn-flat">Guardar</button>
					</div>
					<div class="col-xs-4">
						<button type="button" id="cerrarPermod" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
