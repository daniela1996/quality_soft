	<!--Modal de Vehiculos-->
	<div class="modal fade" id="modalvehiculos" role="dialog" aria-labelledby = "myModalLabel">
		<div class="modal-dialog modalEnsanchado" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
				</div>
				<?php $alertasid = array('errorVehiculos', 'successVehiculos', 'warningVehiculos'); include('template/alert_template.php'); ?>
				<div class="modal-body">
					<div class="box-body">
						<div class="tab-content">
							<div id="infoprincipalMenu" class="tab-pane fade in active modalTabMargin">
								<div id= "InformacionVehiculo"></div>
								<div id= "modalcompleto" class="greatInputContainer">
									<form id="vehiculosForm">
										<input type="hidden" class="form-control vehiculosinputs" id="id">
										<div class="row">
											<div class="form-group col-md-6">
												<label>GPS:</label>
												<input type="text" class="form-control vehiculosinputs" id="imei" name="imei">
											</div>
											<div class="form-group col-md-6">
												<label>Marca:</label>
												<input type="text" class="form-control vehiculosinputs" id="marca" name="marca">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label>Modelo:</label>
												<input type="text" class="form-control vehiculosinputs" id="modelo" name="modelo">
											</div>
											<div class="form-group col-md-6">
												<label>Color:</label>
												<input type="text" class="form-control vehiculosinputs" id="color" name="color">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label>Año:</label>
												<input type="text" class="form-control vehiculosinputs" id="year" name="year">
											</div>
											<div class="form-group col-md-6">
												<label>Placa:</label>
												<input type="text" class="form-control vehiculosinputs" id="plate" name="plate">
											</div>
										</div>										
										<div class="row">
											<div class="form-group col-md-6">
												<label>Motor:</label>
												<input type="text" class="form-control vehiculosinputs" id="motor" name="motor">
											</div>
											<div class="form-group col-md-6">
												<label>Chasis:</label>
												<input type="text" class="form-control vehiculosinputs" id="chasis" name = "chasis">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label>Compañía:</label>
												<input type="text" class="form-control vehiculosinputs" id="compania" name="compania">
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-xs-4">
						<button type="button" id="enviarVehiculo" class="btn btn-primary btn-block btn-flat">Guardar</button>
					</div>
					<div class="col-xs-4">
						<button type="button" id="cerrarModalVehiculos" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!--Modal de gestiongps-->
	<div class="modal fade" id="modalgestiongps" role="dialog" aria-labelledby = "myModalLabel">
		<div class="modal-dialog modalEnsanchado" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span id="theHeader2"></span></h4>
				</div>
				<?php $alertasid = array('errorGestion', 'successGestion', 'warningGestion'); include('template/alert_template.php'); ?>
				<div class="modal-body" style="max-height: 450px; overflow-y: auto; overflow-x: hidden;">
					<div class="box-body">
						<div class="tab-content">
							<div id="infoprincipalMenu" class="tab-pane fade in active modalTabMargin">
								<div id="InformacionCompleta"></div>
								<div id = "modalcomplete" class="greatInputContainer">
									<form id="GPSForm">
										<input type="hidden" class="form-control gestiongpsinputs" id="autoid">
										<div class="row">
											<div class="form-group col-md-6">
												<label>País:</label>
												<div id="div_pais">
													<select type="text" class="form-control gestiongpsinputs" id="pais" name="pais">
														<option value="">Seleccionar...</option>
														<?php foreach($pais as $fila) { ?>
														<?= '<option value="'.$fila['COD_PAIS'].'">'.$fila['NOMBRE'].'</option>' ?>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="form-group col-md-6">
												<label>Departamento:</label>
												<div id="div_depto">
													<select type="text" class="form-control gestiongpsinputs" id="depto" name="depto">
														<option value="">Seleccionar...</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label>Municipio:</label>
												<input type="text" class="form-control gestiongpsinputs" id="municipio" name="municipio" maxlength="50">
											</div>
											<div class="form-group col-md-6">
												<label>Alias:</label>
												<input type="text" class="form-control gestiongpsinputs" id="descripcion" name="descripcion" maxlength="100">
											</div>
										</div>
										<div class="row">	
											<div class="form-group col-md-6">
												<label>Cod. Nodo:</label>
												<input type="text" class="form-control gestiongpsinputs" id="cod_vehiculo" name="cod_vehiculo" maxlength="50">
											</div>
											<div class="form-group col-md-6">
												<label>Celular:</label>
												<input type="text" class="form-control gestiongpsinputs" id="celular" name="celular" maxlength="50">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label>Cuenta:</label>
												<input type="text" class="form-control gestiongpsinputs" id="cuenta" name="cuenta" maxlength="50">
											</div>
											<div class="form-group col-md-6">
												<label>Galones:</label>
												<input type="text" class="form-control gestiongpsinputs" id="galones" name="galones" maxlength="50">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label>Voltaje mínimo:</label>
												<input type="text" class="form-control gestiongpsinputs" id="voltajemin" name="voltajemin" maxlength="50">
											</div>
											<div class="form-group col-md-6">
												<label>Voltaje:</label>
												<input type="text" class="form-control gestiongpsinputs" id="voltaje" name="voltaje" maxlength="50">
											</div>
										</div>
										<!-- <div class="row">
											<div class=" col-md-12">
												<label>Alias:</label>
												<textarea type="text" class="form-control gestiongpsinputs" id="descripcion" name="descripcion" maxlength="500"></textarea>
											</div>
										</div> -->
									</form>
								</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div class="col-xs-4">
							<button type="button" id="enviargestion" class="btn btn-primary btn-block btn-flat">Guardar</button>
						</div>
						<div class="col-xs-4">
							<button type="button" id="cerrarModalGestion" class="btn btn-default btn-block btn-flat" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
		</div>