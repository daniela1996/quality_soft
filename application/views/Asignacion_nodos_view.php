<section class="content-header">
	<h1>GPS: <small> Asignación de Vehículos</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Asignación de Vehículos</h2>
			<div class="filtroDePreguntas btn-group">
    			<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Agregar Asignación <span class="caret"></span>
    			</button>
    			<ul class="dropdown-menu">
      			<li><a id = 'newAsignacion'>Asignar vehículo a usuarios</a></li>
      			<li><a id = 'newAsignacionUser'>Asignar usuario a vehículos</a></li>
    			</ul>
  			</div>
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable" id = "TblNodos">
					<thead>
						<tr>
							<th class="oculto"></th>
							<th class="center cargarInfo">GPS</th>
							<th class="oculto"></th>
							<th class="center cargarInfo">Usuario</th>
							<th class="center cargarInfo">Fecha Inicio</th>
							<th class="center cargarInfo">Fecha Final</th>
							<th class="oculto"></th>
							<th class="center cargarInfo">Tipo asignación</th>
							<th class="center cargarInfo">Acción</th>
						</tr>
					</thead>
					<tbody class="tableViewer" id="anclaTabla">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="modalAsignacion" role="dialog" aria-labelledby = "myModalLabel">
		<div id="principal"></div>
		<div class="modal-dialog modalEnsanchado" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
				</div>
				<?php $alertasid = array('errorAsignacion', 'successAsignacion', 'warningAsignacion'); include('template/alert_template.php'); ?>
				<div class="modal-body" style="max-height: 500px; overflow-y: auto; overflow-x: hidden;">
					<div class="box-body">
						<div id="sure" class="form-group">
							<p id= "pregunta" class="displayoncenter aligntitle"></p>
							<input type="text" id="auto_id" class="oculto">
							<input type="text" id="imei" class="oculto">
							<input type="text" id="cod_us" class="oculto">
						</div>
						<div class="tab-content">
							<div id="infoprincipalMenu" class="tab-pane fade in active modalTabMargin">
								<div id= "modalcompleto" class="greatInputContainer">
									<form id = "asignacionesForm">
									 <div class="form-check">
    									<label class="form-check-label">
      									<input type="checkbox" class="form-check-input" id="temporal">
      									Asignación temporal
    									</label>
  									</div>
									<div class="row oculto" id = "fechas">
											<div class="form-group col-md-6">
												<label>Fecha de Inicio:</label>
                    								<input type='text' class="form-control asignacionesinput" placeholder="dd/mm/yyyy" id='FechaInicial' name = 'FechaInicial'/>
											</div>
											<div class="form-group col-md-6">
												<label>Fecha de Finalización:</label>
                    								<input type='text' class="form-control asignacionesinput" id='FechaFinal' name = 'FechaFinal' placeholder="dd/mm/yyyy" />
											</div>
									</div>
									<div class="row" id = 'tabs'>
											<div class="form-group col-md-6" id = "tab1">
											<div class="displayoncenter" id="displayTables">
											<table class="table table-striped table-bordered estiloTabla datatables" id = "TblNodoId">
												<thead>
													<tr>
														 <th class="oculto"></th>
														<th class="center cargarInfo">GPS</th>
													</tr>
												</thead>
												<tbody class="tableViewer" id="anclaTabla">
												</tbody>
											</table>
											</div>
											</div>

											<div class="form-group col-md-6" id = "tab2">
											<div class="displayoncenter" id="displayTableUser">
											<table class="table table-striped table-bordered estiloTabla datatables" id = "TblUserId">
												<thead>
													<tr>
														<th class="oculto"></th>
														<th class="center cargarInfo">Usuario</th>
													</tr>
												</thead>
												<tbody class="tableViewer" id="anclaTabla">
												</tbody>
											</table>
											</div>
										</div>
									</div>
									<div class = "row">
											<div class = "form-group col-md-12">
												<label>Descripción:</label>
												<textarea type="text" class="form-control asignacionesinput" id="descripcion" name="descripcion" maxlength="200"></textarea>
											</div>
									</div>
								</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
						<div class="col-xs-4">
							<button type="button" id="guardarAsig" class="btn btn-primary btn-block btn-flat">Guardar</button>
						</div>
						<div class="col-xs-4">
							<button type="button" id="cerrarModalAsignacion" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
						</div>
				</div>
			</div>
		</div>
	</div>