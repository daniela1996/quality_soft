<section class="content-header">
	<h1>GPS: <small> Información de Vehículos</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Información de Vehículos</h2>
			<div class="filtroDePreguntas">
				<span id="newVehiculo" class="btn btn-success"><i class='icon-edit icon-white fa fa-plus'></i> Agregar vehículo</span>
			</div>
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable" id="TblVehiculos">
					<thead>
						<tr>
							<th class ="oculto"></th>
							<th class="center cargarInfo">GPS</th>
							<th class="center cargarInfo">Marca</th>
							<th class="center cargarInfo">Modelo</th>
							<th class="center cargarInfo">Color</th>
							<th class="center cargarInfo">Año</th>
							<th class="center cargarInfo">Placa</th>
							<th class="center cargarInfo">Motor</th>
							<th class="center cargarInfo">Chasis</th>
							<th class="center cargarInfo">Compañía</th>
							<th class="center cargarInfo">Acción</th>
						</tr>
					</thead>
					<tbody class="tableViewer" id="anclaTabla">
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
