<section class="content-header">
			<h1>Prueba: <small> Prueba</small></h1>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-body">
					<h2 class="displayoncenter aligntitle">Prueba</h2>
					<!-- <div class="filtroDePreguntas">
						<span id="newMantenimiento" class="btn btn-primary"><i class='icon-edit icon-white fa fa-plus'></i> Nuevo</span>
					</div> -->
					<form id="principalForm">
					<div class="col-md-4">
						<div class="form-group">
							<label>Proyectos:</label>
							<div id="div_proyecto">
								<select type="text" class="form-control pruebasinputs" id="proyecto" name="proyecto">
									<option disabled="disabled" value=""></option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Módulos:</label>
							<div id="div_modulo">
								<select type="text" class="form-control pruebasinputs" id="modulo" name="modulo">
									<option disabled="disabled" value=""></option>
								</select>
							</div>
						</div>
					</div>
					<input type="hidden" class="form-control pruebasinputs" id="ciclo2">
					<input type="hidden" class="form-control pruebasinputs" id="data">
					</form>
					<!-- <div class="col-md-4"> -->
						<!-- <div class="form-group"> -->
							<div class="filtro btn-group">
								<button type="button" id="accionesCasoDePrueba" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Acciones <span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li><a id="agregarPrueba"><i class='fa fa-plus'></i>Agregar</a></li>
									<li><a id="btnFinishCycle"><span class="glyphicon glyphicon-check"></span>Finalizar Ciclo</a></li>
									<!-- <li><a>Enviar Correo</a></li> -->
								</ul>
							</div>
							<!-- <button id="agregarPrueba" type="button" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp; Agregar</button> -->
							<!-- <button data-toggle="tooltip" title="" class="btn btn-success" id="btnFinishCycle" name="btnTableEdit" disabled><span class="glyphicon glyphicon-check"></span></button> -->
						<!-- </div> -->
					<!-- </div> -->


						<div class="col-md-12">
							<div class="panel panel-default">
								<div align="center" class="panel-heading">Casos de Pruebas<span id="nombre_modulo2"></span><span id="numero_ciclo2"></span></div>
								<div class="displayoncenter" id="displayTables">
									<button class='up'><span class='glyphicon glyphicon-chevron-up'></span></button><button class='down'><span class='glyphicon glyphicon-chevron-down'></span></button>
									<table id = "tblPruebas" class="table table-striped table-bordered estiloTabla dataTableSimple">
										<thead>
											<tr>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<th>Caso de Prueba</th>
												<th>Tipo de Caso</th>
												<th>Fecha Inicial</th>
												<th>Fecha Final</th>
												<th>Resultados</th>
												<th>Observaciones</th>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<th class="oculto"></th>
												<!-- <th class="oculto"></th> -->
												<th>Acción</th>
												<!-- <th>Mover Fila</th> -->
											</tr>
										</thead>
										<tbody class="tableViewer" id="anclaTabla">
										</tbody>
									</table>
								</div>
							</div>
						</div>
					
				</div>
			</div>
		</section>
		<div class="modal fade" id="modalPrueba" role="dialog">
		    <div class="modal-dialog modal-lg">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
		        </div>
		        <?php $alertasid = array('errorPruebas', 'successPruebas', 'warningPruebas'); include('template/alert_template.php'); ?>
		        <div class="modal-body">
		        	<div class="alert alert-danger alert-dismissable hidden" id = "fechaFinal">
						<strong>¡Error!</strong> No ha ingresado una fecha incial.
					</div>
					<div class="alert alert-danger alert-dismissable hidden" id = "chkboxIncidencias">
						<strong>¡Error!</strong> No ha marcado ningún tipo de incidencia y ha seleccionado un caso de pruebas con incidencia.
					</div>
					<div class="alert alert-danger alert-dismissable hidden" id = "selectorCriticidad">
						<strong>¡Error!</strong> No ha seleccionado un tipo de criticidad.
					</div>
					<div class="alert alert-danger alert-dismissable hidden" id = "selectorEstadoIncidencia">
						<strong>¡Error!</strong> No ha seleccionado un estado de incidencia.
					</div>
					<div class="alert alert-danger alert-dismissable hidden" id = "resultado">
						<strong>¡Error!</strong> No puede cambiar el resultado porque tiene una dependencia de incidencia seleccionada.
					</div>
		        	<form id="tablaForm">
		        		<input type="hidden" class="form-control pruebasinputs" id="idProyecto">
		        		<input type="hidden" class="form-control pruebasinputs" id="idModulo">
		        		<input type="hidden" class="form-control pruebasinputs" id="idPrueba">
		        		
			        	<div class="col-md-6">
							<div class="form-group">
								<label>Caso de Prueba:</label><span id="numero_resultado" style='visibility:hidden'></span>
								<input type="text" class="form-control pruebasinputs withoutTwoOrMoreSpaces" maxlength="150" id="casoPrueba" name="casoPrueba">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Tipo de Caso:</label>
								<div id="div_tipoCaso">
									<select type="text" class="form-control pruebasinputs" id="tipoCaso" name="tipoCaso">
										<option disabled="disabled" value=""></option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
						 		<label for="initialdate" class="control-label">Fecha Estimada Inicial:</label>
						  		<input id="initialdate" name="initialdate" type="text" class="form-control fechas" autocomplete="off"/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							  <label for="finaldate" class="control-label">Fecha Estimada Final:</label>
							  <input id="finaldate" name="finaldate" type="text" class="form-control fechas" autocomplete="off"/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Resultados:</label>
								<div id="div_idtabla">
									<select type="text" class="form-control pruebasinputs" id="results" name="results">
										<option disabled="disabled" value=""></option>
									</select>
								</div>
							</div>
						</div>
						<div id="incidencia" oculto>
							<!-- CHECKLIST -->
								<div class="col-md-12">	
									<label>Tipo de Incidencia: &nbsp; </label>
									<ul style="list-style-type: none;" id="incidencias">
									</ul>
								</div>
								<!-- END CHECKLIST -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Criticidad:</label>
									<div id="div_criticidad">
										<select type="text" class="form-control pruebasinputs" id="criticidad" name="criticidad">
											<option disabled="disabled" value=""></option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Estado de Incidencia:</label>
									<div id="div_estadoIncidencia">
										<select type="text" class="form-control pruebasinputs" id="estadoIncidencia" name="estadoIncidencia">
											<option disabled="disabled" value=""></option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="col-md-6">
							<div class="form-group">
								<label>Ciclo de Pruebas:</label>
								<input type="text" class="form-control pruebasinputs" id="ciclo" name="ciclo">
							</div>
						</div> -->
						<div class="col-md-12">
							<div class="form-group">
								<label>Observaciones:</label>
								<textarea type="text" class="form-control pruebasinputs withoutTwoOrMoreSpaces" id="observacion" maxlength="250" name="observacion"></textarea>
							</div>
						</div>
						<input type="hidden" class="form-control pruebasinputs" id="ciclo">
						<input type="hidden" class="form-control pruebasinputs" id="posicion">
						<!-- <div class="col-md-12">
						  	<div class="form-group">
						  		<label class="checkbox-inline text-bold"><input id = "chk" type="checkbox" value="1">¿Agregar Evidencias?</label>
							</div>
						</div> -->
						<!-- <div id="img" class="form-group"> -->
							<!-- <div class="col-md-6">
								<div class="form-group">
									<label>Caso de Prueba:</label>
									<input type="text" class="form-control pruebasinputs" id="prueba" name="prueba" disabled>
								</div>
							</div> -->
							<div class="col-md-6">
								<div class="form-group">
									<label>Resultado Esperado:</label>
									<input type="text" class="form-control pruebasinputs withoutTwoOrMoreSpaces" maxlength="250" id="esperado" name="esperado">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Resultado Obtenido:</label>
									<input type="text" class="form-control pruebasinputs withoutTwoOrMoreSpaces" maxlength="250" id="obtenido" name="obtenido">
								</div>
							</div>
							<!-- <form id="frm-example" name="frm-example">
								<div class="col-md-12" id="uploadFile_div">
									<label>Evidencias:</label>
									<div class="col-md-12">
										<img id="imagenesView" src="">
										<input id="imagenes" name="imagenes[]" type="file" multiple/>
									</div>
								</div>
							</form> -->
							<!-- <div class="col-md-6">
							<video>
						     	<source type="video/mp4" src="">
							</video>
							</div> -->

							<!-- ES LA CORRECTA -->
							<!-- <form enctype="multipart/form-data"> -->
								<div class="col-md-12">
									<label>Evidencias:</label>
									<div class="form-group">	
										<div class="file-loading col-md-12">
											<!-- <img id="imagenesView" src=""> -->
											<!-- <img type="hidden" id="imagenesClonacion" src=""> -->
											<input id="imagenes" name="imagenes[]" type="file" enctype="multipart/form-data" showUpload="false" multiple>
										</div>
									</div>
								</div>
								<!-- <div class="col-md-12">
									<div class="form-group">
										<button id="clear" type="button">Limpiar</button>
									</div>
								</div> -->
							<!-- </form> -->
							<!-- <form enctype="multipart/form-data">
								<div class="col-md-12">
									<label>Evidencias:</label>
									<div class="form-group">	
										<div class="file-loading col-md-12">
											<input id="video" name="video[]" type="file" multiple class="file" data-show-upload="false" showDrag="true" data-show-preview="true" dropZoneEnabled= "true">
										</div>
									</div>
								</div>
							</form> -->


						<!-- </div> -->
					</form>
		        </div>
		        <div class="modal-footer">
		        	<div class="col-md-12">
			          <button id="enviarPrueba" type="button"></button>
			          <button id="cerrarPrueba" type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-ban"></i> Cancelar</button>
		          	</div>
		        </div>
		      </div>
		      
		    </div>
  		</div>
  		<div class="modal fade" id="modalClonacion" role="dialog">
		    <div class="modal-dialog modal-lg">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title" id="myModalLabel"><span id="theHeader4"></span></h4>
		        </div>
		        <?php $alertasid = array('errorPruebas', 'successPruebas', 'warningPruebas'); include('template/alert_template.php'); ?>
		        <div class="modal-body">
		        	<form id="tablaForm2">
		        		<div class="col-md-6">
							<div class="form-group">
								<label>Caso de Prueba:</label>
								<input type="text" class="form-control pruebasinputs" id="casoPrueba2" name="casoPrueba2">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Proyectos:</label>
								<div id="div_proyecto2">
									<select type="text" class="form-control pruebasinputs" id="proyecto2" name="proyecto2">
										<option disabled="disabled" value=""></option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Módulos:</label>
								<div id="div_modulo2">
									<select type="text" class="form-control pruebasinputs" id="modulo2" name="modulo2">
										<option disabled="disabled" value=""></option>
									</select>
								</div>
							</div>
						</div>
					</form>
		        </div>
		        <div class="modal-footer">
		        	<div class="col-md-12">
			          <button id="enviarPrueba2" type="button"></button>
			          <button id="cerrarPrueba2" type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-ban"></i> Cancelar</button>
		          	</div>
		        </div>
		      </div>
		      
		    </div>
  		</div>
  		<div class="modal fade" id="modalDelete" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title" id="myModalLabel"><span id="theHeader2"></span></h4>
		        </div>
		        <div class="modal-body">
		        	<div id="sure" class="form-group">
						<p>¿Está seguro que desea eliminar el Caso de Prueba: <span id="nombre_span"></span>?</p>
					</div>
		        </div>
		        <div class="modal-footer">
		          <button id= "eliminarPrueba" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i> Eliminar</button>
		          <button id= "cerrarSure" type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-times-circle"></i> Cerrar</button>
		        </div>
		      </div>
		      
		    </div>
  		</div>
  		<div class="modal fade" id="modalCycle" role="dialog">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title" id="myModalLabel"><span id="theHeader3"></span></h4>
		        </div>
		        <div class="modal-body">
		        	<form id="cicloForm">
			        	<div align="center" id="sure" class="form-group">
							<p>¿Está seguro que desea finalizar el ciclo <span id="numero_ciclo"></span> del módulo <span id="nombre_modulo"></span>?</p>
							<!-- <div class col="col-md-12"> -->
								<p>Observación del Módulo:</p>
								<!-- <div class col="col-md-12"> -->
									<textarea rows="4" class="form-control withoutTwoOrMoreSpaces" cols="50" maxlength="150" id="obsModulo"></textarea>
								<!-- </div> -->
							<!-- </div> -->
						</div>
		        	</form>
		        </div>
		        <div class="modal-footer">
		          <button id= "finalizarCiclo" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-ok-sign"></i> Finalizar</button>
		          <button id= "cerrarCiclo" type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #f4f4f4; color:black"><i class="fa fa-ban"></i> Cancelar</button>
		        </div>
		      </div>
		      
		    </div>
  		</div>

<style type="text/css">
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}

#accionesCasoDePrueba{
	margin: 24px;
	margin-bottom: 0px;
	margin-left: 0px;
}

#imagenesView{
	width: 250px;
	height: 250px;
}
</style>