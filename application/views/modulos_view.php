<section class="content-header">
	<h1>Usuarios:<small> Módulos</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Módulos</h2>
			<div class="filtroDePreguntas">
				<span id="newModulo" class="btn btn-success"><i class='icon-edit icon-white fa fa-plus'></i> Agregar Módulo</span>
			</div>
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable">
					<thead>
						<tr>
							<th class="oculto"></th>
							<th>Código</th>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Fecha</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody id="anclaTabla">
						<?php if($modulos) {
							foreach($modulos as $fila) { ?>
							<tr>
								<td class="oculto"><?=$fila['MODULEID']?></td>
								<td><?=$fila['CODEMODULE']?></td>
								<td><?=$fila['NAMEMODULE']?></td>
								<td><?=$fila['DESCMODULE']?></td>
								<td><?=$fila['DATES']?></td>
								<td>
									<a href="perfilesmodulos?pm=2&vl=<?php echo $fila['MODULEID'];?>">
										<span class='linkModulo btn btn-warning botonVED' data-toggle="tooltip" data-placement="right" title="Asignar">
										<i class='icon-edit icon-white fa fa-list'></i></span>
									</a>
									<span class='editModulo btn btn-success botonVED' data-toggle="tooltip" data-placement="right" title="Editar">
										<i class='icon-edit icon-white fa fa-pencil'></i>
									</span>
									<span class='deleteModulo btn btn-danger botonVED' data-toggle="tooltip" data-placement="right" title="Eliminar">
										<i class='icon-edit icon-white fa fa-trash-o'></i>
									</span>
								</td>
							</tr>
						<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="modalModulos"   role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
			</div>
			<?php $alertasid = array('errorModulos', 'successModulos', 'warningModulos'); include('template/alert_template.php'); ?>
			<div class="modal-body">
				<div class="box-body">
					<div class="form-group">
						<input type="hidden" class="form-control moduleinputs" id="id_modulo">
					</div>
					<div id="sure" class="form-group">
						<p>¿Esta seguro que desea eliminar al Modulo: <span id="nombre_span"></span>?</p>
					</div>
					<form id="modulosForm">
					<div class="greatInputContainer">
						<div id="codigo_container" class="form-group">
							<label>Código:</label>
							<input type="text" class="form-control moduleinputs letrassinespacios" id="codigo_modulo" name="codigo_modulo">
						</div>
						<div id="nombre_container" class="form-group">
							<label>Nombre:</label>
							<input type="text" class="form-control moduleinputs letras" id="nombre_modulo" name="nombre_modulo">
						</div>
						<div id="descripcion_container" class="form-group">
							<label>Descripción:</label>
							<input type="text" class="form-control moduleinputs" id="descripcion_modulo" name="descripcion_modulo">
						</div>
					</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-xs-4">
					<button type="button" id="enviarModulo" class="btn btn-primary btn-block btn-flat">Guardar</button>
				</div>
				<div class="col-xs-4">
					<button type="button" id="cerrarModulo" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
