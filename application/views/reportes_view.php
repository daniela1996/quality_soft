<section class="content-header">
			<h1>Reportes: <small> Reportes</small></h1>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-body">
					<h2 class="displayoncenter aligntitle">Reportes</h2>

						<div class="col-md-6">
							<div class="panel panel-default">
								<div align="center" class="panel-heading">Reporte Detalle Módulos<span id="nombre_modulo2"></span><span id="numero_ciclo2"></span></div>
								<div class="panel-body" id="displayTables">
									<div class="form-group">
										<label>Proyectos:</label>
										<div id="div_proyecto">
											<select type="text" class="form-control pruebasinputs" id="proyecto" name="proyecto">
												<option disabled="disabled" value=""></option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<button id="reporte_modulos" type="button" disabled="disabled"></button>
										</div>
										<div class="col-md-6">
												<button id="enviarModulo" class="btn btn-primary pull-right" disabled><i class='glyphicon glyphicon-envelope'></i>&nbsp;&nbsp;Enviar reporte</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-default">
								<div align="center" class="panel-heading">Reporte Incidencias<span id="nombre_modulo2"></span><span id="numero_ciclo2"></span></div>
								<div class="panel-body" id="displayTables">
									<div class="form-group">
										<label>Proyectos:</label>
										<div id="div_proyecto">
											<select type="text" class="form-control pruebasinputs" id="proyecto1" name="proyecto1">
												<option disabled="disabled" value=""></option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<button id="reporte_incidencias" type="button" disabled="disabled"></button>
										</div>
										<div class="col-md-6">
												<button id="enviarIncidencias" class="btn btn-primary pull-right" disabled><i class='glyphicon glyphicon-envelope'></i>&nbsp;&nbsp;Enviar  reporte</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-default">
								<div align="center" class="panel-heading">Reportería Diaria<span id="nombre_modulo2"></span><span id="numero_ciclo2"></span></div>
								<div class="panel-body" id="displayTables">
									<div class="form-group">
										<label>Proyectos:</label>
										<div id="div_proyecto">
											<select type="text" class="form-control pruebasinputs" id="proyecto2" name="proyecto2">
												<option disabled="disabled" value=""></option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<button id="reportería_diaria" type="button" disabled="disabled"></button>
										</div>
										<div class="col-md-6">
												<button id="enviarDiaria" class="btn btn-primary pull-right" disabled><i class='glyphicon glyphicon-envelope'></i>&nbsp;&nbsp;Enviar  reporte</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-default">
								<div align="center" class="panel-heading">Matriz De Casos<span id="nombre_modulo2"></span><span id="numero_ciclo2"></span></div>
								<div class="panel-body" id="displayTables">
									<div class="form-group">
										<label>Proyectos:</label>
										<div id="div_proyecto">
											<select type="text" class="form-control pruebasinputs" id="proyecto3" name="proyecto3">
												<option disabled="disabled" value=""></option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<button id="matriz" type="button" disabled="disabled"></button>
										</div>
										<div class="col-md-6">
												<button id="enviarMatriz" class="btn btn-primary pull-right" disabled><i class='glyphicon glyphicon-envelope'></i>&nbsp;&nbsp;Enviar  reporte</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					
				</div>
			</div>
		</section>