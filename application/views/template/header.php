<? header("Content-Type: text/html; charset=UTF-8"); ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Quality Software</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" type="text/css" href="<?= base_url();?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/fileinput/css/fileinput.css" media="all">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="<?= base_url();?>plugins/fileinput/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>
   
    <!-- <script src="<?= base_url();?>plugins/fileinput/theme2.js" type="text/javascript"></script> -->
    <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/fullcalendar/fullcalendar.print.css" media="print">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/cropper/cropper.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/alertify/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>dist/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>dist/css/centralStyle.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>dist/css/someStyle.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>dist/css/comprasStyle.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/datepicker/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/datatables/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>dist/css/jquery.faloading.css"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/jquery-timepicker/jquery.timepicker.css" /> -->
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>plugins/jquery-timepicker/lib/bootstrap-datepicker.css" />
    
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
    <script src="<?= base_url(); ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>plugins/fileinput/js/plugins/sortable.js"></script>
    <script type="text/javascript" src="<?= base_url();?>plugins/fileinput/js/fileinput.js"></script>
    <script type="text/javascript" src="<?= base_url();?>plugins/fileinput/js/locales/fr.js"></script>
    <script type="text/javascript" src="<?= base_url();?>plugins/fileinput/js/locales/es.js"></script>
    <script src="<?= base_url();?>plugins/fileinput/themes/explorer-fa/theme.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>


    <script src="<?= base_url(); ?>plugins/jquery-validation/1.17.0/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>plugins/jquery-validation/1.17.0/localization/messages_es.min.js"></script>
    <script src="<?= base_url(); ?>plugins/jquery-validation/1.17.0/additional-methods.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="crossorigin="anonymous"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" rel="stylesheet" type="text/css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <!-- FileStyle -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/1.2.3/bootstrap-filestyle.js"></script> -->
    <!-- <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <!-- SELECTPICKER -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <a href="<?= base_url();?>index.php/home" class="logo">
          <span class="logo-mini"><b>QS</b></span>
          <span class="logo-lg"><b>Quality Software</b></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <?php if ($image) { ?>
                              <img src="data:image/jpeg;base64,<?=$image?>" class="img-circle" alt="User Image">
                            <?php } else { ?>
                              <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                            <?php } ?>
                          </div>
                          <h4> Support Team <small><i class="fa fa-clock-o"></i> 5 mins</small></h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <?php if ($image) { ?>
                              <img src="data:image/jpeg;base64,<?=$image?>" class="img-circle" alt="User Image">
                            <?php } else { ?>
                              <img src="../dist/img/user2-128x128.jpg" class="img-circle" alt="User Image">
                            <?php } ?>
                          </div>
                          <h4> AdminLTE Design Team <small><i class="fa fa-clock-o"></i> 2 hours</small></h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <?php if ($image) { ?>
                              <img src="data:image/jpeg;base64,<?=$image?>" class="img-circle" alt="User Image">
                            <?php } else { ?>
                              <img src="../dist/img/user2-128x128.jpg" class="img-circle" alt="User Image">
                            <?php } ?>
                          </div>
                          <h4> Developers <small><i class="fa fa-clock-o"></i> Today</small></h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <?php if ($image) { ?>
                              <img src="data:image/jpeg;base64,<?=$image?>" class="img-circle" alt="User Image">
                            <?php } else { ?>
                              <img src="../dist/img/user2-128x128.jpg" class="img-circle" alt="User Image">
                            <?php } ?>
                          </div>
                          <h4> Sales Department <small><i class="fa fa-clock-o"></i> Yesterday</small></h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <?php if ($image) { ?>
                              <img src="data:image/jpeg;base64,<?=$image?>" class="img-circle" alt="User Image">
                            <?php } else { ?>
                              <img src="../dist/img/user2-128x128.jpg" class="img-circle" alt="User Image">
                            <?php } ?>
                          </div>
                          <h4> Reviewers <small><i class="fa fa-clock-o"></i> 2 days</small></h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <h3>Design some buttons<small class="pull-right">20%</small></h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <h3>Create a nice theme<small class="pull-right">40%</small></h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">40% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <h3>Some task I need to do<small class="pull-right">60%</small></h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">60% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <h3>Make beautiful transitions<small class="pull-right">80%</small></h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown user user-menu" id="SingOut">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded= "false">
                  <?php if ($image) { ?>
                    <img src="data:image/jpeg;base64,<?=$image?>" class="user-image" alt="User Image">
                  <?php } else { ?>
                    <img src="../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <?php } ?>
                  <span class="hidden-xs"><?= ' '.$fname.' '; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <?php if ($image) { ?>
                      <img src="data:image/jpeg;base64,<?=$image?>" class="img-circle" alt="User Image">
                    <?php } else { ?>
                      <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <?php } ?>
                    <p><?=' '.$fname.' ';?> - Web Developer <small> Member since <?=' '.$dates.' ';?></small></p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a data-toggle="modal" data-target="#perfil" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?= base_url();?>index.php/home/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <script type="text/javascript">
        $('#SingOut').click(function() {
          $("#modalSingOut").modal('show');
        });
      </script>

      <div class="modal fade" id="modalSingOut" role="dialog" aria-labelledby = "myModalLabel">
        <div class="modal-dialog modalEnsanchado" role="document">
          <div class="modal-content">
            <div class="modal-header"><h4>Cerrar Sesión <i class="fa fa-lock"></i></h4></div>
            <div class="modal-body">
              <div class="form-group">
              <p><h4 class="displayoncenter aligntitle">¿Est&aacute; seguro que desea finalizar la sesión?</h4></p>
              </div>
            </div>
            <div class="modal-footer">
              <div class="col-xs-4">
                <a href="<?= base_url();?>index.php/home/logout" class="btn btn-primary btn-block btn-flat">Cerrar sesi&oacute;n</a>
              </div>
              <div class="col-xs-4">
              <button type="button" id="cerrarModal" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cancelar</button>
            </div>
            </div>
          </div>
        </div>
      </div>
      <aside class="main-sidebar">
        <section class="sidebar">
          <div class="user-panel">
            <div class="pull-left image">
              <?php if ($image) { ?>
                <img src="data:image/jpeg;base64,<?=$image?>" class="img-circle" alt="User Image">
              <?php } else { ?>
                <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
              <?php } ?>
            </div>
            <div class="pull-left info">
              <p><?= ' '.$fname.' '; ?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <ul class="sidebar-menu">
            <li class="header">Menú</li>
           <?= $this->dynamic_menu->build_menu($uniqueid, base_url()); ?>
 <div class="modal fade" id="versionModal" data-backdrop="static" data-keyboard="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="text-center"><img src="<?php echo base_url('/img/QS-LOGO.PNG');?>" width="80%"></h2>
            </div>
            <div class="modal-body form" style="text-align: center;">
              <strong>Versión 1</strong>
              <p></p>
              <strong>Quality Software Online-<p> No.042/2018 </p>
                <p>Internacional</strong> </p>
            </div>
            <div class="modal-footer bg-blue">
              <div class="col-lg-12 text-center"><small><b><a href="http://www.sternemedia.com/" target="_blank">www.sternemedia.com</a></small></b></small></div>
                <b></b>
                <div class="col-lg-12 text-center" style="font-size: 80%;"><small>Copyright &#169; SterneMedia.All rights reserved.</b></small>
              </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="perfil"   role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Profile</h4>
      </div>
      <div class="modal-body">
				<p><?= 'User:'.'  '.$fname.' '; ?></p>
				<p><?= 'Celular:'.'  '.$celular.' '; ?></p>
				<p><?= 'Email:'.'  '.$email.' '; ?></p>
				<div class="control-group">
					<label class="control-label">Change assword</label>
					<div class="controls">
					  <input type="password" class="form-control"  required="" name="password" placeholder="New password">
				  </div>
			  </div>
			</div>
      <div class="modal-footer">
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Change</button>
        </div>
         <div class="col-xs-4">
          <button type="submit" class="btn btn-default btn-block btn-flat"data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content-wrapper">
  <div class="row" class="content">
    <?php $alertasid = array('errorAlerta', 'successAlerta', 'warningAlerta'); include('alert_template.php'); ?>
      <section id="MyAlerts">
        <div class="col-md-12">
            <div data-alerts="alerts"  data-titles="{&quot;danger&quot;: &quot;&lt;em&gt;Advertencia!&lt;/em&gt;&quot;}" data-ids="error" data-fade="5000"></div>
            <div data-alerts="alerts"  data-titles="{&quot;success&quot;: &quot;&lt;em&gt;Correcto!&lt;/em&gt;&quot;}" data-ids="success" data-fade="5000"></div>
        </div>
      </section>
  </div>