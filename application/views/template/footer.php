     </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <strong>Copyright &copy;  <a href="http://www.sternemedia.com">SterneMedia, LLC</a>.</strong> All rights reserved.
      </footer>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> -->
    <!-- <script src="<?= base_url(); ?>plugins/jQuery/jQuery-2.1.4.min.js"></script> -->
    <script src="<?=base_url();?>plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url();?>plugins/datatables/media/js/dataTables.bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.11/features/scrollResize/dataTables.scrollResize.js"></script> -->
    <!-- <script type="text/javascript" src="<?= base_url();?>plugins/jquery-timepicker/jquery.timepicker.js"></script>
    <script type="text/javascript" src="<?= base_url();?>plugins/jquery-timepicker/lib/bootstrap-datepicker.js"></script> -->
    <script type="text/javascript" src="<?= base_url();?>dist/js/jquery.faloading.js"></script>
    <script src="<?=base_url();?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?=base_url();?>plugins/fastclick/fastclick.min.js"></script>
    <script src="<?=base_url();?>dist/js/app.min.js"></script>
    <script src="<?=base_url();?>dist/js/demo.js"></script>
    <script src="<?=base_url();?>plugins/moment/moment.min.js"></script>
    <script src="<?=base_url();?>plugins/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?=base_url();?>plugins/select2/select2.full.min.js"></script>
    <!-- <script src="<?=base_url();?>plugins/uploader/fileinput.js"></script> -->
    <script src="<?=base_url();?>plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?=base_url();?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="<?=base_url();?>plugins/input-mask/jquery.inputmask.regex.extensions.js"></script>
    <!--<script src="<?=base_url();?>sternemedia/SterneMedia.js"></script>-->
    <script src="<?=base_url();?>dist/js/jquery.bsAlerts.js"></script>
    <script src="<?=base_url();?>plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?=base_url();?>plugins/datepicker/locales/bootstrap-datepicker.es.min.js"></script>
    <!-- <script src="<?=base_url();?>dist/js/jquery-barcode.js"></script> -->
    <script src="<?=base_url();?>plugins/jscolor/jscolor.js"></script>
    <script src="<?=base_url();?>plugins/cropper/cropper.js"></script>
    <script src="<?=base_url();?>plugins/alertify/alertify.min.js"></script>
    <script src="<?=base_url();?>bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script> -->
    <script src="<?=base_url();?>sternemedia/Global.js"></script>
    <script>
      $(document).ready(function() {
        $('#editarTicketTabla').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": true,
          "language": languageConf,
          "responsive": true
        });
        $(".selectBuscar").select2();
        function ini_events(ele) {
          ele.each(function() {
            var eventObject = {
              title: $.trim($(this).text())
            };
            $(this).data('eventObject', eventObject);
            $(this).draggable({
              zIndex: 1070,
              revert: true,
              revertDuration: 0
            });
          });
        }
        ini_events($('#external-events div.external-event'));
      });
    </script>
    <script type="text/javascript" src="<?=base_url();?>sternemedia/<?=$jsfile?>"></script>
    <!--<script type="text/javascript" src="<?=base_url();?>sternemedia/<?=$js?>"></script>-->
  </body>
</html>
