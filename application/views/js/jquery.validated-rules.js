	$(document).ready(function() {
			
			/*	jQuery.validator.addMethod("lettersonly", function(value, element) {
					return this.optional(element) || /^[a-z]+$/i.test(value);
					}, "Please enter only letters without space."); */
					
			// validate contact form on keyup and submit
			$("#").validate({
			
			 errorElement: "span", 
			 
			 
			//set the rules for the fields
			rules: {
			
				NOMBRE2: {
					required: true,
					minlength: 2,
					maxlength:25
					//lettersonly: true
				},
				NOMBRE: {
					required: true,
					minlength: 2,
					maxlength:25
					//lettersonly: true,
				EMAIL2: {
					required: true,
					email: true
				},
				APELLIDO: {
					required: true,
					minlength: 5,
					maxlength:15
				},
				CELULAR: {
					required: true,
					minlength: 5,
					maxlength:15
				},
				ID : {
					required :true
				},
				
				PUESTO : {
					required :true
				},
				
				EMPRESA : {
					required :true
				},	
				
			},
			//set messages to appear inline
			messages: {
			
			
				
				password: {
				required: "Please provide a password.",
				minlength: "Your password must be at least 5 characters long",
				maxlength: "Password can not be more than 15 characters"
				},
				
				EMAIL2: "Email invalido requiredo.",
				
				terms: "You must agree to our terms.",
				gender: "Gender is required",
				state: "Select state"
				
				
				
			},
			
		errorPlacement: function(error, element) {               
					error.appendTo(element.parent());     
				}

		});
	});