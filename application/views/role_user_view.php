<section class="content-header">
	<h1>Usuarios: <small>Roles por Usuarios</small></h1>
</section>
<section class="content">
	<div class="box">
		
		<div class="box-body">
			<?php if($both) { ?>
				<h2 class="displayoncenter aligntitle"><?= (($roleUsuario == 1) ? "Orden por Roles": "Orden por Usuarios");?></h2>
				<h4 class = "displayoncenter"><?php $roleUsInfo = ($roleUsuario == 1) ? $result[0]['NAMEROLE'] : $result[0]['NAME']; ?><?=$roleUsInfo?></h4>
				<div class="filtroDePreguntas">
					<?php if($result) { ?>
						<span id='asociarRoleUser' class='btn btn-success'>
							<i class='icon-edit icon-white fa fa-plus'></i>
							Asignar
						</span>
					<?php } else { ?>
						<span id='asociarRoleUser' class='btn btn-success'>
							<?php $roleUsInfo = ($roleUsuario == 1) ? " Asignar Usuarios" : " Asignar Roles"; ?>
							<i class='icon-edit icon-white fa fa-plus'></i><?=$roleUsInfo?>
						</span>
					<?php } ?>
				</div>
			<?php } else { ?>
				<h2 class="displayoncenter aligntitle">Orden por Ambos</h2>
			<?php } ?>
			<div class="displayoncenter">
				<table class="table table-striped table-bordered estiloTabla datatable">
					<thead>
						<tr>
							<th class="oculto"></th>
							<?php if(($both == false)||($roleUsuario == 1)){ ?>
								<th class="oculto"></th>
								<th class="oculto">Roles</th>
								<th class="oculto"></th>
								<th>Usuario</th>
							<?php }else{ ?>
								<th class="oculto"></th>
								<th class="oculto">Usuario</th>
								<th class="oculto"></th>
								<th>Roles</th>
							<?php } ?>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody id="anclaTabla">
						<?php if($result) {
							foreach($result as $fila) { ?>
							<tr>
								<td class="oculto"><?=$fila['ROLEUSERID']?></td>
								<?php if(($both == false)||($roleUsuario == 1)){ ?>
									<td class="oculto"><?=$fila['ROLEID']?></td>
									<td class="oculto"><?=$fila['NAMEROLE']?></td>
									<td class="oculto"><?=$fila['USERID']?></td>
									<td><?=$fila['NAME']?></td>
								<?php } else { ?>
									<td class="oculto"><?=$fila['USERID']?></td>
									<td class="oculto"><?=$fila['NAME']?></td>
									<td class="oculto"><?=$fila['ROLEID']?></td>
									<td><?=$fila['NAMEROLE']?></td>
								<?php } ?>
								<td>
								<?php if($fila['ESTADOROLUSUARIO'] == 0){ ?>
									<span class='asociacion btn btn-success botonVED'>Habilitar</span>
								<?php } else { ?>
									<span class='desligar btn btn-danger botonVED'>Deshabilitar</span>
								<?php } ?>
								</td>
							</tr>
						<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<?php if ($both) { ?>
	<div class="modal fade" id="modalRolesUsusarios" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
				</div>
				<?php $alertasid = array('errorRolesUsuarios', 'successRolesUsuarios', 'warningRolesUsuarios'); include('template/alert_template.php'); ?>
				<div class="modal-body">
					<div class="box-body">
						<div class="form-group">
							<input type="hidden" class="form-control" id="id_rol_usuario">
							<p id="roleOrUser" hidden="true"><?= $idVal; ?></p>
						</div>
						<?php if($roleUsuario == 2) { ?>
						<div id="rol_container" class="form-group">
							<label>Rol:</label>
							<select class="form-control selectBuscar" id="rol_rolus">
								<option value="0">Seleccione un Rol</option>
								<?php foreach($role as $fila) { ?>
									<?= ($fila['HABILITADO'] == 1) ? '<option value="'.$fila['ROLEID'].'" disabled>'.$fila['NAMEROLE'].'</option>': '<option value="'.$fila['ROLEID'].'">'.$fila['NAMEROLE'].'</option>'; ?>
								<?php } ?>
							</select>
						</div>
						<?php } else { ?>
						<div id="usuario_container" class="form-group">
							<label>Usuario:</label>
							<select class="form-control selectBuscar" id="usuario_rolus">
								<option value="0">Seleccione un Usuario</option>
								<?php foreach($user as $fila) { ?>
									<?= ($fila['HABILITADO'] == 1) ? '<option value="'.$fila['U_ID'].'" disabled>'.$fila['NAME'].'</option>': '<option value="'.$fila['U_ID'].'">'.$fila['NAME'].'</option>'; ?>
								<?php } ?>
							</select>
						</div>
						<?php } ?>
						<div id="addContainer">
							<div id="addLeft" class="leftContainer"></div>
							<div id="addRight" class="rightContainer"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-xs-4">
						<button type="button" id="enviarRolus" class="btn btn-primary btn-block btn-flat">Guardar</button>
					</div>
					<div class="col-xs-4">
						<button type="button" id="cerrarRolus" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
