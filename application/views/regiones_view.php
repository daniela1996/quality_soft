		 <section class="content-header">
			<h1>Regiones: <small> Regiones</small></h1>
		</section>
		<section class="content">
			<div class="box">
				<div class="box-body">
					<h2 class="displayoncenter aligntitle">Regiones</h2>
					<div class="filtroDePreguntas">
						<span id="newRegion" class="btn btn-success"><i class='icon-edit icon-white fa fa-plus'></i> Agregar Región</span>
					</div>
					<div class="displayoncenter" id="displayTable">
						<table id = "tablaRegiones" class="table table-striped table-bordered estiloTabla datatable">
							<thead>
								<tr>
									<th class="oculto"></th>
									<th class="oculto"></th>
									<th class="oculto"></th>
									<th>País</th>
									<th>Departamento</th>
									<th>Municipio</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody class="tableViewer" id="anclaTabla">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
		<div class="modal fade" id="modalRegiones" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modalEnsanchado" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
					</div>
					<?php $alertasid = array('errorRegiones', 'successRegiones', 'warningRegiones'); include('template/alert_template.php'); ?>
				<ul class="nav nav-tabs ligeroNav">
		        	<li id="infoprincipal" class="active"><a data-toggle="tab" href="#infoprincipalMenu">País</a></li>
					<li id="departamentoinfo"><a data-toggle="tab" href="#infodepto">Departamento</a></li>
					<li id="municipioinfo"><a data-toggle="tab" href="#infomun">Municipio</a></li>

		     	 </ul>
					<div class="modal-body">
						<div class="box-body">
							<div class="tab-content">
								<div id="infoprincipalMenu" class="tab-pane fade in active modalTabMargin">
									<div class="greatInputContainer">
									<form id="regionesForm">
										<input type="hidden" class="form-control regionesinputs" id="codpais">
										
										<div class="form-group">
											<label>País:</label>
											<input type="text" class="form-control regionesinputs letras" id="nombrePais" name="nombrePais" maxlength="50">
										</div>
										
										<div class="row">
											<div class="col-xs-4">
													<button type="button" id="enviarRegion" class="btn btn-primary btn-block btn-flat">Guardar</button>
											</div>
											<div class="col-xs-4">
													<button type="button" id="cerrarRegion" class="btn btn-default btn-block btn-flat" data-dismiss="modal">Cerrar</button>
											</div>
										</div>
									</form>
									</div>
								</div>
								
								<div id="infodepto" class="tab-pane fade">
									<form id="deptoForm">
									<input type="hidden" class="form-control regionesinputs" id="coddepto">
									
									<div id="paisContainer" class="form-group">
										<label>País:</label>
										<div id="div_cod_pais">
											<select type="text" class="form-control regionesinputs" id="cod_pais" name="cod_pais">
											</select>
										</div>
									</div>
									<div class="form-group">
										<label>Departamento:</label>
										<input type="text" class="form-control regionesinputs letras" id="nombre_departamento" name="nombre_departamento" maxlength="50">
									</div>
									<div class="row">
										<div class="col-xs-4">
											<button type="button" id="enviardepto" class="btn btn-primary btn-block btn-flat">Guardar</button>
										</div>
										<div class="col-xs-4">
											<button type="button" id="cerrarRegion1" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
										</div>
									</div>
									</form>
								</div>
								<div id="infomun" class="tab-pane fade">
									<form id="munForm">
									<input type="hidden" class="form-control regionesinputs" id="codmun">

									<div class="form-group">
										<label>País:</label>
										<div id="div_cod">
											<select type="text" class="form-control regionesinputs" id="cod" name="cod">
											</select>
										</div>
									</div>

									<div class="form-group">
										<label>Departamento:</label>
										<div id="div_cod_depto">
											<select type="text" class="form-control regionesinputs" id="cod_depto" name="cod_depto">
												<option value="">Seleccionar...</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label>Municipio:</label>
										<input type="text" class="form-control regionesinputs letras" id="nombre_muni" name="nombre_muni" maxlength="60">
									</div>

									<div class="row">
										<div class="col-xs-4">
											<button type="button" id="enviarmun" class="btn btn-primary btn-block btn-flat">Guardar</button>
										</div>
										<div class="col-xs-4">
											<button type="button" id="cerrarRegion2" class="btn btn-default btn-block btn-flat" data-dismiss="modal" >Cerrar</button>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer"></div>
				</div>
			</div>
		</div>
