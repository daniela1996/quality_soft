<section class="content-header">
	<h1>GPS: <small> Información de GPS</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Información de GPS</h2>
			<div class="filtroDePreguntas">
				<span id="newGestiongps" class="btn btn-success"><i class='icon-edit icon-white fa fa-plus'></i> Asignar GPS</span>
			</div>
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable" id="TblGestionGps">
					<thead>
						<tr>
							<th class="oculto"></th>
							<th class="oculto">País</th>
							<th class="oculto">Departamento</th>
							<th class="oculto">Ciudad</th>
							<th>GPS</th>
							<th>Cod. Nodo</th>
							<th>Celular</th>
							<th>Cuenta</th>
							<th>Descripción</th>
							<th class="oculto">Galones</th>
							<th class="oculto">Voltaje</th>
							<th class="oculto">Voltaje Min</th>
							<th class="oculto"></th>
							<th class="oculto"></th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody class="tableViewer" id="anclaTabla">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

	