<section class="content-header">
	<h1>Usuarios:<small> Roles</small></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<h2 class="displayoncenter aligntitle">Roles</h2>
			<div class="filtroDePreguntas">
				<span id="newRol" class="btn btn-success"><i class='icon-edit icon-white fa fa-plus'></i> Agregar Rol</span>
			</div>
			<div class="displayoncenter" id="displayTable">
				<table class="table table-striped table-bordered estiloTabla datatable">
					<thead>
						<tr>
							<th class="oculto"></th>
							<th>Código</th>
							<th>Nombre</th>
							<th>Descripción</th>
							<th class="oculto"></th>
							<th>Módulo</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody id="anclaTabla">
						<?php if($result) {
							foreach($result as $fila) { ?>
							<tr>
								<td class="oculto"><?=$fila['ROLEID']?></td>
								<td><?=$fila['CODEROLE']?></td>
								<td><?=$fila['NAMEROLE']?></td>
								<td><?=$fila['DESCROLE']?></td>
								<td class="oculto"><?=$fila['IDMODULES']?></td>
								<td><?=$fila['NAMEMODULE']?></td>
								<td>
									<a href="rolesuser?ru=1&vl=<?=$fila['ROLEID'];?>">
										<span class='btn bg-orange botonVED' data-toggle="tooltip" data-placement="right" title="Asignar">
											<i class='icon-edit icon-white fa fa-arrow-circle-o-right'></i>
										</span>
									</a>
									<span class='editRol btn btn-success botonVED' data-toggle="tooltip" data-placement="right" title="Editar">
										<i class='icon-edit icon-white fa fa-pencil'></i>
									</span>
									<span class='deleteRol btn btn-danger botonVED' data-toggle="tooltip" data-placement="right" title="Eliminar">
										<i class='icon-edit icon-white fa fa-trash-o'></i>
									</span>
								</td>
							</tr>
							<?php } } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="modalRol" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><span id="theHeader"></span></h4>
			</div>
			<div class="modal-body">
				<?php $alertasid = array('errorRoles', 'successRoles', 'warningRoles'); include('template/alert_template.php'); ?>
				<div class="box-body">
					<input type="hidden" class="form-control" id="id_rol">
					<div id="sure" class="form-group">
						<p>¿Esta seguro que desea eliminar el Rol: <span id="nombre_span"></span>?</p>
					</div>
					<form id="rolesForm">
					<div class="greatInputContainer">
						<div id="codigo_container" class="form-group">
							<label>Código:</label>
								<input type="text" class="form-control rolsinputs letrassinespacios" id="codigo_rol" name="codigo_rol">
						</div>
						<div id="nombre_container" class="form-group">
							<label>Nombre:</label>
								<input type="text" class="form-control rolsinputs letras" id="nombre_rol" name="nombre_rol">
						</div>
						<div id="modulo_container" class="form-group">
							<label>Módulo:</label>
							<select class="form-control selectBuscar rolsinputs" id="modulo_rol" name="modulo_rol">
								<option value="">Seleccione...</option>
								<?php foreach($modulo as $fila) { ?>
									<option value="<?=$fila['MODULEID']?>"><?=$fila['NAMEMODULE']?></option>
								<?php } ?>
							</select>
						</div>
						<div id="descripcion_container" class="form-group">
							<label>Descripcion:</label>
								<input type="text" class="form-control rolsinputs letras" id="descripcion_rol" name="descripcion_rol">
						</div>
					</div>
				</form>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-xs-4">
					<button type="button" id="enviarRol" class="btn btn-primary btn-block btn-flat">Guardar</button>
				</div>
				<div class="col-xs-4">
					<button type="button" id="cerrarRol" class="btn btn-default btn-block btn-flat" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
