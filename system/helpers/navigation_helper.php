<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * menu_ul()
 * Genera una lista desordenada con la clase current en el elemento seleccionado.
 */
if ( ! function_exists('menu_ul'))
{
    function menu_ul($sel = 'home')
   {
        $CI =& get_instance();
        $items = $CI->config->item('navigation');
        
     
     /*
     
     <li class="treeview">
           
           
           
              <a href="#">
                <i class="fa fa-user"></i> <span></span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<? echo base_url();?>"><i class="fa fa-circle-o"></i>Add User</a></li>
                <li><a href="../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
              </ul>
            </li>
              
     */
         // $menu= '<li class="header">MAIN NAVIGATION</li>'."\n";
             $menu = '<ul class="sidebar-menu">'."\n";
        foreach($items as $item)
        {
        	
           
           
           
              
              
        	
            $current = (in_array($sel, $item)) ? ' class="current"' : '';
            $id = (!empty($item['id'])) ? ' id="'.$item['id'].'"' : '';
            $menu .= 
            '<li class="treeview">
            <a href="'.$item['link'].'">
                <i class="fa fa-user"></i> <span>'.$item['title'].'</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<? echo base_url();?>"><i class="fa fa-circle-o"></i></a></li>
              </ul>
            </li>'."\n";//<li'.$current.'><a href="'.$item['link'].'"'.$id.'>'.$item['title'].'</a></li>'."\n";
        }
        $menu .= '</ul>'."\n";
        return $menu;
    }
}

// ------------------------------------------------------------------------

/**
 * menu_p
 * Genera un parrafo con la clase current en el elemento seleccionado
 */
if ( ! function_exists('menu_p'))
{
    function menu_p($sel = 'home', $separator = '')
   {
        $CI =& get_instance();
        $items = $CI->config->item('navigation');

        $count = count($items);
        $i = 0;
        $menu = "\n".'<p class="bottom_nav">';
        foreach($items as $item)
        {
            $current = (in_array($sel, $item)) ? ' class="current"' : '';
            $id = (!empty($item['id'])) ? ' id="'.$item['id'].'"' : '';
            $menu .= '<a'.$current.' href="'.$item['link'].'"'.$id.'>'.$item['title'].'</a>';
            $i++;
            if($count != $i)
            {
                $menu .= ' '.$separator.' ';
            }
        }
        $menu .= '</p>'."\n";
        return $menu;
    }
}


/* End of file navigation_helper.php */
/* Location: ./system/helpers/navigation_helper.php */